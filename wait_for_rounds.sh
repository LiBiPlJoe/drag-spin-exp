#!/bin/bash
while ! docker logs drag-spin-exp-gameserver-1 | grep "Rnd: ";
do
    docker logs drag-spin-exp-gameserver-1 --tail=5
    sleep 5
    echo "Waiting for rounds to show up in logs..."
done
