namespace DragonsSpine
{
    using System;
    using System.Collections.Generic;
	using System.IO;
    using System.Threading;

    public class PC : Character
    {
        public PC(): base()
        {
			this.IsPC = true;
        }

        public PC(System.Data.DataRow dr) : base()
        {
            this.IsPC = true;
            this.PlayerID = Convert.ToInt32(dr["playerID"]);
            this.Notes = dr["notes"].ToString();
            this.accountID = Convert.ToInt32(dr["accountID"]);
            this.account = dr["account"].ToString();
            this.Name = dr["name"].ToString();
            this.gender = (Globals.eGender)Convert.ToInt16(dr["gender"]);
            this.race = dr["race"].ToString();
            this.classFullName = dr["classFullName"].ToString();
            this.BaseProfession = (ClassType)Enum.Parse(typeof(ClassType), dr["classType"].ToString());
            this.visualKey = dr["visualKey"].ToString();
            // temporary
            if (this.visualKey == null || this.visualKey == "")
                Character.SetCharacterVisualKey(this);
            this.Alignment = (Globals.eAlignment)Convert.ToInt32(dr["alignment"]);
            this.confRoom = Convert.ToInt32(dr["confRoom"]);
            this.ImpLevel = (Globals.eImpLevel)Convert.ToInt32(dr["impLevel"]);
            this.IsAncestor = Convert.ToBoolean(dr["ancestor"]);
            this.AncestorID = Convert.ToInt32(dr["ancestorID"]);
            this.FacetID = Convert.ToInt16(dr["facet"]);
            this.LandID = Convert.ToInt16(dr["land"]);
            this.MapID = Convert.ToInt16(dr["map"]);
            this.X = Convert.ToInt16(dr["xCord"]);
            this.Y = Convert.ToInt16(dr["yCord"]);
            this.Z = Convert.ToInt32(dr["zCord"]);
            this.CurrentCell = Cell.GetCell(this.FacetID, this.LandID, this.MapID, this.X, this.Y, this.Z);
            this.dirPointer = dr["dirPointer"].ToString();
            this.Stunned = Convert.ToInt16(dr["stunned"]);
            this.floating = Convert.ToInt16(dr["floating"]);
            this.IsDead = Convert.ToBoolean(dr["dead"]);
            this.fighterSpecialization = (Globals.eSkillType)Enum.Parse(typeof(Globals.eSkillType), dr["fighterSpecialization"].ToString());
            this.Level = Convert.ToInt16(dr["level"]);
            this.Experience = Convert.ToInt64(dr["exp"]);
            this.Hits = Convert.ToInt32(dr["hits"]);
            this.HitsAdjustment = Convert.ToInt32(dr["hitsAdjustment"]);
            this.HitsMax = Convert.ToInt32(dr["hitsMax"]);
            this.HitsDoctored = Convert.ToInt32(dr["hitsDoctored"]);
            this.StaminaMax = Convert.ToInt32(dr["stamina"]);
            this.StaminaAdjustment = Convert.ToInt32(dr["staminaAdjustment"]);
            this.Stamina = Convert.ToInt32(dr["stamLeft"]);
            this.Mana = Convert.ToInt32(dr["mana"]);
            this.ManaMax = Convert.ToInt32(dr["manaMax"]);
            this.ManaAdjustment = Convert.ToInt32(dr["manaAdjustment"]);
            this.Age = Convert.ToInt32(dr["age"]);
            this.RoundsPlayed = Convert.ToInt64(dr["roundsPlayed"]);
            this.Deaths = Convert.ToInt32(dr["numDeaths"]);
            this.Kills = Convert.ToInt32(dr["numKills"]);
            this.bankGold = Convert.ToInt64(dr["bankGold"]);
            this.Strength = Convert.ToInt32(dr["strength"]);
            this.Dexterity = Convert.ToInt32(dr["dexterity"]);
            this.Intelligence = Convert.ToInt32(dr["intelligence"]);
            this.Wisdom = Convert.ToInt32(dr["wisdom"]);
            this.Constitution = Convert.ToInt32(dr["constitution"]);
            this.Charisma = Convert.ToInt32(dr["charisma"]);
            this.strengthAdd = Convert.ToInt32(dr["strengthAdd"]);
            this.dexterityAdd = Convert.ToInt32(dr["dexterityAdd"]);
            //
            this.birthday = Convert.ToDateTime(dr["birthday"]);
            this.lastOnline = Convert.ToDateTime(dr["lastOnline"]);
            //this.encumbrance = Convert.ToInt32(dr["encumbrance"]);
            // Underworld specific
            this.UW_hitsMax = Convert.ToInt32(dr["UW_hitsMax"]);
            this.UW_hitsAdjustment = Convert.ToInt32(dr["UW_hitsAdjustment"]);
            this.UW_staminaMax = Convert.ToInt32(dr["UW_staminaMax"]);
            this.UW_staminaAdjustment = Convert.ToInt32(dr["UW_staminaAdjustment"]);
            this.UW_manaMax = Convert.ToInt32(dr["UW_manaMax"]);
            this.UW_manaAdjustment = Convert.ToInt32(dr["UW_manaAdjustment"]);
            this.UW_hasIntestines = Convert.ToBoolean(dr["UW_intestines"]);
            this.UW_hasLiver = Convert.ToBoolean(dr["UW_liver"]);
            this.UW_hasLungs = Convert.ToBoolean(dr["UW_lungs"]);
            this.UW_hasStomach = Convert.ToBoolean(dr["UW_stomach"]);
            //Player vs. Player
            this.currentKarma = Convert.ToInt32(dr["currentKarma"]);
            this.lifetimeKarma = Convert.ToInt64(dr["lifetimeKarma"]);
            this.pvpNumDeaths = Convert.ToInt64(dr["pvpDeaths"]);
            this.pvpNumKills = Convert.ToInt64(dr["pvpKills"]);
            this.currentMarks = Account.GetCurrentMarks(this.accountID);
            this.lifetimeMarks = Convert.ToInt32(dr["lifetimeMarks"]);
            int a = 0;
            string[] pFlagged = dr["playersFlagged"].ToString().Split(Protocol.ASPLIT.ToCharArray());
            if (dr["playersFlagged"].ToString() != "")
            {
                for (a = 0; a < pFlagged.Length; a++)
                {
                    this.PlayersFlagged.Add(Convert.ToInt32(pFlagged[a]));
                }
            }
            if (dr["playersKilled"].ToString() != "")
            {
                pFlagged = dr["playersKilled"].ToString().Split(Protocol.ASPLIT.ToCharArray());
                for (a = 0; a < pFlagged.Length; a++)
                {
                    this.PlayersKilled.Add(Convert.ToInt32(pFlagged[a]));
                }
            }
            DAL.DBPlayer.loadPlayerSettings(this); // add the player's settings
            DAL.DBPlayer.loadPlayerSkills(this); // add skills to the player
            this.LeftHand = DAL.DBPlayer.loadPlayerHeld(this.PlayerID, false); // load left hand item
            this.RightHand = DAL.DBPlayer.loadPlayerHeld(this.PlayerID, true); // load right hand item
            if (this.RightHand != null) {
                Utils.Log("Character " + this.PlayerID + " has " + this.RightHand.name + " in their right hand (the secret ingredient is " + this.RightHand.special + ").", Utils.LogType.Unknown);
            }

            this.sackList = DAL.DBPlayer.loadPlayerSack(this.PlayerID);
            this.wearing = DAL.DBPlayer.loadPlayerWearing(this.PlayerID);
            this.lockerList = DAL.DBPlayer.loadPlayerLocker(this.PlayerID);
            this.beltList = DAL.DBPlayer.loadPlayerBelt(this.PlayerID);
            this.RightRing1 = DAL.DBPlayer.loadRings(this.PlayerID, 1);
            this.RightRing2 = DAL.DBPlayer.loadRings(this.PlayerID, 2);
            this.RightRing3 = DAL.DBPlayer.loadRings(this.PlayerID, 3);
            this.RightRing4 = DAL.DBPlayer.loadRings(this.PlayerID, 4);
            this.LeftRing1 = DAL.DBPlayer.loadRings(this.PlayerID, 5);
            this.LeftRing2 = DAL.DBPlayer.loadRings(this.PlayerID, 6);
            this.LeftRing3 = DAL.DBPlayer.loadRings(this.PlayerID, 7);
            this.LeftRing4 = DAL.DBPlayer.loadRings(this.PlayerID, 8);
            this.spellList = (IntStringMap)DAL.DBPlayer.loadPlayerSpells(this.PlayerID);
            DAL.DBPlayer.loadPlayerEffects(this);
            this.questList = DAL.DBPlayer.loadPlayerQuests(this.PlayerID);
            DAL.DBPlayer.loadPlayerFlags(this, true);
            // TODO: load player quests
        }

        public static Object GetField(int playerID, string field, object var, string comments)
        {
            if (comments != null)
            {
                Utils.Log(comments, Utils.LogType.Unknown);
            }

            Object obj = DAL.DBPlayer.getPlayerField(playerID, field, var.GetType());

            if (obj == null)
            {
                Utils.Log("FAILURE: PC.getField(" + playerID + ", " + field + ", " + var.ToString() + ", Comments: " + comments + ")", Utils.LogType.SystemFailure);
                return null;
            }
            return obj;
        }

        public static void saveField(int playerID, string field, object var, string comments) // save a single PC field
        {
            if (comments != null)
            {
                Utils.Log(comments, Utils.LogType.Unknown);
            }

            int result = DAL.DBPlayer.savePlayerField(playerID, field, var);

            if (result != 1)
            {
                Utils.Log("FAILURE: PC.saveField(" + playerID + ", " + field + ", " + var.ToString() + ", Comments: " + comments + ")", Utils.LogType.SystemFailure);
            }
        }

        public void ThreadSave() // launch a new thread of save() that saves the npc
        {
            Thread saveThread = new Thread(Save);
            saveThread.Start();
        }

        public void Save() // save entire PC
        {
            try
            {
                //if(this.PlayerID < 0)
                //{
                //    return;
                //}

                PC pc = (PC)this.MemberwiseClone();

                // store a knight's relevant stats before adding worn effects (knight ring)
                int saveKnightMana = -1;
                int saveKnightManaMax = -1;
                if (pc.knightRing)
                {
                    saveKnightMana = pc.Mana;
                    saveKnightManaMax = pc.ManaMax;
                }
                /*
                // remove held item effects
                if (pc.RightHand != null && pc.RightHand.effectType.Length > 0 && pc.RightHand.wearLocation == Globals.eWearLocation.None)
                {
                    Effect.RemoveWornEffectFromCharacter(pc, pc.RightHand);
                }
                if (pc.LeftHand != null && pc.LeftHand.effectType.Length > 0 && pc.LeftHand.wearLocation == Globals.eWearLocation.None)
                {
                    Effect.RemoveWornEffectFromCharacter(pc, pc.LeftHand);
                }
                //remove all ring effects back on the player
                if (pc.RightRing1 != null && pc.RightRing1.effectType.Length > 0)
                {
                    Effect.RemoveWornEffectFromCharacter(pc, pc.RightRing1);
                }
                if (pc.RightRing2 != null && pc.RightRing2.effectType.Length > 0)
                {
                    Effect.RemoveWornEffectFromCharacter(pc, pc.RightRing2);
                }
                if (pc.RightRing3 != null && pc.RightRing3.effectType.Length > 0)
                {
                    Effect.RemoveWornEffectFromCharacter(pc, pc.RightRing3);
                }
                if (pc.RightRing4 != null && pc.RightRing4.effectType.Length > 0)
                {
                    Effect.RemoveWornEffectFromCharacter(pc, pc.RightRing4);
                }
                if (pc.LeftRing1 != null && pc.LeftRing1.effectType.Length > 0)
                {
                    Effect.RemoveWornEffectFromCharacter(pc, pc.LeftRing1);
                }
                if (pc.LeftRing2 != null && pc.LeftRing2.effectType.Length > 0)
                {
                    Effect.RemoveWornEffectFromCharacter(pc, pc.LeftRing2);
                }
                if (pc.LeftRing3 != null && pc.LeftRing3.effectType.Length > 0)
                {
                    Effect.RemoveWornEffectFromCharacter(pc, pc.LeftRing3);
                }
                if (pc.LeftRing4 != null && pc.LeftRing4.effectType.Length > 0)
                {
                    Effect.RemoveWornEffectFromCharacter(pc, pc.LeftRing4);
                }
                */
                //revert a knight's related stats back for player save
                if (saveKnightMana != -1 && saveKnightManaMax != -1)
                {
                    pc.Mana = saveKnightMana;
                    pc.ManaMax = saveKnightManaMax;
                }
                
                // Now save all the stats and arraylists
                DAL.DBPlayer.SavePlayerStats(pc);
                if (pc.IsNewPC)
                {
                    pc.PlayerID = DAL.DBPlayer.getPlayerID(pc.Name);
                    for (int a = 0; a < pc.sackList.Count; a++)
                    {
                        Item item = (Item)pc.sackList[a];
                        if (item.attuneType > Globals.eAttuneType.None)
                            item.AttuneItem(pc.PlayerID, "New character spellbook.");
                    }
                }
                DAL.DBPlayer.SavePlayerSettings(pc); // save settings
                DAL.DBPlayer.SavePlayerSkills(pc); // save skills
                DAL.DBPlayer.savePlayerHeld(pc); // save held items
                DAL.DBPlayer.savePlayerSack(pc); // save sack items
                DAL.DBPlayer.savePlayerWearing(pc); // save worn items
                DAL.DBPlayer.savePlayerLocker(pc); // save locker items
                DAL.DBPlayer.savePlayerBelt(pc); // save belt items
                DAL.DBPlayer.savePlayerRings(pc); // save rings worn
                DAL.DBPlayer.savePlayerSpells(pc); // save spells known
                DAL.DBPlayer.savePlayerEffects(pc); // save non item effects
                DAL.DBPlayer.SavePlayerQuests(pc); // save player quests
                DAL.DBPlayer.savePlayerFlags(pc); // save player flags (quests flags, content flags)

            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public static void LoadCharacter(Character ch, PC pc1) // load character
        {
            ch.PlayerID = pc1.PlayerID;
            ch.accountID = pc1.accountID;
            ch.account = pc1.account;
            ch.Name = pc1.Name;
            ch.gender = pc1.gender;
            ch.race = pc1.race;
            ch.BaseProfession = pc1.BaseProfession;
            ch.classFullName = pc1.classFullName;
            ch.BaseProfession = pc1.BaseProfession;
            ch.visualKey = pc1.visualKey;
            ch.Alignment = pc1.Alignment;
            if (ch.PCState != Globals.ePlayerState.CONFERENCE)
                ch.confRoom = pc1.confRoom;
            ch.ImpLevel = pc1.ImpLevel;
            ch.IsImmortal = pc1.IsImmortal;
            ch.friendsList = pc1.friendsList;
            ch.friendNotify = pc1.friendNotify;
            ch.ignoreList = pc1.ignoreList;
            ch.receivePages = pc1.receivePages;
            ch.receiveTells = pc1.receiveTells;
            ch.filterProfanity = pc1.filterProfanity;
            ch.showStaffTitle = pc1.showStaffTitle;
            ch.macros = pc1.macros;
            ch.displayCombatDamage = pc1.displayCombatDamage;
            ch.IsAncestor = pc1.IsAncestor;
            ch.AncestorID = pc1.AncestorID;
            ch.echo = pc1.echo;
            ch.IsAnonymous = pc1.IsAnonymous;
            ch.LandID = pc1.LandID;
            ch.MapID = pc1.MapID;
            ch.X = pc1.X;
            ch.Y = pc1.Y;
            ch.CurrentCell = Cell.GetCell(pc1.FacetID, pc1.LandID, pc1.MapID, pc1.X, pc1.Y, pc1.Z);
            ch.dirPointer = pc1.dirPointer;
            ch.Stunned = pc1.Stunned;
            ch.floating = pc1.floating;
            ch.IsDead = pc1.IsDead;
            ch.IsHidden = pc1.IsHidden;
            ch.IsInvisible = pc1.IsInvisible;
            ch.Level = pc1.Level;
            ch.Experience = pc1.Experience;
            ch.Hits = pc1.Hits;
            ch.HitsAdjustment = pc1.HitsAdjustment;
            ch.HitsMax = pc1.HitsMax;
            ch.HitsDoctored = pc1.HitsDoctored;
            ch.StaminaAdjustment = pc1.StaminaAdjustment;
            ch.StaminaMax = pc1.StaminaMax;
            ch.Stamina = pc1.Stamina;
            ch.Mana = pc1.Mana;
            ch.ManaAdjustment = pc1.ManaAdjustment;
            ch.ManaMax = pc1.ManaMax;
            ch.hitsRegen = pc1.hitsRegen;
            ch.manaRegen = pc1.manaRegen;
            ch.staminaRegen = pc1.staminaRegen;
            ch.Age = pc1.Age;
            ch.RoundsPlayed = pc1.RoundsPlayed;
            ch.Deaths = pc1.Deaths;
            ch.Kills = pc1.Kills;
            ch.bankGold = pc1.bankGold;
            ch.Strength = pc1.Strength;
            ch.Dexterity = pc1.Dexterity;
            ch.Intelligence = pc1.Intelligence;
            ch.Wisdom = pc1.Wisdom;
            ch.Constitution = pc1.Constitution;
            ch.Charisma = pc1.Charisma;
            ch.strengthAdd = pc1.strengthAdd;
            ch.dexterityAdd = pc1.dexterityAdd;

            ch.mace = pc1.mace;
            ch.bow = pc1.bow;
            ch.flail = pc1.flail;
            ch.dagger = pc1.dagger;
            ch.rapier = pc1.rapier;
            ch.twoHanded = pc1.twoHanded;
            ch.staff = pc1.staff;
            ch.shuriken = pc1.shuriken;
            ch.sword = pc1.sword;
            ch.threestaff = pc1.threestaff;
            ch.halberd = pc1.halberd;
            ch.unarmed = pc1.unarmed;
            ch.thievery = pc1.thievery;
            ch.magic = pc1.magic;
            ch.bash = pc1.bash;

            ch.highMace = pc1.highMace;
            ch.highBow = pc1.highBow;
            ch.highFlail = pc1.highFlail;
            ch.highDagger = pc1.highDagger;
            ch.highRapier = pc1.highRapier;
            ch.highTwoHanded = pc1.highTwoHanded;
            ch.highStaff = pc1.highStaff;
            ch.highShuriken = pc1.highShuriken;
            ch.highSword = pc1.highSword;
            ch.highThreestaff = pc1.highThreestaff;
            ch.highHalberd = pc1.highHalberd;
            ch.highUnarmed = pc1.highUnarmed;
            ch.highThievery = pc1.highThievery;
            ch.highMagic = pc1.highMagic;
            ch.highBash = pc1.highBash;

            ch.trainedMace = pc1.trainedMace;
            ch.trainedBow = pc1.trainedBow;
            ch.trainedFlail = pc1.trainedFlail;
            ch.trainedDagger = pc1.trainedDagger;
            ch.trainedRapier = pc1.trainedRapier;
            ch.trainedTwoHanded = pc1.trainedTwoHanded;
            ch.trainedStaff = pc1.trainedStaff;
            ch.trainedShuriken = pc1.trainedShuriken;
            ch.trainedSword = pc1.trainedSword;
            ch.trainedThreestaff = pc1.trainedThreestaff;
            ch.trainedHalberd = pc1.trainedHalberd;
            ch.trainedUnarmed = pc1.trainedUnarmed;
            ch.trainedThievery = pc1.trainedThievery;
            ch.trainedMagic = pc1.trainedMagic;
            ch.trainedBash = pc1.trainedBash;

            ch.birthday = pc1.birthday;
            ch.lastOnline = pc1.lastOnline;

            ch.Shielding = pc1.Shielding;

            #region Resistances
            ch.FireResistance = pc1.FireResistance;
            ch.ColdResistance = pc1.ColdResistance;
            ch.LightningResistance = pc1.LightningResistance;
            ch.DeathResistance = pc1.DeathResistance;
            ch.BlindResistance = pc1.BlindResistance;
            ch.FearResistance = pc1.FearResistance;
            ch.StunResistance = pc1.StunResistance;
            ch.PoisonResistance = pc1.PoisonResistance;
            ch.ZonkResistance = pc1.ZonkResistance; 
            #endregion

            #region Protections
            ch.FireProtection = pc1.FireProtection;
            ch.ColdProtection = pc1.ColdProtection;
            ch.LightningProtection = pc1.LightningProtection;
            ch.DeathProtection = pc1.DeathProtection;
            ch.PoisonProtection = pc1.PoisonProtection; 
            #endregion

            ch.RightHand = pc1.RightHand;
            
            ch.LeftHand = pc1.LeftHand;

            //Underworld specific
            ch.UW_hitsMax = pc1.UW_hitsMax;
            ch.UW_staminaMax = pc1.UW_staminaMax;
            ch.UW_manaMax = pc1.UW_manaMax;
            ch.UW_hasIntestines = pc1.UW_hasIntestines;
            ch.UW_hasLiver = pc1.UW_hasLiver;
            ch.UW_hasLungs = pc1.UW_hasLungs;
            ch.UW_hasStomach = pc1.UW_hasStomach;

            //Player vs. Player
            ch.currentKarma = pc1.currentKarma;
            ch.lifetimeKarma = pc1.lifetimeKarma;
            ch.currentMarks = Account.GetCurrentMarks(ch.accountID);
            ch.lifetimeMarks = pc1.lifetimeMarks;
            ch.pvpNumDeaths = pc1.pvpNumDeaths;
            ch.pvpNumKills = pc1.pvpNumKills;

            ch.Timeout = Character.INACTIVITY_TIMEOUT;
            ch.spellList = pc1.spellList;
            ch.wearing = pc1.wearing;
            ch.sackList = pc1.sackList;
            ch.lockerList = pc1.lockerList;
            ch.beltList = pc1.beltList;
            ch.RightRing1 = pc1.RightRing1;
            ch.RightRing2 = pc1.RightRing2;
            ch.RightRing3 = pc1.RightRing3;
            ch.RightRing4 = pc1.RightRing4;
            ch.LeftRing1 = pc1.LeftRing1;
            ch.LeftRing2 = pc1.LeftRing2;
            ch.LeftRing3 = pc1.LeftRing3;
            ch.LeftRing4 = pc1.LeftRing4;

            //reset temp stats
            ch.TempStrengthRaw = 0;
            ch.TempDexterityRaw = 0;
            ch.TempIntelligenceRaw = 0;
            ch.TempWisdomRaw = 0;
            ch.TempConstitutionRaw = 0;
            ch.TempCharismaRaw = 0;

            ch.effectList = new Dictionary<Effect.EffectType, Effect>();
            ch.wornEffectList = new List<Effect>();

            foreach (Effect effect in pc1.effectList.Values)
                Effect.CreateCharacterEffect(effect.effectType, effect.effectAmount, ch, effect.duration, null);


            // store a knight's relevant stats before adding ring effects (knight ring)
            int savedKnightMana = 0;
            int savedKnightManaMax = 0;
            if (ch.BaseProfession == ClassType.Knight)
            {
                savedKnightMana = ch.Mana;
                savedKnightManaMax = ch.ManaMax;
            }

            #region Add Held Item Effects
            if (ch.RightHand != null && ch.RightHand.effectType.Length > 0 && ch.RightHand.wearLocation == Globals.eWearLocation.None)
            {
                Effect.AddWornEffectToCharacter(ch, ch.RightHand);
            }
            if (ch.LeftHand != null && ch.LeftHand.effectType.Length > 0 && ch.LeftHand.wearLocation == Globals.eWearLocation.None)
            {
                Effect.AddWornEffectToCharacter(ch, ch.LeftHand);
            } 
            #endregion

            #region Add Ring Effects
            if (ch.RightRing1 != null && ch.RightRing1.effectType.Length > 0)
            {
                Effect.AddWornEffectToCharacter(ch, ch.RightRing1);
            }
            if (ch.RightRing2 != null && ch.RightRing2.effectType.Length > 0)
            {
                Effect.AddWornEffectToCharacter(ch, ch.RightRing2);
            }
            if (ch.RightRing3 != null && ch.RightRing3.effectType.Length > 0)
            {
                Effect.AddWornEffectToCharacter(ch, ch.RightRing3);
            }
            if (ch.RightRing4 != null && ch.RightRing4.effectType.Length > 0)
            {
                Effect.AddWornEffectToCharacter(ch, ch.RightRing4);
            }

            if (ch.LeftRing1 != null && ch.LeftRing1.effectType.Length > 0)
            {
                Effect.AddWornEffectToCharacter(ch, ch.LeftRing1);
            }
            if (ch.LeftRing2 != null && ch.LeftRing2.effectType.Length > 0)
            {
                Effect.AddWornEffectToCharacter(ch, ch.LeftRing2);
            }
            if (ch.LeftRing3 != null && ch.LeftRing3.effectType.Length > 0)
            {
                Effect.AddWornEffectToCharacter(ch, ch.LeftRing3);
            }
            if (ch.LeftRing4 != null && ch.LeftRing4.effectType.Length > 0)
            {
                Effect.AddWornEffectToCharacter(ch, ch.LeftRing4);
            } 
            #endregion

            // revert a knight's related stats after adding ring effects
            if (ch.BaseProfession == ClassType.Knight)
            {
                if (ch.knightRing)
                {
                    ch.Mana = savedKnightMana;
                    ch.ManaMax = savedKnightManaMax;
                }
                else
                {
                    ch.Mana = 0;
                    ch.ManaMax = 0;
                }
            }

            #region Add Inventory Effects
            foreach (Item wItem in ch.wearing)
            {
                try
                {
                    if (wItem.effectType.Length > 0)
                    {
                        Effect.AddWornEffectToCharacter(ch, wItem);
                    }
                }
                catch (Exception e)
                {
                    Utils.LogException(e);
                }
            } 
            #endregion

            ch.questList = pc1.questList;
            ch.questFlags = pc1.questFlags;
            ch.contentFlags = pc1.contentFlags;

            ch.encumbrance = ch.GetEncumbrance();
            return;
        }

        public static bool SwitchCharacter(Character ch, int id)
        {
            PC pc = PC.GetPC(id);

            if (pc != null && pc.accountID == ch.accountID)
            {
                PC.LoadCharacter(ch, pc);
                Protocol.UpdateUserLists();
                return true;
            }

            return false;
        }

		public static bool SelectNewCharacter(Character ch, int charnum) // select a new character
		{
			string[] playerlist = DAL.DBPlayer.GetCharacterList("playerID", ch.accountID);

			int id = Convert.ToInt32(playerlist[charnum - 1]);

			PC pc = PC.GetPC(id); // get new character from the database

			if (pc != null)
			{
				PC.LoadCharacter(ch, pc);
                Protocol.UpdateUserLists();
				return true;
			}
			else return false;
		}

        public static bool PlayerExists(string playerName) // return if player name exists in the Player table
        {
            return DAL.DBPlayer.playerExists(playerName);
        }

		public static string GetCharacterList(int accountID, bool protocol, Character currentCharacter)
		{
		string list = "";
        int i;
            string[] characterList = DAL.DBPlayer.GetCharacterList("name", accountID);
			i = 1;
			list = "Character Listing\n\r";
			list +="-----------------\n\r";
			
			foreach(String name in characterList)
			{
				if (name == null) break;
				list +="\n\r"+i+". "+name;
				i++;
			}
			return list;
		}

        public static PC GetPC(int playerID) // return a PC
        {
            return DAL.DBPlayer.GetPCByID(playerID);
        }

        public static PC getOnline(int playerID) // return a PC that is online
        {
            foreach (Character chr in Character.menuList)
            {
                if (chr.PlayerID == playerID) { return (PC)chr; }
            }
            foreach (Character chr in Character.confList)
            {
                if (chr.PlayerID == playerID) { return (PC)chr; }
            }
            foreach (Character chr in Character.pcList)
            {
                if (chr.PlayerID == playerID) { return (PC)chr; }
            }
            return null;
        }

        public static PC GetOnline(string playerName) // return a PC that is online
        {
            foreach (Character chr in Character.menuList)
            {
                if (chr.Name.ToLower() == playerName.ToLower()) { return (PC)chr; }
            }
            foreach (Character chr in Character.confList)
            {
                if (chr.Name.ToLower() == playerName.ToLower()) { return (PC)chr; }
            }
            foreach (Character chr in Character.pcList)
            {
                if (chr.Name.ToLower() == playerName.ToLower()) { return (PC)chr; }
            }
            return null;
        }

        public static string GetName(int playerID) // return player name
        {
            return (string)DAL.DBPlayer.getPlayerField(playerID, "name", Type.GetType("System.String"));
        }

        public static int GetPlayerID(string playerName)
        {
            return DAL.DBPlayer.getPlayerID(playerName);
        }

		override public void AddToWorld() {
			base.AddToWorld();
		}
		override public void RemoveFromWorld() {
			base.RemoveFromWorld();
		}

        public static void ClearLivePcData() {
            DAL.DBPlayer.ClearLivePcData();
        }
    }
}
