using System;
using System.Collections;
using System.Collections.Generic;

namespace DragonsSpine
{
    /// <summary>
    /// 
    /// </summary>
	public class ArtificialIntel : Creature
	{
        public enum ActionType { None, Take, Use, Cast, Special, Move, Combat, Follow }
        public enum CommandControl { None, Follow_Only, Movement_Only, Follow_And_Movement, Full }

        public enum Priority
        {
            None,
            Wander, // wander around
            Socialize, // get to a friend
            Interact, // get to a friend
            SearchCorpse, // search through a corpse
            GetObject, // pick up a nearby valuable object
            Advance, // move closer to an enemy
            Attack, // attack an enemy
            Investigate, // mostHated is null, but wasn't a round ago so investigate where mostHated was
            InvestigateMagic, // investigate where magic was warmed
            PrepareSpell, // prepare a spell
            GetWeapon, // get a weapon, either here or somewhere in sight
            RangeMove, // move away for a ranged attack
            GoHome, // lair critters return home, quest npcs return to spawn coords
            RaiseDead, // priests raise the dead
            Buff,
            Rest, // rest (eg: cancel prepared spell)
            SpellSling, // cast a prepared spell
            Evaluate, // creature is acquiring a target
            FleeEffect, // flee from an effect in the npc's cell
            Flee, // flee
            Enforce, // enforce the local law
            LairDefense, // defend our lair
            Heal // heal myself or ally
        }

		public static void CreateContactList(NPC npc)
		{
            try
            {
                if (npc.IsDead) { return; } // return if the creature is dead
                if (npc.CurrentCell == null) { return; } // return if the npc does not have a cell.
                npc.totalFL = 0;
                npc.totalHate = 0;
                npc.enemyList.Clear();
                npc.friendList.Clear();
                npc.targetList.Clear();
                npc.localCells.Clear();
                npc.seenList.Clear();

                Cell[] cellArray = Cell.GetVisibleCellArray(npc.CurrentCell, 3);

                for (int j = 0; j < 49; j++)
                {
                    if (cellArray[j] == null || !npc.CurrentCell.visCells[j] ||
                        ((cellArray[j].Effects.ContainsKey(Effect.EffectType.Darkness) ||
                         cellArray[j].IsAlwaysDark) && !npc.HasNightVision))
                    {
                        // do nothing
                    }
                    else if ((npc.CurrentCell.Effects.ContainsKey(Effect.EffectType.Darkness) ||
                        npc.CurrentCell.IsAlwaysDark) && !npc.HasNightVision)
                    {
                        // do nothing
                    }
                    else
                    {
                        #region create list of targets, friends and enemies
                        if (cellArray[j].Characters.Count > 0)
                        {
                            foreach(Character chr in new List<Character>(cellArray[j].Characters))
                            {
                                if (chr == null) continue;
                                if (chr != npc && !chr.IsDead && !npc.IsBlind)
                                {
                                    if (Rules.DetectHidden(chr, npc) && Rules.DetectInvisible(chr, npc)) // add detected hidden only
                                    {
                                        if (!chr.IsImmortal) // do not add immortal characters to target list
                                        {
                                            npc.targetList.Add(chr); // add to visible target list
                                            npc.seenList.Add(chr);
                                            /*
                                             * Caution should be used here when determining which array the visible
                                             * character object is added to. We do not want the AI to be too intelligent... or do we? -Eb
                                             * 
                                             * Since alignment is the key factor in determining if a creature is an enemy, we use
                                             * Rules.DetectAlignment to determine if we're going to add this chr to the enemy list.
                                             * Note that if a player is flagged Rules.DetectAlignment will return true
                                             * 
                                             */
                                            
                                            if (Rules.DetectAlignment(chr, npc))
                                                npc.enemyList.Add(chr);
                                            else
                                                npc.friendList.Add(chr);
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        if (npc.GetCellCost(cellArray[j]) <= 2)
                            npc.localCells.Add(cellArray[j]);
                    }
                }
            } catch(Exception e)
            {
                Utils.LogException(e);
            }
            try{
                System.Collections.Generic.List<int> idToRemoveList;
                foreach(Character friend in new List<Character>(npc.friendList))
                {
                    idToRemoveList = new System.Collections.Generic.List<int>();
                    foreach (int playerID in new List<int>(friend.PlayersFlagged))
                    {
                        PC pc = PC.getOnline(playerID);

                        // player is no longer online, add to temp list and remove from flagged list
                        if (pc == null)
                        {
                            idToRemoveList.Add(playerID);
                            continue;
                        }

                        // TODO: why is this commented out? -Eb
                        // remove player ID from friend's flagged list if not same land / map
                        //if (pc.LandID != friend.LandID && pc.MapID != friend.MapID)
                        //{
                        //    friend.PlayersFlagged.Remove(playerID);
                        //    continue;
                        //}

                        if (!npc.PlayersFlagged.Contains(playerID))
                        {
                            npc.PlayersFlagged.Add(playerID);
                        }
                    }

                    if (idToRemoveList.Count > 0)
                    {
                        foreach (int playerID in idToRemoveList)
                        {
                            friend.PlayersFlagged.Remove(playerID);
                            npc.PlayersFlagged.Remove(playerID);
                        }
                    }
                }                

                //if (npc.npcType == NPCType.Creature)
                //{
                    if (npc.mostHated == null)
                        npc.TargetName = "";

                    if (npc.questList.Count > 0)
                        ArtificialIntel.EscortQuestLogic(npc);

                    ArtificialIntel.AssignFearLove(npc);

                    ArtificialIntel.Rate(npc);
                //}
            }
            catch (Exception e)
            {
                Utils.Log("Error in Create AI.ContactList() lower: " + npc.GetLogString(), Utils.LogType.Exception);
                Utils.LogException(e);
            }
		}

        public static void EscortQuestLogic(NPC npc)
        {
            // the escort NPC has the escort-to NPC's npcid in required items, as well as any response strings and finish strings (2 of them)
            // the escort-to NPC has the complete quest in it, with item rewards, and the escorted NPC's npcID in RequiredItems[1]
            // please note there should be two separate QuestIDs, one for the NPC being escorted and one for the escort-to NPC
            try
            {
                // npc is the npc being escorted
                foreach (Quest q in npc.questList)
                {
                    PC questor = null; // the player performing the escort
                    Character target = null; // the target NPC the escort NPC is being escorted to
                    Quest activeQuest = null; // to be supplied by the target NPC

                    if (q.Requirements.ContainsValue(Quest.QuestRequirement.NPC))
                    {
                        foreach (int npcid in q.RequiredItems.Values)
                        {
                            foreach (Character chr in new List<Character>(npc.targetList))
                            {
                                if (!chr.IsPC && chr.npcID == npcid)
                                {
                                    questor = PC.GetOnline(npc.FollowName); // the target was following the questor
                                    target = chr;
                                    if (target != null && questor != null)
                                    {
                                        if (Cell.GetCellDistance(npc.X, npc.Y, target.X, target.Y) <= 0)
                                        {
                                            foreach (Quest qwest in target.questList)
                                            {
                                                if (qwest.Requirements.ContainsValue(Quest.QuestRequirement.NPC))
                                                {
                                                    foreach (int escortedID in qwest.RequiredItems.Values)
                                                    {
                                                        if (escortedID == npc.npcID)
                                                        {
                                                            activeQuest = questor.GetQuest(qwest.QuestID);
                                                            if (activeQuest == null)
                                                            {
                                                                if (qwest.BeginQuest(questor, true))
                                                                {
                                                                    activeQuest = questor.GetQuest(qwest.QuestID);
                                                                    if (q.FinishStrings.ContainsKey(1))
                                                                    {
                                                                        string emote = Utils.ParseEmote(q.FinishStrings[1]);
                                                                        string finish = q.FinishStrings[1];
                                                                        if (emote != "")
                                                                        {
                                                                            finish = finish.Replace("{" + emote + "}", "");
                                                                            questor.WriteToDisplay(npc.Name + " " + emote);
                                                                        }
                                                                        if (finish.Length > 0)
                                                                        {
                                                                            npc.SendToAllInSight(npc.Name + ": " + finish);
                                                                        }
                                                                    }

                                                                    npc.PetOwner = null;
                                                                    questor.Pets.Remove(npc); // remove npc from pets list

                                                                    activeQuest.FinishStep(target, questor, 1); // finish the quest

                                                                    npc.canCommand = false; // no longer command the escorted npc
                                                                    Effect.CreateCharacterEffect(Effect.EffectType.Hello_Immobility, 0, target, -1, null); // perma root
                                                                    npc.BreakFollowMode(); // clear follow mode
                                                                    if (q.DespawnsNPC)
                                                                    {
                                                                        npc.RoundsRemaining = 0;
                                                                        npc.special += " despawn"; // flag npc to despawn in RoundsRemaining rounds
                                                                    }

                                                                    // text from escorted NPC if any
                                                                    if (q.FinishStrings.ContainsKey(2))
                                                                    {
                                                                        string emote = Utils.ParseEmote(q.FinishStrings[2]);
                                                                        string finish = q.FinishStrings[2];
                                                                        if (emote != "")
                                                                        {
                                                                            finish = finish.Replace("{" + emote + "}", "");
                                                                            if (questor.Group == null)
                                                                            {
                                                                                questor.WriteToDisplay(npc.Name + " " + emote);
                                                                            }
                                                                            else
                                                                            {
                                                                                questor.Group.SendGroupMessage(npc.Name + " " + emote);
                                                                            }
                                                                        }
                                                                        if (finish.Length > 0)
                                                                        {
                                                                            if (questor.Group == null)
                                                                            {
                                                                                questor.WriteToDisplay(npc.Name + ": " + finish);
                                                                            }
                                                                            else
                                                                            {
                                                                                questor.Group.SendGroupMessage(npc.Name + " " + finish);
                                                                            }
                                                                        }
                                                                    }
                                                                    npc.questList.Clear(); // prevent the quest from being repeated
                                                                    return;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public static void AssignFearLove(NPC npc)
        {
            int distance = 0;
            int highfear = 1;
            int highhate = 1;
            int lowfear = -1;
            int minFL = 0;
            int maxFL = 0;
            int fearLoveX = 0;
            int fearLoveY = 0;
            int minH = 0;
            int maxH = 0;
            int hateX = 0;
            int hateY = 0;
            npc.mostFeared = null;
            npc.mostLoved = null;
            npc.mostHated = null;

            Character target;
            try
            {
                npc.fear = new int[npc.targetList.Count];
                npc.hate = new int[npc.targetList.Count];

                if (npc.animal || npc.IsUndead)
                {
                    #region Animal Instinct Fear / Love / Hate / Centers
                    // friends: (1000 * (friend->perceivedStrength() / own->perceivedStrength())) / distance
                    // enemies: (-1000 * (enemy->perceivedStrength() / own->perceivedStrength())) / distance
                    for (int a = 0; a < npc.targetList.Count; a++)
                    {
                        target = npc.targetList[a];
                        if (target != null)
                        {
                            distance = Cell.GetCellDistance(npc.X, npc.Y, target.X, target.Y) + 1;
                            if (Rules.DetectAlignment(target, npc)) // enemy
                            {
                                npc.fear[a] = (int)(-1000 * (float)(perceivedStrength(target) / perceivedStrength(npc))) / distance;

                                if (npc.IsUndead)
                                {
                                    npc.hate[a] = 10000 / distance;
                                }
                                else if (npc.animal)
                                {
                                    npc.hate[a] = 2300 / distance;
                                }
                                else
                                {
                                    npc.hate[a] = 1800 / distance;
                                }
                                npc.totalHate += npc.hate[a];
                                if (npc.hate[a] > highhate)
                                {
                                    highhate = npc.hate[a];
                                    npc.mostHated = target;
                                }
                            }
                            else
                            {
                                npc.fear[a] = (int)(1000 * (float)(perceivedStrength(target) / perceivedStrength(npc))) / distance;
                                if (target.Name == npc.FollowName)
                                {
                                    npc.fear[a] = 100000;
                                }
                            }
                            if (npc.fear[a] > highfear)
                            {
                                highfear = npc.fear[a];
                                npc.mostLoved = target;
                            }
                            if (npc.fear[a] < lowfear)
                            {
                                lowfear = npc.fear[a];
                                npc.mostFeared = target;
                            }
                            npc.totalFL += npc.fear[a];
                        }
                        // fear / love
                        try
                        {
                            if (npc.mostLoved != null || npc.mostFeared != null)
                            {
                                minFL = Math.Min(Math.Abs(npc.totalFL), Math.Abs(npc.fear[a]));
                                maxFL = Math.Max(Math.Abs(npc.totalFL), Math.Abs(npc.fear[a]));
                                fearLoveX += (int)((target.X - fearLoveX) * ((float)minFL / maxFL));
                                fearLoveY += (int)((target.Y - fearLoveY) * ((float)minFL / maxFL));
                                npc.fearCenterX = fearLoveX;
                                npc.fearCenterY = fearLoveY;
                            }
                        }
                        catch
                        {
                            Utils.Log("AI (animal fear/love): " + npc.GetLogString(), Utils.LogType.SystemFailure);
                            npc.SendToAll("I'm broken and despawning.");
                            World.GetFacetByID(npc.FacetID).Spawns[npc.SpawnZoneID].NumberInZone -= 1;
                            npc.RemoveFromWorld();
                            return;
                        }
                        // hate
                        try
                        {
                            if (npc.mostHated != null)
                            {
                                minH = Math.Min(npc.totalHate, npc.hate[a]);
                                maxH = Math.Max(npc.totalHate, npc.hate[a]);
                                hateX += (int)((target.X - hateX) * ((float)minH / maxH));
                                hateY += (int)((target.Y - hateY) * ((float)minH / maxH));
                                npc.hateCenterX = hateX;
                                npc.hateCenterY = hateY;
                            }
                        }
                        catch
                        {
                            Utils.Log("AI (animal hate): " + npc.GetLogString(), Utils.LogType.SystemFailure);
                            npc.SendToAll("I'm broken and despawning.");
                            World.GetFacetByID(npc.FacetID).Spawns[npc.SpawnZoneID].NumberInZone -= 1;
                            npc.RemoveFromWorld();
                            return;
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Mind Driven Fear / Love / Hate / Centers for entities seen
                    //for (int a = 0; a < npc.targetList.Count; a++)
                    int a = -1;
                    foreach(Character targ in new List<Character>(npc.targetList))
                    {
                        target = targ;
                        a++;
                        if (npc == null) continue;
                        if (target != null)
                        {
                            distance = Cell.GetCellDistance(npc.X, npc.Y, target.X, target.Y) + 1;
                            if (Rules.DetectAlignment(target, npc))
                            {
                                npc.fear[a] = (int)((-500 * ((float)perceivedStrength(target) / perceivedStrength(npc))) +
                                    (-500 * ((float)perceivedDanger(target) / perceivedDanger(npc)))) / distance;

                                npc.hate[a] = 1800 / distance;

                                npc.totalHate += npc.hate[a];

                                if (npc.hate[a] > highhate)
                                {
                                    highhate = npc.hate[a];
                                    npc.mostHated = target;
                                }
                            }
                            else
                            {
                                if (npc.Name == target.Name)
                                {
                                    npc.fear[a] = (int)((500 * ((float)perceivedStrength(target) / perceivedStrength(npc))) +
                                        (500 * ((float)perceivedDanger(target) / perceivedDanger(npc)))) / distance;
                                    if (target.Name == npc.FollowName)
                                    {
                                        npc.fear[a] = 100000;
                                    }
                                }
                                else
                                {
                                    npc.fear[a] = 0;
                                }

                            }
                            if (npc.fear[a] > highfear)
                            {
                                highfear = npc.fear[a];
                                npc.mostLoved = target;
                            }
                            if (npc.fear[a] < lowfear)
                            {
                                lowfear = npc.fear[a];
                                npc.mostFeared = target;
                            }
                            npc.totalFL += npc.fear[a];
                        }
                        // fear / love
                        try
                        {
                            if (npc.mostLoved != null || npc.mostFeared != null)
                            {
                                minFL = Math.Min(Math.Abs(npc.totalFL), Math.Abs(npc.fear[a]));
                                maxFL = Math.Max(Math.Abs(npc.totalFL), Math.Abs(npc.fear[a]));
                                fearLoveX += (int)((target.X - fearLoveX) * ((float)minFL / maxFL));
                                fearLoveY += (int)((target.Y - fearLoveY) * ((float)minFL / maxFL));
                                npc.fearCenterX = fearLoveX;
                                npc.fearCenterY = fearLoveY;
                            }
                        }
                        catch
                        {
                            Utils.Log("AI (creature fear/love): " + npc.GetLogString(), Utils.LogType.SystemFailure);
                            npc.SendToAll("I'm broken and despawning.");
                            World.GetFacetByID(npc.FacetID).Spawns[npc.SpawnZoneID].NumberInZone -= 1;
                            npc.RemoveFromWorld();
                        }
                        // hate
                        try
                        {
                            if (npc.mostHated != null && npc.hate[a] > 0)
                            {
                                minH = Math.Min(npc.totalHate, npc.hate[a]);
                                maxH = Math.Max(npc.totalHate, npc.hate[a]);
                                hateX += (int)((target.X - hateX) * ((float)minH / maxH));
                                hateY += (int)((target.Y - hateY) * ((float)minH / maxH));
                                npc.hateCenterX = hateX;
                                npc.hateCenterY = hateY;
                            }
                        }
                        catch
                        {
                            Utils.Log("AI (creature hate): " + npc.GetLogString(), Utils.LogType.SystemFailure);
                            npc.SendToAll("I'm broken and despawning.");
                            World.GetFacetByID(npc.FacetID).Spawns[npc.SpawnZoneID].NumberInZone -= 1;
                            npc.RemoveFromWorld();
                        }
                    }
                    #endregion
                }

                if (npc.fearCenterX == 0 && npc.fearCenterY == 0)
                {
                    npc.fearCenterX = npc.X;
                    npc.fearCenterY = npc.Y;
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                Utils.Log("Error in assign fear / love.", Utils.LogType.Exception);
            }
        }

		public static float perceivedStrength(Character target)
		{
			int strength = target.Strength;

			int modifier = target.GetWeaponSkillLevel(target.RightHand);

			float health = (float)target.Hits / target.HitsFull;

			return (strength * health) + modifier;
		}

        public static int perceivedDanger(Character target)
        {
            int danger = 0;

            Item torso = target.GetInventoryItem(Globals.eWearLocation.Torso);

            if (torso != null)
                danger = (int)torso.armorClass * 100;

            if (target.RightHand != null)
            {
                if (target.RightHand.baseType == Globals.eItemBaseType.Bow)
                {
                    danger += 400;
                    if (target.RightHand.nocked)
                        danger += 100;
                }
                danger += 50;
            }

            if (danger == 0) { danger = 50; } // assign some danger to AC 0

            return danger;
        }

        public static void Rate(NPC npc)
        {
            Priority cur_pri = Priority.None;
            ActionType action = ActionType.None;
            Priority new_pri = Priority.None;
            try
            {
                if (!npc.animal) // if creature is not an animal
                {
                    new_pri = rate_TAKE(npc);
                    if (new_pri > cur_pri)
                    {
                        action = ActionType.Take; // pickup
                        cur_pri = new_pri;
                    }
                    new_pri = rate_USE(npc);
                    if (new_pri > cur_pri)
                    {
                        action = ActionType.Use;
                        cur_pri = new_pri;
                    }
                }
                new_pri = rate_CAST(npc);
                if (new_pri > cur_pri)
                {
                    action = ActionType.Cast;
                    cur_pri = new_pri;
                }
                new_pri = rate_SPECIAL(npc);
                if (new_pri > cur_pri)
                {
                    action = ActionType.Special;
                    cur_pri = new_pri;
                }
                new_pri = rate_MOVE(npc);
                if (new_pri > cur_pri)
                {
                    action = ActionType.Move; // move
                    cur_pri = new_pri;
                }
                new_pri = rate_COMBAT(npc);
                if (new_pri > cur_pri)
                {
                    action = ActionType.Combat; // attack
                    cur_pri = new_pri;
                }
                ExecuteAction(npc, action, cur_pri);
                return;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                cur_pri = Priority.None;
                action = ActionType.Move;
                return;
            }
        }

		#region Rate Actions
		public static Priority rate_TAKE(NPC npc)
		{
			Priority pri = Priority.None;

            npc.pItem = null;

            int a;

            #region Find and take a weapon if we are not wielding one and we have an enemy
            if (npc.mostHated != null && npc.RightHand == null) // have enemy, right hand is empty
            {
                if (npc.CurrentCell.Items.Count > 0)
                {
                    ArrayList weaponsList = new ArrayList(); // weapons in our current cell
                    Item weapon;
                    for (a = 0; a < npc.CurrentCell.Items.Count; a++)
                    {
                        weapon = (Item)npc.CurrentCell.Items[a];
                        if (weapon.itemType == Globals.eItemType.Weapon) // weapon
                        {
                            if (weapon.attunedID == 0 || weapon.attunedID == -(npc.npcID)) // not bound to a player
                            {
                                weaponsList.Add(weapon);
                            }
                        }
                    }
                    if (weaponsList.Count == 0) { return Priority.None; } // if there are no weapons return
                    if ((npc.Intelligence + npc.TempIntelligence >= 9 || npc.IsPureMelee) && !npc.IsUndead) // smart critter will know which weapon to look for
                    {
                        ArrayList skillTypes = new ArrayList();
                        for (a = 0; a < weaponsList.Count; a++)
                        {
                            weapon = (Item)weaponsList[a];
                            skillTypes.Add(weapon.skillType);
                        }
                        Globals.eSkillType bestSkillType = npc.GetBestSkill(skillTypes.ToArray()); // now the AI knows which weapon here is best for it
                        if (npc.GetSkillExperience(bestSkillType) > npc.unarmed) // only pick up a weapon if the bestSkillType available is better than our unarmed skill
                        {
                            ArrayList preferredWeapons = new ArrayList();
                            for (a = 0; a < weaponsList.Count; a++)
                            {
                                weapon = (Item)weaponsList[a];
                                if (weapon.skillType == bestSkillType)
                                {
                                    preferredWeapons.Add(weapon); // to get here there would be at least one preferred weapon
                                }
                            }
                            if (npc.IsPureMelee && npc.Level >= 9) // this is where things get hairy - level 9+ pure melee classes know which weapon would be best
                            {
                                for (a = 0; a < preferredWeapons.Count; a++)
                                {
                                    weapon = (Item)preferredWeapons[a];
                                    if (npc.pItem == null)
                                    {
                                        npc.pItem = weapon;
                                    }
                                    else
                                    {
                                        if (npc.pItem.combatAdds < weapon.combatAdds)
                                        {
                                            npc.pItem = weapon;
                                        }
                                    }
                                }
                                pri = Priority.GetWeapon;
                            }
                            else
                            {
                                npc.pItem = (Item)preferredWeapons[Rules.dice.Next(0, preferredWeapons.Count - 1)]; // pick up random preferred weapon
                                pri = Priority.GetWeapon;
                            }
                        }
                    }
                    else
                    {
                        npc.pItem = (Item)weaponsList[Rules.dice.Next(0, weaponsList.Count - 1)]; // pick up a random weapon

                        Globals.eSkillType skillType = Globals.eSkillType.Unarmed;

                        if (npc.RightHand != null)
                        {
                            skillType = npc.RightHand.skillType;
                        }

                        if (Skills.GetSkillLevel(npc.GetSkillExperience(skillType)) > Skills.GetSkillLevel(npc.GetSkillExperience(npc.pItem.skillType)))
                        {
                            pri = Priority.GetWeapon;
                        }
                    }
                }
                else
                {
                    return pri; // cellItemList.Count <= 0 so let's return
                }
            }
            #endregion

            if (npc.mostHated == null && npc.IsGreedy)
            {
                try
                {
                    if (npc.CurrentCell.Items.Count > 0 && !npc.CurrentCell.IsLair)
                    {
                        foreach (Item item in new List<Item>(npc.CurrentCell.Items))
                        {
                            if (item.itemType == Globals.eItemType.Coin)
                            {
                                // not currently affected by encumbrance
                                npc.pItem = item;
                                pri = Priority.GetObject;
                                break;
                            }
                            else if (item.baseType == Globals.eItemBaseType.Gem)
                            {
                                if (npc.SackCountMinusGold < Character.MAX_SACK)
                                {
                                    npc.pItem = item;
                                    pri = Priority.GetObject;
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Utils.Log(npc.GetLogString() + " error in rate_TAKE", Utils.LogType.Unknown);
                    Utils.LogException(e);
                }
            }

			return pri;
		}

        public static Priority rate_USE(NPC npc)
        {
            //TODO
            return Priority.None;
        }

        public static Priority rate_CAST(NPC npc)
        {
            Priority pri = Priority.None;
            int distance = 0;

            try
            {
                #region Enforcer AI will impcast death at an enemy
                if (npc.aiType == AIType.Enforcer) // enforcer AI
                {
                    if (npc.mostHated != null)
                    {
                        return Priority.Enforce;
                    }
                }
                #endregion

                #region Priest AI will raise the dead if no enemy is present
                if (npc.aiType == AIType.Priest && npc.mostHated == null) // look in all visible cells for a player's corpse to raise
                {
                    Cell[] cellArray = Cell.GetVisibleCellArray(npc.CurrentCell, 3);

                    for (int j = 0; j < 49; j++)
                    {
                        if (cellArray[j] == null || !npc.CurrentCell.visCells[j] ||
                            ((npc.CurrentCell.Effects.ContainsKey(Effect.EffectType.Darkness) ||
                                    npc.CurrentCell.IsAlwaysDark) && !npc.HasNightVision))
                        {
                            // do nothing
                        }
                        else
                        {
                            for (int a = 0; a < cellArray[j].Items.Count; a++)
                            {
                                Item item = cellArray[j].Items[a];
                                if (item.itemType == Globals.eItemType.Corpse)
                                {
                                    PC deadPC = PC.GetOnline(item.special);
                                    if (deadPC != null && deadPC.IsDead)
                                        return Priority.RaiseDead;
                                }
                            }
                        }
                    }
                } 
                #endregion

                #region Get the distance to our most hated enemy
                if (npc.mostHated != null)
                {
                    distance = Cell.GetCellDistance(npc.CurrentCell.X, npc.CurrentCell.Y, npc.mostHated.CurrentCell.X, npc.mostHated.CurrentCell.Y); // get distance between me and my most hated
                } 
                #endregion

                #region Unlimited spell casting based on species or if creature is an animal with a spell available
                if (npc.castMode == CastMode.Unlimited)
                {
                    #region mostHated != null
                    if (npc.mostHated != null)
                    {
                        switch (npc.species)
                        {
                            case Globals.eSpecies.FireDragon:
                            case Globals.eSpecies.IceDragon:
                            case Globals.eSpecies.LightningDrake:
                            case Globals.eSpecies.TundraYeti:
                            case Globals.eSpecies.WindDragon:
                                if (distance >= 1 && distance <= 3)
                                {
                                    if (Rules.RollD(1, 100) <= 10)
                                    {
                                        pri = Priority.SpellSling;
                                    }
                                }
                                break;
                            //give thisson 25% chance of throwing death at target
                            case Globals.eSpecies.Thisson:
                                if (distance >= 0 && distance <= 3)
                                {
                                    if (Rules.RollD(1, 100) <= 25)
                                    {
                                        pri = Priority.SpellSling;
                                    }
                                }
                                break;
                            case Globals.eSpecies.Unknown:
                            default:
                                if (distance > -1)
                                {
                                    if (npc.animal && npc.spellList.Count > 0)
                                    {
                                        if (Rules.RollD(1, 100) <= 8)
                                        {
                                            pri = Priority.SpellSling;
                                        }
                                    }
                                }
                                break;
                        }
                    }
                    #endregion
                    else
                    {
                        // TODO: beneficial unlimited spell casting
                    }
                }
                #endregion

                #region else if Limited casting ability (spells must be prepped and mana will be spent)
                else if (npc.castMode == CastMode.Limited)
                {
                    #region mostHated != null
                    if (npc.mostHated != null) // we have an enemy
                    {
                        if (npc.preppedSpell != null) // cast a spell if we have mostHated and a prepped spell
                        {
                            pri = Priority.SpellSling;
                        }
                        else
                        {
                            switch (distance)
                            {
                                case 0:
                                    if (npc.aiType != AIType.Priest && npc.aiType != AIType.Enforcer)
                                    {
                                        if (npc.Mana >= npc.ManaMax)
                                        {
                                            pri = Priority.RangeMove;
                                        }
                                    }
                                    else
                                    {
                                        pri = Priority.PrepareSpell;
                                    }
                                    break;
                                case 1:
                                    if (npc.aiType != AIType.Priest && npc.aiType != AIType.Enforcer)
                                    {
                                        pri = Priority.RangeMove;
                                    }
                                    else
                                    {
                                        pri = Priority.PrepareSpell;
                                    }
                                    break;
                                default:
                                    if (npc.TargetName != npc.mostHated.Name && Rules.CheckPerception(npc))
                                    {
                                        pri = Priority.PrepareSpell;
                                    }
                                    else
                                    {
                                        if (npc.TargetName == npc.mostHated.Name) // this causes a delay in reaction time
                                        {
                                            pri = Priority.PrepareSpell;
                                        }
                                        else
                                        {
                                            pri = Priority.Evaluate;
                                        }
                                    }
                                    break;
                            }
                        }
                    } 
                    #endregion
                    else // we do not have an enemy
                    {
                        if (npc.preppedSpell != null) // we have a prepped spell
                        {
                            if (!npc.preppedSpell.IsBeneficial) // prepped spell is not beneficial
                            {
                                pri = Priority.Rest; // cancel the non beneficial prepped spell
                            }
                            else
                            {
                                pri = Priority.SpellSling;
                            }
                        }
                        else
                        {
                            // do not buff if we are less than 25% mana
                            if (npc.Mana > .25 * npc.ManaFull)
                            {
                                //Dictionary<string, Spell> beneficialSpells = new Dictionary<string, Spell>();
                                List<string> beneficialSpellCommands = new List<string>();

                                // abjuration spells only for now
                                foreach (int spellID in npc.spellList.ints)
                                {
                                    Spell spell = Spell.GetSpell(spellID);
                                    if (spell.IsBeneficial)
                                    {
                                        beneficialSpellCommands.Add(spell.SpellCommand.ToLower());
                                    }
                                }

                                if (beneficialSpellCommands.Count > 0)
                                {
                                    switch (npc.BaseProfession)
                                    {
                                        case ClassType.Wizard:
                                            #region Wizard Buffs
                                            if (beneficialSpellCommands.Contains("shield") && npc.HasManaAvailable("shield"))
                                            {
                                                if (!npc.effectList.ContainsKey(Effect.EffectType.Shield))
                                                {
                                                    npc.buffSpellCommand = "shield";
                                                    npc.buffTargetID = npc.worldNpcID;
                                                    pri = Priority.Buff;
                                                }
                                                else if (npc.Group != null)
                                                {
                                                    foreach (NPC groupNPC in new List<NPC>(npc.Group.GroupNPCList))
                                                    {
                                                        if (!groupNPC.effectList.ContainsKey(Effect.EffectType.Shield))
                                                        {
                                                            npc.buffSpellCommand = "shield";
                                                            npc.buffTargetID = groupNPC.worldNpcID;
                                                            pri = Priority.Buff;
                                                        }
                                                    }
                                                }
                                            }
                                            if (pri == Priority.Buff) break;
                                            if (beneficialSpellCommands.Contains("prfireice") && npc.HasManaAvailable("prfireice"))
                                            {
                                                if (!npc.effectList.ContainsKey(Effect.EffectType.Protection_from_Fire_and_Ice) &&
                                                    (!npc.immuneFire && !npc.immuneCold))
                                                {
                                                    npc.buffSpellCommand = "prfireice";
                                                    npc.buffTargetID = npc.worldNpcID;
                                                    pri = Priority.Buff;
                                                }
                                                else if (npc.Group != null)
                                                {
                                                    foreach (NPC groupNPC in new List<NPC>(npc.Group.GroupNPCList))
                                                    {
                                                        if (!groupNPC.effectList.ContainsKey(Effect.EffectType.Protection_from_Fire_and_Ice) &&
                                                            (!npc.immuneFire && !npc.immuneCold))
                                                        {
                                                            npc.buffSpellCommand = "prfireice";
                                                            npc.buffTargetID = groupNPC.worldNpcID;
                                                            pri = Priority.Buff;
                                                        }
                                                    }
                                                }
                                            }
                                            if (pri == Priority.Buff) break;
                                            if (beneficialSpellCommands.Contains("prfire") && npc.HasManaAvailable("prfire"))
                                            {
                                                if (!npc.effectList.ContainsKey(Effect.EffectType.Protection_from_Fire) && !npc.immuneFire)
                                                {
                                                    npc.buffSpellCommand = "prfire";
                                                    npc.buffTargetID = npc.worldNpcID;
                                                    pri = Priority.Buff;
                                                }
                                                else if (npc.Group != null)
                                                {
                                                    foreach (NPC groupNPC in new List<NPC>(npc.Group.GroupNPCList))
                                                    {
                                                        if (!groupNPC.effectList.ContainsKey(Effect.EffectType.Protection_from_Fire) && !npc.immuneFire)
                                                        {
                                                            npc.buffSpellCommand = "prfire";
                                                            npc.buffTargetID = groupNPC.worldNpcID;
                                                            pri = Priority.Buff;
                                                        }
                                                    }
                                                }
                                            }
                                            if (pri == Priority.Buff) break;
                                            if (beneficialSpellCommands.Contains("prcold") && npc.HasManaAvailable("prcold"))
                                            {
                                                if (!npc.effectList.ContainsKey(Effect.EffectType.Protection_from_Fire) && !npc.immuneCold)
                                                {
                                                    npc.buffSpellCommand = "prcold";
                                                    npc.buffTargetID = npc.worldNpcID;
                                                    pri = Priority.Buff;
                                                }
                                                else if (npc.Group != null)
                                                {
                                                    foreach (NPC groupNPC in new List<NPC>(npc.Group.GroupNPCList))
                                                    {
                                                        if (!groupNPC.effectList.ContainsKey(Effect.EffectType.Protection_from_Fire) && !npc.immuneCold)
                                                        {
                                                            npc.buffSpellCommand = "prcold";
                                                            npc.buffTargetID = groupNPC.worldNpcID;
                                                            pri = Priority.Buff;
                                                        }
                                                    }
                                                }
                                            }
                                            break;
                                            #endregion
                                        case ClassType.Thaumaturge:
                                            #region Thaumaturge Buffs
                                            try
                                            {
                                                if (beneficialSpellCommands.Contains("strength") && npc.HasManaAvailable("strength"))
                                                {
                                                    if (!npc.effectList.ContainsKey(Effect.EffectType.Temporary_Strength))
                                                    {
                                                        npc.buffSpellCommand = "strength";
                                                        npc.buffTargetID = npc.worldNpcID;
                                                        pri = Priority.Buff;
                                                    }
                                                    else if (npc.Group != null)
                                                    {
                                                        foreach (NPC groupNPC in new List<NPC>(npc.Group.GroupNPCList))
                                                        {
                                                            if (!groupNPC.effectList.ContainsKey(Effect.EffectType.Temporary_Strength))
                                                            {
                                                                npc.buffSpellCommand = "strength";
                                                                npc.buffTargetID = groupNPC.worldNpcID;
                                                                pri = Priority.Buff;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            catch (Exception e)
                                            {
                                                Utils.Log("Error in AI, while casting strength buff as a thaumaturge.", Utils.LogType.SystemFailure);
                                                Utils.LogException(e);
                                            }

                                            if (pri == Priority.Buff) break;

                                            try
                                            {
                                                if (beneficialSpellCommands.Contains("prfireice") && npc.HasManaAvailable("prfireice"))
                                                {
                                                    if (!npc.effectList.ContainsKey(Effect.EffectType.Protection_from_Fire_and_Ice) &&
                                                        (!npc.immuneFire && !npc.immuneCold))
                                                    {
                                                        npc.buffSpellCommand = "prfireice";
                                                        npc.buffTargetID = npc.worldNpcID;
                                                        pri = Priority.Buff;
                                                    }
                                                    else if (npc.Group != null)
                                                    {
                                                        foreach (NPC groupNPC in new List<NPC>(npc.Group.GroupNPCList))
                                                        {
                                                            if (!groupNPC.effectList.ContainsKey(Effect.EffectType.Protection_from_Fire_and_Ice) &&
                                                                (!npc.immuneFire && !npc.immuneCold))
                                                            {
                                                                npc.buffSpellCommand = "prfireice";
                                                                npc.buffTargetID = groupNPC.worldNpcID;
                                                                pri = Priority.Buff;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            catch (Exception e)
                                            {
                                                Utils.Log("Error in AI, while casting prfireice buff as a thaumaturge.", Utils.LogType.SystemFailure);
                                                Utils.LogException(e);
                                            }

                                            if (pri == Priority.Buff) break;

                                            try
                                            {
                                                if (beneficialSpellCommands.Contains("prfire") && npc.HasManaAvailable("prfire"))
                                                {
                                                    if (!npc.effectList.ContainsKey(Effect.EffectType.Protection_from_Fire) && !npc.immuneFire)
                                                    {
                                                        npc.buffSpellCommand = "prfire";
                                                        npc.buffTargetID = npc.worldNpcID;
                                                        pri = Priority.Buff;
                                                    }
                                                    else if (npc.Group != null)
                                                    {
                                                        foreach (NPC groupNPC in new List<NPC>(npc.Group.GroupNPCList))
                                                        {
                                                            if (!groupNPC.effectList.ContainsKey(Effect.EffectType.Protection_from_Fire) && !npc.immuneFire)
                                                            {
                                                                npc.buffSpellCommand = "prfire";
                                                                npc.buffTargetID = groupNPC.worldNpcID;
                                                                pri = Priority.Buff;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            catch (Exception e)
                                            {
                                                Utils.Log("Error in AI, while casting prfire buff as a thaumaturge.", Utils.LogType.SystemFailure);
                                                Utils.LogException(e);
                                            }

                                            if (pri == Priority.Buff) break;

                                            try
                                            {
                                                if (beneficialSpellCommands.Contains("prcold") && npc.HasManaAvailable("prcold"))
                                                {
                                                    if (!npc.effectList.ContainsKey(Effect.EffectType.Protection_from_Fire) && !npc.immuneCold)
                                                    {
                                                        npc.buffSpellCommand = "prcold";
                                                        npc.buffTargetID = npc.worldNpcID;
                                                        pri = Priority.Buff;
                                                    }
                                                    else if (npc.Group != null)
                                                    {
                                                        foreach (NPC groupNPC in new List<NPC>(npc.Group.GroupNPCList))
                                                        {
                                                            if (!groupNPC.effectList.ContainsKey(Effect.EffectType.Protection_from_Fire) && !npc.immuneCold)
                                                            {
                                                                npc.buffSpellCommand = "prcold";
                                                                npc.buffTargetID = groupNPC.worldNpcID;
                                                                pri = Priority.Buff;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            catch (Exception e)
                                            {
                                                Utils.Log("Error in AI, while casting prcold buff as a thaumaturge.", Utils.LogType.SystemFailure);
                                                Utils.LogException(e);
                                            }
                                            break;

                                            #endregion
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }
                } 
                #endregion
                else if (npc.castMode == CastMode.NoPrep)
                {
                    if (npc.mostHated != null)
                    {
                    }
                    else
                    {
                        if (npc.IsSpellUser && npc.IsHybrid)
                        {
                            switch (npc.BaseProfession)
                            {
                                case ClassType.Knight:
                                    if (npc.HasManaAvailable("bless"))
                                    {
                                        if (!npc.effectList.ContainsKey(Effect.EffectType.Bless))
                                        {
                                            npc.buffSpellCommand = "bless";
                                            npc.buffTargetID = npc.worldNpcID;
                                            pri = Priority.Buff;
                                        }
                                        else if (npc.Group != null)
                                        {
                                            foreach (NPC groupNPC in new List<NPC>(npc.Group.GroupNPCList))
                                            {
                                                if (!groupNPC.effectList.ContainsKey(Effect.EffectType.Bless))
                                                {
                                                    npc.buffSpellCommand = "bless";
                                                    npc.buffTargetID = groupNPC.worldNpcID;
                                                    pri = Priority.Buff;
                                                }
                                            }
                                        }
                                    }
                                    if (pri == Priority.Buff) break;
                                    if (!npc.effectList.ContainsKey(Effect.EffectType.Temporary_Strength))
                                    {
                                        npc.buffSpellCommand = "strength";
                                        npc.buffTargetID = npc.worldNpcID;
                                        pri = Priority.Buff;
                                    }
                                    else if (npc.Group != null)
                                    {
                                        foreach (NPC groupNPC in new List<NPC>(npc.Group.GroupNPCList))
                                        {
                                            if (!groupNPC.effectList.ContainsKey(Effect.EffectType.Temporary_Strength))
                                            {
                                                npc.buffSpellCommand = "strength";
                                                npc.buffTargetID = groupNPC.worldNpcID;
                                                pri = Priority.Buff;
                                            }
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                #region Look for a wounded ally or heal myself if I can, have mana, and do not have a spell prepped
                /*  Lawful healers will look for an ally at or below 50% health to heal and if they find none, will heal themselves if
                    they are at or below %50. All other alignments will heal themselves first and then look for an ally. Any AI that doesn't
                    have a mostHated will remain in "heal mode" until they are back at full health. */
                if (npc.IsHealer() && // we area a healer
                    npc.HasManaAvailable(Spell.GetSpell("cure").SpellID) && // we have mana to cast the cure spell
                    npc.preppedSpell == null) // we do not have a spell prepared
                // if this creature can heal itself and others
                {
                    Character wounded = null;
                    wounded = (Character)npc;
                    #region Lawful Healers
                    if (npc.Alignment == Globals.eAlignment.Lawful) // lawful creatures will try to heal others first
                    {
                        for (int a = 0; a < npc.friendList.Count; a++) // loop through target array and see if there is another wounded
                        {
                            Character friend = npc.friendList[a];
                            if (friend.Hits / friend.HitsFull < wounded.Hits / wounded.HitsFull) // designate the most wounded
                            {
                                wounded = friend;
                            }
                        }
                        if (npc.aiType == AIType.Priest) // AIType.Priest will heal up to and including 75% health
                        {
                            if (wounded != null && wounded.Hits > (int)(wounded.HitsFull * .75))
                            {
                                wounded = null;
                            }
                        }
                        else // all other healers will heal up to and including 50% health
                        {
                            if (wounded != null && wounded.Hits > (int)(wounded.HitsFull * .50))
                            {
                                wounded = null;
                            }
                        }
                    }
                    #endregion
                    #region Non Lawful Healers
                    else
                    {
                        wounded = null; // null the wounded character
                        if (npc.Hits <= (int)(npc.HitsFull * .50)) // if creatures health is below 50% designate itself as wounded
                        {
                            wounded = npc;
                        }
                        else
                        {
                            for (int a = 0; a < npc.friendList.Count; a++) // loop through target array and see if there is another wounded
                            {
                                Character friend = npc.friendList[a];
                                if (wounded == null)
                                {
                                    wounded = friend;
                                }
                                else
                                {
                                    if (friend.Hits / friend.HitsFull < wounded.Hits / wounded.HitsFull) // designate the most wounded
                                    {
                                        wounded = friend;
                                    }
                                }
                            }
                            if (npc.aiType == AIType.Priest) // even though there are currently no non lawful AIType.Priest, there may be in the future
                            {
                                if (wounded != null && wounded.Hits >= (int)(wounded.HitsFull * .75)) // do not heal a wounded character if health is above 75%
                                {
                                    wounded = null;
                                }
                            }
                            else
                            {
                                if (wounded != null && wounded.Hits >= (int)(wounded.HitsFull * .50)) // do not heal a wounded character if health is above 50%
                                {
                                    wounded = null;
                                }
                            }
                        }
                    } 
                    #endregion

                    if (wounded != null && npc.HasManaAvailable(Spell.GetSpell("cure").SpellID)) // creature or a friend is wounded and I can cure them
                    {
                        npc.buffTargetID = wounded.GetID(); // temporary - need a more unique way of picking targets, names are not always unique
                        pri = Priority.Heal;
                    }
                    else if (wounded == null && npc.mostHated == null && npc.Hits < npc.HitsFull && npc.HasManaAvailable(Spell.GetSpell("cure").SpellID)) // top off my hits
                    {
                        npc.buffTargetID = npc.GetID();
                        pri = Priority.Heal;
                    }
                }
                #endregion

                return pri;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return pri;
            }
        }

        public static Priority rate_SPECIAL(NPC npc)
        {
            Priority pri = Priority.None;
            if (npc.mostHated == null)
            {
                // npc was immobile, is now mobile, has no follow target, and is not at spawn coords
                if ((!npc.IsMobile || npc.wasImmobile) && npc.PetOwner == null &&
                    npc.CurrentCell != Cell.GetCell(npc.FacetID, npc.LandID, npc.MapID, npc.spawnXCord, npc.spawnYCord, npc.spawnZCord))
                    return Priority.GoHome;

                if (npc.preppedSpell != null) // return no SPECIAL priority if we have a spell prepped
                    return pri;

                if (npc.lairCritter && !npc.HasPatrol)
                {
                    if (npc.X != npc.lairXCord && npc.Y != npc.lairYCord) // not at our lair coordinate
                    {
                        int distance = Cell.GetCellDistance(npc.X, npc.Y, npc.lairXCord, npc.lairYCord);

                        if (distance >= 10) // warp back to our lair
                        {
                            npc.CurrentCell = Cell.GetCell(npc.FacetID, npc.LandID, npc.MapID, npc.lairXCord, npc.lairYCord, npc.lairZCord);
                        }
                        else if (distance <= 3) // within view of our lair
                        {
                            if (npc.Hits < npc.HitsFull) // we may rest or we may test our lair defenses
                            {
                                int random = Rules.RollD(1, 100);
                                if (random <= 50)
                                {
                                    if (npc.preppedSpell == null)
                                    {
                                        pri = Priority.Rest;
                                    }
                                }
                                else
                                {
                                    pri = Priority.LairDefense;
                                }
                            }
                        }
                        else if (npc.preppedSpell == null && !npc.HasPatrol && npc.PetOwner == null)
                        {
                            pri = Priority.GoHome;
                        }
                    }
                    else // we are at our lair coordinate
                    {
                        if (npc.Hits < npc.HitsFull) // we're not casting a spell, we're at our lair, we're wounded... so let's rest
                        {
                            int random = Rules.RollD(1, 100);
                            if (random <= 50)
                            {
                                if (npc.preppedSpell == null)
                                {
                                    pri = Priority.Rest;
                                }
                            }
                            else
                            {
                                pri = Priority.LairDefense;
                            }
                        }
                    }
                }
                else // not a lair critter
                {
                    if (npc.Hits <= (int)(npc.HitsFull * .15)) // we're below 15% health so let's rest
                    {
                        pri = Priority.Rest;
                    }
                }
            }
            else
            {
                if (npc.lairCritter && !npc.HasPatrol)
                {
                    int distance = Cell.GetCellDistance(npc.X, npc.Y, npc.lairXCord, npc.lairYCord);
                    if (distance >= 10)
                    {
                        npc.CurrentCell = Cell.GetCell(npc.FacetID, npc.LandID, npc.MapID, npc.lairXCord, npc.lairYCord, npc.lairZCord);
                    }
                }
            }
            return pri;
        }

        public static Priority rate_MOVE(NPC npc)
        {
            Priority pri = Priority.None;

            if (!npc.IsMobile)
            {
                if (npc.mostHated == null || npc.IsTrulyImmobile) // AI has no enemy or is truly not mobile (currently only statues, check property)
                {
                    return pri;
                }

                if (npc.mostHated != null) // we have a most hated and it should be possible for us to move since we are not truly immobile
                {
                    npc.IsMobile = true;
                    npc.wasImmobile = true;
                }
                else return pri; // return Priority.None if we're not supposed to be moving around
            }
            else
            {
                if (npc.mostHated == null && npc.wasImmobile && npc.PetOwner == null)
                {
                    if (npc.X != npc.spawnXCord || npc.Y != npc.spawnYCord || npc.Z != npc.spawnZCord)
                    {
                        return Priority.GoHome;
                    }
                    else // we are at our spawn coordinates so let's become immobile again and return no move priority
                    {
                        npc.IsMobile = false;
                        npc.wasImmobile = false;
                        return pri;
                    }
                }
            }

            // return no priority if the AI is supposed to be standing still
            if (npc.mostHated == null && npc.effectList.ContainsKey(Effect.EffectType.Hello_Immobility))
            {
                return pri;
            }
            else if (npc.mostHated != null && npc.effectList.ContainsKey(Effect.EffectType.Hello_Immobility))
            {
                npc.effectList[Effect.EffectType.Hello_Immobility].StopCharacterEffect();
            }

            if (npc.Hits < (int)(npc.HitsFull / 5))
            {
                if (!npc.lairCritter && !npc.IsUndead && npc.friendList.Count <= 0 &&
                    (npc.Group == null || npc.Group.GroupNPCList.Count <= 0) && npc.enemyList.Count > 0)
                {
                    pri = Priority.Flee;
                    return pri;
                }
            }

            if (npc.totalFL == 0 && npc.totalHate == 0)
            {
                pri = Priority.Wander; // low priority move

                if (npc.previousMostHated != null)
                    pri = Priority.Investigate;

                if (npc.IsMagicSniffer)
                {
                    if (World.magicWithinRange(npc))
                    {
                        pri = Priority.InvestigateMagic;
                    }
                }
            }
            else if (npc.totalHate == 0 && npc.totalFL > 0)
            {
                // move toward friend FLCenter - normal
                if (Cell.GetCellDistance(npc.X, npc.Y, npc.fearCenterX, npc.fearCenterY) > 2)
                {
                    pri = Priority.Socialize;
                }
                else
                {
                    pri = Priority.Interact;
                }

                if (npc.mostHated == null && npc.previousMostHated != null)
                {
                    pri = Priority.Investigate;
                }

                if (npc.IsMagicSniffer)
                {
                    if (World.magicWithinRange(npc))
                    {
                        pri = Priority.InvestigateMagic;
                    }
                }

            }
            else if (npc.totalFL < 0 && Math.Abs(npc.totalFL) > npc.totalHate)
            {
                pri = Priority.Advance;
            }
            else
            {
                if (npc.TargetName != npc.mostHated.Name)
                {
                    npc.EmitSound(npc.attackSound);
                }
                npc.TargetName = npc.mostHated.Name;
                npc.FollowName = npc.mostHated.Name;
                pri = Priority.Advance;
            }

            return pri;
        }

		public static Priority rate_COMBAT(NPC npc)
		{
			Priority pri = Priority.None;

			Character mostHated = npc.mostHated;

            if (npc.Hits < (int)(npc.HitsFull / 5))
            {
                if (!npc.lairCritter && !npc.IsUndead && npc.friendList.Count <= 0 &&
                    (npc.Group == null || npc.Group.GroupNPCList.Count <= 0) && npc.enemyList.Count > 0)
                {
                    pri = Priority.Flee;
                    return pri;
                }
            }

            if (npc.mostHated != null)
            {
                if (mostHated.Hits <= 0) { return pri; }

                int distance = Cell.GetCellDistance(npc.X, npc.Y, mostHated.X, mostHated.Y);

                switch (distance)
                {
                    case 0:
                        if (npc.RightHand != null && npc.RightHand.baseType == Globals.eItemBaseType.Bow) // holding a bow at distance 0
                        {
                            if (npc.RightHand.nocked)
                            {
                                pri = Priority.Attack;
                            }
                            else
                            {
                                pri = Priority.RangeMove;
                            }
                        }
                        else
                        {
                            pri = Priority.Attack;
                        }
                        break;
                    case 1:
                        if (npc.RightHand != null && npc.RightHand.baseType == Globals.eItemBaseType.Bow) // holding a bow at distance 1
                        {
                            if (npc.RightHand.nocked)
                            {
                                pri = Priority.Attack;
                            }
                            else
                            {
                                pri = Priority.RangeMove;
                            }
                        }
                        else if (npc.RightHand != null && (npc.RightHand.baseType == Globals.eItemBaseType.Halberd || npc.RightHand.returning)) // wielding a halberd
                        {
                            pri = Priority.Attack;
                        }
                        else if (npc.BaseProfession == ClassType.Martial_Artist && npc.Stamina >= (int)(npc.StaminaMax * .15) && Rules.RollD(1, 10) <= npc.Level) // martial artist
                        {
                            pri = Priority.Attack;
                        }
                        break;
                    case 2:
                        if (npc.RightHand != null && (npc.RightHand.baseType == Globals.eItemBaseType.Bow || npc.RightHand.returning)) // holding a bow at distance 2
                        {
                            pri = Priority.Attack;
                        }
                        else if (npc.BaseProfession == ClassType.Martial_Artist && npc.Stamina >= (int)(npc.StaminaMax * .15) && Rules.RollD(1, 20) <= npc.Level)
                        {
                            pri = Priority.Attack;
                        }
                        break;
                    case 3:
                        if (npc.RightHand != null && (npc.RightHand.baseType == Globals.eItemBaseType.Bow || npc.RightHand.returning))
                        {
                            pri = Priority.Attack;
                        }
                        else if (npc.BaseProfession == ClassType.Martial_Artist && npc.GetWeaponSkillLevel(null) >= 9 &&
                            npc.Stamina >= (int)(npc.StaminaMax * .15) && Rules.RollD(1, 30) < npc.Level)// MA with 3rd Dan+ skill
                        {
                            pri = Priority.Attack;
                        }
                        break;
                    default:
                        break;
                }
            }
			return pri;
		}
		#endregion

        public static void ExecuteAction(NPC npc, ActionType actionType, Priority pri)
        {
            if (npc == null)
                return;
            if (npc.CurrentCell == null)
            {
                Utils.Log(npc.GetLogString() + " removing from world - null CELL", Utils.LogType.SystemFailure);
                npc.RemoveFromWorld();
            }
            if (npc.Group != null && npc.Group.GroupLeaderID == npc.worldNpcID)
            {
                #region Group Related
                try
                {
                    //if (ch.Group.GroupNPCList != null)
                    //{
                        foreach(NPC groupCreature in new List<NPC>(npc.Group.GroupNPCList))
                        {
                            if (npc.Group != null && npc.Group.GroupNPCList != null)
                            {
                                if (groupCreature != null && groupCreature != npc && !groupCreature.IsDead)
                                {
                                    groupCreature.mostHated = npc.mostHated;
                                    groupCreature.mostFeared = npc.mostFeared;
                                    groupCreature.mostLoved = npc.mostLoved;
                                    groupCreature.targetList = npc.targetList;
                                    groupCreature.friendList = npc.friendList;
                                    groupCreature.enemyList = npc.enemyList;
                                    groupCreature.hate = npc.hate;
                                    groupCreature.hateCenterX = npc.hateCenterX;
                                    groupCreature.hateCenterY = npc.hateCenterY;
                                    groupCreature.fear = npc.fear;
                                    groupCreature.fearCenterX = npc.fearCenterX;
                                    groupCreature.fearCenterY = npc.fearCenterY;
                                    groupCreature.totalFL = npc.totalFL;
                                    groupCreature.totalHate = npc.totalHate;
                                    groupCreature.localCells = npc.localCells;
                                    groupCreature.pItem = npc.pItem; // only the leader deals with items
                                    try
                                    {
                                        // group creatures will cast and do combat against the most hated...
                                        if (groupCreature.mostHated != null && (actionType == ActionType.Cast || actionType == ActionType.Combat))
                                        {
                                            ArtificialIntel.ExecuteAction(groupCreature, actionType, pri);
                                        }
                                    }
                                    catch
                                    {
                                        continue;
                                    }
                                }
                            }
                        }
                    //}
                }
                catch (Exception e)
                {
                    Utils.Log("Failure at ExecuteAction(" + npc.GetLogString() + ", " + actionType.ToString() + ", " + pri.ToString() + ") Group Related Failure", Utils.LogType.SystemFailure);
                    Utils.Log("Exception Data: " + e.Data + " Source: " + e.Source, Utils.LogType.Exception);
                    Utils.LogException(e);
                }
                #endregion
            }

            int distance = -1;

            if (npc.mostHated != null)
            {
                npc.previousMostHated = npc.mostHated;
            }

            if (pri == Priority.None)
            {
                return;
            }

            if (pri == Priority.Evaluate && actionType != ActionType.Special)
            {
                actionType = ActionType.Special;
            }

            if (pri == Priority.Flee && actionType != ActionType.Move)
            {
                actionType = ActionType.Move;
            }

            if (pri == Priority.RangeMove && actionType != ActionType.Move)
            {
                actionType = ActionType.Move;
            }

            if (pri == Priority.Rest && actionType != ActionType.Special)
            {
                actionType = ActionType.Special;
            }

            if (pri == Priority.Buff && actionType != ActionType.Cast)
            {
                actionType = ActionType.Cast;
            }

            if (pri == Priority.Heal && actionType != ActionType.Cast)
            {
                actionType = ActionType.Cast;
            }

            try
            {
                switch (actionType)
                {
                    case ActionType.Take:
                        #region Take
                        if (npc.pItem == null)  // pItem should not be null if this action is being executed...
                        {
                            return;
                        }
                        if (pri == Priority.GetWeapon) // we're picking up a weapon
                        {
                            Command.ParseCommand(npc, "take", npc.pItem.name);
                        }
                        else if (pri == Priority.GetObject)
                        {
                            Command.ParseCommand(npc, "take", npc.pItem.name + ";put " + npc.pItem.name + " in sack");
                            npc.pItem = null;
                        }
                        #endregion
                        break;
                    case ActionType.Use:
                        #region Use
                        //if (ch.pItem.itemType == Item.ItemType.Wearable)
                        //{
                        //    Command.parseCommand(ch, "wear", ch.pItem.name);
                        //    if (whichhand(ch, ch.pItem) != -1)
                        //    {
                        //        if (whichhand(ch, ch.pItem) == 1)
                        //        {
                        //            ch.putItemInSack(ch.pItem);
                        //            ch.UnEquipLeftHand(ch.LeftHand);
                        //        }
                        //        else if (whichhand(ch, ch.pItem) == 0)
                        //        {
                        //            ch.putItemInSack(ch.pItem);
                        //            ch.UnEquipRightHand(ch.RightHand);
                        //        }
                        //    }
                        //}
                        //else
                        //{
                        //    ch.putItemInSack(ch.pItem);
                        //    if (whichhand(ch, ch.pItem) == 1)
                        //        ch.UnEquipLeftHand(ch.LeftHand);
                        //    else if (whichhand(ch, ch.pItem) == 0)
                        //        ch.UnEquipRightHand(ch.RightHand);
                        //}
                        #endregion
                        break;
                    case ActionType.Cast:
                        #region Cast
                        #region Priority.RaiseDead
                        if (pri == Priority.RaiseDead)
                        {
                            Cell[] cellArray = Cell.GetVisibleCellArray(npc.CurrentCell, 3);

                            for (int j = 0; j < 49; j++)
                            {
                                if (cellArray[j] == null || !npc.CurrentCell.visCells[j] ||
                                    ((npc.CurrentCell.Effects.ContainsKey(Effect.EffectType.Darkness) ||
                                    npc.CurrentCell.IsAlwaysDark) && !npc.HasNightVision))
                                {
                                    // do nothing
                                }
                                else
                                {
                                    for (int a = 0; a < cellArray[j].Items.Count; a++)
                                    {
                                        Item item = cellArray[j].Items[a];
                                        if (item.itemType == Globals.eItemType.Corpse)
                                        {
                                            PC deadPC = PC.GetOnline(item.special);
                                            if (deadPC != null && deadPC.IsDead)
                                            {
                                                if (npc.CurrentCell != cellArray[j])
                                                {
                                                    npc.AIGotoXYZ(cellArray[j].X, cellArray[j].Y, cellArray[j].Z);
                                                }
                                                else
                                                {
                                                    goto castAction;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        }
                        #endregion
                    castAction:
                        switch (npc.castMode)
                        {
                            case CastMode.Unlimited: // no prep is needed and no mana is used (eg: AIType.Enforcer aka Axe Glacier lawful statues, salamander, dragons, ice lizards)
                                #region Unlimited
                                NPC.PrepareSpell(npc, pri);
                                if (npc.preppedSpell.TargetType == Globals.eTargetType.Point_Blank_Area_Effect)
                                {
                                    Command.ParseCommand(npc, "cast", "");
                                }
                                else if (npc.preppedSpell.IsBeneficial)
                                {
                                    if (Map.FindTargetInView(npc, npc.buffTargetID, true, true) != null)
                                    {
                                        Command.ParseCommand(npc, "impcast", npc.preppedSpell.SpellCommand + " at " + npc.buffTargetID);
                                    }
                                    else
                                    {
                                        npc.preppedSpell = null;
                                    }
                                }
                                else
                                {
                                    if (Map.FindTargetInView(npc, npc.mostHated.GetID(), false, false) != null)
                                    {
                                        Command.ParseCommand(npc, "impcast", npc.preppedSpell.SpellCommand + " at " + npc.mostHated.GetID());
                                    }
                                    else
                                    {
                                        npc.preppedSpell = null;
                                    }
                                }
                                break; 
                                #endregion
                            case CastMode.Limited: // spells need to be warmed and then cast, spending mana when cast
                                #region Limited
                                if (npc.preppedSpell == null)
                                {
                                    if (!Creature.PrepareSpell(npc, pri)) // cannot prepare a spell (eg: not enough mana)
                                    {
                                        goto alternativeOptions;
                                    }
                                }
                                else
                                {
                                    if (npc.preppedSpell.TargetType == Globals.eTargetType.Point_Blank_Area_Effect)
                                    {
                                        Command.ParseCommand(npc, "cast", "");
                                    }
                                    else if (npc.preppedSpell.IsBeneficial)
                                    {
                                        if (Map.FindTargetInView(npc, npc.buffTargetID, true, true) != null)
                                        {
                                            Command.ParseCommand(npc, "cast", npc.preppedSpell.SpellCommand + " at " + npc.buffTargetID);
                                        }
                                        else goto cancelCast;
                                    }
                                    else
                                    {
                                        if (Map.FindTargetInView(npc, npc.mostHated.Name, false, false) != null)
                                        {
                                            if (npc.preppedSpell.TargetType == Globals.eTargetType.Area_Effect)
                                            {
                                                if (Cell.GetCellDistance(npc.X, npc.Y, npc.mostHated.X, npc.mostHated.Y) <= 1)
                                                {
                                                    goto cancelCast;
                                                }
                                            }
                                            Command.ParseCommand(npc, "cast", npc.preppedSpell.SpellCommand + " at " + npc.mostHated.Name);
                                        }
                                        else goto cancelCast;
                                    }
                                }
                                break; 
                                #endregion
                            case CastMode.NoPrep: // no prep time is needed, but mana is spent when spell is cast (eg: knights)
                                #region NoPrep
                                if (!Creature.PrepareSpell(npc, pri))
                                {
                                    goto alternativeOptions;
                                }
                                if (npc.preppedSpell.TargetType == Globals.eTargetType.Point_Blank_Area_Effect)
                                {
                                    Command.ParseCommand(npc, "cast", "");
                                }
                                else if (npc.preppedSpell.IsBeneficial)
                                {
                                    if (Map.FindTargetInView(npc, npc.buffTargetID, true, true) != null)
                                    {
                                        Command.ParseCommand(npc, "cast", npc.preppedSpell.SpellCommand + " at " + npc.buffTargetID);
                                    }
                                    else goto cancelCast;
                                }
                                else
                                {
                                    if (Map.FindTargetInView(npc, npc.mostHated.Name, false, false) != null)
                                    {
                                        Command.ParseCommand(npc, "cast", npc.preppedSpell.SpellCommand + " at " + npc.mostHated.Name);
                                    }
                                    else goto cancelCast;
                                }
                                break; 
                                #endregion
                            case CastMode.Never: // cannot cast spells (possible use in future to toggle a spellcasters ability)
                            default:
                                break;
                        }
                        break;
                    cancelCast:
                        if (npc.mostHated == null)
                        {
                            ExecuteAction(npc, ActionType.Special, Priority.Rest);
                            break;
                        }
                        else
                        {
                            if (npc.preppedSpell != null)
                            {
                                npc.preppedSpell = null;
                                npc.EmitSound(Sound.GetCommonSound(Sound.CommonSound.SpellFail));
                            }
                            // continue to alternativeOptions
                        }
                        #region alternativeOptions: could not prepare a spell even though it was our priority
                    alternativeOptions:
                        if (npc.mostHated != null)
                        {
                            distance = Cell.GetCellDistance(npc.X, npc.Y, npc.mostHated.X, npc.mostHated.Y);
                        }
                        if (pri >= Priority.PrepareSpell && pri < Priority.Buff)
                        {
                            if (npc.mostHated != null)
                            {
                                if (distance > 0)
                                {
                                    ExecuteAction(npc, ActionType.Move, Priority.Advance);
                                }
                                else
                                {
                                    ExecuteAction(npc, ActionType.Combat, Priority.Attack);
                                }
                            }
                        }
                        else if (pri == Priority.Buff)
                        {
                            ExecuteAction(npc, ActionType.Special, Priority.Rest);
                        }
                        else if (pri == Priority.Heal)
                        {
                            if (npc.buffTargetID == (npc as Character).worldNpcID) // flee if we were trying to heal and could not
                            {
                                if (Rules.CheckPerception(npc))
                                {
                                    ExecuteAction(npc, ActionType.Move, Priority.RangeMove);
                                }
                                else
                                {
                                    if (npc.mostHated != null)
                                    {
                                        if (distance > 0)
                                        {
                                            ExecuteAction(npc, ActionType.Move, Priority.Advance);
                                        }
                                        else
                                        {
                                            ExecuteAction(npc, ActionType.Combat, Priority.Attack);
                                        }
                                    }
                                    else
                                    {
                                        ExecuteAction(npc, ActionType.Special, Priority.Rest);
                                    }
                                }
                            }
                            else
                            {
                                if (npc.mostHated != null)
                                {
                                    if (distance > 0)
                                    {
                                        ExecuteAction(npc, ActionType.Move, Priority.Advance);
                                    }
                                    else
                                    {
                                        ExecuteAction(npc, ActionType.Combat, Priority.Attack);
                                    }
                                }
                                else
                                {
                                    ExecuteAction(npc, ActionType.Special, Priority.Rest);
                                }
                            }
                        }
                        #endregion
                        #endregion
                        break;
                    case ActionType.Special:
                        #region Special
                        if (pri == Priority.Evaluate)
                        {
                            // do nothing
                        }

                        if (pri == Priority.Rest)
                        {
                            if (npc.lairCritter && npc.X == npc.lairXCord && npc.Y == npc.lairYCord)
                            {
                                if (npc.Hits < npc.HitsFull)  // increase stats
                                {
                                    npc.Hits += (int)(npc.HitsFull * .05);
                                    if (npc.Hits > npc.HitsFull) { npc.Hits = npc.HitsFull; }
                                }
                                if (npc.Stamina < npc.StaminaFull)
                                {
                                    npc.Stamina += (int)(npc.StaminaFull * .05);
                                    if (npc.Stamina > npc.StaminaFull) { npc.Stamina = npc.StaminaFull; }
                                }
                                if (npc.Mana < npc.ManaFull)
                                {
                                    npc.Mana += (int)(npc.ManaFull * .05);
                                    if (npc.Mana > npc.ManaFull) { npc.Mana = npc.ManaFull; }
                                }
                            }
                            Command.ParseCommand(npc, "rest", null);
                            break;
                        }

                        if (pri == Priority.GoHome)
                        {
                            if (npc.lairCritter)
                            {
                                npc.AIGotoXYZ(npc.lairXCord, npc.lairYCord, npc.lairZCord);
                                break;
                            }
                            else
                            {
                                if (npc.special.ToLower().Contains("despawn"))
                                {

                                }

                                // warp the npc back home
                                npc.CurrentCell = Cell.GetCell(npc.FacetID, npc.LandID, npc.MapID, npc.spawnXCord, npc.spawnYCord, npc.spawnZCord);
                                
                                // for quest npcs that remain immortal until following / active with a player character
                                if (npc.WasImmortal)
                                {
                                    npc.WasImmortal = false;
                                    npc.IsImmortal = true;
                                }
                                break;
                            }
                        }

                        if (npc.lairCritter)
                        {
                            if (pri == Priority.LairDefense)
                            {
                                string spellCommand = " ";
                                switch (npc.species)
                                {
                                    case Globals.eSpecies.FireDragon:
                                    case Globals.eSpecies.IceDragon:
                                        spellCommand = "drbreath";
                                        break;
                                    case Globals.eSpecies.LightningDrake:
                                        spellCommand = "lightning";
                                        break;
                                    case Globals.eSpecies.TundraYeti:
                                        spellCommand = "blizzard";
                                        break;

                                }
                                #region Special Lair Critter Defense
                                switch (npc.species)
                                {
                                    case Globals.eSpecies.FireDragon:
                                    case Globals.eSpecies.IceDragon:
                                    case Globals.eSpecies.LightningDrake:
                                    case Globals.eSpecies.TundraYeti:
                                        if (Rules.dice.Next(npc.HitsFull) >= npc.Hits)
                                        {
                                            switch (Rules.dice.Next(9) + 1)
                                            {
                                                case 1:
                                                case 2:
                                                    npc.SendToAllInSight("The " + npc.Name + " snorts and then unleashes an ear piercing roar!");
                                                    Creature.DragonFear(npc);
                                                    break;
                                                case 3:
                                                case 4:
                                                    if (npc.species == Globals.eSpecies.FireDragon)
                                                    {
                                                        npc.SendToAllInSight("The " + npc.Name + " hisses and puffs smoke from " + Character.possessive[(int)npc.gender].ToLower() + " nostrils as it looks about " + Character.possessive[(int)npc.gender].ToLower() + " lair.");
                                                    }
                                                    else
                                                    {
                                                        npc.SendToAllInSight("The " + npc.Name + " growls and scans around " + Character.possessive[(int)npc.gender].ToLower() + " lair for intruders.");
                                                    }
                                                    break;
                                                case 5:
                                                case 6:
                                                    npc.SendToAllInSight("The " + npc.Name + " emits a guttural growl as " + Character.pronoun[(int)npc.gender].ToLower() + " moves about " + Character.possessive[(int)npc.gender].ToLower() + " lair.");
                                                    if (!Map.isCharNextToWall(npc))
                                                    {
                                                        npc.doAIMove();
                                                        if (!Map.isCharNextToWall(npc))
                                                        {
                                                            npc.doAIMove(); ;
                                                        }
                                                    }
                                                    break;
                                                case 7:
                                                case 8:
                                                    string direction = "";
                                                    do
                                                    {
                                                        switch (Rules.dice.Next(7) + 1)
                                                        {
                                                            case 1: direction = "n"; break;
                                                            case 2: direction = "s"; break;
                                                            case 3: direction = "e"; break;
                                                            case 4: direction = "w"; break;
                                                            case 5: direction = "ne"; break;
                                                            case 6: direction = "nw"; break;
                                                            case 7: direction = "se"; break;
                                                            case 8: direction = "sw"; break;
                                                        }
                                                    }
                                                    while (Map.IsSpellPathBlocked(Map.GetCellRelevantToCell(npc.CurrentCell, direction, false)));
                                                    Command.ParseCommand(npc, "impcast", spellCommand + " " + direction);
                                                    break;
                                                default:
                                                    npc.SendToAllInSight("The " + npc.Name + " roars fiercely and scrapes the floor of " + Character.possessive[(int)npc.gender].ToLower() + " lair with " + Character.possessive[(int)npc.gender].ToLower() + " massive claws.");
                                                    Creature.DragonFear(npc);
                                                    break;
                                            }
                                        }
                                        break;
                                    default:
                                        break;

                                }
                                switch (npc.Name)
                                {
                                    case "giant":
                                        if (Rules.dice.Next(npc.HitsFull) >= npc.Hits)
                                        {
                                            switch (Rules.dice.Next(9) + 1)
                                            {
                                                case 1:
                                                case 2:
                                                    npc.SendToAllInSight("The " + npc.Name + " stomps " + Character.possessive[(int)npc.gender].ToLower() + " massive foot!");
                                                    Creature.GiantStomp(npc);
                                                    break;
                                                case 3:
                                                case 4:
                                                    npc.SendToAllInSight("The " + npc.Name + " sniffs the air and squints " + Character.possessive[(int)npc.gender].ToLower() + " eyes.");
                                                    break;
                                                case 5:
                                                case 6:
                                                    if (npc.RightHand == null)
                                                    {
                                                        npc.SendToAllInSight("The " + npc.Name + " growls and then slams " + Character.possessive[(int)npc.gender].ToLower() + " fist into the ground!");
                                                    }
                                                    else
                                                    {
                                                        npc.SendToAllInSight("The " + npc.Name + " roars loudly and slams " + Character.possessive[(int)npc.gender].ToLower() + " " + npc.RightHand.name + " into the ground!");
                                                    }
                                                    Creature.GiantStomp(npc);
                                                    break;
                                                case 7:
                                                case 8:
                                                    npc.SendToAllInSight("The " + npc.Name + " growls and begins to look around " + Character.possessive[(int)npc.gender].ToLower() + " lair.");
                                                    if (!Map.isCharNextToWall(npc)) { AIMakePCMove(npc); }
                                                    break;
                                                default:
                                                    npc.SendToAllInSight("The " + npc.Name + " speaks, 'Me iz gonna find da little ting dat pokes me an' hidez like orcy.'");
                                                    if (!Map.isCharNextToWall(npc))
                                                    {
                                                        npc.doAIMove();
                                                        if (!Map.isCharNextToWall(npc))
                                                        {
                                                            npc.doAIMove();
                                                        }
                                                    }
                                                    break;
                                            }
                                        }
                                        break;
                                    default:
                                        break;
                                }
                                #endregion
                            }
                        }
                        #endregion
                        break;
                    case ActionType.Move:
                        #region Move

                        if (npc.mostHated == null)
                        {
                            if (Rules.RollD(1, 100) < 10)
                            {
                                if(npc.idleSound != "")
                                    npc.EmitSound(npc.idleSound);
                                if (npc.MoveString != "")
                                    npc.SendShout(npc.MoveString);
                            }
                        }

                        if (!npc.IsMobile) { return; } // return if this creature is not mobile

                        #region Get distance to mostHated
                        if (npc.mostHated != null)
                        {
                            distance = Cell.GetCellDistance(npc.X, npc.Y, npc.mostHated.X, npc.mostHated.Y);
                        }
                        #endregion

                        #region FleeEffect
                        if (pri == Priority.FleeEffect)
                        {
                            List<Cell> cList = Map.GetAdjacentCells(npc.CurrentCell,npc);
                            if (cList != null)
                            {
                                int rand = Rules.dice.Next(cList.Count);
                                Cell nCell = (Cell)cList[rand];
                                if (!npc.IsPC)
                                {
                                    if (npc.Group != null)
                                    {
                                        npc.Group.Remove((NPC)npc);
                                    }
                                }
                                npc.AIGotoXYZ(nCell.X, nCell.Y, nCell.Z);
                            }
                            return;
                        }
                        #endregion

                        #region Priority.Flee
                        else if (pri == Priority.Flee)
                        {
                            if (npc.mostHated != null)
                            {
                                if (!npc.IsPC)
                                {
                                    if (npc.Group != null)
                                    {
                                        npc.Group.Remove((NPC)npc);
                                    }
                                }
                                ArtificialIntel.BackAwayFromCell(npc, npc.mostHated.CurrentCell);
                            }
                            else
                            {
                                //Utils.Log("Null mosthated on flee attempt", Utils.LogType.Unknown);
                                ArtificialIntel.BackAwayFromCell(npc, npc.CurrentCell);
                            }
                            return;
                        }
                        #endregion

                        #region Priority.RangeMove
                        else if (pri == Priority.RangeMove)
                        {
                            if (npc.mostHated != null)
                            {
                                switch (distance)
                                {
                                    case 1:
                                        if (!ArtificialIntel.BackAwayFromCell(npc, npc.mostHated.CurrentCell))
                                        {
                                            if (npc.RightHand != null && npc.RightHand.baseType == Globals.eItemBaseType.Bow)
                                            {
                                                Command.ParseCommand(npc, "nock", null);
                                                break;
                                            }
                                            else // no other actions right now require a back away
                                            {
                                                npc.doAIMove();
                                                return;
                                            }
                                        }
                                        break;
                                    default:
                                        foreach (Cell cell in npc.localCells)
                                        {
                                            if (Cell.GetCellDistance(npc.mostHated.X, npc.mostHated.Y, cell.X, cell.Y) >= 2)
                                            {
                                                npc.AIGotoXYZ(cell.X, cell.Y, cell.Z);
                                                return;
                                            }
                                        }
                                        break;
                                }
                            }
                            npc.doAIMove();
                            return;
                        }
                        #endregion

                        #region Priority.Advance
                        else if (pri == Priority.Advance)
                        {
                            if (npc.mostHated != null)
                            {
                                npc.AIGotoXYZ(npc.mostHated.X, npc.mostHated.Y, npc.mostFeared.Z);
                                if (npc.Speed <= 3)
                                {
                                    if (Cell.GetCellDistance(npc.X, npc.Y, npc.mostHated.X, npc.mostHated.Y) == 1)
                                    {
                                        if (npc.RightHand != null && npc.RightHand.baseType == Globals.eItemBaseType.Halberd)
                                        {
                                            ArtificialIntel.ExecuteAction(npc, ArtificialIntel.ActionType.Combat, ArtificialIntel.Priority.Attack);
                                        }
                                    }
                                }
                                else
                                {
                                    switch (npc.Speed)
                                    {
                                        case 4:
                                            if (Rules.RollD(1, 100) <= 25) // 25 percent chance to move and attack
                                            {
                                                ArtificialIntel.ExecuteAction(npc, ArtificialIntel.ActionType.Combat, ArtificialIntel.Priority.Attack);
                                            }
                                            break;
                                        case 5:
                                            if (Rules.RollD(1, 100) <= 50) // 50 percent chance to move and attack
                                            {
                                                ArtificialIntel.ExecuteAction(npc, ArtificialIntel.ActionType.Combat, ArtificialIntel.Priority.Attack);
                                            }
                                            break;
                                        case 6:
                                            if (Rules.RollD(1, 100) <= 75)
                                            {
                                                ArtificialIntel.ExecuteAction(npc, ArtificialIntel.ActionType.Combat, ArtificialIntel.Priority.Attack);
                                            }
                                            break;
                                        default:
                                            ArtificialIntel.ExecuteAction(npc, ArtificialIntel.ActionType.Combat, ArtificialIntel.Priority.Attack);
                                            break;
                                    }
                                }
                            }
                            else if (npc.previousMostHated != null)
                            {
                                npc.AIGotoXYZ(npc.previousMostHated.X, npc.previousMostHated.Y, npc.previousMostHated.Z);
                            }
                            return;
                        }
                        #endregion

                        #region Priority.InvestigateMagic
                        else if (pri == Priority.InvestigateMagic)
                        {
                            string [] xyz = npc.gotoWarmedMagic.Split("|".ToCharArray());
                            int rx = Convert.ToInt32(xyz[0]);
                            int ry = Convert.ToInt32(xyz[1]);
                            int rz = Convert.ToInt32(xyz[2]);
                            npc.AIGotoXYZ(rx, ry, rz);
                        }
                        #endregion

                        #region Priority.Investigate
                        else if (pri == Priority.Investigate)
                        {
                            npc.AIGotoXYZ(npc.previousMostHated.X, npc.previousMostHated.Y, npc.previousMostHated.Z);
                            npc.previousMostHated = null;
                            return;
                        } 
                        #endregion

                        if (npc.FollowName != "") // we have another character we're following
                        {
                            Character target = Map.FindTargetInView(npc, npc.FollowName, false, false);

                            if (target != null && npc.CurrentCell != target.CurrentCell)
                            {
                                if (!Rules.DetectHidden(target, npc) && !Rules.DetectInvisible(target, npc))
                                {
                                    npc.BreakFollowMode();
                                    return;
                                }

                                npc.MoveList.Clear();

                                npc.AIGotoXYZ(target.X, target.Y, target.Z);

                                #region Moving after most hated
                                if (npc.mostHated != null)
                                {
                                    if (npc.Speed <= 3)
                                    {
                                        if (Cell.GetCellDistance(npc.X, npc.Y, npc.mostHated.X, npc.mostHated.Y) == 1)
                                        {
                                            if (npc.RightHand != null && npc.RightHand.baseType == Globals.eItemBaseType.Halberd)
                                            {
                                                ArtificialIntel.ExecuteAction(npc, ArtificialIntel.ActionType.Combat, ArtificialIntel.Priority.Attack);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        switch (npc.Speed)
                                        {
                                            case 4:
                                                if (Rules.RollD(1, 100) <= 25) // 25 percent chance to move and attack
                                                {
                                                    ArtificialIntel.ExecuteAction(npc, ArtificialIntel.ActionType.Combat, ArtificialIntel.Priority.Attack);
                                                }
                                                break;
                                            case 5:
                                                if (Rules.RollD(1, 100) <= 50) // 50 percent chance to move and attack
                                                {
                                                    ArtificialIntel.ExecuteAction(npc, ArtificialIntel.ActionType.Combat, ArtificialIntel.Priority.Attack);
                                                }
                                                break;
                                            case 6:
                                                if (Rules.RollD(1, 100) <= 75)
                                                {
                                                    ArtificialIntel.ExecuteAction(npc, ArtificialIntel.ActionType.Combat, ArtificialIntel.Priority.Attack);
                                                }
                                                break;
                                            default:
                                                ArtificialIntel.ExecuteAction(npc, ArtificialIntel.ActionType.Combat, ArtificialIntel.Priority.Attack);
                                                break;
                                        }
                                    }
                                } 
                                #endregion
                                return;
                            }
                            else if (target == null)
                            {
                                npc.FollowName = "";
                            }
                        }

                        if (npc.totalFL == 0 && npc.totalHate == 0) // low priority move
                        {
                            #region Creature with a patrol route
                            if (npc.HasPatrol)
                            {
                                if (npc.MoveList.Count > 0)
                                {
                                    npc.patrolWaitRoundsRemaining = 0;

                                    npc.DoNextListMove();
                                }
                                else if (npc.patrolWaitRoundsRemaining > 0)
                                {
                                    npc.patrolWaitRoundsRemaining--;
                                }
                                else
                                {
                                    npc.patrolWaitRoundsRemaining = 0;

                                    string[] xyz = npc.patrolKeys[npc.patrolCount].Split("|".ToCharArray());

                                    if (xyz[0].ToLower() == "w")
                                    {
                                        Utils.Log(npc.GetLogString() + " is now waiting on patrol. " + npc.patrolWaitRoundsRemaining.ToString() + " rounds remaining.", Utils.LogType.SystemWarning);
                                        npc.patrolWaitRoundsRemaining = Convert.ToInt32(xyz[1]);
                                    }
                                    else
                                    {

                                        int rx = Convert.ToInt32(xyz[0]);
                                        int ry = Convert.ToInt32(xyz[1]);
                                        int rz = Convert.ToInt32(xyz[2]);

                                        if (rz != npc.Z) // switching z coordinates
                                        {
                                            npc.AIGotoNewZ(rx, ry, rz);
                                        }
                                        else
                                        {
                                            npc.AIGotoXYZ(rx, ry, rz);
                                        }

                                        npc.patrolCount++;

                                        if (npc.patrolCount > npc.patrolKeys.Count - 1)
                                        {
                                            npc.patrolCount = 0;
                                        }
                                    }
                                }
                            }
                            #endregion
                            else
                            {
                                npc.doAIMove();
                            }
                        }
                        else if (npc.totalHate == 0 && npc.totalFL > 0) // move toward friend FLCenter - normal
                        {
                            #region Creature with a patrol route
                            if (npc.HasPatrol) // if the creature has a patrol route
                            {
                                if (npc.MoveList.Count > 0)
                                {
                                    npc.patrolWaitRoundsRemaining = 0;
                                    npc.DoNextListMove();
                                }
                                else if (npc.patrolWaitRoundsRemaining > 0)
                                {
                                    npc.patrolWaitRoundsRemaining--;
                                }
                                else
                                {
                                    npc.patrolWaitRoundsRemaining = 0;

                                    string[] xyz = npc.patrolKeys[npc.patrolCount].Split("|".ToCharArray());

                                    // npc is being instructed to wait xyz[1] rounds
                                    if (xyz[0].ToLower() == "w")
                                    {
                                        Utils.Log(npc.GetLogString() + " is now waiting on patrol. " + npc.patrolWaitRoundsRemaining.ToString() + " rounds remaining.", Utils.LogType.SystemWarning);
                                        npc.patrolWaitRoundsRemaining = Convert.ToInt32(xyz[1]);
                                    }
                                    else
                                    {
                                        int rx = Convert.ToInt32(xyz[0]);
                                        int ry = Convert.ToInt32(xyz[1]);
                                        int rz = Convert.ToInt32(xyz[2]);

                                        if (rz != npc.Z)
                                        {
                                            npc.AIGotoNewZ(rx, ry, rz);
                                        }
                                        else
                                        {
                                            npc.AIGotoXYZ(rx, ry, rz);
                                        }

                                        if (npc.X == rx && npc.Y == ry && npc.Z == rz)
                                        {
                                            npc.patrolCount++;
                                        }

                                        if (npc.patrolCount > npc.patrolKeys.Count - 1)
                                        {
                                            npc.patrolCount = 0;
                                        }
                                    }
                                }
                            }
                            #endregion
                            else
                            {
                                npc.doAIMove();
                            }
                        }
                        else if (npc.totalFL < 0 && Math.Abs(npc.totalFL) > npc.totalHate) // run away - too dangerous - highest
                        {
                            ArtificialIntel.BackAwayFromCell(npc, npc.mostFeared.CurrentCell);
                        }
                        else // move toward center of hate - high
                        {
                            npc.AIGotoXYZ(npc.hateCenterX, npc.hateCenterY, npc.Z);
                        }
                        #endregion
                        break;
                    case ActionType.Combat:
                        #region Combat
                        try
                        {
                            // mostHated may have been killed by a group member
                            if (npc.mostHated == null || npc.mostHated.IsDead) break;

                            if (Map.FindTargetInView(npc as Character, npc.mostHated.Name, false, false) == null) break;

                            #region Get distance to mostHated
                            distance = Cell.GetCellDistance(npc.X, npc.Y, npc.mostHated.X, npc.mostHated.Y);
                            #endregion

                            npc.mostHated.numAttackers++; // there's an attack coming so increase the target's number of attackers

                            if (npc.RightHand != null && npc.RightHand.baseType == Globals.eItemBaseType.Halberd) // wielding a halberd type weapon
                            {
                                Command.ParseCommand(npc, "poke", npc.mostHated.Name);
                            }
                            else if (npc.RightHand != null && npc.RightHand.baseType == Globals.eItemBaseType.Bow) // wielding a bow
                            {
                                if (npc.RightHand.nocked)
                                {
                                    Command.ParseCommand(npc, "shoot", npc.mostHated.Name);
                                }
                                else
                                {
                                    Command.ParseCommand(npc, "nock", null);
                                }
                            }
                            else if (npc.BaseProfession == ClassType.Martial_Artist)
                            {
                                switch (distance)
                                {
                                    case 0:
                                        if (npc.Stamina >= (int)(npc.StaminaMax * .25) && Rules.RollD(1, 4) == 3) // 1 in 4 chance of kicking 
                                        // *note change this in the future to consider if the creature is wearing damage boots or gauntlets
                                        {
                                            Command.ParseCommand(npc, "kick", npc.mostHated.Name);
                                        }
                                        else
                                        {
                                            Command.ParseCommand(npc, "kill", npc.mostHated.Name);
                                        }
                                        break;
                                    case 1:
                                    case 2:
                                    case 3:
                                        if (npc.HasRandomName) // random name means this is a humanoid NPC so they're gonna yell a ki yah
                                        {
                                            Command.ParseCommand(npc, "jumpkick", npc.mostHated.Name + "/Ki Yah!/!");
                                        }
                                        else
                                        {
                                            Command.ParseCommand(npc, "jumpkick", npc.mostHated.Name);
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                if (npc.RightHand != null && npc.LeftHand == null)
                                {
                                    if (npc.RightHand.skillType != Globals.eSkillType.Two_Handed)
                                    {
                                        Item shield = npc.RemoveFromBelt("shield");
                                        if (shield != null)
                                        {
                                            npc.EquipLeftHand(shield);
                                        }
                                    }
                                }
                                Command.ParseCommand(npc, "kill", npc.mostHated.Name);
                            }
                        #endregion
                        }
                        catch (Exception e)
                        {
                            Utils.Log("Failure at AI.ExecuteAction(" + npc.GetLogString() + ", " + actionType.ToString() + ", " + pri.ToString() + ") Combat Switch", Utils.LogType.SystemFailure);
                            Utils.LogException(e);
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                Utils.Log("Failure at AI.execute_action " + npc.GetLogString() + " Action: " + actionType.ToString() + " Priority: " + pri.ToString() + " (" + npc.LastCommand + ")", Utils.LogType.SystemFailure);
                Utils.LogException(e);
            }
        }
        public static void NPCCastNormalSpell(NPC npc, string args)
        {
            if (npc.preppedSpell == null)
            {
                return;
            }
            npc.preppedSpell.CastSpell(npc, args);
            //deduct MPs if NPC castMode is Limited.
            if (npc.castMode == CastMode.Limited)
            {
                npc.Mana -= npc.preppedSpell.ManaCost;
                npc.updateMP = true;
            }
            npc.preppedSpell = null;
            
        }
        #region CommandAI
        public static void Follow(NPC npc, Character commander)
        {
            if (npc.canCommand)
            {
                if (npc.questList.Count > 0)
                {
                    foreach (Quest q in npc.questList)
                    {
                        if (q.ResponseStrings.ContainsKey("follow") && !q.PlayerMeetsRequirements((PC)commander, true))
                        {
                            return;
                        }
                    }
                }

                if (npc.FollowName == commander.Name)
                {
                    commander.WriteToDisplay(npc.Name + " is already following you.");
                }
                else
                {
                    if (npc.FollowName != "" && PC.GetOnline(npc.FollowName) != null)
                    {
                        if (Map.FindTargetInView(npc, npc.FollowName, false, false) != null)
                        {
                            commander.WriteToDisplay(npc.Name + " is already following someone.");
                            return;
                        }
                    }

                    npc.FollowName = commander.Name;

                    // set pet owner
                    npc.PetOwner = commander;
                    // add this npc to ch pets
                    commander.Pets.Add(npc);

                    commander.WriteToDisplay(npc.Name + " begins to follow you.");

                    if (!npc.IsMobile)
                    {
                        npc.wasImmobile = true;
                        npc.IsMobile = true;
                    }

                    if (npc.IsImmortal)
                    {
                        npc.IsImmortal = false;
                        npc.WasImmortal = true;
                    }

                    if (npc.effectList.ContainsKey(Effect.EffectType.Hello_Immobility))
                    {
                        npc.effectList[Effect.EffectType.Hello_Immobility].StopCharacterEffect();
                    }
                }
            }
            return;
        }

        public static void Go_Direction(NPC npc, Character commander, string[] args)
        {
            if (npc.canCommand && npc.PetOwner == commander)
            {
                if (npc.effectList.ContainsKey(Effect.EffectType.Hello_Immobility)) // remove perma-root
                {
                    npc.effectList[Effect.EffectType.Hello_Immobility].StopCharacterEffect();
                }

                foreach (string dir in args)
                {
                    if (dir == "n" || dir == "s" || dir == "e" || dir == "w" || dir == "nw" || dir == "sw" || dir == "ne" || dir == "se" || dir == "up" ||
                        dir == "u" || dir == "d" || dir == "down")
                    {
                        Command.ParseCommand(npc, dir, "");
                    }
                }

                Effect.CreateCharacterEffect(Effect.EffectType.Hello_Immobility, 0, npc, 1, commander);
            }
        }

        public static void Go_Climb(NPC npc, Character commander, string args)
        {
            if (npc.canCommand && npc.PetOwner == commander)
            {
                if (npc.effectList.ContainsKey(Effect.EffectType.Hello_Immobility)) // remove perma-root
                {
                    npc.effectList[Effect.EffectType.Hello_Immobility].StopCharacterEffect();
                }

                Command.ParseCommand(npc, "climb", args);

                Effect.CreateCharacterEffect(Effect.EffectType.Hello_Immobility, 0, npc, 1, commander);
            }
        } 
        #endregion

		public static bool BackAwayFromCell(NPC npc, Cell cellToBackAwayFrom)
		{
            ArrayList directions = new ArrayList();

            Map.Direction targetDirection = Map.GetDirection(npc.CurrentCell, cellToBackAwayFrom);

            switch (targetDirection)
            {
                case Map.Direction.East:
                    directions.Add(Map.Direction.West.ToString());
                    directions.Add(Map.Direction.Northwest.ToString());
                    directions.Add(Map.Direction.Southwest.ToString());
                    break;
                case Map.Direction.North:
                    directions.Add(Map.Direction.South.ToString());
                    directions.Add(Map.Direction.Southwest.ToString());
                    directions.Add(Map.Direction.Southeast.ToString());
                    break;
                case Map.Direction.Northeast:
                    directions.Add(Map.Direction.South.ToString());
                    directions.Add(Map.Direction.Southwest.ToString());
                    directions.Add(Map.Direction.West.ToString());
                    break;
                case Map.Direction.Northwest:
                    directions.Add(Map.Direction.South.ToString());
                    directions.Add(Map.Direction.Southeast.ToString());
                    directions.Add(Map.Direction.East.ToString());
                    break;
                case Map.Direction.South:
                    directions.Add(Map.Direction.North.ToString());
                    directions.Add(Map.Direction.Northwest.ToString());
                    directions.Add(Map.Direction.Northeast.ToString());
                    break;
                case Map.Direction.Southeast:
                    directions.Add(Map.Direction.North.ToString());
                    directions.Add(Map.Direction.Northwest.ToString());
                    directions.Add(Map.Direction.West.ToString());
                    break;
                case Map.Direction.Southwest:
                    directions.Add(Map.Direction.North.ToString());
                    directions.Add(Map.Direction.Northeast.ToString());
                    directions.Add(Map.Direction.East.ToString());
                    break;
                case Map.Direction.None:
                    List<Cell> cList = Map.GetAdjacentCells(npc.CurrentCell,npc);
                    if (cList != null)
                    {
                        int rand = Rules.dice.Next(cList.Count);
                        Cell nCell = (Cell)cList[rand];
                        npc.AIGotoXYZ(nCell.X, nCell.Y, nCell.Z);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
            }

            for (int a = 0; a < directions.Count; a++)
            {
                Cell cell = Map.GetCellRelevantToCell(npc.CurrentCell, directions[a].ToString().ToLower(), true);
                if(npc.GetCellCost(cell) <= 2)
                {
                    npc.AIGotoXYZ(cell.X, cell.Y, cell.Z);
                    return true;
                }
            }
            return false;
		}

        
	}
}
