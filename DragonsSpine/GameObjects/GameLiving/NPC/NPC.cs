namespace DragonsSpine
{
	using System;
	using System.Collections;
    using System.Collections.Generic;
	using System.Reflection;
	using System.IO;
	using System.Net.Sockets;
    using System.Timers;

    public class NPC : Character
    {

        Point[] directions = {
			new Point(-1,-1), 
			new Point(-1, 0),
			new Point(-1, 1),
			new Point( 0,-1),
			new Point( 0, 1),
			new Point( 1,-1),
			new Point( 1, 0),
			new Point( 1, 1)
		};

        // TODO: change this from NPC to just hold the DataRow for the npc - change creation to references - no need to load 
        // the NPC then store it in the dictionary. Waste of space.
        static Dictionary<int, System.Data.DataRow> npcDictionary = new Dictionary<int, System.Data.DataRow>();
        public static Dictionary<int, System.Data.DataRow> NPCDictionary
        {
            get { return npcDictionary; }
        }
        public enum NPCType { Creature, Merchant }
        public enum AIType { Unknown, Enforcer, Priest, Sheriff, Summoned, Quest }
        public enum CastMode { Never, Limited, Unlimited, NoPrep }
        public enum DemonName { Asmodeus, Pazuzu, Glamdrang, Damballa, Thamuz, Perdurabo, Samael }
       
		public int[] hate;
		public int[] fear;

		public int hateCenterX = 0;
		public int hateCenterY = 0;
		public int fearCenterY = 0;
		public int fearCenterX = 0;
		public int totalHate = 0;
		public int totalFL = 0;

		public Character mostHated;
		public Character mostFeared;
		public Character mostLoved;

        public List<Character> targetList = new List<Character>(); // contains all visible character objects (all visible targets, both friend and enemy)
        public List<Character> enemyList = new List<Character>(); // contains only visible enemy character objects
        public List<Character> friendList = new List<Character>(); // contains only visible friend character objects

        public Character previousMostHated;

        public string gotoWarmedMagic = "";

        public int buffTargetID = 0; // temporary storage of a Character object playerID or worldNpcID for a beneficial spell that was prepped
        public string buffSpellCommand = "";
        // Timer npcTimer;
		public Item pItem;

		public List<Cell> localCells = new List<Cell>();        

        public IntStringMap abjurationSpells = new IntStringMap(); // abjuration spells
        public IntStringMap alterationSpells = new IntStringMap(); // beneficial alteration spells
        public IntStringMap alterationHarmfulSpells = new IntStringMap(); // harmful alteration spells
        public IntStringMap conjurationSpells = new IntStringMap(); // conjuration spells
        public IntStringMap divinationSpells = new IntStringMap(); // divination spells
        public IntStringMap evocationSpells = new IntStringMap(); // evocation spells
        public IntStringMap evocationAreaEffectSpells = new IntStringMap(); // area effect evocation spells

        public Dictionary<int, List<Item>> receivedItems = new Dictionary<int, List<Item>>(); // items received via the give command form a player

		public int NPCIdent; // database key of the NPC ID
		public NPCType npcType; // this is the type of NPC, monster, merchant, etc..
		public AIType aiType; // this is the base class used to create this NPC

        /*  CastMode
         *  Never - never casts a spell
         *  Limited - casts spells from this npc's spellList and uses mana
         *  Unlimited - casts any spell from the global spellList and does not use mana
         *  NoPrep - casts spells from this npc's spellList, uses mana, no spell prep needed
         */
		public CastMode castMode = CastMode.Never; // never casts, casts using mana pool, unlimited casting, no prep needed (uses mana pool)

        #region Private Data
        int gold = 0;
        short groupAmount = 0; // greather than 0 will spawn with a group
        string groupMembers = ""; // list of npc id's this npc will be the leader of
        double merchantMarkup = 1.5;
        bool mobile = false; // true if this npc can move, or false if it is perma-rooted (ie: statues)
        List<string> moveList = new List<string>(); // holds currently stacked set of move commands  
        string moveString = ""; // string shouted on a move
        public bool randomName = false; // spawn with random name
        bool spectral = false; // if true, worn and held items disappear upon death
        public int speed; // how many cells this npc can move in one round
        bool waterDweller = false; // must spawn in water, remains in water
        int zoneID = 0;
        int roundsRemaining = 0; // rounds remaining until this NPC despawns
        string weaponRequirement = ""; // weapon required to hit this NPC (in the item's special attribute)
        protected Brain m_Brain;
        #endregion

        public string WeaponRequirement
        {
            get { return this.weaponRequirement; }
            set { this.weaponRequirement = value; }
        }

        public int Gold
        {
            get { return this.gold; }
            set { this.gold = value; }
        }
        public short GroupAmount
        {
            get { return this.groupAmount; }
            set { this.groupAmount = value; }
        }
        public string GroupMembers
        {
            get { return this.groupMembers; }
            set { this.groupMembers = value; }
        }
        public bool HasRandomName
        {
            get { return this.randomName; }
            set { this.randomName = value; }
        }
        public bool IsGreedy
        {
            get
            {
                if (this.trainerType > Merchant.TrainerType.None)
                    return false;

                if (this.animal)
                    return false;

                if (this.lairCritter)
                    return false;

                if (this.Alignment == Globals.eAlignment.Chaotic || this.Alignment == Globals.eAlignment.Evil ||
                    this.Alignment == Globals.eAlignment.Neutral)
                    return true;

                return false;
            }
        }
        public Brain Brain
        {
            get { return m_Brain; }
            set { m_Brain = value; }
        }
        public bool IsTrulyImmobile
        {
            get
            {
                if (this.Name.ToLower().Contains("statue"))
                {
                    return true;
                }
                return false;
            }
        }
        public bool IsMobile
        {
            get { return this.mobile; }
            set { this.mobile = value; }
        }
        public bool IsSpectral
        {
            get { return this.spectral; }
            set { this.spectral = value; }
        }
        public bool IsWaterDweller
        {
            get { return this.waterDweller; }
            set { this.waterDweller = value; }
        }
        public double MerchantMarkup
        {
            get { return this.merchantMarkup; }
            set { this.merchantMarkup = value; }
        }
        public List<string> MoveList
        {
            get { return this.moveList; }
            set { this.moveList = value; }
        }
        public string MoveString
        {
            get { return this.moveString; }
            set { this.moveString = value; }
        }
        public int SpawnZoneID
        {
            get { return this.zoneID; }
            set { this.zoneID = value; }
        }
        public int Speed
        {
            get { return this.speed; }
            set { this.speed = value; }
        }
        public int RoundsRemaining
        {
            get { return this.roundsRemaining; }
            set { this.roundsRemaining = value; }
        }
        private static NPC m_Owner;
        public static NPC Owner
        {
            get { return m_Owner; }
            set { m_Owner = value; }
        }
        #region Lair Specific
        public bool lairCritter = false; // true if this creature will return home to it's lair
        public string lairCells = "";
        public List<Cell> lairCellsList = new List<Cell>();
        public int lairXCord = 0;
        public int lairYCord = 0;
        public int lairZCord = 0;
        
        #endregion

        public int spawnXCord = 0;
        public int spawnYCord = 0;
        public int spawnZCord = 0;
        public bool wasImmobile = false;
        //public object lockObject = new object();
        #region Attack Strings
        public string attackString1 = "";
        public string attackString2 = "";
        public string attackString3 = "";
        public string attackString4 = "";
        public string attackString5 = "";
        public string attackString6 = ""; 
        #endregion

        #region Block Strings
        public string blockString1 = "";
        public string blockString2 = "";
        public string blockString3 = ""; 
        #endregion

        #region Patrol Specific
        public string patrolRoute = "";	// x|y|z x|y|z etc...
        public bool HasPatrol
        {
            get
            {
                if (this.patrolRoute.Length > 0)
                {
                    return true;
                }
                return false;
            }
        }
        public int patrolCount = 0; // which key we're at on our patrolKeys
        public int patrolWaitRoundsRemaining = 0; // how many more rounds we will wait at current cell
        public List<string> patrolKeys; // initialized when npc's are loaded into the npc dictionary
        #endregion

        #region Items
        public string spawnArmor; // array containing itemIDs of npc.wearing items
		public string spawnLeftHand; // array of left hand itemsIDs
		public int spawnLeftHandOdds; // chance to spawn with one left hand array item
		public string spawnRightHand; // array of right hand itemIDs
        /// <summary>
        /// lootXXXXAmount
        ///    -1 = npc gets all lootXXXXArray items put into sack, otherwise npc gets that amount
        /// 
        /// lootXXXXArray
        ///    the array of items available as loot for this npc
        /// 
        /// lootXXXXOdds
        ///    -1 = npc automatically gets one piece of loot from lootXXXXArray, otherwise if Rules.dice.Next(100) is
        ///    lower than lootXXXXOdds npc gets one piece of loot from lootXXXXArray
        /// </summary>
        //lootVeryCommon eg: low value gems
        public int lootVeryCommonAmount = 0;
        public string lootVeryCommonArray = "";
        public int lootVeryCommonOdds = 100;
        //lootCommon
        public int lootCommonAmount = 0;
        public string lootCommonArray = "";
        public int lootCommonOdds = 100;
        //lootRare
        public int lootRareAmount = 0;
        public string lootRareArray = "";
        public int lootRareOdds = 100;
        //lootVeryRare
        public int lootVeryRareAmount = 0;
        public string lootVeryRareArray = "";
        public int lootVeryRareOdds = 100;
        //lootLair
        //only the most difficult lair NPCs should have an amount greater than 1 (possible duplicate items)
        public int lootLairAmount = 0;
        public string lootLairArray = "";
        public int lootLairOdds = 0;
        //lootAlways is an array of loot the npc will always have eg: quest items such as the Knight quest agate
        public string lootAlwaysArray = "";
        //lootBelt
        public int lootBeltAmount = 0;
        public string lootBeltArray = "";
        public int lootBeltOdds = 0; 
        #endregion

        #region Constructors (2)
        public NPC()
        {
            this.IsPC = false;
            m_Owner = this;
            // this.npcTimer = new Timer(DragonsSpineMain.MasterRoundInterval);
            // this.npcTimer.AutoReset = true;
            // this.npcTimer.Elapsed += new ElapsedEventHandler(NPCEvent);
            this.Brain = new Brain(this);
        }

        public NPC(System.Data.DataRow dr) : base()
        {            
            this.IsPC = false;
            this.NPCIdent = Convert.ToInt32(dr["catalogID"]);
            this.npcID = Convert.ToInt32(dr["npcID"]);
            this.Name = dr["name"].ToString();
            this.attackSound = dr["attackSound"].ToString();
            this.deathSound = dr["deathSound"].ToString();
            this.idleSound = dr["idleSound"].ToString();
            this.MoveString = dr["movementString"].ToString();
            this.shortDesc = dr["shortDesc"].ToString();
            this.longDesc = dr["longDesc"].ToString();
            this.visualKey = dr["visualKey"].ToString();
            this.baseArmorClass = Convert.ToDouble(dr["baseArmorClass"]);
            this.THAC0Adjustment = Convert.ToInt32(dr["thac0Adjustment"]);
            this.special = dr["special"].ToString();
            this.npcType = (NPC.NPCType)Enum.Parse(typeof(NPC.NPCType), dr["npcType"].ToString(), true);
            this.aiType = (NPC.AIType)Enum.Parse(typeof(NPC.AIType), dr["aiType"].ToString(), true);
            this.species = (Globals.eSpecies)Enum.Parse(typeof(Globals.eSpecies), dr["species"].ToString(), true);
            this.Gold = Convert.ToInt32(dr["gold"]);
            this.BaseProfession = (Character.ClassType)Enum.Parse(typeof(Character.ClassType), dr["classType"].ToString(), true);
            this.classFullName = dr["classFullName"].ToString();
            if (dr["classFullName"].ToString() == "")
            {
                this.classFullName = this.BaseProfession.ToString().Replace("_", " ");
            }
            this.Experience = Convert.ToInt32(dr["exp"]);
            this.HitsMax = Convert.ToInt32(dr["hitsMax"]);
            this.Hits = this.HitsMax;
            this.Alignment = (Globals.eAlignment)Enum.Parse(typeof(Globals.eAlignment), dr["alignment"].ToString(), true);
            this.StaminaMax = Convert.ToInt32(dr["stamina"]);
            this.ManaMax = Convert.ToInt32(dr["manaMax"]);
            this.Speed = Convert.ToInt32(dr["speed"]);
            this.Strength = Convert.ToInt32(dr["strength"]);
            this.Dexterity = Convert.ToInt32(dr["dexterity"]);
            this.Intelligence = Convert.ToInt32(dr["intelligence"]);
            this.Wisdom = Convert.ToInt32(dr["wisdom"]);
            this.Constitution = Convert.ToInt32(dr["constitution"]);
            this.Charisma = Convert.ToInt32(dr["charisma"]);
            this.lootVeryCommonAmount = Convert.ToInt32(dr["lootVeryCommonAmount"]);
            this.lootVeryCommonArray = dr["lootVeryCommonArray"].ToString();
            this.lootVeryCommonOdds = Convert.ToInt32(dr["lootVeryCommonOdds"]);
            this.lootCommonAmount = Convert.ToInt32(dr["lootCommonAmount"]);
            this.lootCommonArray = dr["lootCommonArray"].ToString();
            this.lootCommonOdds = Convert.ToInt32(dr["lootCommonOdds"]);
            this.lootRareAmount = Convert.ToInt32(dr["lootRareAmount"]);
            this.lootRareArray = dr["lootRareArray"].ToString();
            this.lootRareOdds = Convert.ToInt32(dr["lootRareOdds"]);
            this.lootVeryRareAmount = Convert.ToInt32(dr["lootVeryRareAmount"]);
            this.lootVeryRareArray = dr["lootVeryRareArray"].ToString();
            this.lootVeryRareOdds = Convert.ToInt32(dr["lootVeryRareOdds"]);
            this.lootLairAmount = Convert.ToInt32(dr["lootLairAmount"]);
            this.lootLairArray = dr["lootLairArray"].ToString();
            this.lootLairOdds = Convert.ToInt32(dr["lootLairOdds"]);
            this.lootAlwaysArray = dr["lootAlwaysArray"].ToString();
            this.lootBeltAmount = Convert.ToInt32(dr["lootBeltAmount"]);
            this.lootBeltArray = dr["lootBeltArray"].ToString();
            this.lootBeltOdds = Convert.ToInt32(dr["lootBeltOdds"]);
            this.spawnArmor = dr["spawnArmorArray"].ToString();
            this.spawnLeftHand = dr["spawnLeftHandArray"].ToString();
            this.spawnLeftHandOdds = Convert.ToInt32(dr["spawnLeftHandOdds"]);
            this.spawnRightHand = dr["spawnRightHandArray"].ToString();
            this.mace = Convert.ToInt64(dr["mace"]);
            this.bow = Convert.ToInt64(dr["bow"]);
            this.dagger = Convert.ToInt64(dr["dagger"]);
            this.flail = Convert.ToInt64(dr["flail"]);
            this.rapier = Convert.ToInt64(dr["rapier"]);
            this.twoHanded = Convert.ToInt64(dr["twoHanded"]);
            this.staff = Convert.ToInt64(dr["staff"]);
            this.shuriken = Convert.ToInt64(dr["shuriken"]);
            this.sword = Convert.ToInt64(dr["sword"]);
            this.threestaff = Convert.ToInt64(dr["threestaff"]);
            this.halberd = Convert.ToInt64(dr["halberd"]);
            this.unarmed = Convert.ToInt32(dr["unarmed"]);
            this.thievery = Convert.ToInt64(dr["thievery"]);
            this.magic = Convert.ToInt64(dr["magic"]);
            this.bash = Convert.ToInt64(dr["bash"]);
            this.Level = Convert.ToInt16(dr["level"]);
            this.animal = Convert.ToBoolean(dr["animal"]);
            if (dr["tanningResult"].ToString().Length > 0)
            {
                this.tanningResult = Utils.ConvertStringToIntArray(dr["tanningResult"].ToString());
            }
            this.IsUndead = Convert.ToBoolean(dr["undead"]);
            this.IsSpectral = Convert.ToBoolean(dr["spectral"]);
            if (Convert.ToBoolean(dr["hidden"]))
            {
                this.AddPermanentEffect(Effect.EffectType.Hide_In_Shadows);
            }
            this.poisonous = Convert.ToInt16(dr["poisonous"]);
            this.waterDweller = Convert.ToBoolean(dr["waterDweller"]);
            if (Convert.ToBoolean(dr["fly"]))
            {
                this.AddPermanentEffect(Effect.EffectType.Flight);
            }
            if (Convert.ToBoolean(dr["breatheWater"]) || this.IsWaterDweller)
            {
                this.AddPermanentEffect(Effect.EffectType.Breathe_Water);
            }
            if (Convert.ToBoolean(dr["nightVision"]))
            {
                this.AddPermanentEffect(Effect.EffectType.Night_Vision);
            }
            this.lairCritter = Convert.ToBoolean(dr["lair"]);
            this.lairCells = dr["lairCells"].ToString();
            this.IsMobile = Convert.ToBoolean(dr["mobile"]);
            this.HasRandomName = Convert.ToBoolean(dr["randomName"]);
            this.castMode = (NPC.CastMode)Convert.ToInt32(dr["castMode"]);
            this.canCommand = Convert.ToBoolean(dr["command"]);
            this.attackString1 = dr["attackString1"].ToString();
            this.attackString2 = dr["attackString2"].ToString();
            this.attackString3 = dr["attackString3"].ToString();
            this.attackString4 = dr["attackString4"].ToString();
            this.attackString5 = dr["attackString5"].ToString();
            this.attackString6 = dr["attackString6"].ToString();
            this.blockString1 = dr["blockString1"].ToString();
            this.blockString2 = dr["blockString2"].ToString();
            this.blockString3 = dr["blockString3"].ToString();
            this.merchantType = (Merchant.MerchantType)Enum.Parse(typeof(Merchant.MerchantType), dr["merchantType"].ToString(), true);
            this.MerchantMarkup = Convert.ToDouble(dr["merchantMarkup"]);
            this.trainerType = (Merchant.TrainerType)Enum.Parse(typeof(Merchant.TrainerType), dr["trainerType"].ToString(), true);
            this.interactiveType = (Merchant.InteractiveType)Enum.Parse(typeof(Merchant.InteractiveType), dr["interactiveType"].ToString(), true);
            this.gender = (Globals.eGender)Convert.ToInt16(dr["gender"]);
            this.race = dr["race"].ToString();
            this.Age = Convert.ToInt32(dr["age"]);
            this.patrolRoute = dr["patrolRoute"].ToString();
            this.immuneBlind = Convert.ToBoolean(dr["immuneBlind"]);
            this.immuneCold = Convert.ToBoolean(dr["immuneCold"]);
            this.immuneCurse = Convert.ToBoolean(dr["immuneCurse"]);
            this.immuneDeath = Convert.ToBoolean(dr["immuneDeath"]);
            this.immuneFear = Convert.ToBoolean(dr["immuneFear"]);
            this.immuneFire = Convert.ToBoolean(dr["immuneFire"]);
            this.immuneLightning = Convert.ToBoolean(dr["immuneLightning"]);
            this.immunePoison = Convert.ToBoolean(dr["immunePoison"]);
            this.immuneStun = Convert.ToBoolean(dr["immuneStun"]);
            if (dr["spells"].ToString() != "") // can dictate in the database what spells to give an npc
            {
                string[] knownSpells = dr["spells"].ToString().Split(" ".ToCharArray());
                for (int a = 0; a < knownSpells.Length; a++)
                {
                    int id = Convert.ToInt32(knownSpells[a]);
                    string chant = Spell.GenerateMagicWords();
                    while (this.spellList.Exists(chant)) // if this chant exists in the spell list generate another
                    {
                        chant = Spell.GenerateMagicWords();
                    }
                    this.spellList.Add(id, chant);
                }
            }
            else if (this.IsSpellUser)
            {
                NPC.CreateGenericSpellList(this);
            }

            if (this.spellList.Count > 0) // break down the spellList IntStringMap into smaller IntStringMap objects for ease during AI processing
            {
                NPC.FillSpellLists(this);
            }

            if (dr["quests"].ToString().Length > 0)
            {
                NPC.FillQuestList(this, dr["quests"].ToString());
            }

            this.groupAmount = Convert.ToInt16(dr["groupAmount"]);
            this.groupMembers = dr["groupMembers"].ToString();
            this.weaponRequirement = dr["weaponRequirement"].ToString();

            // questID~flag^questID~flag, where if questID <= 0 then the quest started is not required to get the flag
            if (dr["questFlags"].ToString().Length > 1)
            {
                string[] s = dr["questFlags"].ToString().Split(Protocol.ISPLIT.ToCharArray());

                for (int a = 0; a < s.Length; a++)
                {
                    this.questFlags.Add(s[a]);
                }
            }

        }
        #endregion
        void AddPermanentEffect(Effect.EffectType effectType)
        {
            Effect effect = new Effect(effectType, 0, -1, this, this);
        }

        static void CreateGenericSpellList(NPC npc)
        {
            string chant = "";
            foreach (Spell spell in Spell.spellDictionary.Values)
            {
                if (spell.IsClassSpell(npc.BaseProfession))
                {
                    if (Skills.GetSkillLevel(npc.magic) >= spell.RequiredLevel || npc.IsHybrid)
                    {
                        chant = Spell.GenerateMagicWords();
                        while (npc.spellList.Exists(chant)) // if this chant exists in the spell list generate another
                        {
                            chant = Spell.GenerateMagicWords();
                        }
                        npc.spellList.Add(spell.SpellID, chant);
                    }
                }
            }
        }

        static void FillSpellLists(NPC npc)
        {
            string spellChant;

            for (int a = 0; a < npc.spellList.Count; a++)
            {
                Spell spell = Spell.GetSpell((int)npc.spellList.ints[a]);
                spellChant = (string)npc.spellList.strings[a];

                switch (spell.SpellType)
                {
                    case Globals.eSpellType.Abjuration:
                        npc.abjurationSpells.Add(spell.SpellID, spellChant);
                        break;
                    case Globals.eSpellType.Alteration: // two types of alteration spell, beneficial and harmful
                        if (spell.IsBeneficial)
                        {
                            npc.alterationSpells.Add(spell.SpellID, spellChant);
                        }
                        else
                        {
                            npc.alterationHarmfulSpells.Add(spell.SpellID, spellChant);
                        }
                        break;
                    case Globals.eSpellType.Conjuration:
                        npc.conjurationSpells.Add(spell.SpellID, spellChant);
                        break;
                    case Globals.eSpellType.Divination:
                        npc.divinationSpells.Add(spell.SpellID, spellChant);
                        break;
                    case Globals.eSpellType.Evocation: // we separate these primarily for use by wizards
                        if (spell.TargetType == Globals.eTargetType.Area_Effect)
                        {
                            npc.evocationAreaEffectSpells.Add(spell.SpellID, spellChant);
                        }
                        else
                        {
                            npc.evocationSpells.Add(spell.SpellID, spellChant);
                        }
                        break;
                }
            }
        }

        static void FillQuestList(NPC npc, string questInfo)
        {
            string[] quests = questInfo.Split(" ".ToCharArray());
            for (int a = 0; a < quests.Length; a++)
            {
                npc.questList.Add(Quest.GetQuest(Convert.ToInt32(quests[a])));
            }
        }

        public override void AddToWorld()
        {
			base.AddToWorld();
		}

        public static void Add(int npcid, System.Data.DataRow dr)
        {
            NPC.npcDictionary.Add(npcid, dr);
        }

        public static void ClearLiveNpcData() {
            DAL.DBNPC.ClearLiveNpcData();
        }

		public static void LoadNPCDictionary()
		{
		    DAL.DBNPC.LoadNPCDictionary();
		}

        public static void DoSpawn() // spawn some critters
        {
            int sxcord = 0;
            int sycord = 0;
            int szcord = 0;

            int attempts = 0;
            NPC npc = null;
            int number = 0;
            // Loop through the spawnzonelinks and check the status of each zone
            foreach (Facet facet in World.Facets)
            {
                foreach (SpawnZone szl in facet.Spawns.Values)
                {
                    try
                    {
                        if (szl.IsEnabled)
                        {
                            attempts = 0;

                            // check if numInGame is less than MaxAllowedInWorld
                            if (szl.NumberInZone < szl.MaxAllowedInZone && szl.Timer > szl.SpawnTimer)
                            {
                                while (szl.NumberInZone < szl.MaxAllowedInZone)
                                {
                                    // spawn attempt count is too high, log error and move on
                                    if (attempts >= 40)
                                    {
                                        Utils.Log("SpawnZone " + szl.ZoneID + " did not find a suitable spawning point after " + attempts + " attempts.", Utils.LogType.SystemWarning);
                                        szl.IsEnabled = false;
                                        attempts = 0;
                                        break;
                                    }
                                    else
                                    {
                                        attempts++;
                                    }

                                    // random spawn z range has precedence
                                    if (szl.SpawnZRange.Count > 0)
                                    {
                                        szl.Z = szl.SpawnZRange[Rules.dice.Next(0, szl.SpawnZRange.Count)];
                                        szl.EstablishSpawnRadius(szl.Z);
                                        string[] xyzRand = szl.SpawnCells[Rules.dice.Next(0, szl.SpawnCells.Count)].Split(",".ToCharArray());
                                        sxcord = Convert.ToInt32(xyzRand[0]);
                                        sycord = Convert.ToInt32(xyzRand[1]);
                                        szcord = szl.Z;
                                    }

                                    else if (szl.Radius <= 0)
                                    {
                                        sxcord = szl.X;
                                        sycord = szl.Y;
                                        szcord = szl.Z;
                                    }
                                    else
                                    {
                                        string[] xyzRand = szl.SpawnCells[Rules.dice.Next(0, szl.SpawnCells.Count)].Split(",".ToCharArray());
                                        sxcord = Convert.ToInt32(xyzRand[0]);
                                        sycord = Convert.ToInt32(xyzRand[1]);
                                        szcord = Convert.ToInt32(xyzRand[2]);
                                    }

                                    if (Cell.GetCell(facet.FacetID, szl.LandID, szl.MapID, sxcord, sycord, szcord) == null)
                                    {
                                        Utils.Log("SpawnZone " + szl.ZoneID + " is spawning outside of map bounds. F: " + facet.FacetID + " L: " + szl.LandID + " M: " + szl.MapID +
                                            " X: " + sxcord + " Y: " + sycord + " Z: " + szl.Z, Utils.LogType.SystemWarning);
                                        break;
                                    }

                                    bool goSpawn = true;

                                    string tile = Map.returnTile(Cell.GetCell(facet.FacetID, szl.LandID, szl.MapID, sxcord, sycord, szl.Z).CellGraphic);

                                    switch (tile)
                                    {
                                        case "~~":		//Don't spawn in water(until we tell you to)
                                            NPC waterDweller = NPC.CopyNPCFromDictionary(szl.NPCID);
                                            if (!waterDweller.IsWaterDweller)
                                            {
                                                goSpawn = false;
                                            }
                                            break;
                                        case "[]":		//Don't spawn in wall
                                        case "WW":		//Don't spawn in wall
                                        case "/\\":     //Don't spawn in mountain
                                        case "TT":		//Don't spawn in Impenetrible forest
                                        case "SD":		//Don't spawn in secret door
                                        case "mm":		//Don't spawn in an altar
                                        case "MM":
                                        case "--":		//Don't spawn in closed doors
                                        case "| ":		//Don't spawn in closed doors
                                        case "==":		//Don't spawn in counters
                                        case "CC":
                                        case "%%":		//Dont spawn in air
                                        case "**":		//Don't spawn in fire (until we tell you to)
                                            goSpawn = false;
                                            break;
                                    }

                                    if (goSpawn)
                                    {
                                        try
                                        {
                                            if (szl.NPCList.Length > 0)
                                            {
                                                szl.NPCList.Trim();

                                                string[] npclist = szl.NPCList.Split(" ".ToCharArray());
                                                number++;
                                                npc = LoadNPC(Convert.ToInt32(npclist[Rules.dice.Next(npclist.Length)]), facet.FacetID, szl.LandID, szl.MapID, sxcord, sycord, szcord, szl.ZoneID);
                                            }
                                            else
                                            {
                                                number++;
                                                npc = LoadNPC(szl.NPCID, facet.FacetID, szl.LandID, szl.MapID, sxcord, sycord, szcord, szl.ZoneID);
                                            }
                                            
                                            #region Load Group NPCs and Create Creature Group
                                            if (npc != null && npc.GroupAmount > 0)
                                            {
                                                // a initialized to group amount - 1 because there is already 1 npc spawned
                                                for (int a = npc.GroupAmount - 1; a > 0; a--)
                                                {
                                                    number++;
                                                    NPC groupNPC = LoadNPC(npc.npcID, npc.FacetID, npc.LandID, npc.MapID, npc.X, npc.Y, npc.Z, szl.ZoneID);
                                                    // give the group npc the same held items as the leader...
                                                    groupNPC.RightHand = null;
                                                    if (npc.RightHand != null)
                                                    {
                                                        groupNPC.RightHand = Item.CopyItemFromDictionary(npc.RightHand.itemID);
                                                    }
                                                    groupNPC.LeftHand = null;
                                                    if (npc.LeftHand != null)
                                                    {
                                                        groupNPC.LeftHand = Item.CopyItemFromDictionary(npc.LeftHand.itemID);
                                                    }
                                                    if (npc.Group == null)
                                                    {
                                                        Group.CreateCreatureGroup(npc, groupNPC);
                                                    }
                                                    else
                                                    {
                                                        npc.Group.Add(groupNPC);
                                                    }
                                                    szl.NumberInZone++; //This is to add the total number of mobs in the group to the total in the zone
                                                }
                                            }
                                            else if (npc != null && npc.GroupMembers.Length > 0)
                                            {
                                                string[] memberIDs = npc.GroupMembers.Split(" ".ToCharArray());
                                                for (int a = 0; a < memberIDs.Length; a++)
                                                {
                                                    number++;
                                                    NPC groupNPC = LoadNPC(Convert.ToInt32(memberIDs[a]), npc.FacetID, npc.LandID, npc.MapID, npc.X, npc.Y, npc.Z, szl.ZoneID);
                                                    // give the group npc the same held items as the leader...
                                                    groupNPC.RightHand = null;
                                                    if (npc.RightHand != null)
                                                    {
                                                        groupNPC.RightHand = Item.CopyItemFromDictionary(npc.RightHand.itemID);
                                                    }
                                                    groupNPC.LeftHand = null;
                                                    if (npc.LeftHand != null)
                                                    {
                                                        groupNPC.LeftHand = Item.CopyItemFromDictionary(npc.LeftHand.itemID);
                                                    }
                                                    if (npc.Group == null)
                                                    {
                                                        Group.CreateCreatureGroup(npc, groupNPC);
                                                    }
                                                    else
                                                    {
                                                        npc.Group.Add(groupNPC);
                                                    }
                                                    szl.NumberInZone++; //This is to add the total number of mobs in the group to the total in the zone
                                                }
                                            }
                                            #endregion
                                           
                                            if (npc != null)
                                            {
                                                szl.NumberInZone++;

                                                szl.Timer = 0;

                                                if (szl.SpawnMessage.Length > 0)
                                                {
                                                    foreach (Character ch in new List<Character>(Character.pcList))
                                                    {
                                                        if (ch.CurrentCell.LandID == szl.LandID && ch.CurrentCell.MapID == szl.MapID)
                                                        {
                                                            ch.WriteToDisplay(szl.SpawnMessage); //TODO: add sound informatino to spawn message
                                                            ch.SendSound(Sound.GetCommonSound(Sound.CommonSound.EarthQuake));
                                                        }
                                                    }
                                                }

                                                attempts = 0;
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            Utils.Log("Verify SpawnZone " + szl.ZoneID + " NPCID and / or NPCList", Utils.LogType.SystemWarning);
                                            Utils.LogException(e);
                                        }
                                    }
                                }
                            }//end if
                        }
                    } //end if spawn enabled
                    catch (Exception e)
                    {
                        Utils.Log("Error with SpawnZone " + szl.ZoneID, Utils.LogType.SystemFailure);
                        Utils.LogException(e);
                    }
                }

            }
            if (number != 0)
            {
                //Utils.Log(number + " new creatures added on round " + DragonsSpineMain.GameRound, Utils.LogType.Unknown);
            }
        }

        public static NPC CopyNPCFromDictionary(int npcID)
        {
            if (NPC.npcDictionary.ContainsKey(npcID))
            {
                System.Data.DataRow dr = npcDictionary[npcID];

                NPC npc = new NPC(dr);
                switch (npc.npcType)
                {
                    case NPCType.Creature:
                        return new Creature(npc);
                    case NPCType.Merchant:
                        return new Merchant(npc);
                }
            }
            Utils.Log("NPC.CopyNPCFromDictionary(" + npcID + ") NPC ID does not exist.", Utils.LogType.SystemWarning);
            return null;
        }

		public string randomNPCName(NPC npc)
		{
			string[] MALENAME = new string[] {"Alf","Eunei","Deui","Emaoa","Olaozi","Juado","Euho","Iheia","Koemo","Uiquo","Iujeua",
				"Eurui","Iyeojo","Eulioi","Qioqu","Iefoeoo","Euneea","Eazua","Seihot","Lanekob","Juruew","Johokh","Tzcl","Skik","Bhym",
				"Juis","Xyoak","Bdiavub","Uivie","Eote","Ezeei","Auqie","Eupaia","Osioce","Agoido","Oameio","Iogoe","Zeirue","Ikuue",
				"Zoef","Haup","Jaych","Wutram","Asirerd","Etho","Truf","Areag","Ereit","Cia","Etho",
				"Abaet","Abarden","Aboloft","Acamen","Achard","Ackmard","Adeen","Aerden","Afflon","Aghon","Agnar","Ahalfar","Ahburn","Ahdun","Airen",
				"Airis","Aldaren","Aldren","Alkirk","Allso","Amerdan","Amitel","Anfar","Anumi","Anumil","Asden","Asdern","Asen","Aslan","Atar","Atgur",
				"Atlin","Auchfor","Auden","Ault","Ayrie","Aysen","Bacohl","Badeek","Baguk","Balati","Bapader","Barkydle","Basden","Bayde",
				"Bedic","Beeron","Bein","Beson","Betur","Bevurlde","Bewul","Biedgar","Bildon","Biston","Bithon","Boal","Bocldelr","Bolrock",
				"Brakdern","Bredere","Bredin","Bredock","Breen","Bristan","Buchmeid","Bue","Busma","Buthomar","Bydern","Caelholdt","Cainon","Calden",
				"Camchak","Camilde","Cardon","Casden","Cayold","Celbahr","Celorn","Celthric","Cemark","Cerdern","Cespar","Cether","Cevelt","Chamon",
				"Chesmarn","Chidak","Cibrock","Cipyar","Ciroc","Codern","Colthan","Cos","Cosdeer","Cuparun","Cusmirk","Cydare","Cylmar","Cythnar",
				"Cyton","Daburn","Daermod","Dak","Dakamon","Dakkone","Dalburn","Dalmarn","Dapvhir","Darkkon","Darko","Darmor","Darpick","Dasbeck",
				"Dask","Defearon","Derik","Derrin","Desil","Dessfar","Dinfar","Dismer","Doceon","Dochrohan","Dokoran","Dorn","Dosoman","Drakoe",
				"Drakone","Drandon","Drit","Dritz","Drophar","Dryden","Dryn","Duba","Dukran","Duran","Durmark","Dusaro","Dyfar","Dyten","Eard",
				"Efamar","Efar","Egmardern","Eiridan","Ekgamut","Elik","Elson","Elthin","Enbane","Endor","Enidin","Enoon","Enro","Erikarn","Erim",
				"Eritai","Escariet","Espardo","Etar","Etburn","Etdar","Ethen","Etmere","Etran","Eythil","Faoturk","Faowind","Fenrirr","Fetmar",
				"Feturn","Ficadon","Fickfylo","Fildon","Firedorn","Firiro","Floran","Folmard","Fraderk","Fronar","Fydar","Fyn","Gafolern","Galain",
				"Gauthus","Gemardt","Gemedern","Gemedes","Gerirr","Geth","Gib","Gibolock","Gibolt","Gith","Gom","Gosford","Gothar","Gothikar",
				"Gresforn","Grimie","Gryn","Gundir","Guthale","Gybol","Gybrush","Gyin","Halmar","Harrenhal","Hectar","Hecton","Heramon","Hermenze",
				"Hermuck","Hezak","Hildale","Hildar","Hileict","Hydale","Hyten","Iarmod","Idon","Ieli","Ieserk","Ikar","Ilgenar","Illilorn","Illium",
				"Ingel","Ipedorn","Isen","Isil","Ithric","Jackson","Jalil","Jamik","Janus","Jayco","Jaython","Jesco","Jespar","Jethil","Jex","Jib",
				"Jibar","Jin","Juktar","Julthor","Jun","Justal","Kafar","Kaldar","Kellan","Keran","Kesad","Kesmon","Kethren","Kib","Kibidon","Kiden",
				"Kilbas","Kilburn","Kildarien","Kimdar","Kinorn","Kip","Kirder","Kodof","Kolmorn","Kyrad","Lackus","Lacspor","Laderic","Lafornon",
				"Lahorn","Laracal","Ledale","Leit","Lephar","Lephidiles","Lerin","Lesphares","Letor","Lidorn","Lin","Liphanes","Loban","Lox",
				"Ludokrin","Luphildern","Lupin","Lurd","Macon","Madarlon","Mafar","Marderdeen","Mardin","Markard","Markdoon","Marklin","Mashasen",
				"Mathar","Medarin","Medin","Mellamo","Meowol","Merdon","Meridan","Merkesh","Mesah","Mesard","Mesophan","Mesoton","Mezo","Mickal",
				"Migorn","Milo","Miphates","Mitalrythin","Mitar","Modric","Modum","Mudon","Mufar","Mujarin","Mylo","Mythil","Nadeer","Nalfar",
				"Namorn","Naphates","Neowyld","Nidale","Nikpal","Nikrolin","Niktohal","Niro","Noford","Nothar","Nuthor","Nuwolf","Nydale","Nythil",
				"Obarho","Ocarin","Occelot","Occhi","Odaren","Odeir","Ohethlic","Okar","Omaniron","Omarn","Orin","Ospar","Othelen","Oxbaren",
				"Padan","Palid","Papur","Peitar","Pelphides","Pender","Pendus","Perder","Perol","Phairdon","Phemedes","Phexides","Picon","Pictal",
				"Picumar","Pildoor","Ponith","Poran","Poscidion","Prothalon","Puthor","Pyder","Qeisan","Qidan","Quiad","Quid","Quiss","Qupar",
				"Qysan","Radagmal","Randar","Raysdan","Rayth","Resboron","Reth","Rethik","Rhithik","Rhithin","Rhysling","Riandur","Rikar","Rismak",
				"Riss","Ritic","Rogeir","Rogist","Rogoth","Rophan","Rulrindale","Rydan","Ryfar","Ryfar","Ryodan","Rysdan","Rythen","Rythern","Sabal",
				"Sadareen","Safilix","Samon","Samot","Sasic","Scoth","Secor","Sed","Sedar","Senick","Senthyril","Serin","Sermak","Seryth","Sesmidat",
				"Setlo","Shardo","Shillen","Silco","Sildo","Silforrin","Silpal","Sithik","Sothale","Staph","Suktor","Suth","Sutlin","Syr","Syth",
				"Sythril","Talberon","Telpur","Temil","Temilfist","Teslanar","Tespar","Tessino","Tethran","Thiltran","Tholan","Ticharol","Tobale",
				"Tolkolie","Tolle","Tolsar","Toma","Tothale","Tousba","Tuk","Tuscanar","Tusdar","Tyden","Uerthe","Ugmar","Uhrd","Undin","Updar",
				"Uther","Vaccon","Vacone","Valynard","Vectomon","Veldahar","Vethelot","Vider","Vigoth","Vilan","Vildar","Vinald","Vinkolt","Virde",
				"Voltain","Volux","Voudim","Vythethi","Wakiern","Walkar","Wanar","Wekmar","Werymn","Weshin","Willican","Wilte","Wiltmar","Witfar",
				"Wuthmon","Wyder","Wyeth","Xander","Xavier","Xenil","Xex","Xithyl","Xuio","Yaeth","Yabaro","Yepal","Yesirn","Yssik","Yssith","Zak",
				"Zakarn","Zecane","Zeke","Zerin","Zessfar","Zidar","Zigmal","Zile","Zilocke","Zio","Zoru","Zotar","Zutar","Zyten"};

			string[] FEMALENAME = new string[] {"Wiceillan","Cren","Galaveth","Ociramwen","Jerayssi","Dwirari","Laroicia","Fen","Leran","Beliswen","Eowelavia",
				"Selirith","Haebeth","Ederawiel","Crilaveth","Aseillan","Mardoclya","Dirathien","Perassi","Olaesien","Veama","Kediraven","Cadaelian","Frinia",
				"Cadelani","Yiniel","Sevorerien","Olaresa","Kailacien","Gweamwen","Venia","Gausa","Qoen","Sedith","Cimma","Aberadia","Morewien","Lothymeth",
				"Ybirethiel","Larireni","Seviarith","Ferang","Graemma","Gleridia","Gweavia","Wode","Ybean","Kayrwen","Aceivia","Wicardovia",
				"Acele","Acholate","Ada","Adiannon","Adorra","Ahanna","Akara","Akassa","Akia","Amaerilde","Amara","Amarisa","Amarizi","Ana","Andonna",
				"Ani","Annalyn","Archane","Ariannona","Arina","Arryn","Asada","Awnia","Ayne","Basete","Bathelie","Bethe","Brana","Brianan","Bridonna",
				"Brynhilde","Calene","Calina","Celestine","Celoa","Cephenrene","Chani","Chivahle","Chrystyne","Corda","Cyelena","Dalavesta","Desini",
				"Dylena","Ebatryne","Ecematare","Efari","Enaldie","Enoka","Enoona","Errinaya","Fayne","Frederika","Frida","Gene","Gessane","Gronalyn",
				"Gvene","Gwethana","Halete","Helenia","Hildandi","Hyza","Idona","Ikini","Ilene","Illia","Iona","Jessika","Jezzine","Justalyne",
				"Kassina","Kilayox","Kilia","Kilyne","Kressara","Laela","Laenaya","Lelani","Lenala","Linovahle","Linyah","Lloyanda","Lolinda",
				"Lyna","Lynessa","Mehande","Melisande","Midiga","Mirayam","Mylene","Nachaloa","Naria","Narisa","Nelenna","Niraya","Nymira",
				"Ochala","Olivia","Onathe","Ondola","Orwyne","Parthinia","Pascheine","Pela","Perifilly","Pharysene","Philadona","Prisane","Prysala",
				"Pythe","Qara","Qiala","Quasee","Rhyanon","Rivatha","Ryiah","Sanala","Sathe","Senira","Sennetta","Sepherene","Serane","Sevestra",
				"Sidara","Sidathe","Sina","Sunete","Synestra","Sythini","Szene","Tabika","Tabithi","Tajule","Tamare","Teresse","Tolida","Tonica",
				"Treka","Tressa","Trinsa","Tryane","Tybressa","Tycane","Tysinni","Undaria","Uneste","Urda","Usara","Useli","Ussesa","Venessa",
				"Veseere","Voladea","Vysarane","Vythica","Wanera","Welisarne","Wellisa","Wesolyne","Wyeta","Yilvoxe","Ysane","Yve","Yviene",
				"Yvonnette","Yysara","Zana","Zathe","Zecele","Zenobia","Zephale","Zephere","Zerma","Zestia","Zilka","Zoura","Zrye","Zyneste","Zynoa"};

            
            if (npc.gender == Globals.eGender.Male)
            {
                if (npc.race == "Leng" || World.GetFacetByIndex(0).GetLandByID(npc.LandID).Name == "Leng" || World.GetFacetByIndex(0).GetLandByID(npc.LandID).Name == "Torii")
                {
                    return getJapaneseName();
                }
                else if (Rules.RollD(1, 100) >= 50)
                {
                    return getRandomName(npc.gender);
                }
                return MALENAME[Rules.dice.Next(0, MALENAME.Length - 1)];
            }
            else
            {
                if (npc.race == "Leng" || World.GetFacetByIndex(0).GetLandByID(npc.LandID).Name == "Leng" || World.GetFacetByIndex(0).GetLandByID(npc.LandID).Name == "Torii")
                {
                    return getJapaneseName();
                }
                else if (Rules.RollD(1, 100) >= 50)
                {
                    return getRandomName(npc.gender);
                }
                return FEMALENAME[Rules.dice.Next(0, FEMALENAME.Length - 1)];
            }
		}

        public static string getJapaneseName()
        {
            string[] JAPANESE = new string[] { "a", "i", "u", "e", "o", "ka", "ki", "ku", "ke", "ko", "sa", "shi", "su", "se", "so", "ta", "chi", "tsu", "te", "to",
                "na", "ni", "nu", "ne", "no", "ha", "hi", "fu", "he", "ho", "ma", "mi", "mu", "me", "mo", "ra", "ri", "ru", "re", "ro", "ya", "yu", "yo", "wa", "wo",
                "ga", "gi", "gu", "ge", "go", "za", "ji", "zu", "ze", "zo", "da", "de", "do", "ba", "bi", "bu", "be", "bo", "pa", "pi", "pu", "pe", "po", "n" };

            int length = Rules.RollD(1, 3) + 1;
            string name = "";
            for (int a = 0; a < length; a++)
            {
                if (a == 0)
                {
                    name += JAPANESE[Rules.dice.Next(0, JAPANESE.Length - 1)];
                }
                else
                {
                    name += JAPANESE[Rules.dice.Next(0, JAPANESE.Length)];
                }
            }
            name = name.Substring(0, 1).ToUpper() + name.Substring(1);
            return name;
        }

        public static string getRandomName(Globals.eGender gender)
        {
            string[] vowels = { "a", "a", "a", "e", "e", "e", "i", "i", "i", "o", "o", "o", "u", "u", "u" };
            string[] consts = { "b", "b", "b", "c", "d", "d", "d", "f", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "r", "r", "s", "s", "s", "t", "t", "t", "v", "w", "x", "y", "z" };
            string name = "";
            bool lco = false;
            switch (Rules.dice.Next(1, 3))
            {
                case 1:
                    name = name + consts[Rules.dice.Next(consts.Length)].ToUpper();
                    lco = true;
                    break;
                case 2:
                    name = name + vowels[Rules.dice.Next(vowels.Length)].ToUpper();
                    lco = false;
                    break;
                default:
                    break;
            }
            for (int l = 0; l < Rules.dice.Next(2, 5); l++)
            {
                if (lco)
                {
                    if (Rules.dice.Next(1, 2) == 1)
                        name = name + vowels[Rules.dice.Next(vowels.Length)];
                }
                switch (Rules.dice.Next(1, 3))
                {
                    case 1:
                        name = name + consts[Rules.dice.Next(consts.Length)];
                        lco = true;
                        break;
                    case 2:
                        name = name + vowels[Rules.dice.Next(vowels.Length)];
                        lco = false;
                        break;
                    default:
                        break;
                }
            }
            if (gender == Globals.eGender.Female)
            {
                name += "ia";
            }
            return name;
        }

		public static NPC CreateNPC(int NPCnum, short facetID, short landID, short mapID, int xcord, int ycord, int zcord)
		{
			Item gold = null;

			NPC npc = NPC.CopyNPCFromDictionary(Convert.ToInt32(NPCnum));

			if(npc == null)
			{
				return null;
			}
			else
			{
				try
				{
					int lootNum = 0;
					int rollOdds = 0;

                    npc.CurrentCell = Cell.GetCell(facetID, landID, mapID, xcord, ycord, zcord);

                    npc.spawnXCord = npc.X;
                    npc.spawnYCord = npc.Y;
                    npc.spawnZCord = npc.Z;
                    npc.Level = npc.Level; // set the npc's level

                    if (npc.gender == Globals.eGender.Random) // determine gender if random
					{
						if(Rules.RollD(1, 20) > 8)
						{
                            npc.gender = Globals.eGender.Male;
						}
						else
						{
                            npc.gender = Globals.eGender.Female;
                            npc.visualKey = npc.visualKey.Replace("male_", "female_");
						}
					}

                    if (npc.race == "random") // determine race if random
					{
                        npc.race = (string)Enum.GetNames(typeof(Globals.eHomeland)).GetValue(Rules.dice.Next(0, Enum.GetNames(typeof(Globals.eHomeland)).Length));
					}

                    if (npc.Age < World.AgeCycles[0]) // determine age if random (young or very young)
                    {
                        npc.Age = Rules.dice.Next(World.AgeCycles[0], World.AgeCycles[2] + 1);
                    }

                    if (npc.HasRandomName) // determine name if random
                    {
                        npc.Name = npc.randomNPCName(npc);
                    }

					#region NPC's Stat Adds
					if(npc.Strength >= 16)
					{
						npc.strengthAdd = 1;

						//melee
                        if (npc.IsPureMelee || npc.IsHybrid)
						{
							switch(npc.Level)
							{
								case 6:
								case 7:
								case 8:
									npc.strengthAdd += 1; break;
								case 9:
								case 10:
								case 11:
									npc.strengthAdd += 2; break;
								case 12:
								case 13:
								case 14:
									npc.strengthAdd += 3; break;
								case 15:
								case 16:
								case 17:
									npc.strengthAdd += 4; break;
								case 18:
								case 19:
								case 20:
									npc.strengthAdd += 5; break;
								case 21:
								case 22:
								case 23:
									npc.strengthAdd += 6; break;
								case 24:
								case 25:
								case 26:
									npc.strengthAdd += 7; break;
								case 27:
								case 28:
								case 29:
									npc.strengthAdd += 8; break;
								case 30:
								case 31:
								case 32:
									npc.strengthAdd += 9; break;
								default:
									break;
							}
						}
							//magic users
						else
						{
							switch(npc.Level)
							{
								case 7:
								case 8:
								case 9:
								case 10:
									npc.strengthAdd += 1; break;
								case 11:
								case 12:
								case 13:
								case 14:
									npc.strengthAdd += 2; break;
								case 15:
								case 16:
								case 17:
								case 18:
									npc.strengthAdd += 3; break;
								case 19:
								case 20:
								case 21:
								case 22:
									npc.strengthAdd += 4; break;
								case 23:
								case 24:
								case 25:
								case 26:
									npc.strengthAdd += 5; break;
								case 27:
								case 28:
								case 29:
								case 30:
									npc.strengthAdd += 6; break;
								case 31:
								case 32:
									npc.strengthAdd += 7; break;
								default:
									break;
							}
						}
					}
					if(npc.Dexterity >= 16)
					{
						npc.dexterityAdd = 1;

						//melee
                        if (npc.IsPureMelee || npc.IsHybrid)
						{
							switch(npc.Level)
							{
								case 6:
								case 7:
								case 8:
									npc.dexterityAdd += 1; break;
								case 9:
								case 10:
								case 11:
									npc.dexterityAdd += 2; break;
								case 12:
								case 13:
								case 14:
									npc.dexterityAdd += 3; break;
								case 15:
								case 16:
								case 17:
									npc.dexterityAdd += 4; break;
								case 18:
								case 19:
								case 20:
									npc.dexterityAdd += 5; break;
								case 21:
								case 22:
								case 23:
									npc.dexterityAdd += 6; break;
								case 24:
								case 25:
								case 26:
									npc.dexterityAdd += 7; break;
								case 27:
								case 28:
								case 29:
									npc.dexterityAdd += 8; break;
								case 30:
								case 31:
								case 32:
									npc.dexterityAdd += 9; break;
								default:
									break;
							}
						}
							//magic users
						else
						{
							switch(npc.Level)
							{
								case 7:
								case 8:
								case 9:
								case 10:
									npc.dexterityAdd += 1; break;
								case 11:
								case 12:
								case 13:
								case 14:
									npc.dexterityAdd += 2; break;
								case 15:
								case 16:
								case 17:
								case 18:
									npc.dexterityAdd += 3; break;
								case 19:
								case 20:
								case 21:
								case 22:
									npc.dexterityAdd += 4; break;
								case 23:
								case 24:
								case 25:
								case 26:
									npc.dexterityAdd += 5; break;
								case 27:
								case 28:
								case 29:
								case 30:
									npc.dexterityAdd += 6; break;
								case 31:
								case 32:
									npc.dexterityAdd += 7; break;
								default:
									break;
							}
						}
					}
					#endregion

					#region NPC's Items
                    #region Right Hand
                    if (npc.spawnRightHand.Length > 0 && npc.spawnRightHand != "0")
                    {
                        String[] righthandArray = npc.spawnRightHand.Split(" ".ToCharArray());
                        lootNum = Rules.dice.Next(righthandArray.Length);
                        npc.RightHand = Item.CopyItemFromDictionary(Convert.ToInt32(righthandArray[lootNum]));
                    } 
                    #endregion
                    #region Left Hand
                    if (npc.spawnLeftHand.Length > 0 && npc.spawnLeftHand != "0" && npc.RightHand == null || (npc.RightHand != null && npc.RightHand.skillType != Globals.eSkillType.Bow && npc.RightHand.skillType != Globals.eSkillType.Two_Handed))
                    {
                        String[] lefthandArray = npc.spawnLeftHand.Split(" ".ToCharArray());
                        rollOdds = Rules.RollD(1, 100);
                        if (rollOdds <= npc.spawnLeftHandOdds)
                        {
                            lootNum = Rules.dice.Next(lefthandArray.Length);
                            npc.LeftHand = Item.CopyItemFromDictionary(Convert.ToInt32(lefthandArray[lootNum]));
                        }
                    } 
                    #endregion
                    #region Armor
                    if (npc.spawnArmor.Length > 0 && npc.spawnArmor != "0")
                    {
                        string[] armorArray = npc.spawnArmor.Split(" ".ToCharArray());
                        lootNum = 0;
                        while (lootNum <= armorArray.Length - 1)
                        {
                            Item wItem = Item.CopyItemFromDictionary(Convert.ToInt32(armorArray[lootNum]));
                            if (wItem != null)
                            {
                                if (Globals.Max_Wearable[(int)wItem.wearLocation] > 1)
                                {
                                    if (npc.GetInventoryItem(wItem.wearLocation, Globals.eWearOrientation.Right) == null)
                                        wItem.wearOrientation = Globals.eWearOrientation.Right;
                                    else if (npc.GetInventoryItem(wItem.wearLocation, Globals.eWearOrientation.Left) == null)
                                        wItem.wearOrientation = Globals.eWearOrientation.Left;
                                    else wItem = null;
                                }

                                if (wItem != null)
                                    npc.WearItem(wItem);
                            }
                            lootNum++;
                        }
                    } 
                    #endregion
                    #region Gold
                    if (npc.Gold > 0 && !npc.lairCritter)
                    {
                        gold = Item.CopyItemFromDictionary(Item.ID_COINS);
                        gold.coinValue = Rules.dice.Next((int)((int)npc.Gold / 2), (int)(npc.Gold) + 1);
                        npc.SackItem(gold);
                    } 
                    #endregion
                    if (npc.BaseProfession == ClassType.Knight && npc.Alignment == Globals.eAlignment.Lawful)
                    {
                        Item knightRing = Item.CopyItemFromDictionary(Item.ID_KNIGHTRING);
                        knightRing.attuneType = Globals.eAttuneType.None;
                        npc.RightRing1 = Item.CopyItemFromDictionary(Item.ID_KNIGHTRING);
                    }
                    #region Loot
                    //only non lair critters use very common, common, rare, very rare loot arrays
                    if (!npc.lairCritter)
                    {
                        #region Very Common Loot
                        if (npc.lootVeryCommonArray.Length > 0)
                        {
                            String[] lootArray = npc.lootVeryCommonArray.Split(" ".ToCharArray());
                            //if odds is -1, put all array items into sack
                            if (npc.lootVeryCommonOdds == -1)
                            {
                                lootNum = 0;
                                while (lootNum <= lootArray.Length - 1)
                                {
                                    if (lootArray[lootNum].ToString() != "")
                                    {
                                        Item itm = Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum]));
                                        npc.SackItem(itm);
                                    }
                                    lootNum++;
                                }
                            }
                            //if odds is 100, put 1 array item into sack
                            else if (npc.lootVeryCommonOdds == 100)
                            {
                                lootNum = Rules.dice.Next(lootArray.Length);
                                if (lootArray[lootNum].ToString() != "")
                                {
                                    npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                                }
                            }
                            //if odds > 0 use loot amount and odds to fill up the sack
                            else
                            {
                                while (npc.lootVeryCommonAmount > 0)
                                {
                                    rollOdds = Rules.dice.Next(100);
                                    if (rollOdds <= npc.lootVeryCommonOdds)
                                    {
                                        lootNum = Rules.dice.Next(lootArray.Length);
                                        if (lootArray[lootNum].ToString() != "")
                                        {
                                            npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                                        }
                                    }
                                    npc.lootVeryCommonAmount--;
                                }
                            }
                        }
                        #endregion
                        #region Common Loot
                        if (npc.lootCommonArray.Length > 0)
                        {
                            String[] lootArray = npc.lootCommonArray.Split(" ".ToCharArray());
                            //if odds is -1, put all array items into sack 
                            if (npc.lootCommonOdds == -1)
                            {
                                lootNum = 0;
                                while (lootNum <= lootArray.Length - 1)
                                {
                                    if (lootArray[lootNum].ToString() != "")
                                    {
                                        Item itm = Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum]));
                                        npc.SackItem(itm);
                                    }
                                    lootNum++;
                                }
                            }
                            //if odds is 100, put 1 array item into sack
                            else if (npc.lootCommonOdds == 100)
                            {
                                lootNum = Rules.dice.Next(lootArray.Length);
                                if (lootArray[lootNum].ToString() != "")
                                {
                                    npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                                }
                            }
                            //if odds > 0 use loot amount and odds to fill up the sack
                            else
                            {
                                while (npc.lootCommonAmount > 0)
                                {
                                    rollOdds = Rules.dice.Next(100);
                                    if (rollOdds <= npc.lootCommonOdds)
                                    {
                                        lootNum = Rules.dice.Next(lootArray.Length);
                                        npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                                    }
                                    npc.lootCommonAmount--;
                                }
                            }
                        }
                        #endregion
                        #region Rare Loot
                        if (npc.lootRareArray.Length > 0)
                        {
                            String[] lootArray = npc.lootRareArray.Split(" ".ToCharArray());
                            //if odds is -1, put all array items into sack
                            if (npc.lootRareOdds == -1)
                            {
                                lootNum = 0;
                                while (lootNum <= lootArray.Length - 1)
                                {
                                    Item itm = Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum]));
                                    npc.SackItem(itm);
                                    Utils.Log(NPC.GetLogString(npc) + " has rare loot [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootRare);
                                    lootNum++;
                                }
                            }
                            //if odds is 100, put 1 array item into sack
                            else if (npc.lootRareOdds == 100)
                            {
                                lootNum = Rules.dice.Next(lootArray.Length);
                                npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                                Utils.Log(NPC.GetLogString(npc) + " has rare loot [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootRare);
                            }
                            //if odds > 0 use loot amount and odds to fill up the sack
                            else
                            {
                                while (npc.lootRareAmount > 0)
                                {
                                    rollOdds = Rules.dice.Next(100);
                                    if (rollOdds <= npc.lootRareOdds)
                                    {
                                        lootNum = Rules.dice.Next(lootArray.Length);
                                        npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                                        Utils.Log(NPC.GetLogString(npc) + " has rare loot [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootRare);
                                    }
                                    npc.lootRareAmount--;
                                }
                            }
                        }
                        #endregion
                        #region Very Rare Loot
                        if (npc.lootVeryRareArray.Length > 0)
                        {
                            String[] lootArray = npc.lootVeryRareArray.Split(" ".ToCharArray());
                            //if odds is -1, put all array items into sack
                            if (npc.lootVeryRareOdds == -1)
                            {
                                lootNum = 0;
                                while (lootNum <= lootArray.Length - 1)
                                {
                                    Item itm = Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum]));
                                    npc.SackItem(itm);
                                    Utils.Log(NPC.GetLogString(npc) + " has very rare loot [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootVeryRare);
                                    lootNum++;
                                }
                            }
                            //if odds is 100, put 1 array item into sack
                            else if (npc.lootVeryRareOdds == 100)
                            {
                                lootNum = Rules.dice.Next(lootArray.Length);
                                npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                                Utils.Log(NPC.GetLogString(npc) + " has very rare loot [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootVeryRare);
                            }
                            //if odds > 0 use loot amount and odds to fill up the sack
                            else
                            {
                                while (npc.lootVeryRareAmount > 0)
                                {
                                    rollOdds = Rules.dice.Next(100);
                                    if (rollOdds <= npc.lootVeryRareOdds)
                                    {
                                        lootNum = Rules.dice.Next(lootArray.Length);
                                        npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                                        Utils.Log(NPC.GetLogString(npc) + " has very rare loot [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootVeryRare);
                                    }
                                    npc.lootVeryRareAmount--;
                                }
                            }
                        }
                        #endregion
                    }
                    //all npcs use lootLair, lootAlways, lootBelt
                    #region Lair Loot
                    if (npc.lootLairArray.Length > 0)
                    {
                        String[] lootArray = npc.lootLairArray.Split(" ".ToCharArray());
                        //if odds is -1, put all array items into sack
                        if (npc.lootLairOdds == -1)
                        {
                            lootNum = 0;
                            while (lootNum <= lootArray.Length - 1)
                            {
                                Item itm = Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum]));
                                npc.SackItem(itm);
                                lootNum++;
                            }
                        }
                        //if odds is 100, put 1 array item into sack
                        else if (npc.lootLairOdds == 100)
                        {
                            lootNum = Rules.dice.Next(lootArray.Length);
                            npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                        }
                        //if odds > 0 use loot amount and odds to fill up the sack
                        else
                        {
                            while (npc.lootLairAmount > 0)
                            {
                                rollOdds = Rules.dice.Next(100);
                                if (rollOdds <= npc.lootLairOdds)
                                {
                                    lootNum = Rules.dice.Next(lootArray.Length - 1);
                                    npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                                }
                                npc.lootLairAmount--;
                            }
                        }
                    }
                    #endregion
                    #region Always Has Loot
                    if (npc.lootAlwaysArray.Length > 0)
                    {
                        String[] lootArray = npc.lootAlwaysArray.Split(" ".ToCharArray());

                        lootNum = 0;
                        while (lootNum <= lootArray.Length - 1)
                        {
                            Item itm = Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum]));
                            npc.SackItem(itm);
                            Utils.Log(NPC.GetLogString(npc) + " has always loot [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootAlways);
                            lootNum++;
                        }
                    }
                    #endregion
                    #region Belt Items
                    if (npc.lootBeltArray.Length > 0)
                    {
                        String[] lootArray = npc.lootBeltArray.Split(" ".ToCharArray());
                        //if odds is -1, put all array items onto belt
                        if (npc.lootBeltOdds == -1)
                        {
                            lootNum = 0;
                            while (lootNum <= lootArray.Length - 1)
                            {
                                Item itm = Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum]));
                                npc.BeltItem(itm);
                                Utils.Log(NPC.GetLogString(npc) + " has belt item [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootBelt);
                                lootNum++;
                            }
                        }
                        //if odds is 100, put 1 array item onto belt
                        else if (npc.lootBeltOdds == 100)
                        {
                            lootNum = Rules.dice.Next(lootArray.Length);
                            npc.BeltItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                            Utils.Log(NPC.GetLogString(npc) + " has belt item [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootBelt);
                        }
                        //if odds > 0 use loot amount and odds to fill up the belt
                        else
                        {
                            while (npc.lootBeltAmount > 0)
                            {
                                rollOdds = Rules.dice.Next(100);
                                if (rollOdds <= npc.lootBeltOdds)
                                {
                                    bool wearThis = true;
                                    lootNum = Rules.dice.Next(lootArray.Length);
                                    Item bItem = Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum]));
                                    foreach (Item wItem in npc.wearing)
                                    {
                                        if (bItem.wearLocation == wItem.wearLocation) { wearThis = false; }
                                    }
                                    if (bItem.wearLocation == Globals.eWearLocation.None) { wearThis = false; }
                                    rollOdds = Rules.dice.Next(100);
                                    if (wearThis && rollOdds <= 74)
                                    {
                                        npc.WearItem(bItem);
                                        Utils.Log(NPC.GetLogString(npc) + " has belt item [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootBelt);
                                    }
                                    else
                                    {
                                        npc.BeltItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                                        Utils.Log(NPC.GetLogString(npc) + " has belt item [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootBelt);
                                    }
                                }
                                npc.lootBeltAmount--;
                            }
                        }
                    }
                    #endregion 
                    #endregion
                    #endregion

                    #region NPC's Resistances
                    //set npc resistance
					if(npc.Level >= 3)
					{
						switch(npc.BaseProfession)
						{
								//TODO: fd, id, ld (immunities)
							case ClassType.Fighter:
								npc.StunResistance = 2;
								break;
							case ClassType.Knight:
								npc.DeathResistance = 2;
								break;
							case ClassType.Martial_Artist:
								npc.LightningResistance = 2;
								break;
							case ClassType.Thaumaturge:
								npc.BlindResistance = 1;
								npc.FearResistance = 1;
								break;
							case ClassType.Thief:
								npc.PoisonResistance = 2;
								break;
							case ClassType.Wizard:
								npc.FireResistance = 1;
								npc.ColdResistance = 1;
								break;
							default:
								break;
						}
						if(npc.Level > 3)
						{
							int addResistance = npc.Level - 3;
							npc.FireResistance += addResistance;
							npc.ColdResistance += addResistance;
							npc.LightningResistance += addResistance;
							npc.DeathResistance += addResistance;
							npc.BlindResistance += addResistance;
							npc.FearResistance += addResistance;
							npc.StunResistance += addResistance;
							npc.PoisonResistance += addResistance;
						}
					}
					#endregion

					#region NPC's Random Value Items

					//set random coin values for npc sack items
					foreach(Item sItem in npc.sackList)
					{
						if(sItem.vRandLow > 0)
						{
							sItem.coinValue = Rules.dice.Next(sItem.vRandLow,sItem.vRandHigh);
						}
					}
					foreach(Item bItem in npc.beltList)
					{
						if(bItem.vRandLow > 0)
						{
							bItem.coinValue = Rules.dice.Next(bItem.vRandLow,bItem.vRandHigh);
						}
					}
					//do the npcs effects, specials and random coin values for worn items
					foreach(Item wItem in npc.wearing)
					{
						if(wItem.vRandLow > 0)
						{
							wItem.coinValue = Rules.dice.Next(wItem.vRandLow,wItem.vRandHigh);
						}
						if(wItem.effectType.Length > 0)
						{
							Effect.AddWornEffectToCharacter(npc, wItem);
						}
					}
					#endregion

                    if (npc.BaseProfession == ClassType.Fighter && npc.Level >= 9 &&
                        npc.RightHand != null && (npc.HasRandomName || npc.race != "") &&
                        Rules.CheckPerception(npc))
                    {
                        npc.fighterSpecialization = npc.RightHand.skillType;
                    }

                    if (npc.HitsMax <= 0)
                    {
                        npc.HitsMax = 5 * npc.Level;
                        npc.HitsMax += Rules.GetHitsGain(npc, npc.Level);
                    }

                    npc.Hits = npc.HitsMax;

                    if (npc.ManaMax <= 0 && npc.IsSpellUser)
                    {
                        npc.ManaMax = Rules.GetManaGain(npc, npc.Level);
                    }

                    npc.Mana = npc.ManaMax;

                    if (npc.StaminaMax <= 0)
                    {
                        npc.StaminaMax = Rules.GetStaminaGain(npc, npc.Level);
                    }

                    npc.Stamina = npc.StaminaMax;
                    
					npc.AddToWorld();

					#region Lair Critter Loot and Cell Flagging
					//if this is a lair critter let's take care of it's lair loot and lair cell flagging
					if(npc.lairCritter && npc.lairCells.Length > 0)
					{
                        npc.lairXCord = npc.X;
                        npc.lairYCord = npc.Y;
                        npc.lairZCord = npc.Z;

                        if (npc.lairCells.Length > 0)
                        {
                            string[] lc = npc.lairCells.Split(" ".ToCharArray());
                            foreach (string lcell in lc)
                            {
                                string[] lc2 = lcell.Split("|".ToCharArray());
                                int lx = Convert.ToInt32(lc2[0]);
                                int ly = Convert.ToInt32(lc2[1]);
                                Cell lairCell = Cell.GetCell(npc.FacetID, npc.LandID, npc.MapID, lx, ly, npc.Z);
                                if (lairCell != null)
                                {
                                    lairCell.IsLair = true;
                                    npc.lairCellsList.Add(lairCell);
                                }
                            }

                            if (npc.lootVeryCommonArray.Length > 0 && npc.lootCommonArray.Length > 0 &&
                                npc.lootRareArray.Length > 0 && npc.lootVeryRareArray.Length > 0)
                            {
                                string[] lairVeryCommonLoot = npc.lootVeryCommonArray.Split(" ".ToCharArray());
                                string[] lairCommonLoot = npc.lootCommonArray.Split(" ".ToCharArray());
                                string[] lairRareLoot = npc.lootRareArray.Split(" ".ToCharArray());
                                string[] lairVeryRareLoot = npc.lootVeryRareArray.Split(" ".ToCharArray());
                                //load the npc's lair loot
                                Map.LoadLairLoot(npc, lairVeryCommonLoot, lairCommonLoot, lairRareLoot, lairVeryRareLoot);
                            }
                        }
					}
					#endregion
                    
                    // npc.npcTimer.Start();
					return npc;
				}
				catch(Exception e)
				{
                    Utils.LogException(e);
					return null;
				}
			}
		}

        protected internal void NPCEvent(object sender, ElapsedEventArgs e)
        {
            lock (lockObject)
            {
                #region NPC
                NPC npc = this;
                Character ch = this as Character;
                if (ch.effectList.ContainsKey(Effect.EffectType.Wizard_Eye)) {
                    Utils.Log("NPC Event - Wizard Eye - returning.", Utils.LogType.SystemGo);
                    return;
                }

                //if (World.GetNumberPlayersInMap(ch.MapID) == 0) continue;
                if (!DragonsSpineMain.ProcessEmptyWorld)
                {
                    if (World.GetNumberPlayersInMap(ch.MapID) == 0) {
                        Utils.Log("NPC Event - Empty World - returning.", Utils.LogType.SystemGo);
                        return;
                    }
                }

                if (ch.CurrentCell != null)
                    ch.Map.UpdateCellVisible(ch.CurrentCell);

                if (npc.RoundsRemaining > 0)
                {
                    npc.RoundsRemaining--;

                    if (npc.RoundsRemaining <= 0)
                    {
                        if (npc.special.ToLower().Contains("figurine"))
                        {
                            if (npc.CurrentCell == null)
                            {
                                npc.RemoveFromWorld();
                                Utils.Log("NPC Event - Figurine - removed - returning.", Utils.LogType.SystemGo);
                                return;
                            }
                            Rules.DespawnFigurine(npc);
                            Utils.Log("NPC Event - Figurine - despawned - returning.", Utils.LogType.SystemGo);

                            return;
                        }
                        else if (npc.special.ToLower().Contains("despawn"))
                        {
                            Rules.UnsummonCreature(npc);
                            Utils.Log("NPC Event - Despawn - unsummoned - returning.", Utils.LogType.SystemGo);
                            return;
                        }
                    }
                }

                ch.debug = 0; // reset each round
                ch.cmdWeight = 0;

                if (ch.IsSummoned) // only summoned npcs gain age
                {
                    ch.Age++;
                }

                if (ch.Stunned == 0)
                {
                    if (ch.IsFeared)
                        Creature.AIMakePCMove(ch);
                    else
                    {
                        if (ch.Group == null || (ch.Group != null && ch.Group.GroupLeaderID == ch.worldNpcID))
                            npc.doAI();
                    }
                }
                else ch.Stunned -= 1;

                if (ch.Group != null && ch.Group.GroupLeaderID == ch.worldNpcID)
                {
                    foreach (NPC groupNPC in new List<NPC>(ch.Group.GroupNPCList))
                        if (groupNPC.worldNpcID != ch.worldNpcID)
                            if (groupNPC.CurrentCell != ch.CurrentCell)
                                groupNPC.CurrentCell = ch.CurrentCell;
                }

                #region Follow Mode for Controlled NPCs
                if (ch.PetOwner != null && ch.canCommand)
                {
                    Character owner = ch.PetOwner;
                    if (owner != null)
                    {
                        if (Map.FindTargetInView(ch, owner.Name, false, false) != null)
                        {
                            ch.CurrentCell = owner.CurrentCell; // place the npc in the pc's cell
                            if (!ch.effectList.ContainsKey(Effect.EffectType.Hello_Immobility)) // perma-root
                            {
                                Effect.CreateCharacterEffect(Effect.EffectType.Hello_Immobility, 0, ch, -1, null);
                            }
                        }
                        else
                        {
                            if (ch.questList.Count > 0)
                            {
                                if (ch.questList[0].FailStrings.Count > 0) // shout a fail string
                                {
                                    ch.SendShout(ch.Name + ": " + ch.questList[0].FailStrings[Convert.ToInt16(Rules.dice.Next(1,
                                        ch.questList[0].FailStrings.Count))]);
                                }
                            }

                            if (ch.effectList.ContainsKey(Effect.EffectType.Hello_Immobility)) // remove perma-root
                            {
                                ch.effectList[Effect.EffectType.Hello_Immobility].StopCharacterEffect();
                            }
                        }
                    }
                }
                #endregion

                ch.numAttackers = 0;

                if (ch.CurrentCell != null)
                    ch.Map.UpdateCellVisible(ch.CurrentCell);
                #endregion

                // Save live NPC data
                if (Character.NPCList.Contains(ch))
                    DAL.DBNPC.SaveLiveNpc(npc);

            }
        }
        public static NPC LoadNPC(int npcID, short facetID, short landID, short mapID, int xcord, int ycord, int zcord, int spawnzoneid)
        {
            NPC npc = new NPC();
            npc = NPC.CopyNPCFromDictionary(Convert.ToInt32(npcID));

            if (npc == null)
            {
                return null;
            }

            else
            {
                try
                {
                    Item gold = null;
                    int lootNum = 0;
                    int rollOdds = 0;

                    npc.CurrentCell = Cell.GetCell(facetID, landID, mapID, xcord, ycord, zcord);

                    npc.spawnXCord = npc.X;
                    npc.spawnYCord = npc.Y;
                    npc.spawnZCord = npc.Z;

                    #region Patrol Route Placement
                    // if npc is lair critter with patrol, set patrolCount to next cell in patrol route (if there is a match)
                    if (npc.HasPatrol && npc.lairCritter)
                    {
                        for (int a = 0; a < npc.patrolKeys.Count; a++)
                        {
                            string[] xyz = npc.patrolKeys[a].Split("|".ToCharArray());
                            if (xyz[0].ToLower() == "w") // is wait patrol key
                                continue;
                            if (Convert.ToInt32(xyz[0]) == npc.X && Convert.ToInt32(xyz[1]) == npc.Y &&
                                Convert.ToInt32(xyz[2]) == npc.Z)
                            {
                                npc.patrolCount = a + 1;
                                if (npc.patrolCount > npc.patrolKeys.Count - 1)
                                {
                                    npc.patrolCount = 0;
                                }
                                break;
                            }
                        }
                    }
                    else if (npc.HasPatrol) // place NPC at random patrol cell
                    {
                        rollOdds = Rules.dice.Next(0, npc.patrolKeys.Count);
                        string[] xyz = npc.patrolKeys[rollOdds].Split("|".ToCharArray());
                        while (xyz[0].ToLower() == "w") // found wait patrol key
                        {
                            rollOdds = Rules.dice.Next(0, npc.patrolKeys.Count);
                            xyz = npc.patrolKeys[rollOdds].Split("|".ToCharArray());
                        }
                        npc.CurrentCell = Cell.GetCell(facetID, landID, mapID, Convert.ToInt16(xyz[0]), Convert.ToInt16(xyz[1]),
                            Convert.ToInt32(xyz[2]));
                        npc.patrolCount = rollOdds + 1;
                        if (npc.patrolCount > npc.patrolKeys.Count - 1)
                        {
                            npc.patrolCount = 0;
                        }
                    } 
                    #endregion

                    npc.Level = npc.Level; // set the npc's level

                    if (npc.gender == Globals.eGender.Random) // determine gender if random
                    {
                        if (Rules.RollD(1, 20) > 8)
                        {
                            npc.gender = Globals.eGender.Male;
                        }
                        else
                        {
                            npc.gender = Globals.eGender.Female;

                            npc.visualKey = npc.visualKey.Replace("male_", "female_");
                        }
                    }

                    if (npc.race == "random") // determine race if random
                    {
                        npc.race = (string)Enum.GetNames(typeof(Globals.eHomeland)).GetValue(Rules.dice.Next(0, Enum.GetNames(typeof(Globals.eHomeland)).Length));
                    }

                    if (npc.Age < World.AgeCycles[0]) // determine age if random (young or very young)
                    {
                        npc.Age = Rules.dice.Next(World.AgeCycles[0], World.AgeCycles[2] + 1);
                    }

                    if (npc.HasRandomName) // determine name if random
                    {
                        npc.Name = npc.randomNPCName(npc); 
                    }

                    if (!npc.lairCritter) // adjust npc experience based on map multiplier, except lair critters
                    {
                        npc.Experience = Convert.ToInt64(npc.Experience * npc.Map.ExperienceModifier);
                    }

                    #region NPC's Stat Adds
                    if (npc.Strength >= 16)
                    {
                        npc.strengthAdd = 1;

                        //melee
                        if (npc.IsPureMelee || npc.IsHybrid)
                        {
                            switch (npc.Level)
                            {
                                case 6:
                                case 7:
                                case 8:
                                    npc.strengthAdd += 1; break;
                                case 9:
                                case 10:
                                case 11:
                                    npc.strengthAdd += 2; break;
                                case 12:
                                case 13:
                                case 14:
                                    npc.strengthAdd += 3; break;
                                case 15:
                                case 16:
                                case 17:
                                    npc.strengthAdd += 4; break;
                                case 18:
                                case 19:
                                case 20:
                                    npc.strengthAdd += 5; break;
                                case 21:
                                case 22:
                                case 23:
                                    npc.strengthAdd += 6; break;
                                case 24:
                                case 25:
                                case 26:
                                    npc.strengthAdd += 7; break;
                                case 27:
                                case 28:
                                case 29:
                                    npc.strengthAdd += 8; break;
                                case 30:
                                case 31:
                                case 32:
                                    npc.strengthAdd += 9; break;
                                default:
                                    break;
                            }
                        }
                        //magic users
                        else
                        {
                            switch (npc.Level)
                            {
                                case 7:
                                case 8:
                                case 9:
                                case 10:
                                    npc.strengthAdd += 1; break;
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                    npc.strengthAdd += 2; break;
                                case 15:
                                case 16:
                                case 17:
                                case 18:
                                    npc.strengthAdd += 3; break;
                                case 19:
                                case 20:
                                case 21:
                                case 22:
                                    npc.strengthAdd += 4; break;
                                case 23:
                                case 24:
                                case 25:
                                case 26:
                                    npc.strengthAdd += 5; break;
                                case 27:
                                case 28:
                                case 29:
                                case 30:
                                    npc.strengthAdd += 6; break;
                                case 31:
                                case 32:
                                    npc.strengthAdd += 7; break;
                                default:
                                    break;
                            }
                        }
                    }
                    if (npc.Dexterity >= 16)
                    {
                        npc.dexterityAdd = 1;

                        //melee
                        if (npc.IsPureMelee || npc.IsHybrid)
                        {
                            switch (npc.Level)
                            {
                                case 6:
                                case 7:
                                case 8:
                                    npc.dexterityAdd += 1; break;
                                case 9:
                                case 10:
                                case 11:
                                    npc.dexterityAdd += 2; break;
                                case 12:
                                case 13:
                                case 14:
                                    npc.dexterityAdd += 3; break;
                                case 15:
                                case 16:
                                case 17:
                                    npc.dexterityAdd += 4; break;
                                case 18:
                                case 19:
                                case 20:
                                    npc.dexterityAdd += 5; break;
                                case 21:
                                case 22:
                                case 23:
                                    npc.dexterityAdd += 6; break;
                                case 24:
                                case 25:
                                case 26:
                                    npc.dexterityAdd += 7; break;
                                case 27:
                                case 28:
                                case 29:
                                    npc.dexterityAdd += 8; break;
                                case 30:
                                case 31:
                                case 32:
                                    npc.dexterityAdd += 9; break;
                                default:
                                    break;
                            }
                        }
                        //magic users
                        else
                        {
                            switch (npc.Level)
                            {
                                case 7:
                                case 8:
                                case 9:
                                case 10:
                                    npc.dexterityAdd += 1; break;
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                    npc.dexterityAdd += 2; break;
                                case 15:
                                case 16:
                                case 17:
                                case 18:
                                    npc.dexterityAdd += 3; break;
                                case 19:
                                case 20:
                                case 21:
                                case 22:
                                    npc.dexterityAdd += 4; break;
                                case 23:
                                case 24:
                                case 25:
                                case 26:
                                    npc.dexterityAdd += 5; break;
                                case 27:
                                case 28:
                                case 29:
                                case 30:
                                    npc.dexterityAdd += 6; break;
                                case 31:
                                case 32:
                                    npc.dexterityAdd += 7; break;
                                default:
                                    break;
                            }
                        }
                    }
                    #endregion

                    #region NPC's Items
                    #region Right Hand
                    if (npc.spawnRightHand.Length > 0 && npc.spawnRightHand != "0")
                    {
                        string[] righthandArray = npc.spawnRightHand.Split(" ".ToCharArray());
                        lootNum = Rules.dice.Next(righthandArray.Length);
                        npc.RightHand = Item.CopyItemFromDictionary(Convert.ToInt32(righthandArray[lootNum]));
                    } 
                    #endregion
                    #region Left Hand
                    if (npc.spawnLeftHand.Length > 0 && npc.spawnLeftHand != "0" && npc.RightHand == null || (npc.RightHand != null && npc.RightHand.skillType != Globals.eSkillType.Bow && npc.RightHand.skillType != Globals.eSkillType.Two_Handed))
                    {
                        string[] lefthandArray = npc.spawnLeftHand.Split(" ".ToCharArray());
                        rollOdds = Rules.RollD(1, 100);
                        if (rollOdds <= npc.spawnLeftHandOdds)
                        {
                            lootNum = Rules.dice.Next(lefthandArray.Length);
                            npc.LeftHand = Item.CopyItemFromDictionary(Convert.ToInt32(lefthandArray[lootNum]));
                        }
                    } 
                    #endregion
                    #region Armor
                    if (npc.spawnArmor.Length > 0 && npc.spawnArmor != "0")
                    {
                        string[] armorArray = npc.spawnArmor.Split(" ".ToCharArray());
                        lootNum = 0;
                        while (lootNum <= armorArray.Length - 1)
                        {
                            Item wItem = Item.CopyItemFromDictionary(Convert.ToInt32(armorArray[lootNum]));
                            if (wItem != null)
                            {
                                if (Globals.Max_Wearable[(int)wItem.wearLocation] > 1)
                                {
                                    if (npc.GetInventoryItem(wItem.wearLocation, Globals.eWearOrientation.Right) == null)
                                        wItem.wearOrientation = Globals.eWearOrientation.Right;
                                    else if (npc.GetInventoryItem(wItem.wearLocation, Globals.eWearOrientation.Left) == null)
                                        wItem.wearOrientation = Globals.eWearOrientation.Left;
                                    else wItem = null;
                                }

                                if(wItem != null)
                                    npc.WearItem(wItem);
                            }
                            lootNum++;
                        }
                    } 
                    #endregion	
                    if (npc.BaseProfession == ClassType.Knight && npc.Alignment == Globals.eAlignment.Lawful)
                    {
                        Item knightRing = Item.CopyItemFromDictionary(Item.ID_KNIGHTRING);
                        knightRing.attuneType = Globals.eAttuneType.None;
                        npc.RightRing1 = Item.CopyItemFromDictionary(Item.ID_KNIGHTRING);
                    }
                    #region Gold
                    if (npc.Gold > 0 && !npc.lairCritter)
                    {
                        gold = Item.CopyItemFromDictionary(Item.ID_COINS);
                        gold.coinValue = Rules.dice.Next((int)((int)npc.Gold / 2), (int)(npc.Gold) + 1);
                        npc.SackItem(gold);
                    } 
                    #endregion

                    //only non lair critters use very common, common, rare, very rare loot arrays (lair critters use these arrays in Map.loadLairLoot)
                    if (!npc.lairCritter)
                    {
                        #region Very Common Loot
                        if (npc.lootVeryCommonArray.Length > 0)
                        {
                            string[] lootArray = npc.lootVeryCommonArray.Split(" ".ToCharArray());
                            //if odds is -1, put all array items into sack
                            if (npc.lootVeryCommonOdds == -1)
                            {
                                lootNum = 0;
                                while (lootNum <= lootArray.Length - 1)
                                {
                                    Item itm = Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum]));
                                    npc.SackItem(itm);
                                    lootNum++;
                                }
                            }
                            //if odds is 100, put 1 array item into sack
                            else if (npc.lootVeryCommonOdds == 100)
                            {
                                lootNum = Rules.dice.Next(lootArray.Length);
                                npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                            }
                            //if odds > 0 use loot amount and odds to fill up the sack
                            else
                            {
                                while (npc.lootVeryCommonAmount > 0)
                                {
                                    rollOdds = Rules.RollD(1, 100);
                                    if (rollOdds <= npc.lootVeryCommonOdds)
                                    {
                                        lootNum = Rules.dice.Next(lootArray.Length);
                                        npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                                    }
                                    npc.lootVeryCommonAmount--;
                                }
                            }
                        } 
                        #endregion
                        #region Common Loot
                        if (npc.lootCommonArray.Length > 0)
                        {
                            string[] lootArray = npc.lootCommonArray.Split(" ".ToCharArray());
                            //if odds is -1, put all array items into sack 
                            if (npc.lootCommonOdds == -1)
                            {
                                lootNum = 0;
                                while (lootNum <= lootArray.Length - 1)
                                {
                                    Item itm = Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum]));
                                    npc.SackItem(itm);
                                    lootNum++;
                                }
                            }
                            //if odds is 100, put 1 array item into sack
                            else if (npc.lootCommonOdds == 100)
                            {
                                lootNum = Rules.dice.Next(lootArray.Length);
                                npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                            }
                            //if odds > 0 use loot amount and odds to fill up the sack
                            else
                            {
                                while (npc.lootCommonAmount > 0)
                                {
                                    rollOdds = Rules.RollD(1, 100);
                                    if (rollOdds <= npc.lootCommonOdds)
                                    {
                                        lootNum = Rules.dice.Next(lootArray.Length);
                                        npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                                    }
                                    npc.lootCommonAmount--;
                                }
                            }
                        } 
                        #endregion
                        #region Rare Loot
                        if (npc.lootRareArray.Length > 0)
                        {
                            String[] lootArray = npc.lootRareArray.Split(" ".ToCharArray());
                            //if odds is -1, put all array items into sack
                            if (npc.lootRareOdds == -1)
                            {
                                lootNum = 0;
                                while (lootNum <= lootArray.Length - 1)
                                {
                                    Item itm = Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum]));
                                    npc.SackItem(itm);
                                    Utils.Log(NPC.GetLogString(npc) + " has rare loot [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootRare);
                                    lootNum++;
                                }
                            }
                            //if odds is 100, put 1 array item into sack
                            else if (npc.lootRareOdds == 100)
                            {
                                lootNum = Rules.dice.Next(lootArray.Length);
                                npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                                Utils.Log(NPC.GetLogString(npc) + " has rare loot [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootRare);
                            }
                            //if odds > 0 use loot amount and odds to fill up the sack
                            else
                            {
                                while (npc.lootRareAmount > 0)
                                {
                                    rollOdds = Rules.RollD(1, 100);
                                    if (rollOdds <= npc.lootRareOdds)
                                    {
                                        lootNum = Rules.dice.Next(lootArray.Length);
                                        npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                                        Utils.Log(NPC.GetLogString(npc) + " has rare loot [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootRare);
                                    }
                                    npc.lootRareAmount--;
                                }
                            }
                        } 
                        #endregion
                        #region Very Rare Loot
                        if (npc.lootVeryRareArray.Length > 0)
                        {
                            String[] lootArray = npc.lootVeryRareArray.Split(" ".ToCharArray());
                            //if odds is -1, put all array items into sack
                            if (npc.lootVeryRareOdds == -1)
                            {
                                lootNum = 0;
                                while (lootNum <= lootArray.Length - 1)
                                {
                                    Item itm = Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum]));
                                    npc.SackItem(itm);
                                    Utils.Log(NPC.GetLogString(npc) + " has very rare loot [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootVeryRare);
                                    lootNum++;
                                }
                            }
                            //if odds is 100, put 1 array item into sack
                            else if (npc.lootVeryRareOdds == 100)
                            {
                                lootNum = Rules.dice.Next(lootArray.Length);
                                npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                                Utils.Log(NPC.GetLogString(npc) + " has very rare loot [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootVeryRare);
                            }
                            //if odds > 0 use loot amount and odds to fill up the sack
                            else
                            {
                                while (npc.lootVeryRareAmount > 0)
                                {
                                    rollOdds = Rules.RollD(1, 100);
                                    if (rollOdds <= npc.lootVeryRareOdds)
                                    {
                                        lootNum = Rules.dice.Next(lootArray.Length);
                                        npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                                        Utils.Log(NPC.GetLogString(npc) + " has very rare loot [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootVeryRare);
                                    }
                                    npc.lootVeryRareAmount--;
                                }
                            }
                        } 
                        #endregion
                    }
                    // all npcs use lootLair, lootAlways, lootBelt
                    #region Lair Loot
                    if (npc.lootLairArray.Length > 0)
                    {
                        string[] lootArray = npc.lootLairArray.Split(" ".ToCharArray());
                        //if odds is -1, put all array items into sack
                        if (npc.lootLairOdds == -1)
                        {
                            lootNum = 0;
                            while (lootNum <= lootArray.Length - 1)
                            {
                                Item itm = Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum]));
                                npc.SackItem(itm);
                                lootNum++;
                            }
                        }
                        // if odds is 100, put 1 array item into sack
                        else if (npc.lootLairOdds == 100)
                        {
                            lootNum = Rules.dice.Next(lootArray.Length);
                            npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                        }
                        // if odds > 0 use loot amount and odds to fill up the sack
                        else
                        {
                            while (npc.lootLairAmount > 0)
                            {
                                rollOdds = Rules.RollD(1, 100);
                                if (rollOdds <= npc.lootLairOdds)
                                {
                                    lootNum = Rules.dice.Next(lootArray.Length);
                                    npc.SackItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                                }
                                npc.lootLairAmount--;
                            }
                        }
                    } 
                    #endregion
                    #region Always Has Loot
                    if (npc.lootAlwaysArray.Length > 0)
                    {
                        string[] lootArray = npc.lootAlwaysArray.Split(" ".ToCharArray());

                        lootNum = 0;
                        while (lootNum <= lootArray.Length - 1)
                        {
                            Item itm = Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum]));
                            npc.SackItem(itm);
                            Utils.Log(NPC.GetLogString(npc) + " has always loot [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootAlways);
                            lootNum++;
                        }
                    } 
                    #endregion
                    #region Belt Loot
                    if (npc.lootBeltArray.Length > 0)
                    {
                        string[] lootArray = npc.lootBeltArray.Split(" ".ToCharArray());
                        //if odds is -1, put all array items onto belt
                        if (npc.lootBeltOdds == -1)
                        {
                            lootNum = 0;
                            while (lootNum <= lootArray.Length - 1)
                            {
                                Item itm = Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum]));
                                npc.BeltItem(itm);
                                Utils.Log(NPC.GetLogString(npc) + " has belt item [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootBelt);
                                lootNum++;
                            }
                        }
                        //if odds is 100, put 1 array item onto belt
                        else if (npc.lootBeltOdds == 100)
                        {
                            lootNum = Rules.dice.Next(lootArray.Length);
                            npc.BeltItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                            Utils.Log(NPC.GetLogString(npc) + " has belt item [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootBelt);
                        }
                        //if odds > 0 use loot amount and odds to fill up the belt
                        else
                        {
                            while (npc.lootBeltAmount > 0)
                            {
                                rollOdds = Rules.RollD(1, 100);
                                if (rollOdds <= npc.lootBeltOdds)
                                {
                                    bool wearThis = true;
                                    lootNum = Rules.dice.Next(lootArray.Length);
                                    Item bItem = Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum]));
                                    foreach (Item wItem in npc.wearing)
                                    {
                                        if (bItem.wearLocation == wItem.wearLocation) { wearThis = false; }
                                    }
                                    if (bItem.wearLocation == Globals.eWearLocation.None) { wearThis = false; }
                                    rollOdds = Rules.RollD(1, 100);
                                    if (wearThis && rollOdds <= 74)
                                    {
                                        npc.WearItem(bItem);
                                        Utils.Log(NPC.GetLogString(npc) + " wears belt item [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootBelt);
                                    }
                                    else
                                    {
                                        npc.BeltItem(Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])));
                                        Utils.Log(NPC.GetLogString(npc) + " has belt item [" + lootArray[lootNum] + "] " + Item.CopyItemFromDictionary(Convert.ToInt32(lootArray[lootNum])).notes + ".", Utils.LogType.LootBelt);
                                    }
                                }
                                npc.lootBeltAmount--;
                            }
                        }
                    } 
                    #endregion
                    #endregion

                    #region NPC's Resistances
                    //set npc resistance
                    if (npc.Level >= 3)
                    {
                        switch (npc.BaseProfession)
                        {
                            //TODO: fd, id, ld (immunities)
                            case ClassType.Fighter:
                                npc.StunResistance = 2;
                                break;
                            case ClassType.Knight:
                                npc.DeathResistance = 2;
                                break;
                            case ClassType.Martial_Artist:
                                npc.LightningResistance = 2;
                                break;
                            case ClassType.Thaumaturge:
                                npc.BlindResistance = 1;
                                npc.FearResistance = 1;
                                break;
                            case ClassType.Thief:
                                npc.PoisonResistance = 2;
                                break;
                            case ClassType.Wizard:
                                npc.FireResistance = 1;
                                npc.ColdResistance = 1;
                                break;
                            default:
                                break;
                        }
                        if (npc.Level > 3)
                        {
                            int addResistance = npc.Level - 3;
                            npc.FireResistance += addResistance;
                            npc.ColdResistance += addResistance;
                            npc.LightningResistance += addResistance;
                            npc.DeathResistance += addResistance;
                            npc.BlindResistance += addResistance;
                            npc.FearResistance += addResistance;
                            npc.StunResistance += addResistance;
                            npc.PoisonResistance += addResistance;
                        }
                    }
                    #endregion

                    #region NPC's Random Value Items
                    //set random coin values for npc sack items
                    foreach (Item sItem in npc.sackList)
                    {
                        if (sItem.vRandLow > 0)
                        {
                            sItem.coinValue = Rules.dice.Next(sItem.vRandLow, sItem.vRandHigh);
                        }
                    }
                    foreach (Item bItem in npc.beltList)
                    {
                        if (bItem.vRandLow > 0)
                        {
                            bItem.coinValue = Rules.dice.Next(bItem.vRandLow, bItem.vRandHigh);
                        }
                    }
                    //do the npcs effects, specials and random coin values for worn items
                    foreach (Item wItem in npc.wearing)
                    {
                        if (wItem.vRandLow > 0)
                        {
                            wItem.coinValue = Rules.dice.Next(wItem.vRandLow, wItem.vRandHigh);
                        }
                        if (wItem.effectType.Length > 0)
                        {
                            Effect.AddWornEffectToCharacter(npc, wItem);
                        }
                    }
                    #endregion

                    #region Add Effects
                    #region Add Held Item Effects
                    if (npc.RightHand != null && npc.RightHand.effectType.Length > 0 && npc.RightHand.wearLocation == Globals.eWearLocation.None)
                    {
                        Effect.AddWornEffectToCharacter(npc, npc.RightHand);
                    }
                    if (npc.LeftHand != null && npc.LeftHand.effectType.Length > 0 && npc.LeftHand.wearLocation == Globals.eWearLocation.None)
                    {
                        Effect.AddWornEffectToCharacter(npc, npc.LeftHand);
                    }
                    #endregion

                    #region Add Ring Effects
                    if (npc.RightRing1 != null && npc.RightRing1.effectType.Length > 0)
                    {
                        Effect.AddWornEffectToCharacter(npc, npc.RightRing1);
                    }
                    if (npc.RightRing2 != null && npc.RightRing2.effectType.Length > 0)
                    {
                        Effect.AddWornEffectToCharacter(npc, npc.RightRing2);
                    }
                    if (npc.RightRing3 != null && npc.RightRing3.effectType.Length > 0)
                    {
                        Effect.AddWornEffectToCharacter(npc, npc.RightRing3);
                    }
                    if (npc.RightRing4 != null && npc.RightRing4.effectType.Length > 0)
                    {
                        Effect.AddWornEffectToCharacter(npc, npc.RightRing4);
                    }

                    if (npc.LeftRing1 != null && npc.LeftRing1.effectType.Length > 0)
                    {
                        Effect.AddWornEffectToCharacter(npc, npc.LeftRing1);
                    }
                    if (npc.LeftRing2 != null && npc.LeftRing2.effectType.Length > 0)
                    {
                        Effect.AddWornEffectToCharacter(npc, npc.LeftRing2);
                    }
                    if (npc.LeftRing3 != null && npc.LeftRing3.effectType.Length > 0)
                    {
                        Effect.AddWornEffectToCharacter(npc, npc.LeftRing3);
                    }
                    if (npc.LeftRing4 != null && npc.LeftRing4.effectType.Length > 0)
                    {
                        Effect.AddWornEffectToCharacter(npc, npc.LeftRing4);
                    }
                    #endregion

                    if (npc.BaseProfession == ClassType.Knight)
                    {
                        if (npc.knightRing)
                        {
                            npc.Mana = npc.ManaMax;
                        }
                    }

                    #region Add Worn Effects
                    foreach (Item wItem in npc.wearing)
                    {
                        try
                        {
                            if (wItem.effectType.Length > 0)
                            {
                                Effect.AddWornEffectToCharacter(npc, wItem);
                            }
                        }
                        catch (Exception e)
                        {
                            Utils.LogException(e);
                        }
                    }
                    #endregion 
                    #endregion

                    if (npc.BaseProfession == ClassType.Fighter && npc.Level >= 9 &&
                        npc.RightHand != null && (npc.HasRandomName || npc.race != "") &&
                        Rules.CheckPerception(npc))
                    {
                        npc.fighterSpecialization = npc.RightHand.skillType;
                    }

                    if (npc.HitsMax <= 0)
                    {
                        npc.HitsMax = 5 * npc.Level;
                        npc.HitsMax += Rules.GetHitsGain(npc, npc.Level);
                    }

                    npc.Hits = npc.HitsMax;

                    if (npc.ManaMax <= 0 && npc.IsSpellUser)
                    {
                        npc.ManaMax = Rules.GetManaGain(npc, npc.Level);
                    }

                    npc.Mana = npc.ManaMax;

                    if (npc.StaminaMax <= 0)
                    {
                        npc.StaminaMax = Rules.GetStaminaGain(npc, npc.Level);
                    }

                    npc.Stamina = npc.StaminaMax;

                    npc.SpawnZoneID = spawnzoneid;
                    npc.AddToWorld();

                    #region Lair Critter Loot and Cell Flagging
                    //if this is a lair critter let's take care of it's lair loot and lair cell flagging
                    if (npc.lairCritter)
                    {
                        npc.lairXCord = npc.X;
                        npc.lairYCord = npc.Y;
                        npc.lairZCord = npc.Z;

                        if (npc.lairCells.Length > 0)
                        {
                            string[] lc = npc.lairCells.Split(" ".ToCharArray());
                            foreach (string lcell in lc)
                            {
                                string[] lc2 = lcell.Split("|".ToCharArray());
                                int lx = Convert.ToInt32(lc2[0]);
                                int ly = Convert.ToInt32(lc2[1]);
                                Cell lairCell = Cell.GetCell(npc.FacetID, npc.LandID, npc.MapID, lx, ly, npc.Z);
                                if (lairCell != null)
                                {
                                    lairCell.IsLair = true;
                                    npc.lairCellsList.Add(lairCell);
                                }
                            }

                            if (npc.lootVeryCommonArray.Length > 0 && npc.lootCommonArray.Length > 0 &&
                                npc.lootRareArray.Length > 0 && npc.lootVeryRareArray.Length > 0)
                            {
                                string[] lairVeryCommonLoot = npc.lootVeryCommonArray.Split(" ".ToCharArray());
                                string[] lairCommonLoot = npc.lootCommonArray.Split(" ".ToCharArray());
                                string[] lairRareLoot = npc.lootRareArray.Split(" ".ToCharArray());
                                string[] lairVeryRareLoot = npc.lootVeryRareArray.Split(" ".ToCharArray());
                                //load the npc's lair loot
                                Map.LoadLairLoot(npc, lairVeryCommonLoot, lairCommonLoot, lairRareLoot, lairVeryRareLoot);
                            }
                        }
                    }
                    #endregion
                    
                    // npc.npcTimer.Start();
                    return npc;
                }
                catch (Exception e)
                {
                    Utils.Log("Failure at NPC.LoadNPC(NPCnum: " + npcID + ", land: " + landID + ", map: " + mapID + ", xcord: " + xcord + ", ycord: " + ycord + ", spawnzoneid: " + spawnzoneid + ")", Utils.LogType.SystemFailure);
                    Utils.LogException(e);
                    return null;
                }
            }
        }
        public static void ChantDemonSummon(DemonName demonName, Character ch)
        {
            NPC demon = null;
            Cell hellCell = null;
            demon = findNPCinWorld(demonName.ToString());
            #region // Check to see if the demon is in the world and if the player is in hell
            if (demon == null || demon.MapID != World.MAP_HELL)
            {
                ch.WriteToDisplay("You hear a voice in your head: 'Not now i'm busy.'");
                return;
            }
            if (ch.MapID == World.MAP_HELL)
            {
                ch.WriteToDisplay(demonName.ToString() + " refuses your call.");
                return;
            }
            #endregion
            #region // Now we do a wisdom check to see if the demon responds
            if (!Rules.FullStatCheck(ch, "Wisdom"))
            {
                ch.WriteToDisplay("You feel a sense of dread pass over you.");
                return;
            }
            #endregion
            #region // Now we do a Charisma check to see if the demon tries to pull the summoner to hell
            if (Rules.RollD(1, 20) < demon.Charisma - ch.Charisma)
            {
                // We are going to attempt to pull the summoner to Hell
                int chance = demon.Wisdom; // based on the demons wisdom
                //Utils.Log(ch.Name + " has a chance to go to Hell.", Utils.LogType.Unknown);
                if (Rules.dice.Next(1, 100) < chance)
                {
                    // move the character to Hell
                    switch (ch.BaseProfession)
                    {
                        case ClassType.Thief:
                            hellCell = Cell.GetCell(ch.FacetID, World.LAND_UW, World.MAP_HELL,
                                World.GetFacetByID(ch.FacetID).GetLandByID(World.LAND_UW).GetMapByID(World.MAP_HELL).ThiefResX,
                                World.GetFacetByID(ch.FacetID).GetLandByID(World.LAND_UW).GetMapByID(World.MAP_HELL).ThiefResY,
                                World.GetFacetByID(ch.FacetID).GetLandByID(World.LAND_UW).GetMapByID(World.MAP_HELL).ThiefResZ);
                            break;
                        default:
                            hellCell = Cell.GetCell(ch.FacetID, World.LAND_UW, World.MAP_HELL,
                                World.GetFacetByID(ch.FacetID).GetLandByID(World.LAND_UW).GetMapByID(World.MAP_HELL).ResX,
                                World.GetFacetByID(ch.FacetID).GetLandByID(World.LAND_UW).GetMapByID(World.MAP_HELL).ResY,
                                World.GetFacetByID(ch.FacetID).GetLandByID(World.LAND_UW).GetMapByID(World.MAP_HELL).ResZ);
                            break;
                    }
                    if (ch.Alignment == Globals.eAlignment.Evil) // override for evil characters
                    {
                        hellCell = Cell.GetCell(ch.FacetID, World.LAND_UW, World.MAP_HELL,
                                World.GetFacetByID(ch.FacetID).GetLandByID(World.LAND_UW).GetMapByID(World.MAP_HELL).KarmaResX,
                                World.GetFacetByID(ch.FacetID).GetLandByID(World.LAND_UW).GetMapByID(World.MAP_HELL).KarmaResY,
                                World.GetFacetByID(ch.FacetID).GetLandByID(World.LAND_UW).GetMapByID(World.MAP_HELL).KarmaResZ);
                    }
                    ch.WriteToDisplay("The world goes black around you.");
                    ch.SendToAllInSight(ch.Name + " is consumed by a pillar of fire!");
                    ch.CurrentCell = hellCell;
                    ch.WriteToDisplay("You hear a voice in your head: 'Welcome to hell, mortal.'");
                    return;
                }
                ch.WriteToDisplay("You see a river of flames flash before your eyes.");
                return;
            }
            #endregion
            #region // Bring forth the demon to the mortal plane
            demon.lairCells = demon.CurrentCell.X + "|" + demon.CurrentCell.Y + "|" + demon.CurrentCell.Z; // temporary storage
            demon.CurrentCell = ch.CurrentCell;
            demon.aiType = AIType.Summoned;
            demon.species = Globals.eSpecies.Demon;
            demon.RoundsRemaining = Rules.RollD(2, 8) * 2;
            demon.IsSummoned = true;
            demon.special = "despawn";
            demon.Age = 0;
            switch (demonName)
            {
                case DemonName.Perdurabo:
                case DemonName.Samael:
                    demon.Alignment = Globals.eAlignment.Amoral;
                    demon.account = demon.Name; // temporary storage of the demon's name
                    demon.Name = "demon";
                    break;
                case DemonName.Glamdrang:
                    demon.account = demon.Name;
                    demon.Name = "ghoul";
                    demon.Alignment = Globals.eAlignment.Chaotic;
                    break;
                default:
                    demon.Alignment = Globals.eAlignment.Evil;
                    demon.account = demon.Name;
                    demon.Name = "demon";
                    break;
            }
            ch.SendToAllInSight("A rift in the ground opens up near " + ch.Name + ", spilling forth fire and ash.");
            ch.WriteToDisplay("A rift in the ground opens up spilling forth fire and ash.");
            #endregion
        }
        public static NPC findNPCinWorld(string npcName)
        {
            foreach (Character chr in Character.NPCList)
            {
                if (chr.Name.ToLower() == npcName.ToLower() || chr.Name.ToLower().StartsWith(npcName.ToLower()))
                {
                    return (NPC)chr;
                }
            }
            return null;
        }

		// Overrides for NPC commands npc.canCommand must be true
        public override void merchantFollow(Character chr, string args)
        {
            if (chr.Alignment != this.Alignment)
            {
                return;
            }
            if (this.canCommand == false)
            {
                return;
            }
            if (args == "me")
            {
                if (this.FollowName == "")
                {
                    this.FollowName = chr.Name;
                    return;
                }
                this.SendToAllInSight(this.Name + ": I am already following " + this.FollowName + ".");
            }
            else
            {
                if (chr.Name == this.FollowName)
                {
                    this.FollowName = args;
                    return;
                }
            }
            return;
        }

		public override void merchantClimb(Character chr)
		{
			string cellgraphic = this.CurrentCell.CellGraphic;
			if(this.Alignment != chr.Alignment) {return;}
			if(this.canCommand==false)
			{
				return;
			}
			if(!this.IsMobile)
			{
				chr.WriteToDisplay(this.Name+": I don't know how to do that.");
				return;
			}
			switch(cellgraphic)
			{
				case "cd":
					Command.ParseCommand(this,"climb","d");
					break;
				case "cu":
					Command.ParseCommand(this,"climb","up");
					break;
				case "up":
					Command.ParseCommand(this,"up","");
					break;
				case "dn":
					Command.ParseCommand(this,"down","");
					break;
				case "()":
					Command.ParseCommand(this,"climb","d");
					break;
				case "pb":
					Command.ParseCommand(this,"climb","up");
					break;
				default:
					chr.WriteToDisplay(this.Name+": I don't see anything to climb here.");
					break;
			}
			return;
		}
        public override void merchantGo(Character chr, string args)
        {
            if (!this.canCommand)
            {
                return;
            }
            return;
        }

        public override void merchantAttack(Character chr, string args)
        {
            if (!this.canCommand)
            {
                return;
            }
            this.TargetName = args;
            return;
        }

        public bool HasManaAvailable(int spellID)
        {
            if (this.castMode == CastMode.Unlimited)
            {
                return true;
            }

            if (this.Mana >= Spell.GetSpell(spellID).ManaCost)
            {
                return true;
            }

            return false;
        }

        public bool HasManaAvailable(string spellCommand)
        {
            if (this.castMode == CastMode.Unlimited)
            {
                return true;
            }

            if (this.Mana >= Spell.GetSpell(spellCommand).ManaCost)
            {
                return true;
            }

            return false;
        }

        public bool IsHealer() // returns true if this npc can heal itself and others
        {
            if (this.aiType == AIType.Enforcer)
            {
                return true;
            }

            return this.spellList.SpellIDExists(Spell.GetSpell("cure").SpellID); // since the cure spell is currently our only healing spell...
            
        }

        public static string GetLogString(NPC npc)
        {
            if (npc.CurrentCell == null)
            {
                return "[NpcID: " + npc.npcID + " | WorldNpcID: "+npc.worldNpcID+"] " + npc.Name + " [" + Utils.FormatEnumString(npc.Alignment.ToString()) + " " + Utils.FormatEnumString(npc.BaseProfession.ToString()) + "(" + npc.Level + ")]";
            }
            else
            {
                return "[NpcID: " + npc.npcID + " | WorldNpcID: " + npc.worldNpcID + "] " + npc.Name + " [" + Utils.FormatEnumString(npc.Alignment.ToString()) + " " + Utils.FormatEnumString(npc.BaseProfession.ToString()) + "(" + npc.Level + ")] (" + World.GetFacetByIndex(0).GetLandByID(npc.LandID).ShortDesc +
                " - " + World.GetFacetByID(npc.FacetID).GetLandByID(npc.LandID).GetMapByID(npc.MapID).Name + " " + npc.X + ", " + npc.Y + ", " + npc.Z + ")";
            }
        }

        public Globals.eSkillType GetBestSkill(Object[] skillTypes) // retrieve the best skillType out of a designated skillType array
        {
            Globals.eSkillType bestSkill = Globals.eSkillType.None;
            for (int a = 0; a < skillTypes.Length; a++)
            {
                if (Skills.GetSkillLevel(this.GetSkillExperience((Globals.eSkillType)skillTypes[a])) >
                    Skills.GetSkillLevel(this.GetSkillExperience(bestSkill)))
                {
                    bestSkill = (Globals.eSkillType)skillTypes[a];
                }
            }
            return bestSkill;
        }

        public override void doAI()
        {
            lock (lockObject)
            {
                if (this.IsDead) return;

                //if (this.aiType == AIType.Summoned)
                //{
                //    if (this.Age > Convert.ToInt32(this.RoundsRemaining))
                //    {
                //        Rules.UnsummonCreature(this);
                //        return;
                //    }
                //}

                ArtificialIntel.CreateContactList(this);
            }
        }

        public static bool PrepareSpell(NPC npc, ArtificialIntel.Priority pri) // creature spell selection
        {
            #region Priority.Heal
            if (pri == ArtificialIntel.Priority.Heal)
            {
                if (npc.castMode == CastMode.Unlimited)
                {
                    npc.preppedSpell = Spell.GetSpell("cure");
                    return true;
                }
                else
                {
                    if (npc.HasManaAvailable(Spell.GetSpell("cure").SpellID))
                    {
                        if (npc.castMode == CastMode.NoPrep)
                        {
                            npc.preppedSpell = Spell.GetSpell("cure");
                            return true;
                        }
                        else if (npc.castMode == CastMode.Limited)
                        {
                            string[] chant = npc.spellList.GetString(npc.spellList.IndexOf(Spell.GetSpell("cure").SpellID)).Split(" ".ToCharArray());
                            Command.ParseCommand(npc, chant[0], chant[1] + " " + chant[2] + " " + chant[3]);
                            return true;
                        }
                    }
                }
            }
            #endregion

            #region Priority.RaiseDead
            else if (pri == ArtificialIntel.Priority.RaiseDead)
            {
                if (npc.castMode == CastMode.Unlimited)
                {
                    npc.preppedSpell = Spell.GetSpell("raisedead");
                    return true;
                }
                else
                {
                    if (npc.HasManaAvailable(Spell.GetSpell("raisedead").SpellID))
                    {
                        if (npc.castMode == CastMode.NoPrep)
                        {
                            npc.preppedSpell = Spell.GetSpell("raisedead");
                            return true;
                        }
                        else if (npc.castMode == CastMode.Limited)
                        {
                            string[] chant = npc.spellList.GetString(npc.spellList.IndexOf(Spell.GetSpell("raisedead").SpellID)).Split(" ".ToCharArray());
                            Command.ParseCommand(npc, chant[0], chant[1] + " " + chant[2] + " " + chant[3]);
                            return true;
                        }
                    }
                }
            }
            #endregion

            else if (pri >= ArtificialIntel.Priority.PrepareSpell) // we're preparing a spell
            {
                #region mostHated != null
                if (npc.mostHated != null)
                {
                    if (npc.aiType == AIType.Enforcer) // AI Enforcer
                    {
                        npc.preppedSpell = Spell.GetSpell("death");
                        return true;
                    }

                    switch (npc.species)
                    {
                        case Globals.eSpecies.FireDragon:
                        case Globals.eSpecies.IceDragon:
                            npc.preppedSpell = Spell.GetSpell("drbreath");
                            return true;
                        case Globals.eSpecies.LightningDrake:
                            npc.preppedSpell = Spell.GetSpell("lightning");
                            return true;
                        case Globals.eSpecies.TundraYeti:
                            npc.preppedSpell = Spell.GetSpell("blizzard");
                            return true;
                        case Globals.eSpecies.Unknown:
                        default:
                            if (npc.animal)
                            {
                                if (npc.spellList.Count > 0)
                                {
                                    npc.preppedSpell = Spell.GetSpell((int)npc.spellList.ints[Rules.dice.Next(0, npc.spellList.Count - 1)]);
                                    return true;
                                }
                            }
                            break;
                    }

                    int distance = Cell.GetCellDistance(npc.X, npc.Y, npc.mostHated.X, npc.mostHated.Y);

                    int chosenSpellID;

                    string[] chant; // chant used to warm the AI's chosen spell

                    switch (npc.BaseProfession)
                    {
                        case ClassType.Thaumaturge: // thaums typically want to cast either evocation or alteration non beneficial
                            #region if target is stunned, blind, or feared choose an evocation spell
                            if (npc.mostHated.Stunned > 0 || npc.mostHated.IsFeared || npc.mostHated.IsBlind)
                            {
                                goto evocation;
                            }
                            #endregion

                            #region if target is in my cell try a harmful alteration spell (eg: blind, fear, stun)
                            if (distance == 0 &&  // if the enemy is next to us try to alter it (eg: fear) 
                                (npc.mostHated.Stunned <= 0 && !npc.mostHated.IsFeared && !npc.mostHated.IsBlind))
                            {
                                goto alteration;
                            }
                            #endregion

                        evocation:
                            if (npc.evocationSpells.Count > 0)
                            {
                                chosenSpellID = (int)npc.evocationSpells.ints[Rules.dice.Next(0, npc.evocationSpells.Count - 1)];
                                if (!npc.HasManaAvailable(chosenSpellID))  // could not find a suitable evocation spell
                                {
                                    goto alteration;
                                }
                                #region only cast Turn Undead if target is undead and I am not undead
                                if (chosenSpellID == Spell.GetSpell("turnundead").SpellID)
                                {
                                    if (npc.IsUndead || !npc.mostHated.IsUndead) { goto evocation; }
                                }
                                #endregion

                                #region only cast Banish if the target is summoned
                                if (chosenSpellID == Spell.GetSpell("banish").SpellID)
                                {
                                    if (!npc.mostHated.IsSummoned) { goto evocation; }
                                }
                                #endregion

                                #region choose Death over Curse
                                if (chosenSpellID == Spell.GetSpell("curse").SpellID)
                                {
                                    if (npc.evocationSpells.IndexOf(Spell.GetSpell("death").SpellID) != -1 && npc.HasManaAvailable(Spell.GetSpell("death").SpellID))
                                    {
                                        chosenSpellID = Spell.GetSpell("death").SpellID;
                                    }
                                }
                                #endregion

                                chant = npc.evocationSpells.GetString(npc.evocationSpells.IndexOf(chosenSpellID)).Split(" ".ToCharArray());
                                Command.ParseCommand(npc, chant[0], chant[1] + " " + chant[2] + " " + chant[3]);
                                return true;
                            }
                        alteration:
                            if (npc.alterationHarmfulSpells.Count > 0)
                            {
                                chosenSpellID = (int)npc.alterationHarmfulSpells.ints[Rules.dice.Next(0, npc.alterationHarmfulSpells.Count - 1)];
                                if (!npc.HasManaAvailable(chosenSpellID)) { return false; } // could not find a suitable harmful alteration spell
                                chant = npc.alterationHarmfulSpells.GetString(npc.alterationHarmfulSpells.IndexOf(chosenSpellID)).Split(" ".ToCharArray());
                                Command.ParseCommand(npc, chant[0], chant[1] + " " + chant[2] + " " + chant[3]);
                                return true;
                            }
                            break;
                        case ClassType.Wizard:
                            // all of our wizards have at least one evocation direct damage and area effect spell
                            #region Do not cast an area effect spell if an ally is in the area of effect
                            for (int a = 0; a < npc.friendList.Count; a++) // if a friend is near our mostHated, don't fry 'em
                            {
                                Character ally = npc.friendList[a];
                                if (Cell.GetCellDistance(ally.X, ally.Y, npc.mostHated.X, npc.mostHated.Y) <= 1)
                                {
                                    goto directDamage;
                                }
                            }
                            #endregion

                            #region If target is more than 1 cell away choose an area effect spell
                            if (distance > 1 && npc.evocationAreaEffectSpells.Count > 0)
                            {
                                chosenSpellID = (int)npc.evocationAreaEffectSpells.ints[Rules.dice.Next(0, npc.evocationAreaEffectSpells.Count - 1)];
                                if (!npc.HasManaAvailable(chosenSpellID)) { goto directDamage; }
                                chant = npc.evocationAreaEffectSpells.GetString(npc.evocationAreaEffectSpells.IndexOf(chosenSpellID)).Split(" ".ToCharArray());
                                Command.ParseCommand((Character)npc, chant[0], chant[1] + " " + chant[2] + " " + chant[3]);
                                return true;
                            }
                            #endregion

                        directDamage:
                            if (npc.evocationSpells.Count > 0)
                            {
                                chosenSpellID = (int)npc.evocationSpells.ints[Rules.dice.Next(0, npc.evocationSpells.Count - 1)];
                                if (!npc.HasManaAvailable(chosenSpellID)) { return false; }
                                chant = npc.evocationSpells.GetString(npc.evocationSpells.IndexOf(chosenSpellID)).Split(" ".ToCharArray());
                                Command.ParseCommand((Character)npc, chant[0], chant[1] + " " + chant[2] + " " + chant[3]);
                                return true;
                            }
                            break;
                    }
                }
                #endregion
                else
                {
                    if (npc.buffSpellCommand.Length > 0)
                    {
                        if (npc.castMode == CastMode.NoPrep)
                        {
                            npc.preppedSpell = Spell.GetSpell(npc.buffSpellCommand);
                            return true;
                        }

                        Spell buffSpell = null;

                        foreach (int spellID in npc.spellList.ints)
                        {
                            buffSpell = Spell.GetSpell(spellID);
                            if (buffSpell.SpellCommand == npc.buffSpellCommand)
                            {
                                string[] chant = npc.spellList.GetString(npc.spellList.IndexOf(buffSpell.SpellID)).Split(" ".ToCharArray());
                                Command.ParseCommand((Character)npc, chant[0], chant[1] + " " + chant[2] + " " + chant[3]);
                                //Utils.Log(ch.GetLogString() + " warms a " + buffSpell.Name + " spell.", Utils.LogType.SystemWarning);
                                npc.buffSpellCommand = "";
                                return true;
                            }
                        }
                        npc.buffSpellCommand = "";
                    }

                }
            }
            return false;
        }

        public void doAIMove()
        {
            if (!this.IsMobile) { return; }
            this.MoveList.Clear();
            this.localCells.Clear();
            if (this.debug > 4)
            {
                return;
            }
            try
            {
                if (this.CurrentCell == null)
                {
                    Utils.Log("CurrentCell is null in Creature.doAIMove for " + this.GetLogString(), Utils.LogType.Unknown);
                }

                List<Cell> cList = Map.GetAdjacentCells(this.CurrentCell, this);
                if (cList != null)
                {
                    int rand = Rules.dice.Next(cList.Count);
                    Cell nCell = (Cell)cList[rand];
                    this.AIGotoXYZ(nCell.X, nCell.Y, nCell.Z);
                }
            }
            catch (Exception e)
            {
                Utils.Log("Error on doAIMove()", Utils.LogType.Unknown);
                Utils.LogException(e);
                return;
            }
            return;
        }

        public void DoNextListMove() // process the next available move in the creature's movelist
        {
            // Handle one directional move at a time due to 
            // current interpret command code
            if (this.debug > 4)
            {
                return;
            }
            for (int i = 0; i < this.Speed && MoveList.Count != 0; i++)
            {
                if (MoveList[0] == null || MoveList[0] == "")
                {
                    break;
                }

                Command.ParseCommand(this, (string)MoveList[0], "");

                if (MoveList.Count > 0)
                {
                    MoveList.RemoveAt(0);

                    if (MoveList.Count == 0)
                    {
                        try
                        {
                            if ((!this.animal && !this.IsUndead && !this.IsSpectral && this.mostHated == null && Rules.CheckPerception(this)) || this.HasPatrol)
                            {
                                Cell cell = null;

                                for (int xpos = -1; xpos <= 1; xpos++)
                                {
                                    for (int ypos = -1; ypos <= 1; ypos++)
                                    {
                                        if (Cell.CellRequestInBounds(this.CurrentCell, xpos, ypos))
                                        {
                                            cell = Cell.GetCell(this.FacetID, this.LandID, this.MapID, this.X + xpos, this.Y + ypos, this.Z);

                                            if (cell != null)
                                            {
                                                if (cell.IsOpenDoor &&
                                                    Cell.GetCellDistance(this.X, this.Y, cell.X, cell.Y) == 1 &&
                                                    cell.Items.Count == 0 &&
                                                    cell.Characters.Count == 0)
                                                {
                                                    Command.ParseCommand(this, "close", "door " + Map.GetDirection(this.CurrentCell, cell).ToString().ToLower());
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Utils.LogException(e);
                            return;
                        }
                    }
                }
            }
        }

        public void AIGotoNewZ(int x, int y, int z)
        {
            Cell newZCell = Cell.GetCell(this.FacetID, this.LandID, this.MapID, x, y, z);

            if (newZCell != null)
            {
                this.CurrentCell = newZCell;
            }
        }

        public void AIGotoXYZ(int x, int y, int z)
        {
            this.debug++;
            if (this.debug > 4)
            {
                return; // dont let things call this function more than 4 times a round.
            }
            if (this.X == x && this.Y == y && this.Z == z) { return; } // bail out if we aren't going anywhere

            if (this.MoveList.Count <= 0)
            {
                if (this.BuildMoveList(x, y, z)) // search for target and build a move list
                {
                    this.DoNextListMove();
                }
                else
                {
                    this.doAIMove(); // search algorithm was unable to reach target so do something random
                }
            }
            else
            {
                this.DoNextListMove();
            }
        }

        /// <summary>
        /// Uses a breadth-first search algorithm to build a list of 
        /// moves to take the Creature from it's current location 
        /// to (x,y).  Called by Creature.AIGotoXYZ()
        /// </summary>
        /// <param name="ch">the Creature to move</param>
        /// <param name="x">target x coord</param>
        /// <param name="y">target y coord</param>
        public bool BuildMoveList(int x, int y, int z)
        {
            int xMax = World.GetFacetByID(this.FacetID).GetLandByID(this.LandID).GetMapByID(this.MapID).XCordMax[this.Z];
            int yMax = World.GetFacetByID(this.FacetID).GetLandByID(this.LandID).GetMapByID(this.MapID).YCordMax[this.Z];
            int xMin = World.GetFacetByID(this.FacetID).GetLandByID(this.LandID).GetMapByID(this.MapID).XCordMin[this.Z];
            int yMin = World.GetFacetByID(this.FacetID).GetLandByID(this.LandID).GetMapByID(this.MapID).YCordMin[this.Z];

            Point origin = new Point(this.X, this.Y);
            Point target = new Point(x, y);
            Point pos, current;
            int cost;
            int lowest_cost;

            List<Point> unfinished = new List<Point>();
            List<string> moves = new List<string>();
            // the hashtable values are cell cost value
            Hashtable travelled = new Hashtable();
            travelled[origin] = 0;

            pos = current = origin;
            while (pos != target && current != null)
            {
                foreach (Point neighbor in get_neighbor_pos(this.Map.cells, current, xMax, yMax, xMin, yMin))
                {
                    try
                    {
                        if (this.Map.cells.ContainsKey(neighbor.x + "," + neighbor.y + "," + z))
                        {
                            cost = this.GetCellCost(this.Map.cells[neighbor.x + "," + neighbor.y + "," + z]);
                        }
                        else
                        {
                            cost = 10000;
                        }
                    }
                    catch (Exception)
                    {
                        Utils.Log("Error in BuildMoveList(int x, int y, int z) in Creature.cs line 704 : var cost", Utils.LogType.Exception);
                        continue;
                    }
                    if (cost >= 10000)
                    {
                        continue;
                    }

                    pos = neighbor;

                    int v;
                    if (travelled.Contains(pos))
                    {
                        v = (int)travelled[pos];
                    }
                    else
                    {
                        v = 0;
                    }

                    if (0 == v && pos != origin)
                    {
                        travelled[pos] = 1 + (int)travelled[current] + cost;
                        if (pos == target) break;
                        unfinished.Add(pos);
                    }
                }
                if (pos != target)
                {
                    try
                    {
                        if (unfinished.Count > 0)
                        {
                            current = unfinished[0];
                            unfinished.RemoveAt(0);
                        }
                        else
                        {
                            current = null;
                        }
                    }
                    catch
                    {
                        // No unfinished cells
                        current = null;
                    }
                }
            }

            current = pos;
            if (current != target)
            {
                // Target was unreachable!
                return false;
            }

            // now begin building the MoveList using the cost values
            while (current != origin)
            {
                lowest_cost = 100;
                Point best_point = null;
                foreach (Point neighbor in get_neighbor_pos(this.Map.cells, current, xMax, yMax, xMin, yMin))
                {
                    int v;
                    pos = neighbor;
                    if (!travelled.ContainsKey(pos))
                    {
                        continue;
                    }

                    v = (int)travelled[pos];
                    if (v < lowest_cost)
                    {
                        lowest_cost = v;
                        best_point = pos;
                    }
                }
                if (best_point == null) { break; }
                moves.Add(this.GetDirString(best_point, current));
                current = best_point;
            }

            moves.Reverse();
            this.MoveList = moves;
            if (this.MoveList.Count <= 0)
            {
                return false; // Catch for 0 move movelist - Cant get there from here.
            }
            return true;
        }

        private string GetDirString(Point beg, Point end)
        {
            Point dp = end - beg;
            string lhs = "", rhs = "";

            if (dp.y == -1)
                lhs = "n";
            else if (dp.y == 1)
                lhs = "s";

            if (dp.x == -1)
                rhs = "w";
            else if (dp.x == 1)
                rhs = "e";

            return lhs + rhs;
        }

        private List<Point> get_neighbor_pos(Dictionary<string, Cell> cells, Point current, int xmax, int ymax, int xmin, int ymin)
        {
            List<Point> neighbors = new List<Point>();

            try
            {
                if (current == null)
                {
                    return neighbors;
                }

                Point pt;
                foreach (Point d in directions)
                {
                    pt = new Point(current.x + d.x, current.y + d.y);
                    if (pt.x >= xmin && pt.x <= (xmax - 2) && pt.y >= ymin && pt.y <= (ymax - 2))
                    {
                        neighbors.Add(pt);
                    }
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
            return neighbors;
        }

        public int GetCellCost(Cell cell)
        {
            int infinity = 10000;
            int cost;

            if (cell == null)
            {
                return infinity;
            }

            if (cell.Z != this.Z)
            {
                return infinity;
            }

            if (cell.IsLockedHorizontalDoor && !cell.Effects.ContainsKey(Effect.EffectType.Unlocked_Horizontal_Door))
            {
                return infinity;
            }

            if (cell.IsLockedVerticalDoor && !cell.Effects.ContainsKey(Effect.EffectType.Unlocked_Vertical_Door))
            {
                return infinity;
            }

            try
            {
                switch (cell.DisplayGraphic)
                {
                    case "~~":
                        if (IsWaterDweller)
                        {
                            cost = 1;
                        }
                        else if (CanFly)
                        {
                            if (mostHated != null)
                            {
                                cost = 1;
                            }
                            else cost = 5;
                        }
                        else cost = infinity;
                        break;
                    case "%%":
                        if (CanFly)
                        {
                            cost = 5;
                        }
                        else
                        {
                            cost = infinity;
                        }
                        break;
                    case "wb":
                        if (CanFly)
                        {
                            cost = 5;
                        }
                        else
                        {
                            if (mostHated != null)
                            {
                                cost = 2;
                            }
                            else
                            {
                                cost = infinity;
                            }
                        }
                        break;
                    case "--":
                    case "| ":
                    case "\\ ":
                    case "/ ":
                        cost = 2;
                        break;
                    case "~.":
                        if (cell.Effects.ContainsKey(Effect.EffectType.Ice) || cell.Effects.ContainsKey(Effect.EffectType.Dragon__s_Breath_Ice))
                        {
                            if (immuneCold) // immune to fire damage
                            {
                                cost = 1;
                            }
                            else
                            {
                                if (this.mostHated != null)
                                {
                                    cost = 5;
                                }
                                else // do not move into fire if we're not going after an enemy
                                {
                                    cost = infinity;
                                }
                            }
                        }
                        else
                        {
                            cost = 1;
                        }
                        break;
                    case "**":
                        if (immuneFire) // immune to fire damage
                        {
                            cost = 1;
                        }
                        else
                        {
                            if (this.mostHated != null)
                            {
                                cost = 5;
                            }
                            else // do not move into fire if we're not going after an enemy
                            {
                                cost = infinity;
                            }
                        }
                        break;
                    // now the impassable cells
                    case "[]":
                    case "/\\":
                    case "TT":
                    case "SD":
                    case "HD":
                    case "VD":
                    case "==":
                    case "MM":
                    case "WW":
                    case "CC":
                    case "mm":
                    case "##":
                        cost = infinity;
                        break;
                    default:
                        cost = 1;
                        break;
                }
                return cost;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return -1;
            }
        }

        public static void AIMakePCMove(Character target)
        {
            string[] directions = new string[] { "n", "s", "e", "w", "ne", "nw", "se", "sw" };

            if (target.IsBlind && !target.IsFeared)
            {
                Command.ParseCommand(target, "crawl", directions[Rules.dice.Next(0, directions.Length - 1)]);
            }
            else
            {
                Command.ParseCommand(target, directions[Rules.dice.Next(0, directions.Length - 1)], "");
            }
            return;
        }

        public static void CombatAI(NPC npc, Character target)
        {
            string skilltable = "brawling";
            int targethits = target.Hits;

            if (npc.RightHand == null)
            {
                if (npc.BaseProfession == Character.ClassType.Martial_Artist)
                {
                    skilltable = "martialarts";
                }
                else if (npc.IsWyrmKin)
                {
                    // claw - bite - tail - breath
                    int chance = Rules.RollD(1, 100);
                    if (chance <= 85)
                    {
                        skilltable = "claw";
                    }
                    else
                    {
                        skilltable = "bite";
                    }
                }
            }
            if ((npc.animal || npc.RightHand == null) && npc.attackString1.Length > 0)
            {
                int atkMsg = Rules.RollD(1, 6);
                if (skilltable == "bite")
                {
                    atkMsg = 3;
                }
                switch (atkMsg)
                {
                    case 1:
                        target.WriteToDisplay(npc.attackString1);
                        break;
                    case 2: // smash attack
                        target.WriteToDisplay(npc.attackString2);
                        if (npc.Strength >= 20)
                        {
                            AIMakePCMove(target);
                            target.WriteToDisplay("You have been knocked down!");
                            break;
                        }
                        break;
                    case 3:
                        target.WriteToDisplay(npc.attackString3);
                        break;
                    case 4: // poison attack
                        target.WriteToDisplay(npc.attackString4);
                        if (npc.poisonous > 0)
                        {
                            if (!Rules.DND_GetSavingThrow(target, Rules.SavingThrow.ParalyzationPoisonDeath, npc.Level - target.Level))
                            {
                                target.Poisoned = Rules.dice.Next(1, npc.poisonous + 1);
                                target.WriteToDisplay("You have been poisoned.");
                            }
                            else
                            {
                                if (npc.race != "")
                                {
                                    target.WriteToDisplay("You resist " + npc.Name + "'s poison!");
                                }
                                else
                                {
                                    target.WriteToDisplay("You resist the " + npc.Name + "'s poison!");
                                }
                            }
                        }
                        break;
                    case 5:
                        target.WriteToDisplay(npc.attackString5);
                        break;
                    case 6:
                        target.WriteToDisplay(npc.attackString6);
                        break;
                    default:
                        target.WriteToDisplay(npc.Name + " hits with a claw!");
                        break;
                }
            }
            else
            {
                //no weapon and no attackStrings
                if (npc.RightHand == null && npc.attackString1 == "")
                {
                    if (npc.CommandType == Command.CommandType.Kick)
                    {
                        if (npc.race != "")
                        {
                            target.WriteToDisplay(npc.Name + " kicks you!");
                        }
                        else
                        {
                            target.WriteToDisplay("The " + npc.Name + " kicks you!");
                        }
                    }
                    else if (npc.CommandType == Command.CommandType.Jumpkick)
                    {
                        if (npc.race != "")
                        {
                            target.WriteToDisplay(npc.Name + " jumpkicks you!");
                        }
                        else
                        {
                            target.WriteToDisplay("The " + npc.Name + " jumpkicks you!");
                        }
                    }
                    else
                    {
                        if (npc.race != "")
                        {
                            target.WriteToDisplay(npc.Name + " punches you!");
                        }
                        else
                        {
                            target.WriteToDisplay("The " + npc.Name + " punches you!");
                        }
                    }
                }
                else if (npc.CommandType == Command.CommandType.Bash)
                {
                    if (npc.race != "")
                    {
                        target.WriteToDisplay(npc.Name + " bashes you with " + npc.RightHand.shortDesc + "!");
                    }
                    else
                    {
                        target.WriteToDisplay("The " + npc.Name + " bashes you with " + npc.RightHand.shortDesc + "!");
                    }
                }
                else if (npc.RightHand != null) // weapon hit message
                {
                    if (npc.race != "")
                    {
                        target.WriteToDisplay(npc.Name + " hits with " + npc.RightHand.shortDesc + "!");
                    }
                    else
                    {
                        target.WriteToDisplay("The " + npc.Name + " hits with " + npc.RightHand.shortDesc + "!");
                    }
                }
                else // use creatures attackStrings
                {
                    int atkMsg = Rules.dice.Next(1, 6);
                    if (skilltable == "bite")
                    {
                        atkMsg = 3;
                    }
                    switch (atkMsg)
                    {
                        case 1:
                            target.WriteToDisplay(npc.attackString1);
                            break;
                        case 2:
                            target.WriteToDisplay(npc.attackString2);
                            if (npc.Strength >= 20)
                            {
                                AIMakePCMove(target);
                                break;
                            }
                            break;
                        case 3:
                            target.WriteToDisplay(npc.attackString3);
                            break;
                        case 4:
                            target.WriteToDisplay(npc.attackString4);
                            if (npc.poisonous > 0)
                            {
                                if (!Rules.DND_GetSavingThrow(npc, Rules.SavingThrow.ParalyzationPoisonDeath, npc.PoisonResistance))
                                {
                                    Effect.CreateCharacterEffect(Effect.EffectType.Poison, Rules.dice.Next(1, npc.poisonous + 1), target, -1, npc);
                                }
                                else
                                {
                                    if (npc.race != "")
                                    {
                                        npc.WriteToDisplay("You resist " + npc.Name + "'s poison!");
                                    }
                                    else
                                    {
                                        npc.WriteToDisplay("You resist the " + npc.Name + "'s poison!");
                                    }
                                }
                            }
                            break;
                        case 5:
                            target.WriteToDisplay(npc.attackString5);
                            break;
                        case 6:
                            target.WriteToDisplay(npc.attackString6);
                            break;
                        default:
                            target.WriteToDisplay(npc.Name + " hits with a claw!");
                            break;
                    }
                }
            }
        }
    }
}
