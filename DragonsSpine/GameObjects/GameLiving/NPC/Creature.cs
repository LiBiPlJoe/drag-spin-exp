using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace DragonsSpine
{
	public class Creature : NPC
	{
        public Creature()
        {

        }

        public Creature(NPC npc) : base()
        {
            this.npcType = NPCType.Creature;
            this.NPCIdent = npc.NPCIdent;
            this.Name = npc.Name;
            this.attackSound = npc.attackSound;
            this.deathSound = npc.deathSound;
            this.idleSound = npc.idleSound;
            this.npcID = npc.npcID;
            this.worldNpcID = World.GetNextWorldNpcID();
            this.baseArmorClass = npc.baseArmorClass;
            this.THAC0Adjustment = npc.THAC0Adjustment;
            this.MoveString = npc.MoveString;
            this.shortDesc = npc.shortDesc;
            this.longDesc = npc.longDesc;
            this.visualKey = npc.visualKey;
            this.special = npc.special;
            this.npcType = npc.npcType;
            this.aiType = npc.aiType;
            this.species = npc.species;
            this.BaseProfession = npc.BaseProfession;
            this.classFullName = npc.classFullName;
            this.Experience = npc.Experience;
            this.HitsMax = npc.HitsMax;
            this.Hits = this.HitsMax;
            this.Alignment = npc.Alignment;
            this.StaminaMax = npc.StaminaMax;
            this.Stamina = npc.StaminaMax;
            this.ManaMax = npc.ManaMax;
            this.Mana = this.ManaMax;
            this.Strength = npc.Strength;
            this.Dexterity = npc.Dexterity;
            this.Intelligence = npc.Intelligence;
            this.Wisdom = npc.Wisdom;
            this.Constitution = npc.Constitution;
            this.Charisma = npc.Charisma;
            this.Speed = npc.Speed;
            //loot
            this.lootVeryCommonAmount = npc.lootVeryCommonAmount;
            this.lootVeryCommonArray = npc.lootVeryCommonArray;
            this.lootVeryCommonOdds = npc.lootVeryCommonOdds;
            this.lootCommonAmount = npc.lootCommonAmount;
            this.lootCommonArray = npc.lootCommonArray;
            this.lootCommonOdds = npc.lootCommonOdds;
            this.lootRareAmount = npc.lootRareAmount;
            this.lootRareArray = npc.lootRareArray;
            this.lootRareOdds = npc.lootRareOdds;
            this.lootVeryRareAmount = npc.lootVeryRareAmount;
            this.lootVeryRareArray = npc.lootVeryRareArray;
            this.lootVeryRareOdds = npc.lootVeryRareOdds;
            this.lootLairAmount = npc.lootLairAmount;
            this.lootLairArray = npc.lootLairArray;
            this.lootLairOdds = npc.lootLairOdds;
            this.lootAlwaysArray = npc.lootAlwaysArray;
            this.lootBeltAmount = npc.lootBeltAmount;
            this.lootBeltArray = npc.lootBeltArray;
            this.lootBeltOdds = npc.lootBeltOdds;
            //items
            this.spawnArmor = npc.spawnArmor;
            this.spawnLeftHand = npc.spawnLeftHand;
            this.spawnLeftHandOdds = npc.spawnLeftHandOdds;
            this.spawnRightHand = npc.spawnRightHand;
            //skills
            this.unarmed = npc.unarmed;
            this.mace = npc.mace;
            this.bow = npc.bow;
            this.dagger = npc.dagger;
            this.flail = npc.flail;
            this.rapier = npc.rapier;
            this.twoHanded = npc.twoHanded;
            this.staff = npc.staff;
            this.shuriken = npc.shuriken;
            this.sword = npc.sword;
            this.threestaff = npc.threestaff;
            this.halberd = npc.halberd;
            this.thievery = npc.thievery;
            this.magic = npc.magic;
            this.bash = npc.bash;

            this.spellList = npc.spellList;
            this.abjurationSpells = npc.abjurationSpells; // abjuration spells
            this.alterationSpells = npc.alterationSpells; // beneficial alteration spells
            this.alterationHarmfulSpells = npc.alterationHarmfulSpells; // harmful alteration spells
            this.conjurationSpells = npc.conjurationSpells; // conjuration spells
            this.divinationSpells = npc.divinationSpells; // divination spells
            this.evocationSpells = npc.evocationSpells; // evocation spells
            this.evocationAreaEffectSpells = npc.evocationAreaEffectSpells; // area effect evocation spells

            this.animal = npc.animal;
            this.tanningResult = npc.tanningResult;
            this.IsUndead = npc.IsUndead;
            this.IsSpectral = npc.IsSpectral;
            this.poisonous = npc.poisonous;
            this.IsWaterDweller = npc.IsWaterDweller;
            this.IsMobile = npc.IsMobile;
            this.Gold = npc.Gold;
            this.Level = npc.Level;
            this.BaseProfession = npc.BaseProfession;
            this.gender = npc.gender;
            this.race = npc.race;
            this.Age = npc.Age;
            this.canCommand = npc.canCommand;
            this.lairCritter = npc.lairCritter;
            this.lairCells = npc.lairCells;
            this.HasRandomName = npc.HasRandomName;
            this.castMode = npc.castMode;
            this.attackString1 = npc.attackString1;
            this.attackString2 = npc.attackString2;
            this.attackString3 = npc.attackString3;
            this.attackString4 = npc.attackString4;
            this.attackString5 = npc.attackString5;
            this.attackString6 = npc.attackString6;
            this.blockString1 = npc.blockString1;
            this.blockString2 = npc.blockString2;
            this.blockString3 = npc.blockString3;
            this.trainerType = npc.trainerType;

            this.patrolRoute = npc.patrolRoute;
            if (this.patrolRoute.Length > 0)
            {
                this.patrolKeys = new List<string>();
                string[] route = this.patrolRoute.Split(" ".ToCharArray());
                foreach (string nrt in route)
                {
                    this.patrolKeys.Add(nrt);
                }
            }

            this.immuneBlind = npc.immuneBlind;
            this.immuneCold = npc.immuneCold;
            this.immuneCurse = npc.immuneCurse;
            this.immuneDeath = npc.immuneDeath;
            this.immuneFear = npc.immuneFear;
            this.immuneFire = npc.immuneFire;
            this.immuneLightning = npc.immuneLightning;
            this.immunePoison = npc.immunePoison;
            this.immuneStun = npc.immuneStun;
            this.effectList = npc.effectList;
            this.questList = npc.questList;
            this.GroupAmount = npc.GroupAmount;
            this.GroupMembers = npc.GroupMembers;
            this.WeaponRequirement = npc.WeaponRequirement;
            this.questFlags = npc.questFlags;
        }

        public static void DragonFear(NPC dragon)
        {
            int bitcount = 0;
            Cell curCell = null;

            try
            {
                //loop through all visible cells
                for (int ypos = -3; ypos <= 3; ypos += 1)
                {
                    //dragon looks at each cell
                    for (int xpos = -3; xpos <= 3; xpos += 1)
                    {
                        //Check the PC list, and Char list of the cell
                        if (dragon.CurrentCell.visCells[bitcount])
                        {
                            curCell = Cell.GetCell(dragon.FacetID, dragon.LandID, dragon.MapID, dragon.X + xpos, dragon.Y + ypos, dragon.Z);

                            //Look for the character in the charlist of the cell
                            foreach (Character chr in curCell.Characters)
                            {
                                if (chr != dragon)
                                {
                                    if (Rules.DoSpellDamage(dragon, chr, null, 0, "dragon fear") == 1)
                                    {
                                        Effect.CreateCharacterEffect(Effect.EffectType.Fear, 1, chr, Rules.RollD(1, 4), dragon);
                                    }
                                }
                            }
                        }
                        bitcount += 1;
                    }
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public static void GiantStomp(NPC giant)
        {
            int bitcount = 0;
            Cell curCell = null;

            try
            {
                //loop through all visable cells
                for (int ypos = -3; ypos <= 3; ypos += 1)
                {
                    //giant looks at each cell
                    for (int xpos = -3; xpos <= 3; xpos += 1)
                    {
                        //Check the PC list, and Char list of the cell
                        if (giant.CurrentCell.visCells[bitcount])
                        {

                            curCell = Cell.GetCell(giant.FacetID, giant.LandID, giant.MapID, giant.X + xpos, giant.Y + ypos, giant.Z);

                            //Look for the character in the charlist of the cell
                            foreach (Character chr in curCell.Characters)
                            {
                                if (chr != giant && chr.Name != "giant")
                                {
                                    if (Rules.DoSpellDamage(giant, chr, null, 0, "giant stomp") == 1)
                                    {
                                        chr.WriteToDisplay("You are jolted by the shaking ground!");
                                        Rules.DoDamage(chr, giant, Rules.dice.Next(2) + 1, false);
                                    }
                                }
                            }
                        }
                        bitcount += 1;
                    }
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }
	}
}
