using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Timers;


namespace DragonsSpine.GameObjects
{
    /// <summary>
    /// This abstract class represents all game objects.
    /// Originally written by Michael Cohen (Ebony) on 28 August 2008.
    /// </summary>
    abstract public class GameObject
    {
        #region Enumerations
        
        /// <summary>
        /// The game object state enumeration.
        /// </summary>
        public enum eGameObjectState
        {
            /// <summary>
            /// Visible in the world and able to be manipulated.
            /// </summary>
            Active,
            /// <summary>
            /// Exists in the world, is invisible,and not able to be manipulated normally.
            /// </summary>
            Inactive,
            /// <summary>
            /// Deleted and preparing for garbage collection.
            /// </summary>
            Deleted
        } 
        #endregion

        /// <summary>
        /// All objects in the game have saving throws to determine damage to and/or destruction of the object.
        /// </summary>
        public struct SavingThrows
        {
            /// <summary>
            /// Paralyzation / Poison / Death saving throw.
            /// </summary>
            public short PPD;

            /// <summary>
            /// Breath Weapon saving throw.
            /// </summary>
            public short BW;

            /// <summary>
            /// Petrification / Polymorph saving throw.
            /// </summary>
            public short PP;

            /// <summary>
            /// Rod/Staff/Wand saving throw.
            /// </summary>
            public short RSW;

            /// <summary>
            /// Spells saving throw.
            /// </summary>
            public short S;
        }

        /// <summary>
        /// All objects have resistances (adds or minus to saving throw rolls) and protections (damage mitigation) to certain attacks.
        /// </summary>
        public struct Defenses
        {
            public short Fire;

            public short Cold;

            public short Lightning;

            public short Death;

            public short Blind;

            public short Fear;

            /// <summary>
            /// Stun resistance and protection in regards to stun magic.
            /// </summary>
            public short Stun;

            public short Poison;

            /// <summary>
            /// Zonk is the ability to resist and protect vs. physical stuns.
            /// </summary>
            public short Zonk;
        }

        #region Private Data
        /// <summary>
        /// Vowels
        /// </summary>
        private const string m_vowels = "aeuio";
        /// <summary>
        /// Holds the current state of this object.
        /// </summary>
        protected eGameObjectState m_objectState;

        /// <summary>
        /// Holds the current cell of this object.
        /// </summary>
        protected Cell m_cell;

        /// <summary>
        /// Holds the current object that owns this object, if any.
        /// </summary>
        protected GameObject m_owner;

        /// <summary>
        /// Holds the index of this object's location in its Cell's object list.
        /// </summary>
        protected int m_objectID;

        /// <summary>
        /// Holds the boolean for whether this object should be saved to the database.
        /// </summary>
        protected bool m_saveToDatabase;

        /// <summary>
        /// Holds the round when the GameObject was placed in the world.
        /// </summary>
        protected long m_spawnRound;

        /// <summary>
        /// Holds the heading of the GameObject.
        /// </summary>
        protected short m_heading;

        /// <summary>
        /// Holds the amount of cells in any direction this GameObject can see.
        /// </summary>
        protected int m_sightRange;

        /// <summary>
        /// Holds the time when the object was created.
        /// </summary>
        protected DateTime m_creationTime;

        /// <summary>
        /// Holds how many seconds the object has been in the game.
        /// </summary>
        protected ulong m_secondsAlive;

        /// <summary>
        /// Holds the alignment of the object.
        /// </summary>
        protected Globals.eAlignment m_alignment;

        /// <summary>
        /// Holds the current cell the object is located in.
        /// </summary>
        protected Cell m_currentCell;

        /// <summary>
        /// Holds the last command sent by the object.
        /// </summary>
        protected string m_lastCommand;

        /// <summary>
        /// Holds the name of the object.
        /// </summary>
        protected string m_name;

        /// <summary>
        /// Holds notes pertaining to the object.
        /// </summary>
        protected string m_notes;

        /// <summary>
        /// Holds the perception class for this object.
        /// For managability reasons all objects percieve the world around them. Some better than others.
        /// </summary>
        protected AI.Perception m_perception;

        /// <summary>
        /// Holds the short description of this object.
        /// </summary>
        protected string m_shortDesc;

        /// <summary>
        /// Holds the long description of this object.
        /// </summary>
        protected string m_longDesc;

        /// <summary>
        /// Holds the saving throw values for this object.
        /// </summary>
        protected SavingThrows m_savingThrows;

        /// <summary>
        /// Holds the resist values for this object.
        /// </summary>
        protected Defenses m_resistances;

        /// <summary>
        /// Holds the protection values for this object.
        /// </summary>
        protected Defenses m_protections;

        /// <summary>
        /// Holds the visual key for this game object.
        /// </summary>
        protected string m_visualKey;

        protected short m_facetID;

        protected short m_landID;

        protected short m_mapID;

        protected short m_xCord;

        protected short m_yCord;

        protected short m_zCord;

        /// <summary>
        /// Holds the database ID for this game object.
        /// </summary>
        protected int m_databaseID;

        protected string m_special;

        /// <summary>
        /// Holds temporary effects.
        /// </summary>
        protected Dictionary<Effect.EffectType, Effect> m_effectList;

        /// <summary>
        /// Holds the round timer for the object.
        /// </summary>
        protected Timer m_roundTimer;

        protected int m_cmdWeight;

        protected Command.CommandType m_cmdType;
        #endregion


        #region Public Properties
        public Globals.eAlignment Alignment
        {
            get { return m_alignment; }
        }

        /// <summary>
        /// Gets or sets the name of the GameObject.
        /// </summary>
        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }

        public string ShortDesc
        {
            get { return m_shortDesc; }
            set { m_shortDesc = value; }
        }

        public string LongDesc
        {
            get { return m_longDesc; }
            set { m_longDesc = value; }
        }

        /// <summary>
        /// Gets or sets the heading of this object.
        /// </summary>
        public short Heading
        {
            get { return m_heading; }
            set { m_heading = value; }
        }

        /// <summary>
        /// Gets or sets the GameObject's current cell.
        /// </summary>
        public Cell Cell
        {
            get { return m_cell; }
            set { m_cell = value; }
        }

        /// <summary>
        /// Gets the Land this GameObject is located in.
        /// </summary>
        public Land Land
        {
            get { return Cell.Land; }
        }

        /// <summary>
        /// Gets the Map this GameObject is located in.
        /// </summary>
        public Map Map
        {
            get { return Cell.Map; }
        }

        /// <summary>
        /// Gets the GameObject's land ID.
        /// </summary>
        public short LandID
        {
            get { return Cell.LandID; }
        }

        /// <summary>
        /// Gets the GameObject's map ID.
        /// </summary>
        public short MapID
        {
            get { return Cell.MapID; }
        }

        /// <summary>
        /// Gets how many cells in each direction the GameObject can detect.
        /// </summary>
        public int SightRange
        {
            get { return m_sightRange; }
        }

        public string Special
        {
            get { return m_special; }
            set { m_special = value; }
        }

        public eGameObjectState ObjectState
        {
            get { return m_objectState; }
            set { m_objectState = value; }
        }

        public int X
        {
            get { return m_cell.X; }
        }

        public int Y
        {
            get { return m_cell.Y; }
        }

        public int Z
        {
            get { return m_cell.Z; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor.
        /// </summary>
        public GameObject()
        {
            m_alignment = Globals.eAlignment.None;
            m_currentCell = null;
            m_lastCommand = "";
            m_name = "None";
            m_notes = "None";
            m_perception = new AI.Perception();
            m_shortDesc = "";
            m_longDesc = "";
            m_resistances = new Defenses();
            m_protections = new Defenses();
            m_visualKey = "";

            m_effectList = new Dictionary<Effect.EffectType, Effect>();

            // round timer initialization?

            m_cmdWeight = 0;

            m_cmdType = Command.CommandType.None;
        }
        #endregion

        /// <summary>
        /// Adds the object to the world
        /// </summary>
        /// <returns></returns>
        public virtual bool AddToWorld()
        {
            if (m_cell == null || m_objectState != eGameObjectState.Inactive)
                return false;
            m_objectState = eGameObjectState.Active;
            ObjectManager.ObjectList.Add(this);
            if (this is GamePlayer)
            {
                ObjectManager.PlayerList.Add(this as GamePlayer);
            }
            if (this is GameNPC)
            {
                ObjectManager.NPCList.Add(this as GameNPC);
            }
            m_cell.Add(this);
            return true;
        }

        /// <summary>
        /// Removes the object from the world cleanly.
        /// </summary>
        /// <returns>False if there is an error with the removal. True otherwise.</returns>
        public virtual bool RemoveFromWorld()
        {
            if (m_cell == null || m_objectState != eGameObjectState.Active)
                return false;

            m_objectState = eGameObjectState.Inactive;

            ObjectManager.ObjectList.Remove(this);

            if (this is GamePlayer)
            {
                ObjectManager.PlayerList.Remove(this as GamePlayer);
            }

            if (this is GameNPC)
            {
                ObjectManager.NPCList.Remove(this as GameNPC);
            }

            m_cell.Remove(this);

            return true;
        }

        /// <summary>
        /// Get the correct name of the GameObject.
        /// </summary>
        /// <param name="article">If 0 return the article "the" with the name.</param>
        /// <param name="firstLetterUppercase">Bool whether to capitalize first letter.</param>
        /// <returns>The correct name.</returns>
        public virtual string GetName(int article, bool firstLetterUppercase)
        {
            // Proper noun.
            if (char.IsUpper(Name[0]))
            {
                return Name;
            }
            else // Common noun.
            {
                if (article == 0)
                {
                    if (firstLetterUppercase)
                    {
                        return "The " + Name;
                    }
                    else
                    {
                        return "the " + Name;
                    }
                }
                else
                {
                    // If the first letter is a vowel.
                    if (m_vowels.IndexOf(Name[0]) != -1)
                    {
                        if (firstLetterUppercase)
                        {
                            return "An " + Name;
                        }
                        else
                        {
                            return "an " + Name;
                        }
                    }
                    else
                    {
                        if (firstLetterUppercase)
                        {
                            return "A " + Name;
                        }
                        else
                        {
                            return "a " + Name;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get a list of look messages for the GameObject.
        /// </summary>
        /// <param name="player">The player that is looking.</param>
        /// <returns>IList of look messages.</returns>
        public virtual IList GetLookMessages(GamePlayer player)
        {
            IList list = new ArrayList(4);
            list.Add("You are looking at " + GetName(0, false) + ".");
            return list;
        }

        /// <summary>
        /// Get a list of examine messages for the GameObject.
        /// </summary>
        /// <param name="player">The player that is examining.</param>
        /// <returns>IList of examine messages.</returns>
        public virtual IList GetExamineMessages(GamePlayer player)
        {
            IList list = new ArrayList(4);
            list.Add("You are examining " + GetName(0, false) + ".");
            return list;
        }

        /// <summary>
        ///  Get the direction of a sound
        /// </summary>
        /// <param name="ch">the GameLiving that hears the sound</param>
        /// <param name="x">X coord of the sound</param>
        /// <param name="y">Y coord of the sound</param>
        /// <returns>string - direction of sound</returns>
        public static string GetTextDirection(GameLiving ch, int x, int y)
        {
            if (x < ch.X && y < ch.Y)
            {
                return "To the northwest you hear ";
            }
            else if (x < ch.X && y > ch.Y)
            {
                return "To the southwest you hear ";
            }
            else if (x > ch.X && y < ch.Y)
            {
                return "To the northeast you hear ";
            }
            else if (x > ch.X && y > ch.Y)
            {
                return "To the southeast you hear ";
            }
            else if (x == ch.X && y > ch.Y)
            {
                return "To the south you hear ";
            }
            else if (x == ch.X && y < ch.Y)
            {
                return "To the north you hear ";
            }
            else if (x < ch.X && y == ch.Y)
            {
                return "To the west you hear ";
            }
            else if (x > ch.X && y == ch.Y)
            {
                return "To the east you hear ";
            }
            return "You hear ";
        }

        #region Events
        /// <summary>
        /// The event triggered when the object receives speech.
        /// </summary>
        /// <param name="text">The text to receive.</param>
        public virtual void OnReceiveSpeech(string text)
        {
        }
        /// <summary>
        /// The event triggered when the object receives a command
        /// </summary>
        /// <param name="requestor">GameLiving sending the command</param>
        /// <param name="command">The Command</param>
        public virtual void OnReceiveCommand(GameLiving requestor, string[] command)
        {
        }
        /// <summary>
        /// The event triggered when the object receives a shout.
        /// </summary>
        /// <param name="text">The shout that is received.</param>
        public virtual void OnReceiveShout(string text)
        {
        }
        /// <summary>
        /// The event triggered when the object hears a sound
        /// </summary>
        /// <param name="sound">the sound</param>
        public virtual void OnReceiveSound(string sound)
        {
        }
        /// <summary>
        /// The event triggered each tick to process any commands
        /// </summary>
        public virtual void ProcessCommands()
        {
        }
        /// <summary>
        /// The event triggered each round
        /// </summary>
        public virtual void OnRoundEvent()
        {
        }
        /// <summary>
        /// The main object Round Event
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        protected virtual void OnRoundEvent(object obj, ElapsedEventArgs e)
        {
            if (m_objectState == eGameObjectState.Active)
            {
                m_secondsAlive++;
            }
        }

        #endregion
    }
}
