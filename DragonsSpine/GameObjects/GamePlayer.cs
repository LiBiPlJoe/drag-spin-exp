using System;
using System.Collections.Generic;
using System.Text;
using DragonsSpine.GameClients;

namespace DragonsSpine.GameObjects
{
    public class GamePlayer : GameLiving
    {
        private const short DISPLAY_BUFFER_SIZE = 10;
        private const string DEFAULT_AFK_MESSAGE = "I am currently A.F.K. (Away From Keyboard).";

        public const short MAX_IGNORE = 30;
        public const short MAX_FRIENDS = 30;
        public const short MAX_MACROS = 20; 

        #region Structs
        struct PlayerSettings
        {
            /// <summary>
            /// Display combat damage amounts.
            /// </summary>
            public bool DisplayCombatDamage;

            /// <summary>
            /// List of player IDs that will be ignored.
            /// </summary>
            public List<int> IgnoreList;

            /// <summary>
            /// Filter out profanity in text chat.
            /// </summary>
            public bool FilterProfanity;

            /// <summary>
            /// List of player IDs that are friends.
            /// </summary>
            public List<int> FriendsList;

            /// <summary>
            /// Whether or not to notify friends when the owner of these settings logs in and out.
            /// </summary>
            public bool FriendNotify;

            /// <summary>
            /// Holds whether this player will receive pages.
            /// </summary>
            public bool ReceivePages;

            /// <summary>
            /// Holds whether this player will receive tells.
            /// </summary>
            public bool ReceiveTells;

            /// <summary>
            /// Holds whether this player will receive group invites.
            /// </summary>
            public bool ReceiveGroupInvites;

            /// <summary>
            /// Holds the implementor level of the player.
            /// </summary>
            public Globals.eImpLevel ImpLevel;

            /// <summary>
            /// Holds the current color choice (visual key related) of the player.
            /// </summary>
            public string ColorChoice;

            /// <summary>
            /// Holds whether this player's level, stats and location are visible to other players.
            /// </summary>
            public bool IsAnonymous;

            /// <summary>
            /// Holds whether commands will be transmitted back to the player's client.
            /// </summary>
            public bool EchoCommands;

            /// <summary>
            /// Holds whether this player is invisible.
            /// </summary>
            public bool IsInvisible;

            /// <summary>
            /// Holds whether the staff title of this player will be visible.
            /// </summary>
            public bool ShowStaffTitle;

            /// <summary>
            /// Holds a list of macro sets for this player.
            /// </summary>
            public List<MacroSet> MacroSetList;

            /// <summary>
            /// Holds the current conference room of the player.
            /// </summary>
            public short ConfRoom;
        }

        struct MacroSet
        {
            /// <summary>
            /// Holds the name of the macro set.
            /// </summary>
            public string Name;

            /// <summary>
            /// Holds the string array of macros.
            /// </summary>
            public string[] MacroArray;

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="name"></param>
            public MacroSet(string name)
            {
                Name = name;
                MacroArray = new string[MAX_MACROS];
            }

            /// <summary>
            /// Adds a macro the a macro set.
            /// </summary>
            /// <param name="num">The number of the macro (MAX_MACROS).</param>
            /// <param name="macro">The string representation of the macro.</param>
            public void AddMacro(int num, string macro)
            {
                for (int a = 0; a < MAX_MACROS; a++)
                {
                    if (a == num)
                    {
                        MacroArray[a] = macro;
                    }
                }
            }

            /// <summary>
            /// Deletes a macro from the macro set.
            /// </summary>
            /// <param name="num">The number of the macro (MAX_MACROS).</param>
            public void DeleteMacro(int num)
            {
                for (int a = 0; a < MAX_MACROS; a++)
                {
                    if (a == num)
                    {
                        MacroArray[a] = "";
                    }
                }
            }
        }

        struct UnderworldStats
        {
            public short HitsMax;
            public short HitsAdjustment;
            public short StaminaMax;
            public short StaminaAdjustment;
            public short ManaMax;
            public short ManaAdjustment;
            public bool HasIntestines;
            public bool HasLiver;
            public bool HasLungs;
            public bool HasStomach;
        }

        struct PvPStats
        {
            public short CurrentKarma;
            public long LifetimeKarma;
            public long PvPNumDeaths;
            public long PvPNumKills;
            public int CurrentMarks;
            public int LifetimeMarks;
        } 
        #endregion

        #region Private Data
        /// <summary>
        /// Holds current player settings.
        /// </summary>
        private PlayerSettings m_playerSettings;

        #region Non Database Variables
        /// <summary>
        /// Holds whether the player is AFK.
        /// </summary>
        // private bool m_isAFK;

        /// <summary>
        /// Holds the player's current AFK message.
        /// </summary>
        // private string m_afkMessage;

        // private bool m_wasInvisible;

        private string m_protocol;

        private Globals.ePlayerState m_playerState;
        
        #endregion

        #region Database Variables
        /// <summary>
        /// Holds the player's unique ID.
        /// </summary>
        private int m_playerID;

        /// <summary>
        /// Holds the account ID that this player belongs to.
        /// </summary>
        private int m_accountID;

        /// <summary>
        /// Holds the name of the account this player belongs to.
        /// </summary>
        private string m_accountName;

        /// <summary>
        /// Holds whether or not this player is an ancestor and eligible for the ancestor ceremony.
        /// </summary>
        private bool m_isAncestor;

        /// <summary>
        /// Holds the player ID of this player's ancestor.
        /// </summary>
        private int m_ancestorPlayerID;

        /// <summary>
        /// Holds the direction the player is facing.
        /// </summary>
        private string m_directionPointer;

        /// <summary>
        /// Holds the adjustment values, if any, of this player's doctored stats.
        /// </summary>
        private Stats m_doctoredStats;

        /// <summary>
        /// Holds how many rounds this player has played in the game world.
        /// </summary>
        private long m_roundsPlayed;

        /// <summary>
        /// Holds the amount of deaths this player has.
        /// </summary>
        private int m_numDeaths;

        /// <summary>
        /// Holds the amount of kills this player has made.
        /// </summary>
        private int m_numKills;

        /// <summary>
        /// Holds the amount of gold this player has in the bank.
        /// </summary>
        private double m_bankGold;

        /// <summary>
        /// Holds the date and time that this player was created.
        /// </summary>
        private DateTime m_birthday;

        /// <summary>
        /// Holds the date and time this player was last online.
        /// </summary>
        private DateTime m_lastOnline;

        /// <summary>
        /// Holds Underworld specific stats.
        /// </summary>
        private UnderworldStats m_underworldStats;

        /// <summary>
        /// Holds PvP specific stats.
        /// </summary>
        private PvPStats m_pvpStats;

        /// <summary>
        /// Holds a string of friend names, separated by spaces.
        /// </summary>
        private string m_friends;

        /// <summary>
        /// Holds an integer array of friend player IDs.
        /// </summary>
        private int[] m_friendsList;

        /// <summary>
        /// Holds a string of ignored names, separated by spaces.
        /// </summary>
        private string m_ignored;

        /// <summary>
        /// Holds an integer array of ignored player IDs.
        /// </summary>
        private int[] m_ignoreList;

        /// <summary>
        /// Holds a list of items in this player's locker.
        /// </summary>
        private List<Item> m_lockerList;

        /// <summary>
        /// Holds whether the player is carrying a corpse.
        /// </summary>
        // private bool m_isCorpseCarried;

        private string[] m_displayBuffer;

        // private string m_displayText = "";
        #endregion

        #endregion

        #region Public Properties

        

        /// <summary>
        /// Gets or sets the protocol the player is using. (move this to the GameClient class in the future)
        /// </summary>
        public string ClientProtocol
        {
            get { return m_protocol; }
            set { m_protocol = value; }
        }

        public Globals.ePlayerState PlayerState
        {
            get { return m_playerState; }
            set { m_playerState = value; }
        }
        #endregion

        #region Constructor (1)
        /// <summary>
        /// Constructor for a new game player.
        /// </summary>
        /// <param name="dataRow"></param>
        public GamePlayer(System.Data.DataRow dataRow)
            : base()
        {
            InitPlayerSettings();
            InitNonDatabaseVariables();

            //Initialize structs.
            m_doctoredStats = new Stats();
            m_underworldStats = new UnderworldStats();
            m_pvpStats = new PvPStats();

            //Initialize lists.
            m_lockerList = new List<Item>();

            InitDatabaseVariables(dataRow);
        }
        public GamePlayer(System.Data.DataRow dr, GameClient client)
            : base()
        {
        }

        #endregion

        /// <summary>
        /// Initializes the current player settings.
        /// </summary>
        private void InitPlayerSettings()
        {
            m_playerSettings = new PlayerSettings();
            m_playerSettings.DisplayCombatDamage = false;
            m_playerSettings.IgnoreList = new List<int>();
            m_playerSettings.FilterProfanity = true;
            m_playerSettings.FriendsList = new List<int>();
            m_playerSettings.FriendNotify = true;
            m_playerSettings.ReceivePages = true;
            m_playerSettings.ReceiveTells = true;
            m_playerSettings.ReceiveGroupInvites = true;            
            m_playerSettings.ImpLevel = Globals.eImpLevel.USER;
            m_playerSettings.ColorChoice = "brown";
            m_playerSettings.IsAnonymous = false;
            m_playerSettings.EchoCommands = true;
            m_playerSettings.IsInvisible = false;
            m_playerSettings.ShowStaffTitle = false;
            m_playerSettings.MacroSetList = new List<MacroSet>();
        }

        /// <summary>
        /// Initializes variables that are not set from database values.
        /// </summary>
        private void InitNonDatabaseVariables()
        {
            // m_isAFK = false;
            // m_afkMessage = DEFAULT_AFK_MESSAGE;
            // m_wasInvisible = false;
            m_wasImmortal = false;

            m_displayBuffer = new string[DISPLAY_BUFFER_SIZE];
            // m_displayText = "";

            m_protocol = "normal";
        }

        /// <summary>
        /// Initializes variables that are set from database values.
        /// </summary>
        /// <param name="dataRow">The DataRow used to initialize the variables.</param>
        private void InitDatabaseVariables(System.Data.DataRow dr)
        {
            #region GameObject Variables
            m_name = dr["name"].ToString();
            m_notes = dr["notes"].ToString();
            m_alignment = (Globals.eAlignment)Convert.ToInt32(dr["alignment"]);
            m_facetID = Convert.ToInt16(dr["facet"]);
            m_landID = Convert.ToInt16(dr["land"]);
            m_mapID = Convert.ToInt16(dr["map"]);
            m_xCord = Convert.ToInt16(dr["xCord"]);
            m_yCord = Convert.ToInt16(dr["yCord"]);
            m_zCord = Convert.ToInt16(dr["zCord"]);
            m_currentCell = Cell.GetCell(m_facetID, m_landID, m_mapID, m_xCord, m_yCord, m_zCord); 
            #endregion

            #region GameLiving Variables
            m_gender = (Globals.eGender)Convert.ToInt16(dr["gender"]);
            m_race = dr["race"].ToString();
            m_classFullName = dr["classFullName"].ToString();
            m_classType = (ClassType)Enum.Parse(typeof(ClassType), dr["classType"].ToString());
            m_visualKey = dr["visualKey"].ToString();
            //TODO
            if (m_visualKey == null || m_visualKey == "")
            {
                SetCharacterVisualKey();
            }
            m_stunned = Convert.ToInt16(dr["stunned"]);
            m_floating = Convert.ToInt16(dr["floating"]);
            m_isDead = Convert.ToBoolean(dr["dead"]);
            m_fighterSpecialization = (Globals.eSkillType)Enum.Parse(typeof(Globals.eSkillType), dr["fighterSpecialization"].ToString());
            m_level = Convert.ToInt16(dr["level"]);
            m_experience = Convert.ToInt64(dr["exp"]);
            #region Stats
            //(HITS)
            m_statsCurrent.Hits = Convert.ToInt32(dr["hits"]);
            m_statsAdjustment.Hits = Convert.ToInt32(dr["hitsAdjustment"]);
            m_statsMax.Hits = Convert.ToInt32(dr["hitsMax"]);
            //Doctored hits are a game player variable.
            //(STAMINA)
            m_statsCurrent.Stamina = Convert.ToInt32(dr["stamLeft"]);
            m_statsAdjustment.Stamina = Convert.ToInt32(dr["staminaAdjustment"]);
            m_statsMax.Stamina = Convert.ToInt32(dr["stamina"]);
            //(MANA)
            m_statsCurrent.Mana = Convert.ToInt32(dr["mana"]);
            m_statsAdjustment.Mana = Convert.ToInt32(dr["manaAdjustment"]);
            m_statsMax.Mana = Convert.ToInt32(dr["manaMax"]);
            #endregion
            m_age = Convert.ToInt32(dr["age"]);
            #region Ability Scores
            m_abilityScoresBase.Strength = Convert.ToInt16(dr["strength"]);
            m_abilityScoresBase.Dexterity = Convert.ToInt16(dr["dexterity"]);
            m_abilityScoresBase.Intelligence = Convert.ToInt16(dr["intelligence"]);
            m_abilityScoresBase.Wisdom = Convert.ToInt16(dr["wisdom"]);
            m_abilityScoresBase.Constitution = Convert.ToInt16(dr["constitution"]);
            m_abilityScoresBase.Charisma = Convert.ToInt16(dr["charisma"]);
            #endregion
            m_strengthAdd = Convert.ToInt16(dr["strengthAdd"]);
            m_dexterityAdd = Convert.ToInt16(dr["dexterityAdd"]);
            #endregion

            #region GamePlayer Variables
            m_playerID = Convert.ToInt32(dr["playerID"]);
            m_accountID = Convert.ToInt32(dr["accountID"]);
            m_accountName = dr["account"].ToString();
            m_isAncestor = Convert.ToBoolean(dr["ancestor"]);
            m_ancestorPlayerID = Convert.ToInt32(dr["ancestorID"]);
            m_directionPointer = dr["dirPointer"].ToString();
            m_doctoredStats.Hits = Convert.ToInt32(dr["hitsDoctored"]);
            m_roundsPlayed = Convert.ToInt64(dr["roundsPlayed"]);
            m_numDeaths = Convert.ToInt32(dr["numDeaths"]);
            m_numKills = Convert.ToInt32(dr["numKills"]);
            m_bankGold = Convert.ToInt64(dr["bankGold"]);
            m_birthday = Convert.ToDateTime(dr["birthday"]);
            m_lastOnline = Convert.ToDateTime(dr["lastOnline"]);

            #region UnderworldStats Variables
            m_underworldStats.HitsMax = Convert.ToInt16(dr["UW_hitsMax"]);
            m_underworldStats.HitsAdjustment = Convert.ToInt16(dr["UW_hitsAdjustment"]);
            m_underworldStats.StaminaMax = Convert.ToInt16(dr["UW_staminaMax"]);
            m_underworldStats.StaminaAdjustment = Convert.ToInt16(dr["UW_staminaAdjustment"]);
            m_underworldStats.ManaMax = Convert.ToInt16(dr["UW_manaMax"]);
            m_underworldStats.ManaAdjustment = Convert.ToInt16(dr["UW_manaAdjustment"]);
            m_underworldStats.HasIntestines = Convert.ToBoolean(dr["UW_intestines"]);
            m_underworldStats.HasLiver = Convert.ToBoolean(dr["UW_liver"]);
            m_underworldStats.HasLungs = Convert.ToBoolean(dr["UW_lungs"]);
            m_underworldStats.HasStomach = Convert.ToBoolean(dr["UW_stomach"]); 
            #endregion

            #region PvPStats Variables
            m_pvpStats.CurrentKarma = Convert.ToInt16(dr["currentKarma"]);
            m_pvpStats.LifetimeKarma = Convert.ToInt64(dr["lifetimeKarma"]);
            m_pvpStats.PvPNumDeaths = Convert.ToInt64(dr["pvpDeaths"]);
            m_pvpStats.PvPNumKills = Convert.ToInt64(dr["pvpKills"]);
            m_pvpStats.CurrentMarks = Account.GetCurrentMarks(m_accountID);
            m_pvpStats.LifetimeMarks = Convert.ToInt16(dr["lifetimeMarks"]);
            #endregion
            #endregion

            int a = 0;
            string[] pFlagged = dr["playersFlagged"].ToString().Split(Protocol.ASPLIT.ToCharArray());
            if (dr["playersFlagged"].ToString() != "")
            {
                for (a = 0; a < pFlagged.Length; a++)
                {
                    m_playersFlagged.Add(Convert.ToInt32(pFlagged[a]));
                }
            }

            if (dr["playersKilled"].ToString() != "")
            {
                pFlagged = dr["playersKilled"].ToString().Split(Protocol.ASPLIT.ToCharArray());
                for (a = 0; a < pFlagged.Length; a++)
                {
                    m_playersKilled.Add(Convert.ToInt32(pFlagged[a]));
                }
            }

            #region PlayerSettings Variables
            m_playerSettings.ConfRoom = Convert.ToInt16(dr["confRoom"]);
            m_playerSettings.ImpLevel = (Globals.eImpLevel)Convert.ToInt32(dr["impLevel"]);
            #endregion

            LoadPlayerSettings();

            LoadPlayerSkills();

            m_leftHandItem = DAL.DBPlayer.loadPlayerHeld(m_playerID, false);
            m_rightHandItem = DAL.DBPlayer.loadPlayerHeld(m_playerID, true);
            Utils.Log("Character " + m_playerID + " has " + m_rightHandItem.name + " in their right hand (the secret ingredient is " + m_rightHandItem.special + ").", Utils.LogType.Unknown);

            m_sackList = DAL.DBPlayer.loadPlayerSack(m_playerID);
            m_wearingList = DAL.DBPlayer.loadPlayerWearing(m_playerID);
            m_lockerList = DAL.DBPlayer.loadPlayerLocker(m_playerID);
            m_beltList = DAL.DBPlayer.loadPlayerBelt(m_playerID);

            m_lockerList = DAL.DBPlayer.loadPlayerLocker(m_playerID);

            m_rightRing1 = DAL.DBPlayer.loadRings(m_playerID, 1);
            m_rightRing2 = DAL.DBPlayer.loadRings(m_playerID, 2);
            m_rightRing3 = DAL.DBPlayer.loadRings(m_playerID, 3);
            m_rightRing4 = DAL.DBPlayer.loadRings(m_playerID, 4);
            m_leftRing1 = DAL.DBPlayer.loadRings(m_playerID, 5);
            m_leftRing2 = DAL.DBPlayer.loadRings(m_playerID, 6);
            m_leftRing3 = DAL.DBPlayer.loadRings(m_playerID, 7);
            m_leftRing4 = DAL.DBPlayer.loadRings(m_playerID, 8);

            m_spellList = (IntStringMap)DAL.DBPlayer.loadPlayerSpells(m_playerID);

            //DAL.DBPlayer.loadPlayerEffects(this);
            //this.questList = DAL.DBPlayer.loadPlayerQuests(this.PlayerID);
            //DAL.DBPlayer.loadPlayerFlags(this, true);
        }

        private void LoadPlayerSettings()
        {
            System.Data.DataRow drItem = DAL.DBPlayer.GetPlayerSettingsDataRow(m_playerID);

            if (drItem != null)
            {
                m_playerSettings.DisplayCombatDamage = Convert.ToBoolean(drItem["displayCombatDamage"]);
                m_playerSettings.IsAnonymous = Convert.ToBoolean(drItem["anonymous"]);
                m_playerSettings.EchoCommands = Convert.ToBoolean(drItem["echo"]);
                m_playerSettings.FilterProfanity = Convert.ToBoolean(drItem["filterProfanity"]);
                FillFriendsList(Utils.ConvertStringToIntArray(drItem["friendsList"].ToString()));
                m_playerSettings.FriendNotify = Convert.ToBoolean(drItem["friendNotify"]);
                FillIgnoreList(Utils.ConvertStringToIntArray(drItem["ignoreList"].ToString()));
                m_playerSettings.IsInvisible = Convert.ToBoolean(drItem["invisible"]);
                m_playerSettings.ReceiveGroupInvites = Convert.ToBoolean(drItem["receiveGroupInvites"]);
                m_playerSettings.ReceivePages = Convert.ToBoolean(drItem["receivePages"]);
                m_playerSettings.ReceiveTells = Convert.ToBoolean(drItem["receiveTells"]);
                m_playerSettings.ShowStaffTitle = Convert.ToBoolean(drItem["showStaffTitle"]);

                m_isImmortal = Convert.ToBoolean(drItem["immortal"]);

                // TODO: This will need to be modified in the future to allow for more macro sets.
                if (drItem["macros"].ToString() != "")
                {
                    MacroSet macroSet = new MacroSet("Default");

                    string[] macros = drItem["macros"].ToString().Split(Protocol.ISPLIT.ToCharArray());
                    
                    for (int a = 0; a < macros.Length; a++)
                    {
                        macroSet.AddMacro(a, (macros[a]));
                    }
                }
            }
        }

        private void LoadPlayerSkills()
        {
            System.Data.DataRow drItem = DAL.DBPlayer.GetPlayerSettingsDataRow(m_playerID);

            if (drItem != null)
            {
                m_skillSetCurrent.Mace = Convert.ToInt64(drItem["mace"]);
                m_skillSetCurrent.Bow = Convert.ToInt64(drItem["bow"]);
                m_skillSetCurrent.Flail = Convert.ToInt64(drItem["flail"]);
                m_skillSetCurrent.Dagger = Convert.ToInt64(drItem["dagger"]);
                m_skillSetCurrent.Rapier = Convert.ToInt64(drItem["rapier"]);
                m_skillSetCurrent.TwoHanded = Convert.ToInt64(drItem["twoHanded"]);
                m_skillSetCurrent.Staff = Convert.ToInt64(drItem["staff"]);
                m_skillSetCurrent.Shuriken = Convert.ToInt64(drItem["shuriken"]);
                m_skillSetCurrent.Sword = Convert.ToInt64(drItem["sword"]);
                m_skillSetCurrent.Threestaff = Convert.ToInt64(drItem["threestaff"]);
                m_skillSetCurrent.Halberd = Convert.ToInt64(drItem["halberd"]);
                m_skillSetCurrent.Unarmed = Convert.ToInt64(drItem["unarmed"]);
                m_skillSetCurrent.Thievery = Convert.ToInt64(drItem["thievery"]);
                m_skillSetCurrent.Magic = Convert.ToInt64(drItem["magic"]);
                m_skillSetCurrent.Bash = Convert.ToInt64(drItem["bash"]);

                m_skillSetHighest.Mace = Convert.ToInt64(drItem["highmace"]);
                m_skillSetHighest.Bow = Convert.ToInt64(drItem["highbow"]);
                m_skillSetHighest.Flail = Convert.ToInt64(drItem["highflail"]);
                m_skillSetHighest.Dagger = Convert.ToInt64(drItem["highdagger"]);
                m_skillSetHighest.Rapier = Convert.ToInt64(drItem["highrapier"]);
                m_skillSetHighest.TwoHanded = Convert.ToInt64(drItem["hightwoHanded"]);
                m_skillSetHighest.Staff = Convert.ToInt64(drItem["highstaff"]);
                m_skillSetHighest.Shuriken = Convert.ToInt64(drItem["highshuriken"]);
                m_skillSetHighest.Sword = Convert.ToInt64(drItem["highsword"]);
                m_skillSetHighest.Threestaff = Convert.ToInt64(drItem["highthreestaff"]);
                m_skillSetHighest.Halberd = Convert.ToInt64(drItem["highhalberd"]);
                m_skillSetHighest.Unarmed = Convert.ToInt64(drItem["highunarmed"]);
                m_skillSetHighest.Thievery = Convert.ToInt64(drItem["highthievery"]);
                m_skillSetHighest.Magic = Convert.ToInt64(drItem["highmagic"]);
                m_skillSetHighest.Bash = Convert.ToInt64(drItem["highbash"]);

                m_skillSetTrained.Mace = Convert.ToInt64(drItem["trainedmace"]);
                m_skillSetTrained.Bow = Convert.ToInt64(drItem["trainedbow"]);
                m_skillSetTrained.Flail = Convert.ToInt64(drItem["trainedflail"]);
                m_skillSetTrained.Dagger = Convert.ToInt64(drItem["traineddagger"]);
                m_skillSetTrained.Rapier = Convert.ToInt64(drItem["trainedrapier"]);
                m_skillSetTrained.TwoHanded = Convert.ToInt64(drItem["trainedtwoHanded"]);
                m_skillSetTrained.Staff = Convert.ToInt64(drItem["trainedstaff"]);
                m_skillSetTrained.Shuriken = Convert.ToInt64(drItem["trainedshuriken"]);
                m_skillSetTrained.Sword = Convert.ToInt64(drItem["trainedsword"]);
                m_skillSetTrained.Threestaff = Convert.ToInt64(drItem["trainedthreestaff"]);
                m_skillSetTrained.Halberd = Convert.ToInt64(drItem["trainedhalberd"]);
                m_skillSetTrained.Unarmed = Convert.ToInt64(drItem["trainedunarmed"]);
                m_skillSetTrained.Thievery = Convert.ToInt64(drItem["trainedthievery"]);
                m_skillSetTrained.Magic = Convert.ToInt64(drItem["trainedmagic"]);
                m_skillSetTrained.Bash = Convert.ToInt64(drItem["trainedbash"]);

            }
        }

        private void FillFriendsList(int[] array)
        {
            m_friends = "";

            m_friendsList = new int[MAX_FRIENDS];

            if (array == null)
                return;

            for (int a = 0; a < array.Length; a++)
            {
                m_friendsList[a] = array[a];
                m_friends += m_friendsList[a] + Protocol.ASPLIT;
            }

            m_friends = m_friends.Substring(0, m_friends.Length - Protocol.ASPLIT.Length); // remove last VSPLIT
        }

        private void FillIgnoreList(int[] array)
        {
            m_ignored = ""; // clear ignore names list

            m_ignoreList = new int[MAX_IGNORE];

            for (int a = 0; a < array.Length; a++) // loop to fill ignore list and ignore names list
            {
                m_ignoreList[a] = array[a];
                m_ignored += m_ignoreList[a] + Protocol.ASPLIT;
            }

            m_ignored = m_ignored.Substring(0, m_ignored.Length - Protocol.ASPLIT.Length); // remove last VSPLIT
        }

        private void SetCharacterVisualKey()
        {
            m_visualKey = m_gender.ToString().ToLower() + "_" + m_classType.ToString().ToLower() + "_pc_" + m_playerSettings.ColorChoice.ToLower();

            if (m_isDead)
            {
                m_visualKey = "ghost";
            }
            else if (m_name == "rat")
            {
                m_visualKey = "rat";
            }
            else if (m_name == "toad")
            {
                m_visualKey = "toad";
            }
        }
    }
}
