namespace DragonsSpine
{
	using System;
	using System.Collections;
	using System.IO;

	public class Bottle : Item
	{
		private bool m_open;

		public Bottle()
		{
			m_open = false;
		}

        public Bottle(Item item) : base()
        {
            this.catalogID = item.catalogID;
            this.notes = item.notes;
            this.combatAdds = item.combatAdds;
            this.itemID = item.itemID;
            this.worldItemID = item.worldItemID;
            this.itemType = item.itemType;
            this.baseType = item.baseType;
            this.name = item.name;
            this.shortDesc = item.shortDesc;
            this.longDesc = item.longDesc;
            this.visualKey = item.visualKey;
            this.wearLocation = item.wearLocation;
            this.weight = item.weight;
            this.coinValue = item.coinValue;
            this.size = item.size;
            this.effectType = item.effectType;
            this.effectAmount = item.effectAmount;
            this.effectDuration = item.effectDuration;
            this.special = item.special;
            this.minDamage = item.minDamage;
            this.maxDamage = item.maxDamage;
            this.skillType = item.skillType;
            this.vRandLow = item.vRandLow;
            this.vRandHigh = item.vRandHigh;
            this.key = item.key;
            this.isRecall = item.isRecall;
            this.alignment = item.alignment;
            this.spell = item.spell;
            this.spellPower = item.spellPower;
            this.charges = item.charges;
            this.attackType = item.attackType;
            this.blueglow = item.blueglow;
            this.flammable = item.flammable;
            this.fragile = item.fragile;
            this.lightning = item.lightning;
            this.returning = item.returning;
            this.silver = item.silver;
            this.attuneType = item.attuneType;
            this.figExp = item.figExp;
            this.armorClass = item.armorClass;
            this.armorType = item.armorType;
            //
            this.drinkDesc = item.drinkDesc;
            this.fluidDesc = item.fluidDesc;
        }

        public bool IsEmpty()
        {
            if (this.special == "empty")
            {
                return true;
            }
            return false;
        }

		public static bool OpenBottle(Bottle bottle, Character ch)
		{
			if(bottle.m_open)
				return false;
			else
			{
				bottle.m_open = true;
                ch.EmitSound(Sound.GetCommonSound(Sound.CommonSound.OpenBottle));
				return true;
			}
		}

		public static bool CloseBottle(Bottle bottle)
		{
			if(bottle.m_open)
			{
				bottle.m_open = false;
				return true;
			}

			return false;
		}

		public static void DrinkBottle(Bottle bottle, Character ch)
		{
			if(bottle.m_open)
			{
				if(bottle.special != "empty")
				{
                    string[] effectList = bottle.effectType.Split(" ".ToCharArray());
                    string[] amountList = bottle.effectAmount.Split(" ".ToCharArray());
                    string[] durationList = bottle.effectDuration.Split(" ".ToCharArray());

                    for (int a = 0; a < effectList.Length; a++)
                    {
                        Effect.CreateCharacterEffect((Effect.EffectType)Convert.ToInt32(effectList[a]), Convert.ToInt32(amountList[a]),
                            ch, Convert.ToInt32(durationList[a]), ch);
                    }
                    if (bottle.drinkDesc != "")
                    {
                        ch.WriteToDisplay(bottle.drinkDesc);
                    }
                    bottle.special = "empty";
                    bottle.coinValue = 0;
                    ch.EmitSound(Sound.GetCommonSound(Sound.CommonSound.DrinkBottle));
				}
				else ch.WriteToDisplay("The bottle is empty.");
			}
			else ch.WriteToDisplay("You must first open the bottle.");
		}

		public static string GetFluidDesc(Bottle bottle)
		{
            if (bottle.special == "empty")
                return " The " + bottle.name + " is empty.";

            if (bottle.fluidDesc != "")
            {
                if (bottle.m_open)
                {
                    return " Inside the " + bottle.name + " is " + bottle.fluidDesc + " The " + bottle.name + " is open.";
                }
                else
                {
                    return " The " + bottle.name + " is closed.";
                    //return " Inside the " + bottle.name + " is " + bottle.fluidDesc;
                }
            }
            return "";
		}
	}
}
