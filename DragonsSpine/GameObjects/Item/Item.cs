namespace DragonsSpine
{
	using System;
	using System.Collections;
    using System.Collections.Generic;
	using System.Collections.Specialized;
	using System.IO;

	public class Item
	{
        static Dictionary<int, Item> itemDictionary = new Dictionary<int, Item>();

        #region Item ID Constants
        public const int ID_COINS = 30000;
        public const int ID_SPELLBOOK = 31000;
        public const int ID_KNIGHTRING = 13999;
        public const int ID_RECALLRING = 13990;
        public const int ID_GOLDRING = 13100;
        public const int ID_BALMBERRY = 33020;
        public const int ID_POISONBERRY = 33021;
        public const int ID_MANABERRY = 33022;
        public const int ID_STAMINABERRY = 33023;
        public const int ID_MUGWORT = 20090;
        public const int ID_BALM = 20000;
        public const int ID_CORPSE = 30001;
        public const int ID_TIGERSEYE = 30130;
        public const int ID_SUMMONEDMOB = 901;
        public const int ID_TIGERFIG = 30010;
        public const int ID_GRIFFINFIG = 30020;
        public const int ID_DRAKEFIG = 30040;
        public const int ID_DRAGONFIG = 30030;
        public const int ID_SNAKESTAFF = 24502;
        #endregion

		public int catalogID; // this is the control ID number from the database
        public string notes; // notes from the database
        public string visualKey;
        public int worldItemID;
		public int itemID; // this is the unique ID
		public Globals.eItemType itemType; // weapon, wearable, container, miscellaneous, edible, potable, corpse, coin
		public Globals.eItemBaseType baseType; //this is is the base of the item, ie.. axebattlelarge, sword, crossbow, etc..
		public Globals.eSkillType skillType; // this is the skill type being used
		public string name = "undefined"; // name of the item that will be used in the game
        public string unidentifiedName = ""; // unidentified name
        public string identifiedName = ""; // identified name
        public List<int> identifiedList = new List<int>();
        public string shortDesc; // short description of the item...this is the extended name
		public string longDesc; // long description of the item
		public double weight; // weight of this item
		public Globals.eItemSize size; // 0 = belt only, 1 = sack only, 2 = belt or sack, 3 = no container, 4 = belt only (large slot)
		public double coinValue; // coin value
        public string effectType = ""; // magical effect if worn or drink
        public string effectAmount = ""; // magical effect if worn or drink
        public string effectDuration = "";
        public int vRandLow = 0; // if vRandLow > 0, random cValue is between vRandLow and vRandHigh
        public int vRandHigh = 0;
        public long figExp = 0; // this is to hold the exp of figurines
        public bool isRecall = false;	//does the item recall
        public Globals.eAlignment alignment = Globals.eAlignment.None; // alignment of the item
        public string special = "";	
        public Globals.eAttuneType attuneType = Globals.eAttuneType.None; // true if the item will attune to the slayer of the current item owner
        public Globals.eWearLocation wearLocation = Globals.eWearLocation.None;
        public string key = "none"; // control for keys/locks
        public int charges = -1; // charges remaining in an item; -1 = never has charges, 0 = had charges,
        public int spell = -1;	// spell ID of the spell this item can cast
        public int spellPower = -1; // the spell power (casting level) of the spell in this item
        public double armorClass = 0;
        public int combatAdds = 0;
        public string attackType = "brawling";
        public Globals.eArmorType armorType = Globals.eArmorType.None;
        public int minDamage = 1; //mindamage this weapon can inflict
        public int maxDamage = 3; //maxdamage this weapon can inflict

        public Book.BookType bookType = Book.BookType.None;
        public int maxPages = 0; // max pages this book has
        public int currentPage = 0;
        public string[] pages = null;

        public string drinkDesc; // the description displayed when the player drinks the bottle
        public string fluidDesc; // the description displayed when a player looks at an open bottle

        public bool returning = false; // true if this item will stay in hand if thrown
        public bool flammable = false; // true if this item will be destroyed by fire
        public bool blueglow = false;
        public bool silver = false;
        public bool lightning = false;
        public bool fragile = false;

        public short facet = 0;
		public short land = 0; //When on the ground, this is the land the item is in
		public short map = 0; //When on the ground, this is the map the item is in
		public short xcord = 20; //When on the ground, this is the Xcord of the item
		public short ycord = 20; //When on the ground, this is the Ycord of the item
        public int zcord = 0;

        public bool nocked = false;
		public int venom = 0; // if > 0, the amount of poison damage on a successful hit...then reset to 0

        public List<Item> contentList = new List<Item>(); // list of all items inside this item 

        #region Recall Variables
        public bool wasRecall = false; //used for recall reset
        public int recallX = 0;
        public int recallY = 0;
        public int recallZ = 0;
        public short recallMap = 0;
        public short recallLand = 0; 
        #endregion

		//public int[] identified; // if the player has identified this item it contains their playerID TODO: add this to tables/sp's
		public int dropRound = 0; // round item was dropped
		public int attunedID = 0; // if this is an attuned item, this will hold the attuned player's playerID
		public Globals.eWearOrientation wearOrientation = Globals.eWearOrientation.None; // for 2 slot wear locations, 0 = left and 1 = right. for fingers positions = 0 through 7
		public DateTime timeCreated =  DateTime.Now;//.AddSeconds(DateTime.Now.Second);
		public string whoCreated = "SYSTEM";

        //item editor
        public static string[] itemspecials = {"figurine", "snake", "tiger", "griffin", "firedragon", "drake", 
            "pierce", "blunt", "slash", "power", "lucky", "fragile"};

        #region Constructors (3)
        public Item()
        {
            itemID = -1;
            itemType = Globals.eItemType.Miscellaneous;
            baseType = Globals.eItemBaseType.Unknown;
            name = "undefined";
            shortDesc = null;
            longDesc = null;
            weight = -1;
            coinValue = -1;
            special = null;
            timeCreated = DateTime.Now;
            whoCreated = "SYSTEM";
        }

        public Item(Item item) : base()
        {
            this.catalogID = item.catalogID;
            this.worldItemID = World.GetNextWorldItemID();
            this.notes = item.notes;
            this.combatAdds = item.combatAdds;
            this.itemID = item.itemID;
            this.itemType = item.itemType;
            this.baseType = item.baseType;
            this.name = item.name;
            this.unidentifiedName = item.unidentifiedName;
            this.identifiedName = item.identifiedName;
            this.identifiedList = item.identifiedList;
            this.shortDesc = item.shortDesc;
            this.longDesc = item.longDesc;
            this.visualKey = item.visualKey;
            this.wearLocation = item.wearLocation;
            this.weight = item.weight;
            this.coinValue = item.coinValue;
            this.size = item.size;
            this.effectType = item.effectType;
            this.effectAmount = item.effectAmount;
            this.effectDuration = item.effectDuration;
            this.special = item.special;
            this.minDamage = item.minDamage;
            this.maxDamage = item.maxDamage;
            this.skillType = item.skillType;
            this.vRandLow = item.vRandLow;
            this.vRandHigh = item.vRandHigh;
            this.key = item.key;
            this.isRecall = item.isRecall;
            this.alignment = item.alignment;
            this.spell = item.spell;
            this.spellPower = item.spellPower;
            this.charges = item.charges;
            this.attackType = item.attackType;
            this.blueglow = item.blueglow;
            this.flammable = item.flammable;
            this.fragile = item.fragile;
            this.lightning = item.lightning;
            this.returning = item.returning;
            this.silver = item.silver;
            this.attuneType = item.attuneType;
            this.figExp = item.figExp;
            this.armorClass = item.armorClass;
            this.armorType = item.armorType;
        }

        public Item(System.Data.DataRow dr)
        {
            this.catalogID = Convert.ToInt32(dr["catalogID"]);
            this.notes = dr["notes"].ToString();
            this.combatAdds = Convert.ToInt32(dr["combatAdds"]);
            this.itemID = Convert.ToInt32(dr["itemID"]);
            this.itemType = (Globals.eItemType)Enum.Parse(typeof(Globals.eItemType), dr["itemType"].ToString());
            this.baseType = (Globals.eItemBaseType)Enum.Parse(typeof(Globals.eItemBaseType), dr["baseType"].ToString());
            this.name = dr["name"].ToString();
            this.visualKey = dr["visualKey"].ToString();
            this.unidentifiedName = dr["unidentifiedName"].ToString();
            this.identifiedName = dr["identifiedName"].ToString();
            this.shortDesc = dr["shortDesc"].ToString();
            this.longDesc = dr["longDesc"].ToString();
            this.wearLocation = (Globals.eWearLocation)Enum.Parse(typeof(Globals.eWearLocation), dr["wearLocation"].ToString());
            this.weight = Convert.ToInt32(dr["weight"]);
            this.coinValue = Convert.ToInt32(dr["coinValue"]);
            this.size = (Globals.eItemSize)Enum.Parse(typeof(Globals.eItemSize), dr["size"].ToString());
            this.effectType = dr["effectType"].ToString();
            this.effectAmount = dr["effectAmount"].ToString();
            this.effectDuration = dr["effectDuration"].ToString();
            this.special = dr["special"].ToString();
            this.minDamage = Convert.ToInt32(dr["minDamage"]);
            this.maxDamage = Convert.ToInt32(dr["maxDamage"]);
            this.skillType = (Globals.eSkillType)Enum.Parse(typeof(Globals.eSkillType), dr["skillType"].ToString());
            this.vRandLow = Convert.ToInt32(dr["vRandLow"]);
            this.vRandHigh = Convert.ToInt32(dr["vRandHigh"]);
            this.key = dr["key"].ToString();
            this.isRecall = Convert.ToBoolean(dr["recall"]);
            this.alignment = (Globals.eAlignment)Enum.Parse(typeof(Globals.eAlignment), dr["alignment"].ToString());
            this.spell = Convert.ToInt16(dr["spell"]);
            this.spellPower = Convert.ToInt16(dr["spellPower"]);
            this.charges = Convert.ToInt16(dr["charges"]);
            this.attackType = dr["attackType"].ToString();
            this.blueglow = Convert.ToBoolean(dr["blueglow"]);
            this.flammable = Convert.ToBoolean(dr["flammable"]);
            this.fragile = Convert.ToBoolean(dr["fragile"]);
            this.lightning = Convert.ToBoolean(dr["lightning"]);
            this.returning = Convert.ToBoolean(dr["returning"]);
            this.silver = Convert.ToBoolean(dr["silver"]);
            this.attuneType = (Globals.eAttuneType)Enum.Parse(typeof(Globals.eAttuneType), dr["attuneType"].ToString());
            this.figExp = Convert.ToInt32(dr["figExp"]);
            this.armorClass = Convert.ToDouble(dr["armorClass"]);
            this.armorType = (Globals.eArmorType)Convert.ToInt32(dr["armorType"]);
            //
            this.bookType = (Book.BookType)Enum.Parse(typeof(Book.BookType), dr["bookType"].ToString());
            this.maxPages = Convert.ToInt16(dr["maxPages"]);
            this.pages = dr["pages"].ToString().Split(Protocol.ISPLIT.ToCharArray());
            //
            this.drinkDesc = dr["drinkDesc"].ToString();
            this.fluidDesc = dr["fluidDesc"].ToString();

            Utils.Log("New item " + this.itemID + " with special " + this.special + ".", Utils.LogType.Unknown);

        } 
        #endregion

        public static Dictionary<int, Item> ItemDictionary
        {
            get { return Item.itemDictionary; }
        }
	
		//put an item in a container
		public void addToContainer(Item item)
		{
            this.contentList.Add(item);
		}

		public static void dumpCorpse(Item itm, Cell cell) // dump contents of a corpse into a cellItemList
		{
			try
			{
				int z = 0;
				int i = 0;
							
				z = itm.contentList.Count - 1;
				if(itm.contentList.Count > 0)
				{
					ArrayList templist = new ArrayList();
                    Item[] contents = new Item[itm.contentList.Count];
                    itm.contentList.CopyTo(contents);
                    foreach (Item item in contents)
                    {
                        templist.Add(item);
                    }
					z=templist.Count - 1;
					while (z >= 0)
					{		
						Item item = (Item)templist[z];			
						for(i = templist.Count - 1;i > -1 ;i--)
						{
							Item tmpitem = (Item)templist[i];
							if (tmpitem.name == item.name)
							{
								templist.RemoveAt(i);
								itm.contentList.RemoveAt(i);
                                cell.Add(tmpitem);
								z=templist.Count;		
							}
						}								
						z--;
					}						
				}
			}
			catch(Exception e)
			{
                Utils.LogException(e);
			}
		}

        public static bool IsItemOnGround(string itemName, short facet, short land, short map, int xcord, int ycord, int zcord)
        {
            foreach (Item item in Cell.GetCell(facet, land, map, xcord, ycord, zcord).Items)
            {
                if (item.name.ToLower() == itemName.ToLower())
                {
                    return true;
                }
            }
            return false;
        }

        public static Item FindItemOnGround(string itemName, short facet, short land, short map, int xcord, int ycord, int zcord)
        {
            foreach (Item item in Cell.GetCell(facet, land, map, xcord, ycord, zcord).Items)
            {
                if (item.name.ToLower() == itemName.ToLower())
                {
                    return item;
                }
            }
            return null;
        }

        public static Item GetItemFromGround(string itemName, Cell groundCell)
        {
            foreach (Item item in new List<Item>(groundCell.Items))
            {
                if (item.name.ToLower() == itemName.ToLower())
                {
                    groundCell.Remove(item);
                    return item;
                }
            }
            return null;
        }

		// Return an item on ground, remove it from cell
		public static Item GetItemFromGround(string itemname, short facet, short land, short map, int xcord, int ycord, int zcord)
		{
            foreach (Item item in Cell.GetCell(facet, land, map, xcord, ycord, zcord).Items)
            {
                if (item.name.ToLower() == itemname.ToLower())
                {
                    Cell.GetCell(facet, land, map, xcord, ycord, zcord).Remove(item);
                    return item;
                }
            }//end foreach
			return null;
		}

        public static Item[] GetAllItemsFromGround(Cell groundCell)
        {
            Item[] tempItemList = new Item[groundCell.Items.Count];
            groundCell.Items.CopyTo(tempItemList, 0);
            foreach (Item item in tempItemList)
            {
                groundCell.Remove(item);
            }
            return tempItemList;
        }

		public static bool LoadItems() 
		{
            return DAL.DBItem.LoadItems();
		}

        public static string GetItemNameFromItemDictionary(int itemID)
        {
            if(Item.itemDictionary.ContainsKey(itemID))
            {
                return Item.itemDictionary[itemID].name;
            }
            return "";
        }

		public static Item CopyItemFromDictionary(int itemID) 
		{
            try
            {
                if (Item.itemDictionary.ContainsKey(itemID))
                {
                    Item item = Item.itemDictionary[itemID];
                    switch (item.baseType)
                    {
                        case Globals.eItemBaseType.Book:
                            return new Book(item);
                        case Globals.eItemBaseType.Bottle:
                            return new Bottle(item);
                        default:
                            return new Item(item);
                    }
                }
                else
                {
                    Utils.Log("Item.CopyItemFromDictionary(" + itemID + ") ITEM ID does not exist.", Utils.LogType.SystemWarning);
                    return null;
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return null;
            }
		}

        public void AttuneItem(int playerID, string comment) // generally called from the code 
        {
            this.attuneType = Globals.eAttuneType.None;
            this.attunedID = playerID;
            this.coinValue = 0;
            if (comment == null)
                comment = "None.";
            Utils.Log(PC.GetPC(playerID).GetLogString() + " attuned to " + this.GetLogString() + ". Comment: " + comment, Utils.LogType.ItemAttuned);
        }

        public void AttuneItem(Character ch) // called when an item meets it's attuneType requirement
        {
            if (ch.IsPC)
            {
                this.attuneType = Globals.eAttuneType.None;
                this.attunedID = ch.PlayerID;
                this.coinValue = 0; // drop the coin value to 0
                Utils.Log(ch.GetLogString() + " attuned to " + this.GetLogString() + ".", Utils.LogType.ItemAttuned);
                if(ch.PCState == Globals.ePlayerState.PLAYING)
                    ch.WriteToDisplay("You feel a tingling sensation.");
            }
        }

        public string GetLookDescription(Character looker)
        {
            if (this.itemType == Globals.eItemType.Coin)
            {
                return "" + (int)this.coinValue + " coins.";
            }

            string description = this.longDesc + ".";

            if (this.baseType == Globals.eItemBaseType.Bottle)
            {
                description += Bottle.GetFluidDesc((Bottle)this);
            }

            if (this.blueglow)
            {
                description += " It is emitting a faint blue glow.";
            }

            if (looker.BaseProfession == Character.ClassType.Thief) // add thief appraisal to description
            {
                #region thief gem and jewelry appraising
                switch (this.baseType)
                {
                    case Globals.eItemBaseType.Amulet:
                    case Globals.eItemBaseType.Ring:
                    case Globals.eItemBaseType.Gem:
                    case Globals.eItemBaseType.Bracelet:
                    //case BaseType.Figurine:
                        if (this.coinValue == 0)
                        {
                            description += " The " + this.name + " is worthless.";
                        }
                        else if (this.coinValue == 1)
                        {
                            description += " The " + this.name + " is worth " + this.coinValue + " coin.";
                        }
                        else
                        {
                            description += " The " + this.name + " is worth " + this.coinValue + " coins.";
                        }
                        break;
                    default:
                        break;
                } 
                #endregion
            }
            else if (looker.BaseProfession == Character.ClassType.Fighter) // add fighter appraisal to description
            {
                #region fighter armor and weapon appraising
                switch (this.baseType)
                {
                    case Globals.eItemBaseType.Armor:
                    case Globals.eItemBaseType.Helm:
                    case Globals.eItemBaseType.Boots:
                        if (looker.Level >= 10)
                        {
                            if (this.coinValue == 0)
                            {
                                description += " The " + this.name + " is worthless.";
                            }
                            else if (this.coinValue == 1)
                            {
                                description += " The " + this.name + " is worth " + this.coinValue + " coin.";
                            }
                            else
                            {
                                description += " The " + this.name + " is worth " + this.coinValue + " coins.";
                            }
                        }
                        break;
                    case Globals.eItemBaseType.Bow:
                    case Globals.eItemBaseType.Shield:
                    case Globals.eItemBaseType.Mace:
                        if (looker.Level >= 11)
                        {
                            if (this.coinValue == 0)
                            {
                                description += " The " + this.name + " is worthless.";
                            }
                            else if (this.coinValue == 1)
                            {
                                description += " The " + this.name + " is worth " + this.coinValue + " coin.";
                            }
                            else
                            {
                                description += " The " + this.name + " is worth " + this.coinValue + " coins.";
                            }
                        }
                        break;
                    case Globals.eItemBaseType.Dagger:
                    case Globals.eItemBaseType.Flail:
                    case Globals.eItemBaseType.Sword:
                        if (looker.Level >= 12)
                        {
                            if (this.coinValue == 0)
                            {
                                description += " The " + this.name + " is worthless.";
                            }
                            else if (this.coinValue == 1)
                            {
                                description += " The " + this.name + " is worth " + this.coinValue + " coin.";
                            }
                            else
                            {
                                description += " The " + this.name + " is worth " + this.coinValue + " coins.";
                            }
                        }
                        break;
                    case Globals.eItemBaseType.Halberd:
                    case Globals.eItemBaseType.Rapier:
                        if (looker.Level >= 13)
                        {
                            if (this.coinValue == 0)
                            {
                                description += " The " + this.name + " is worthless.";
                            }
                            else if (this.coinValue == 1)
                            {
                                description += " The " + this.name + " is worth " + this.coinValue + " coin.";
                            }
                            else
                            {
                                description += " The " + this.name + " is worth " + this.coinValue + " coins.";
                            }
                        }
                        break;
                    default:
                        break;
                } 
                #endregion
            }

            // level 15+ fighter appraises combat adds of a weapon
            //if (this.itemType == ItemType.Weapon && looker.BaseProfession == Character.ClassType.Fighter && looker.Level >= 15 && this.combatAdds > 0)
            //{
            //    description += " The combat adds are " + this.combatAdds + ".";
            //}

            // add skill level if held weapon
            if((this == looker.RightHand || this == looker.LeftHand) && this.itemType == Globals.eItemType.Weapon)
            {
                description += " You are " + Skills.GetSkillTitle(this.skillType, looker.BaseProfession, looker.GetSkillExperience(this.skillType), looker.gender) + " with this weapon.";
            }

            if (this.attunedID == looker.PlayerID)
            {
                description += " You are soulbound to this item.";
            }

            if (looker.fighterSpecialization == this.skillType && this.skillType != Globals.eSkillType.None)
            {
                description += " You are specialized in the use of this weapon.";
            }
            return description;
        }

        public string GetLogString()
        {
            return "[ItemID: " + this.itemID + " | WorldItemID: "+this.worldItemID+"] " + this.notes;
        }

        public static void SetRecallValues(Item item, Character ch)
        {
            item.recallLand = ch.LandID;
            item.recallMap = ch.MapID;
            item.recallX = ch.X;
            item.recallY = ch.Y;
            item.recallZ = ch.Z;
            ch.WriteToDisplay("You feel a slight electric shock.");
        }

        public bool IsAttunedToOther(Character ch)
        {
            if (attunedID != 0 && attunedID != ch.PlayerID)
                return true;

            return false;
        }

        public bool AlignmentCheck(Character ch)
        {
            if (!ch.IsPC) return true;

            if (alignment == Globals.eAlignment.None || alignment == Globals.eAlignment.All ||
                alignment == ch.Alignment)
                return true;

            return false;
        }
	}
}
