using System;
using System.Collections.Generic;
using System.Text;

namespace DragonsSpine.GameObjects
{
    /// <summary>
    /// This abstract class represents all living objects in the game.
    /// Originally written by Michael Cohen (Ebony) on 28 August 2008.
    /// </summary>
    abstract public class GameLiving : GameObject
    {
        #region Enums
        /// <summary>
        /// The professions enum
        /// The order of this enum cannot be changed
        /// </summary>
        public enum ClassType
        {
            None,
            Fighter,
            Thaumaturge,
            Wizard,
            Martial_Artist,
            Thief,
            Knight
        }
        #endregion

        #region Structs
        public struct AbilityScores
        {
            public short Strength;
            public short Dexterity;
            public short Constitution;
            public short Intelligence;
            public short Wisdom;
            public short Charisma;
        }

        public struct Stats
        {
            public int Hits;
            public int Mana;
            public int Stamina;
        }

        public struct SkillSet
        {
            public long Mace;
            public long Bow;
            public long Flail;
            public long Dagger;
            public long Rapier;
            public long TwoHanded;
            public long Staff;
            public long Shuriken;
            public long Sword;
            public long Threestaff;
            public long Halberd;
            public long Unarmed;
            public long Thievery;
            public long Magic;
            public long Bash;
        }

        public struct LivingSoundInfo
        {
            public string AttackSound;
            public string IdleSound;
            public string DeathSound;

            public LivingSoundInfo(string attack, string idle, string death)
            {
                AttackSound = attack;
                IdleSound = idle;
                DeathSound = death;
            }
        }
        #endregion

        /// <summary>
        /// Holds the current values of stats for this living object.
        /// </summary>
        protected Stats m_statsCurrent;

        /// <summary>
        /// Holds the maximum stat values for this living object.
        /// </summary>
        protected Stats m_statsMax;

        /// <summary>
        /// Holds any adjustments that brings maximum values above their norm.
        /// IE: temporary boosts, hit point quests, etc
        /// </summary>
        protected Stats m_statsAdjustment;

        /// <summary>
        /// Holds base ability score values.
        /// </summary>
        protected AbilityScores m_abilityScoresBase;

        /// <summary>
        /// Holds any temporary ability score adjustments due to spells or items.
        /// </summary>
        protected AbilityScores m_abilityScoresAdjustments;

        /// <summary>
        /// Holds the gender type of this living object.
        /// </summary>
        protected Globals.eGender m_gender;

        protected string m_race;

        protected string m_classFullName;

        protected ClassType m_classType;

        protected bool m_isImmortal;

        protected bool m_wasImmortal;

        protected bool m_isDead;

        protected short m_stunned;

        protected short m_floating;

        protected Globals.eSkillType m_fighterSpecialization;

        protected short m_level;

        protected long m_experience;

        protected int m_age;

        protected short m_strengthAdd;

        protected short m_dexterityAdd;

        protected bool m_isAnimal;

        protected int m_attackRoll;

        protected double m_baseArmorClass;

        protected LivingSoundInfo m_livingSoundInfo;

        protected int m_damageRound;

        protected List<int> m_playersFlagged;

        protected List<int> m_playersKilled;

        protected SkillSet m_skillSetCurrent;

        protected SkillSet m_skillSetHighest;

        protected SkillSet m_skillSetTrained;

        protected Item m_leftHandItem;
        protected Item m_rightHandItem;

        protected Item m_leftRing1;
        protected Item m_leftRing2;
        protected Item m_leftRing3;
        protected Item m_leftRing4;
        protected Item m_rightRing1;
        protected Item m_rightRing2;
        protected Item m_rightRing3;
        protected Item m_rightRing4;        

        protected List<Item> m_wearingList; // worn items
        protected List<Item> m_sackList; // sack for storage
        protected List<Item> m_beltList; // belt for storage

        protected IntStringMap m_spellList;

        protected List<Effect> m_wornEffectList; // effects worn

        protected List<Quest> m_questList;
        protected List<string> m_questFlags;
        protected List<string> m_contentFlags;

        #region Public Properties
        public string ClassFullName
        {
            get { return m_classFullName; }
        }

        /// <summary>
        /// Gets the gender of the GameLiving object.
        /// </summary>
        public Globals.eGender Gender
        {
            get { return m_gender; }
        }

        /// <summary>
        /// Gets the race of the GameLiving object.
        /// </summary>
        public string Race
        {
            get { return m_race; }
        }
        #endregion

        /// <summary>
        /// Default contructor.
        /// </summary>
        public GameLiving()
            : base()
        {
            m_statsCurrent = new Stats();
            m_statsAdjustment = new Stats();
            m_statsMax = new Stats();

            m_abilityScoresBase = new AbilityScores();
            m_abilityScoresAdjustments = new AbilityScores();

            m_gender = Globals.eGender.It;
            m_race = "Unknown";
            m_classFullName = "Fighter";
            m_classType = ClassType.None;
            m_isImmortal = false;
            m_wasImmortal = false;
            m_isDead = false;
            m_stunned = 0;
            m_floating = 3;
            m_fighterSpecialization = Globals.eSkillType.None;
            m_level = 0;
            m_experience = 0;
            m_age = 0;

            m_strengthAdd = 0;
            m_dexterityAdd = 0;

            m_isAnimal = false;

            m_livingSoundInfo = new LivingSoundInfo("","","");

            m_baseArmorClass = 10.0;

            m_damageRound = -3;

            m_playersFlagged = new List<int>();
            m_playersKilled = new List<int>();

            m_skillSetCurrent = new SkillSet();
            m_skillSetHighest = new SkillSet();
            m_skillSetTrained = new SkillSet();

            m_leftHandItem = null;
            m_rightHandItem = null;

            m_leftRing1 = null;
            m_leftRing2 = null;
            m_leftRing3 = null;
            m_leftRing4 = null;
            m_rightRing1 = null;
            m_rightRing2 = null;
            m_rightRing3 = null;
            m_rightRing4 = null;

            m_wearingList = new List<Item>();
            m_sackList = new List<Item>();
            m_beltList = new List<Item>();

            m_spellList = new IntStringMap();

            m_wornEffectList = new List<Effect>();

            m_questList = new List<Quest>();
            m_questFlags = new List<string>();
            m_contentFlags = new List<string>();
        }
    }
}
