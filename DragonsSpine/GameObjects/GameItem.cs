using System;
using System.Collections.Generic;
using System.Text;

namespace DragonsSpine.GameObjects
{
    /// <summary>
    /// This class represents all item objects in the game.
    /// Originally written by Michael Cohen (Ebony) on 29 August 2008.
    /// </summary>
    public class GameItem : GameObject
    {
        #region Constants
        public const int ID_COINS = 30000;
        public const int ID_SPELLBOOK = 31000;
        public const int ID_KNIGHTRING = 13999;
        public const int ID_RECALLRING = 13990;
        public const int ID_GOLDRING = 13100;
        public const int ID_BALMBERRY = 33020;
        public const int ID_POISONBERRY = 33021;
        public const int ID_MANABERRY = 33022;
        public const int ID_STAMINABERRY = 33023;
        public const int ID_MUGWORT = 20090;
        public const int ID_BALM = 20000;
        public const int ID_CORPSE = 30001;
        public const int ID_TIGERSEYE = 30130;
        public const int ID_SUMMONEDMOB = 901;
        public const int ID_TIGERFIG = 30010;
        public const int ID_GRIFFINFIG = 30012;
        public const int ID_DRAKEFIG = 30013;
        public const int ID_DRAGONFIG = 30011;
        public const int ID_SNAKESTAFF = 24502;
        #endregion

        #region Structures
        /// <summary>
        /// The item special properties structure.
        /// </summary>
        protected struct ItemSpecialProperties
        {
            /// <summary>
            /// Holds whether an item returns when thrown.
            /// </summary>
            public bool IsReturning;

            /// <summary>
            /// Holds whether an item is blue glowing.
            /// </summary>
            public bool IsBlueglow;

            /// <summary>
            /// Holds whether an item is silver.
            /// </summary>
            public bool IsSilver;

            /// <summary>
            /// Holds whether an item is fragile.
            /// </summary>
            public bool IsFragile;

            /// <summary>
            /// Holds whether an item is flammable.
            /// </summary>
            public bool IsFlammable;
        }

        /// <summary>
        /// The item volatile properties structure.
        /// </summary>
        protected struct ItemVolatileProperties
        {
            /// <summary>
            /// Holds the coin value of the item.
            /// </summary>
            public long CoinValue;

            /// <summary>
            /// Holds the player ID the item is bound to.
            /// </summary>
            public int SoulBoundID;

            /// <summary>
            /// Holds whether the item is nocked.
            /// </summary>
            public bool IsNocked;

            /// <summary>
            /// Holds the amount of charges the item has.
            /// </summary>
            public short Charges;

            /// <summary>
            /// Holds the type of soulbinding.
            /// </summary>
            public Globals.eAttuneType AttuneType;

            /// <summary>
            /// Holds the amount of figurine experience the item has.
            /// </summary>
            public long FigExp;

            /// <summary>
            /// Holds the amount of venom the item has.
            /// </summary>
            public short Venom;
        }
        #endregion

        #region Private Data
        /// <summary>
        /// Holds the size of the item.
        /// </summary>
        protected Globals.eItemSize m_size;

        /// <summary>
        /// Holds the item type of the GameItem.
        /// </summary>
        protected Globals.eItemType m_itemType;

        /// <summary>
        /// Holds the volatile properties of a GameItem. (values that may change from database templates)
        /// </summary>
        protected ItemVolatileProperties m_volatileProps;

        /// <summary>
        /// Holds the skill type of the GameItem.
        /// </summary>
        protected Globals.eSkillType m_skillType;

        /// <summary>
        /// Holds the base type of the GameItem.
        /// </summary>
        protected Globals.eItemBaseType m_baseType;

        /// <summary>
        /// Holds the special properties of the GameItem.
        /// </summary>
        protected ItemSpecialProperties m_specialProps;

        /// <summary>
        /// Holds the unidentified name of the GameItem.
        /// </summary>
        protected string m_unidentifiedName;

        /// <summary>
        /// Holds the identified name of the GameItem.
        /// </summary>
        protected string m_identifiedName;

        /// <summary>
        /// Holds key tag information for the item.
        /// </summary>
        protected string m_key;

        /// <summary>
        /// Holds a string of effect enumeration values.
        /// </summary>
        protected string m_effectType;

        /// <summary>
        /// Holds a string of effect amounts that corresponds with the effectType string.
        /// </summary>
        protected string m_effectAmount;

        /// <summary>
        /// Holds a string of effect durations that corresponds with the effectType string.
        /// </summary>
        protected string m_effectDuration;

        /// <summary>
        /// Holds a string of spell enumeration values.
        /// </summary>
        protected string m_spellType;

        /// <summary>
        /// Holds a string that corresponds with the spellType string with spell level values.
        /// </summary>
        protected string m_spellLevel;

        /// <summary>
        /// Holds the armor class rating of the item.
        /// </summary>
        protected double m_armorClass;

        /// <summary>
        /// Holds the unique world ID of the item.
        /// </summary>
        protected long m_worldID;

        /// <summary>
        /// Holds the minimum amount of damage the item does in combat.
        /// </summary>
        protected int m_minDamage;

        /// <summary>
        /// Holds the maximum amount of damage the item does in combat.
        /// </summary>
        protected int m_maxDamage;

        /// <summary>
        /// Holds the combat adds of the GameItem.
        /// </summary>
        protected short m_combatAdds;

        /// <summary>
        /// Holds a string of attack types.
        /// </summary>
        protected string m_attackTypes;

        ///<summary>
        /// Holds the weight of the item
        ///</summary>
        protected double m_weight;
        protected int m_spell;
        protected DateTime m_timeCreated;
        protected string m_whoCreated;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the eItemSize of the GameItem.
        /// </summary>
        public Globals.eItemSize Size
        {
            get { return m_size; }
        }
        public DateTime TimeCreated
        {
            get { return m_timeCreated; }
            set { m_timeCreated = value; }
        }
        public string WhoCreated
        {
            get { return m_whoCreated; }
            set { m_whoCreated = value; }
        }
        /// <summary>
        /// Gets the eItemType of the GameItem.
        /// </summary>
        public Globals.eItemType ItemType
        {
            get { return m_itemType; }
        }

        /// <summary>
        /// Gets the armor class rating of the GameItem.
        /// </summary>
        public double ArmorClass
        {
            get { return m_armorClass; }
        }

        /// <summary>
        /// Gets the weight of the GameItem.
        /// </summary>
        public double Weight
        {
            get { return m_weight; }
            set { m_weight = value; }
        }

        /// <summary>
        /// Gets or sets the coin value of the GameItem.
        /// </summary>
        public long CoinValue
        {
            get { return m_volatileProps.CoinValue; }
            set { m_volatileProps.CoinValue = value; }
        }

        /// <summary>
        /// Gets the skill type of the GameItem.
        /// </summary>
        public Globals.eSkillType SkillType
        {
            get { return m_skillType; }
        }

        /// <summary>
        /// Gets the base type of the GameItem.
        /// </summary>
        public Globals.eItemBaseType BaseType
        {
            get { return m_baseType; }
        }

        #region Volatile Properties
        /// <summary>
        /// Gets the soulbound player ID of the GameItem.
        /// </summary>
        public int SoulBoundID
        {
            get { return m_volatileProps.SoulBoundID; }
            set { m_volatileProps.SoulBoundID = value; }
        }

        /// <summary>
        /// Gets or sets whether the GameItem is nocked or not.
        /// </summary>
        public bool IsNocked
        {
            get { return m_volatileProps.IsNocked; }
            set { m_volatileProps.IsNocked = value; }
        }

        /// <summary>
        /// Gets or sets the amount of charges the GameItem has.
        /// </summary>
        public short Charges
        {
            get { return m_volatileProps.Charges; }
            set { m_volatileProps.Charges = value; }
        }

        /// <summary>
        /// Gets or sets how the item binds to players.
        /// </summary>
        public Globals.eAttuneType AttuneType
        {
            get { return m_volatileProps.AttuneType; }
            set { m_volatileProps.AttuneType = value; }
        }

        /// <summary>
        /// Gets or sets how much figurine experience the item has.
        /// </summary>
        public long FigExp
        {
            get { return m_volatileProps.FigExp; }
            set { m_volatileProps.FigExp = value; }
        }
        #endregion


        /// <summary>
        /// Gets key tag information of the GameItem.
        /// </summary>
        public string Key
        {
            get { return m_key; }
        }

        /// <summary>
        /// Gets or sets the unique world ID of the item.
        /// </summary>
        public long WorldID
        {
            get { return m_worldID; }
            set { m_worldID = value; }
        }

        /// <summary>
        /// Gets the minimum amount of damage the item does in combat.
        /// </summary>
        public int MinDamage
        {
            get { return m_minDamage; }
        }

        /// <summary>
        /// Gets the maximum amount of damage the item does in combat.
        /// </summary>
        public int MaxDamage
        {
            get { return m_maxDamage; }
        }

        /// <summary>
        /// Gets the combat adds of the GameItem.
        /// </summary>
        public short CombatAdds
        {
            get { return m_combatAdds; }
        }

        #region Special Properties
        /// <summary>
        /// Gets whether the item is fragile.
        /// </summary>
        public bool IsFragile
        {
            get { return m_specialProps.IsFragile; }
        }

        public bool IsBlueglow
        {
            get { return m_specialProps.IsBlueglow; }
        }

        public bool IsSilver
        {
            get { return m_specialProps.IsSilver; }
        }

        public bool IsFlammable
        {
            get { return m_specialProps.IsFlammable; }
            set { m_specialProps.IsFlammable = value; }
        }

        public bool IsReturning
        {
            get { return m_specialProps.IsReturning; }
        }
        #endregion

        public int ItemID
        {
            get { return m_databaseID; }
            set { m_databaseID = value; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructs a new GameItem object.
        /// </summary>
        public GameItem()
            : base()
        {
            m_size = Globals.eItemSize.No_Container;
            m_itemType = Globals.eItemType.Miscellaneous;
            m_weight = 0;
            m_volatileProps.CoinValue = 0;
            m_skillType = Globals.eSkillType.None;
            m_baseType = Globals.eItemBaseType.Unknown;
            m_specialProps = new ItemSpecialProperties();
            m_volatileProps = new ItemVolatileProperties();
            m_key = "";
            m_effectAmount = "";
            m_effectDuration = "";
            m_effectType = "";
            m_spellType = "";
            m_spellLevel = "";
            m_worldID = World.GetNextWorldItemID();
            m_minDamage = 0;
            m_maxDamage = 0;
            m_combatAdds = 0;
            m_attackTypes = "";
            m_whoCreated = "SYSTEM";
            m_timeCreated = DateTime.Now;
        }
        #endregion

        #region Load Item 
        public void LoadGameItem(System.Data.DataRow dr)
        {
            GameItem item = new GameItem();
            GameItem.LoadGameItem(ref item, dr);
        }
        public static void LoadGameItem(ref GameItem item, System.Data.DataRow dr)
        {
            item.m_databaseID = (int)dr["itemID"];
            item.m_notes = dr["notes"].ToString();
            item.m_size = (Globals.eItemSize)Enum.Parse(typeof(Globals.eItemSize), dr["size"].ToString());
            item.m_itemType = (Globals.eItemType)Enum.Parse(typeof(Globals.eItemType), dr["itemType"].ToString());
            item.m_baseType = (Globals.eItemBaseType)Enum.Parse(typeof(Globals.eItemBaseType), dr["baseType"].ToString());
            item.m_name = dr["name"].ToString();
            item.m_visualKey = dr["visualKey"].ToString();
            item.m_unidentifiedName = dr["unidentifiedName"].ToString();
            item.m_identifiedName = dr["identifiedName"].ToString();
            item.m_shortDesc = dr["shortDesc"].ToString();
            item.m_longDesc = dr["longDesc"].ToString();
            item.m_weight = Convert.ToInt32(dr["weight"]);
            item.m_volatileProps.CoinValue = Convert.ToInt32(dr["coinValue"]);
            item.m_size = (Globals.eItemSize)Enum.Parse(typeof(Globals.eItemSize), dr["size"].ToString());
            item.m_effectType = dr["effectType"].ToString();

            item.m_effectAmount = dr["effectAmount"].ToString();
            item.m_effectDuration = dr["effectDuration"].ToString();
            if (item.m_effectType != null || item.m_effectType != "0")
            {
                //set effects here
            }

            item.m_special = dr["special"].ToString();
            item.m_skillType = (Globals.eSkillType)Enum.Parse(typeof(Globals.eSkillType), dr["skillType"].ToString());
            #region Get random coin value.
            int vRandLow = Convert.ToInt32(dr["vRandLow"]);
            int vRandHigh = Convert.ToInt32(dr["vRandHigh"]);
            if (vRandLow >= 0 && vRandHigh > 0)
            {
                item.m_volatileProps.CoinValue = Rules.dice.Next(vRandLow, vRandHigh + 1);
            }
            #endregion

            item.m_key = dr["key"].ToString();
            item.m_alignment = (Globals.eAlignment)Enum.Parse(typeof(Globals.eAlignment), dr["alignment"].ToString());
            int spellID = Convert.ToInt16(dr["spell"]);
            if (spellID != -1)
            {
                //item.SpellRepertoire.Add(SpellManager.GetSpell(spellID)); // Add spells to the item
            }
            item.m_volatileProps.Charges = Convert.ToInt16(dr["charges"]);
            item.m_attackTypes = dr["attackType"].ToString();
            item.m_specialProps.IsBlueglow = Convert.ToBoolean(dr["blueglow"]);
            item.m_specialProps.IsFlammable = Convert.ToBoolean(dr["flammable"]);
            item.m_specialProps.IsFragile = Convert.ToBoolean(dr["fragile"]);
            item.m_specialProps.IsReturning = Convert.ToBoolean(dr["returning"]);
            item.m_specialProps.IsSilver = Convert.ToBoolean(dr["silver"]);
            item.m_volatileProps.AttuneType = (Globals.eAttuneType)Enum.Parse(typeof(Globals.eAttuneType), dr["attuneType"].ToString());
            item.m_volatileProps.FigExp = Convert.ToInt32(dr["figExp"]);
            item.m_armorClass = Convert.ToDouble(dr["armorClass"]);
            item.m_timeCreated = DateTime.Now;
            item.m_whoCreated = "SYSTEM";


        }
        public static void LoadVolatileProperties(ref GameItem item, System.Data.DataRow dr)
        {
            item.m_volatileProps.Charges = Convert.ToInt16(dr["charges"]);
            item.m_volatileProps.CoinValue = Convert.ToInt64(dr["coinValue"]);
            item.m_volatileProps.FigExp = Convert.ToInt64(dr["FigExp"]);
            //item.m_volatileProps.IsNocked = Convert.ToBoolean(dr["nocked"]);
            item.m_volatileProps.AttuneType = (Globals.eAttuneType)Enum.Parse(typeof(Globals.eAttuneType), dr["attuneType"].ToString());
            item.m_volatileProps.SoulBoundID = Convert.ToInt32(dr["AttunedID"]);
            item.m_special = dr["special"].ToString();
            item.m_timeCreated = Convert.ToDateTime(dr["timeCreated"]);
            item.m_whoCreated = dr["whoCreated"].ToString();
        }
        #endregion

        #region Helper Functions
        public static string ConvertNumberToString(double qty)
        {
            //if (qty == 1) { return "a "; }
            if (qty == 2) { return "two "; }
            if (qty == 3) { return "three "; }
            if (qty == 4) { return "four "; }
            if (qty == 5) { return "five "; }
            if (qty == 6) { return "six "; }
            if (qty > 6 && qty <= 10) { return "several "; }
            if (qty > 10) { return "many "; }
            return "";
        }
        public static string GetLookShortDesc(GameItem item, double quantity)
        {
            if (item.ItemType == Globals.eItemType.Coin)
            {
                if (item.CoinValue == 1)
                    return "a coin";
                else return "coins";
            }

            string what = item.ShortDesc;

            if (quantity > 1)
            {
                if (what.Contains("scales")) { return "vests made of scales"; }
                if (what.Contains("boots")) { return "pairs of boots"; }
                if (what.Contains("greaves")) { return "pairs of greaves"; }
                if (what.Contains("leggings")) { return "pairs of leggings"; }
                if (what.Contains("gauntlets")) { return "pairs of gauntlets"; }
                if (what.Contains("pantaloons")) { return "pairs of pantaloons"; }
                if (what.Contains("berries")) { return "bunches of berries"; }
                if (what.Contains("threestaff")) { return "threestaves"; }
                if (what.Contains("staff")) { return "staves"; }

                what = item.Name + "s";
            }
            else
            {
                if (what.Contains("scales")) { return "a vest made of scales"; }
                if (what.Contains("boots")) { return "a pair of boots"; }
                if (what.Contains("greaves")) { return "a pair of greaves"; }
                if (what.Contains("leggings")) { return "a pair of leggings"; }
                if (what.Contains("gauntlets")) { return "a pair of gauntlets"; }
                if (what.Contains("pantaloons")) { return "a pair of pantaloons"; }
            }

            return what;
        }
        internal string GetLookDescription()
        {
            if (this.ItemType == Globals.eItemType.Coin)
            {
                if (this.CoinValue > 1)
                    return this.CoinValue.ToString() + " gold coins.";
                else
                    return "a gold coin.";
            }
            return this.LongDesc;
        }
        #endregion

    }
}
