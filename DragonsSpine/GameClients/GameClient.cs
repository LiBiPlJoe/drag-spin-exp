using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace DragonsSpine.GameClients
{
    /// <summary>
    /// This abstract class represents all clients connected to the server.
    /// Originally written by Michael Cohen (Ebony) on 13 Septemer 2008.
    /// </summary>
    abstract public class GameClient
    {
        /// <summary>
        /// Holds the IP end point of the client. Instantiated in the constructor.
        /// </summary>
        protected IPEndPoint m_ipEndPoint;

        /// <summary>
        /// Holds the IP address of the client.
        /// </summary>
        protected IPAddress m_ipAddress;

        /// <summary>
        /// Holds the port number of the client.
        /// </summary>
        protected int m_port;

        /// <summary>
        /// Holds the current game player object using this client.
        /// </summary>
        protected GameObjects.GamePlayer m_player;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="ipAddress">The IP address of the client.</param>
        /// <param name="port">The port number of the client.</param>
        public GameClient(IPAddress ipAddress, int port, GameObjects.GamePlayer player)
        {
            m_ipAddress = ipAddress;
            m_port = port;
            m_player = player;

            m_ipEndPoint = new IPEndPoint(ipAddress, port);
        }

        /// <summary>
        /// Sends a text message to the client.
        /// </summary>
        /// <param name="message">The message to send.</param>
        public virtual void Write(string message)
        {
        }

        /// <summary>
        /// Sends a text message with a carriage return to the client.
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <param name="textType">The type of message being sent.</param>
        public virtual void WriteLine(string message, Protocol.TextType textType)
        {
        }

        /// <summary>
        /// Sends a text message to a client in the game.
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <param name="textType">The type of message being sent.</param>
        public virtual void WriteToDisplay(string message, Protocol.TextType textType)
        {
        }

        /// <summary>
        /// Sends a text message that will be displayed at the end of a game round.
        /// </summary>
        /// <param name="message">The message to send.</param>
        public virtual void WriteToDisplay(string message)
        {
        }

        /// <summary>
        /// Sends a message with a carriage return to the client.
        /// </summary>
        /// <param name="message">The message to send.</param>
        public virtual void WriteLine(string message)
        {
        }

        protected string FilterMessage(string message)
        {
            message = message.Replace("%n", m_player.Name);
            message = message.Replace("%r", m_player.Race);
            message = message.Replace("%R", m_player.Race);
            message = message.Replace("%a", Utils.FormatEnumString(m_player.Alignment.ToString()).ToLower());
            message = message.Replace("%A", Utils.FormatEnumString(m_player.Alignment.ToString()));
            message = message.Replace("%c", m_player.ClassFullName.ToLower());
            message = message.Replace("%C", m_player.ClassFullName);
            message = message.Replace("%t", World.CurrentDailyCycle.ToString().ToLower());
            message = message.Replace("%T", World.CurrentDailyCycle.ToString());
            message = message.Replace("%G", m_player.Gender.ToString());
            message = message.Replace("%g", m_player.Gender.ToString().ToLower());
            message = message.Replace("%p", Character.pronoun[Convert.ToInt32(m_player.Gender)].ToLower());
            message = message.Replace("%P", Character.pronoun[Convert.ToInt32(m_player.Gender)]);

            return message;
        }
    }
}
