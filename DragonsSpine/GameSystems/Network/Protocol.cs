using System;
using System.Windows.Forms;

namespace DragonsSpine
{
    public static class Protocol
    {
        #region Protocol
        public enum TextType
        {
            Default, PlayerChat, Enter, Exit, Header, Status, System, Help, Private, Listing, Error,
            Friend, Page, Attuned, CreatureChat, Death, SpellCast, SpellWarm, CombatHit, CombatMiss, SpellHit, SpellMiss
        }

        public static string USPLIT = "?";
        public static string ASPLIT = " "; // attribute delimiter
        public static string ISPLIT = "^"; // item delimiter
        public static string VSPLIT = "~"; // variable delimiter (if multiple items in proto line)
        public static string TEXT_RETURN = (char)27 + "UU" + (char)27;

        public static int MAX_SCORES = 200;

        #region Commands
        public static string PING = (char)27 + "88" + (char)27;
        public static string DELETE_CHARACTER = (char)27 + "89" + (char)27;
        public static string CHARGEN_RECEIVE = (char)27 + "90" + (char)27;
        public static string GET_SCORES = (char)27 + "91" + (char)27;
        public static string GOTO_GAME = (char)27 + "92" + (char)27;
        public static string GOTO_CHARGEN = (char)27 + "93" + (char)27;
        public static string GOTO_MENU = (char)27 + "94" + (char)27;
        public static string GOTO_CONFERENCE = (char)27 + "95" + (char)27;
        public static string LOGOUT = (char)27 + "96" + (char)27;
        public static string SWITCH_CHARACTER = (char)27 + "97" + (char)27;
        public static string SET_PROTOCOL = (char)27 + "98" + (char)27;
        public static string SET_CLIENT = (char)27 + "99" + (char)27;        
        #endregion

        #region Version Information
        public static string VERSION_SERVER = (char)27 + "V0" + (char)27;
        public static string VERSION_SERVER_END = (char)27 + "V1" + (char)27;
        public static string VERSION_CLIENT = (char)27 + "V2" + (char)27;
        public static string VERSION_CLIENT_END = (char)27 + "V3" + (char)27;
        #endregion

        public static string ACCOUNT_INFO = (char)27 + "A0" + (char)27;
        public static string ACCOUNT_INFO_END = (char)27 + "A1" + (char)27;

        #region Character Information
        public static string CHARACTER_LIST = (char)27 + "C0" + (char)27;
        public static string CHARACTER_LIST_END = (char)27 + "C1" + (char)27;
        public static string CHARACTER_STATS = (char)27 + "C2" + (char)27;
        public static string CHARACTER_STATS_END = (char)27 + "C3" + (char)27;
        public static string CHARACTER_RIGHTHAND = (char)27 + "C4" + (char)27;
        public static string CHARACTER_RIGHTHAND_END = (char)27 + "C5" + (char)27;
        public static string CHARACTER_LEFTHAND = (char)27 + "C6" + (char)27;
        public static string CHARACTER_LEFTHAND_END = (char)27 + "C7" + (char)27;
        public static string CHARACTER_INVENTORY = (char)27 + "C8" + (char)27;
        public static string CHARACTER_INVENTORY_END = (char)27 + "C9" + (char)27;
        public static string CHARACTER_SACK = (char)27 + "CA" + (char)27;
        public static string CHARACTER_SACK_END = (char)27 + "CB" + (char)27;
        public static string CHARACTER_BELT = (char)27 + "CC" + (char)27;
        public static string CHARACTER_BELT_END = (char)27 + "CD" + (char)27;
        public static string CHARACTER_RINGS = (char)27 + "CE" + (char)27;
        public static string CHARACTER_RINGS_END = (char)27 + "CF" + (char)27;
        public static string CHARACTER_LOCKER = (char)27 + "CG" + (char)27;
        public static string CHARACTER_LOCKER_END = (char)27 + "CH" + (char)27;
        public static string CHARACTER_SPELLS = (char)27 + "CI" + (char)27;
        public static string CHARACTER_SPELLS_END = (char)27 + "CJ" + (char)27;
        public static string CHARACTER_EFFECTS = (char)27 + "CK" + (char)27;
        public static string CHARACTER_EFFECTS_END = (char)27 + "CL" + (char)27;
        public static string CHARACTER_CURRENT = (char)27 + "CM" + (char)27;
        public static string CHARACTER_CURRENT_END = (char)27 + "CN" + (char)27;
        public static string CHARACTER_SKILLS = (char)27 + "CO" + (char)27;
        public static string CHARACTER_SKILLS_END = (char)27 + "CP" + (char)27;
        public static string CHARACTER_LIST_SPLIT = (char)27 + "CZ" + (char)27;
        #endregion

        #region Main Menu, News, Detect Protocol/Client
        public static string MENU_MAIN = (char)27 + "M0" + (char)27;
        public static string NEWS = (char)27 + "M1" + (char)27;
        public static string NEWS_END = (char)27 + "M2" + (char)27;
        public static string DETECT_PROTOCOL = (char)27 + "M3" + (char)27;
        public static string DETECT_CLIENT = (char)27 + "M4" + (char)27;
        public static string MESSAGEBOX = (char)27 + "M5" + (char)27;
        public static string MESSAGEBOX_END = (char)27 + "M6" + (char)27;
        #endregion

        #region Text
        public static string TEXT_PLAYERCHAT = (char)27 + "T00" + (char)27;
        public static string TEXT_PLAYERCHAT_END = (char)27 + "T01" + (char)27;
        public static string TEXT_HEADER = (char)27 + "T02" + (char)27;
        public static string TEXT_HEADER_END = (char)27 + "T03" + (char)27;
        public static string TEXT_STATUS = (char)27 + "T04" + (char)27;
        public static string TEXT_STATUS_END = (char)27 + "T05" + (char)27;
        public static string TEXT_PRIVATE = (char)27 + "T06" + (char)27;
        public static string TEXT_PRIVATE_END = (char)27 + "T07" + (char)27;
        public static string TEXT_ENTER = (char)27 + "T08" + (char)27;
        public static string TEXT_ENTER_END = (char)27 + "T09" + (char)27;
        public static string TEXT_EXIT = (char)27 + "T10" + (char)27;
        public static string TEXT_EXIT_END = (char)27 + "T11" + (char)27;
        public static string TEXT_SYSTEM = (char)27 + "T12" + (char)27;
        public static string TEXT_SYSTEM_END = (char)27 + "T13" + (char)27;
        public static string TEXT_HELP = (char)27 + "T14" + (char)27;
        public static string TEXT_HELP_END = (char)27 + "T15" + (char)27;
        public static string TEXT_LISTING = (char)27 + "T16" + (char)27;
        public static string TEXT_LISTING_END = (char)27 + "T17" + (char)27;
        public static string TEXT_ERROR = (char)27 + "T18" + (char)27;
        public static string TEXT_ERROR_END = (char)27 + "T19" + (char)27;
        public static string TEXT_FRIEND = (char)27 + "T20" + (char)27;
        public static string TEXT_FRIEND_END = (char)27 + "T21" + (char)27;
        public static string TEXT_PAGE = (char)27 + "T22" + (char)27;
        public static string TEXT_PAGE_END = (char)27 + "T23" + (char)27;
        public static string TEXT_CREATURECHAT = (char)27 + "T24" + (char)27;
        public static string TEXT_CREATURECHAT_END = (char)27 + "T25" + (char)27;
        
        #endregion

        public static string IMP_CHARACTERFIELDS = (char)27 + "I0" + (char)27;
        public static string IMP_CHARACTERFIELDS_END = (char)27 + "I1" + (char)27;

        #region Sound
        public static string SOUND = (char)27 + "S0" + (char)27;
        public static string SOUND_END = (char)27 + "S1" + (char)27;
        public static string SOUND_FROM_CLIENT = (char)27 + "S2" + (char)27; 
        #endregion

        #region CharGen
        public static string CHARGEN_ENTER = (char)27 + "CG0" + (char)27;
        public static string CHARGEN_ROLLER_RESULTS = (char)27 + "CG1" + (char)27;
        public static string CHARGEN_ROLLER_RESULTS_END = (char)27 + "CG2" + (char)27;
        public static string CHARGEN_ERROR = (char)27 + "CG3" + (char)27;
        public static string CHARGEN_INVALIDNAME = (char)27 + "CG4" + (char)27;
        public static string CHARGEN_ACCEPTED = (char)27 + "CG5" + (char)27; 
        #endregion

        #region Conference Room
        public static string CONF_ENTER = (char)27 + "F0" + (char)27;
        public static string CONF_INFO = (char)27 + "F1" + (char)27;
        public static string CONF_INFO_END = (char)27 + "F2" + (char)27;
        #endregion

        #region Game Information
        public static string GAME_CELL = (char)27 + "G0" + (char)27;
        public static string GAME_CELL_END = (char)27 + "G1" + (char)27;
        public static string GAME_CELL_INFO = (char)27 + "G2" + (char)27;
        public static string GAME_CELL_INFO_END = (char)27 + "G3" + (char)27;
        public static string GAME_CELL_CRITTERS = (char)27 + "G4" + (char)27;
        public static string GAME_CELL_CRITTERS_END = (char)27 + "G5" + (char)27;
        public static string GAME_CELL_ITEMS = (char)27 + "G6" + (char)27;
        public static string GAME_CELL_ITEMS_END = (char)27 + "G7" + (char)27;
        public static string GAME_CRITTER_INFO = (char)27 + "G8" + (char)27;
        public static string GAME_CRITTER_INFO_END = (char)27 + "G9" + (char)27;
        public static string GAME_CRITTER_INVENTORY = (char)27 + "GA" + (char)27;
        public static string GAME_CRITTER_INVENTORY_END = (char)27 + "GB" + (char)27;
        public static string GAME_CELL_EFFECTS = (char)27 + "GC" + (char)27;
        public static string GAME_CELL_EFFECTS_END = (char)27 + "GD" + (char)27;
        public static string GAME_WORLD_INFO = (char)27 + "GE" + (char)27;
        public static string GAME_WORLD_INFO_END = (char)27 + "GF" + (char)27;
        public static string GAME_EXIT = (char)27 + "GG" + (char)27;
        public static string GAME_TEXT = (char)27 + "GH" + (char)27;
        public static string GAME_TEXT_END = (char)27 + "GI" + (char)27;
        public static string GAME_NEW_ROUND = (char)27 + "GJ" + (char)27;
        public static string GAME_END_ROUND = (char)27 + "GK" + (char)27;
        public static string GAME_ROUND_DELAY = (char)27 + "GL" + (char)27;
        public static string GAME_ENTER = (char)27 + "GM" + (char)27;
        public static string GAME_POINTER_UPDATE = (char)27 + "GN" + (char)27;
        #endregion

        #region World Information
        public static string WORLD_SPELLS = (char)27 + "W0" + (char)27;
        public static string WORLD_SPELLS_END = (char)27 + "W1" + (char)27;
        public static string WORLD_LANDS = (char)27 + "W2" + (char)27;
        public static string WORLD_LANDS_END = (char)27 + "W3" + (char)27;
        public static string WORLD_MAPS = (char)27 + "W4" + (char)27;
        public static string WORLD_MAPS_END = (char)27 + "W5" + (char)27;
        public static string WORLD_SCORES = (char)27 + "W6" + (char)27;
        public static string WORLD_SCORES_END = (char)27 + "W7" + (char)27;
        public static string WORLD_USERS = (char)27 + "W8" + (char)27;
        public static string WORLD_USERS_END = (char)27 + " W9" + (char)27;
        public static string WORLD_INFORMATION = (char)27 + "WA" + (char)27;
        public static string WORLD_CHARGEN_INFO = (char)27 + "WB" + (char)27;
        public static string WORLD_CHARGEN_INFO_END = (char)27 + "WC" + (char)27;
        public static string WORLD_CELL_INFO = (char)27 + "WD" + (char)27;
        public static string WORLD_CELL_INFO_END = (char)27 + "WE" + (char)27;
        public static string WORLD_ITEMS = (char)27 + "WF" + (char)27;
        public static string WORLD_ITEMS_END = (char)27 + "WG" + (char)27;
        #endregion
        #endregion

        public static string GetTextProtocolString(TextType textType, bool startString)
        {
            try
            {
                System.Reflection.FieldInfo[] fieldInfo = typeof(Protocol).GetFields();

                foreach (System.Reflection.FieldInfo info in fieldInfo)
                {
                    if (info.Name.StartsWith("TEXT_"))
                    {
                        if (startString)
                        {
                            if (info.Name == ("TEXT_" + textType.ToString().ToUpper()))
                            {
                                return (string)info.GetValue(null);
                            }
                        }
                        else
                        {
                            if (info.Name == ("TEXT_" + textType.ToString().ToUpper() + "_END"))
                            {
                                return (string)info.GetValue(null);
                            }
                        }
                    }
                }
                Utils.Log("Protocol.GetTextProtocolString(" + textType.ToString() + ", " + Convert.ToString(startString) + ") failed to find a suitable value.", Utils.LogType.SystemFailure);
                return "";
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return "";
            }
        }

        public static void UpdateUserLists() // called when one of the character array lists is changed, or someone switches characters
        {
            //TODO: this should probably be in a new thread
            int a;
            Character ch;
            for (a = 0; a < Character.pcList.Count; a++)
            {
                ch = Character.pcList[a];
                if (ch.protocol == DragonsSpineMain.APP_PROTOCOL)
                {
                    Protocol.SendUserList(ch);
                }
            }
            for (a = 0; a < Character.confList.Count; a++)
            {
                ch = Character.confList[a];
                if (ch.protocol == DragonsSpineMain.APP_PROTOCOL)
                {
                    Protocol.SendUserList(ch);
                }
            }
            for (a = 0; a < Character.menuList.Count; a++)
            {
                ch = Character.menuList[a];
                if (ch.protocol == DragonsSpineMain.APP_PROTOCOL)
                {
                    Protocol.SendUserList(ch);
                }
            }
            for (a = 0; a < Character.charGenList.Count; a++)
            {
                ch = Character.charGenList[a];
                if (ch.protocol == DragonsSpineMain.APP_PROTOCOL)
                {
                    Protocol.SendUserList(ch);
                }
            }

        }

        public static void SendAccountInfo(Character ch)
        {
            string accountInfo = ch.account + Protocol.VSPLIT +
                ch.accountID + Protocol.VSPLIT +
                ch.lifetimeMarks + Protocol.VSPLIT +
                ch.currentMarks + Protocol.VSPLIT +
                ch.IPAddress;

            ch.WriteLine(Protocol.ACCOUNT_INFO + accountInfo + Protocol.ACCOUNT_INFO_END);
                
        }

        public static void SendCharacterList(Character ch)
        {
            DAL.DBPlayer.p_getCharacterList(ch.accountID, ch);
        }

        public static void SendUserList(Character ch)
        {
            string playerList = "";

            // get everyone at the menu
            foreach (Character user in Character.menuList)
            {
                playerList += FormatUserInfo(ch, user);
            }
            // get everyone in conference rooms
            foreach (Character user in Character.confList)
            {
                playerList += FormatUserInfo(ch, user);
            }
            // get everyone in the game
            foreach (Character user in Character.pcList)
            {
                playerList += FormatUserInfo(ch, user);
            }
            if (playerList.Length > ISPLIT.Length)
            {
                // remove the last ISPLIT
                playerList = playerList.Substring(0, playerList.Length - ISPLIT.Length);
                // send the complete protocol line
                ch.WriteLine(WORLD_USERS + playerList + WORLD_USERS_END);
            }
        }

        #region Formatting
        public static string FormatUserInfo(Character ch, Character user)
        {
            // format: id, title, name, class, level, location, afk

            string userInformation = "";

            if (user.IsAnonymous && !user.IsInvisible)
            {
                if (ch.ImpLevel >= Globals.eImpLevel.GM)
                {
                    userInformation = user.PlayerID + VSPLIT +
                        (int)user.ImpLevel + VSPLIT +
                        user.Name + VSPLIT +
                        user.classFullName + VSPLIT +
                        user.Level + VSPLIT +
                        Conference.GetUserLocation(user) + VSPLIT +
                        user.LandID + VSPLIT +
                        user.MapID + VSPLIT +
                        user.IsDead + VSPLIT +
                        user.IsAnonymous + VSPLIT +
                        user.IsInvisible + VSPLIT +
                        user.afk + VSPLIT +
                        user.receivePages + VSPLIT +
                        user.receiveTells + VSPLIT +
                        user.showStaffTitle;
                    userInformation += ISPLIT;
                }
                else
                {
                    userInformation = user.PlayerID + VSPLIT +
                        (int)user.ImpLevel + VSPLIT +
                        user.Name + VSPLIT +
                        VSPLIT + // do not send class info
                        "0" + VSPLIT; // do not send level info
                    // switch to determine whether to send location information or not
                    switch (user.PCState)
                    {
                        case Globals.ePlayerState.CONFERENCE:
                            userInformation += Conference.GetUserLocation(user) + VSPLIT; // send room location
                            break;
                        default:
                            userInformation += VSPLIT; // do not send location info
                            break;
                    }
                    userInformation +=
                        user.LandID + VSPLIT +
                        user.MapID + VSPLIT +
                        user.IsDead + VSPLIT +
                        user.IsAnonymous + VSPLIT +
                        user.IsInvisible + VSPLIT +
                        user.afk + VSPLIT +
                        user.receivePages + VSPLIT +
                        user.receiveTells + VSPLIT +
                        user.showStaffTitle;
                    userInformation += ISPLIT;
                }

            }
            else if (user.IsInvisible)
            {
                // only send invisible player's information if the viewer's impLevel is >= the user's
                if (ch.ImpLevel >= user.ImpLevel)
                {
                    userInformation = user.PlayerID + VSPLIT +
                        (int)user.ImpLevel + VSPLIT +
                        user.Name + VSPLIT +
                        user.classFullName + VSPLIT +
                        user.Level + VSPLIT +
                        Conference.GetUserLocation(user) + VSPLIT +
                        user.LandID + VSPLIT +
                        user.MapID + VSPLIT +
                        user.IsDead + VSPLIT +
                        user.IsAnonymous + VSPLIT +
                        user.IsInvisible + VSPLIT +
                        user.afk + VSPLIT +
                        user.receivePages + VSPLIT +
                        user.receiveTells + VSPLIT +
                        user.showStaffTitle;

                    userInformation += ISPLIT;
                }
            }
            else // send all available user information
            {
                userInformation = user.PlayerID + VSPLIT +
                    (int)user.ImpLevel + VSPLIT +
                    user.Name + VSPLIT +
                    user.classFullName + VSPLIT +
                    user.Level + VSPLIT +
                    Conference.GetUserLocation(user) + VSPLIT +
                    user.LandID + VSPLIT +
                    user.MapID + VSPLIT +
                    user.IsDead + VSPLIT +
                    user.IsAnonymous + VSPLIT +
                    user.IsInvisible + VSPLIT +
                    user.afk + VSPLIT +
                    user.receivePages + VSPLIT +
                    user.receiveTells + VSPLIT +
                    user.showStaffTitle;

                userInformation += ISPLIT;
            }
            return userInformation;
        }

        public static string FormatItemInfo(Item item)
        {
            return "" + item.itemID + VSPLIT +
                        item.worldItemID + VSPLIT +
                        (int)item.itemType + VSPLIT +
                        (int)item.baseType + VSPLIT +
                        (int)item.skillType + VSPLIT +
                        item.name + VSPLIT +
                        item.shortDesc + VSPLIT +
                        item.longDesc + VSPLIT +
                        item.weight + VSPLIT +
                        (int)item.size + VSPLIT +
                        item.coinValue + VSPLIT +
                        item.effectType + VSPLIT +
                        item.effectAmount + VSPLIT +
                        item.figExp + VSPLIT +
                        item.vRandLow + VSPLIT +
                        item.vRandHigh + VSPLIT +
                        item.venom + VSPLIT +
                        item.isRecall + VSPLIT +
                        item.wasRecall + VSPLIT +
                        item.recallLand + VSPLIT +
                        item.recallMap + VSPLIT +
                        item.recallX + VSPLIT +
                        item.recallY + VSPLIT +
                        (int)item.alignment + VSPLIT +
                        item.dropRound + VSPLIT +
                        item.key + VSPLIT +
                        item.charges + VSPLIT +
                        item.spell + VSPLIT +
                        (int)item.attuneType + VSPLIT +
                        item.attunedID + VSPLIT +
                        item.special + VSPLIT +
                        item.combatAdds + VSPLIT +
                        item.armorClass + VSPLIT +
                        item.minDamage + VSPLIT +
                        item.maxDamage + VSPLIT +
                        (int)item.wearLocation + VSPLIT +
                        (int)item.wearOrientation + VSPLIT + // this
                        item.returning + VSPLIT +
                        item.flammable + VSPLIT +
                        item.timeCreated + VSPLIT +
                        item.whoCreated + VSPLIT +
                        item.visualKey + VSPLIT +
                        item.nocked + VSPLIT +
                        item.unidentifiedName + VSPLIT +
                        item.identifiedName;
        }

        public static string FormatLandInfo(Land land)
        {
            return "" + land.LandID + VSPLIT +
                        land.Name + VSPLIT +
                        land.ShortDesc + VSPLIT +
                        land.LongDesc;
        }

        public static string FormatMapInfo(Map map)
        {
            return "" + map.LandID + VSPLIT +
                        map.MapID + VSPLIT +
                        map.Name + VSPLIT +
                        map.ShortDesc + VSPLIT +
                        map.LongDesc + VSPLIT +
                        map.SuggestedMaximumLevel + VSPLIT +
                        map.SuggestedMinimumLevel + VSPLIT +
                        map.IsPVPEnabled + VSPLIT +
                        map.ExperienceModifier + VSPLIT +
                        map.Difficulty + VSPLIT +
                        (int)map.Climate + VSPLIT;
        }

        public static string FormatCellInfo(Cell cell, Character ch)
        {
            int a;
            string info = Protocol.GAME_CELL_INFO;
            info += cell.LandID + ISPLIT +
                        cell.MapID + ISPLIT +
                        cell.X + ISPLIT +
                        cell.Y + ISPLIT +
                        cell.CellGraphic + ISPLIT +
                        cell.DisplayGraphic + ISPLIT +
                        cell.IsLocker + ISPLIT +
                        cell.IsMapPortal + Protocol.GAME_CELL_INFO_END;

            string critters = Protocol.GAME_CELL_CRITTERS;

            for (a = 0; a < cell.Characters.Count; a++)
            {
                Character crit = (Character)cell.Characters[a];

                if (crit.PlayerID != ch.PlayerID)
                {
                    if (Rules.DetectHidden(crit, ch) && Rules.DetectInvisible(crit, ch))
                    {
                        if (cell != ch.CurrentCell)
                        {
                            if (crit.Group == null || crit.IsPC || (crit.Group != null && crit.Group.GroupLeaderID == crit.worldNpcID))
                            {
                                critters += FormatCellCritter(crit, ch) + USPLIT;
                            }
                        }
                        else
                        {
                            critters += FormatCellCritter(crit, ch) + USPLIT;
                        }
                    }
                }
            }

            if (critters.Length > Protocol.GAME_CELL_CRITTERS.Length)
            {
                critters = critters.Substring(0, critters.Length - USPLIT.Length);
            }

            critters += Protocol.GAME_CELL_CRITTERS_END;

            string items = Protocol.GAME_CELL_ITEMS;
            for (a = 0; a < cell.Items.Count; a++)
            {
                Item item = (Item)cell.Items[a];
                items += FormatCellItem(item) + ISPLIT;
            }
            if (items.Length > Protocol.GAME_CELL_ITEMS.Length)
                items = items.Substring(0, items.Length - ISPLIT.Length);

            items += Protocol.GAME_CELL_ITEMS_END;

            string effects = Protocol.GAME_CELL_EFFECTS;

            foreach (Effect effect in cell.Effects.Values)
                effects += FormatEffectInfo(effect) + ISPLIT;

            if (effects.Length > Protocol.GAME_CELL_EFFECTS.Length)
                effects = effects.Substring(0, effects.Length - ISPLIT.Length);

            effects += Protocol.GAME_CELL_EFFECTS_END;

            return info + critters + items + effects;
        }

        public static string FormatCellCritter(Character crit, Character ch)
        {
            string info = Protocol.GAME_CRITTER_INFO;

            Globals.eAlignment detectedAlignment = Globals.eAlignment.Lawful;
            Character.ClassType detectedClassType = Character.ClassType.Fighter;

            detectedAlignment = crit.Alignment;
            detectedClassType = crit.BaseProfession;
            if (crit.BaseProfession == Character.ClassType.Thief)
            {
                detectedClassType = Rules.DetectThief(crit, ch);
                if (detectedClassType != Character.ClassType.Thief)
                {
                    detectedAlignment = Globals.eAlignment.Lawful;
                }
            }
            info += crit.IsPC + VSPLIT;
            if (crit.IsPC)
            {
                info += crit.PlayerID + VSPLIT +
                    crit.Name + VSPLIT;
            }
            else
            {
                if (crit.Group == null || crit.CurrentCell == ch.CurrentCell)
                {
                    info += crit.worldNpcID + VSPLIT +
                        crit.Name + VSPLIT;
                }
                else
                {
                    info += crit.worldNpcID + VSPLIT +
                        crit.Group.GroupNPCList.Count.ToString() + " " + Cell.Multinames(crit.Name) + VSPLIT;
                }
            }

            info += crit.shortDesc + VSPLIT +
            crit.longDesc + VSPLIT +
            crit.visualKey + VSPLIT +
            (int)detectedClassType + VSPLIT +
            (int)detectedAlignment + VSPLIT +
            crit.Level + VSPLIT +
            (int)crit.gender + VSPLIT +
            crit.race + VSPLIT +
            crit.Hits + VSPLIT +
            crit.HitsMax + VSPLIT;

            if (crit.RightHand != null)
            {
                info += crit.RightHand.itemID + VSPLIT + crit.RightHand.name + VSPLIT + crit.RightHand.longDesc + VSPLIT;
            }
            else
            {
                info += VSPLIT + VSPLIT + VSPLIT;
            }
            if (crit.LeftHand != null)
            {
                info += crit.LeftHand.itemID + VSPLIT + crit.LeftHand.name + VSPLIT + crit.LeftHand.longDesc + VSPLIT;
            }
            else
            {
                info += VSPLIT + VSPLIT + VSPLIT;
            }
            info += crit.GetVisibleArmorName();
            info += Protocol.GAME_CRITTER_INFO_END;

            string inventory = Protocol.GAME_CRITTER_INVENTORY;
            foreach (Item item in crit.wearing)
            {
                if (item != null)
                {
                    if (crit.tanningResult == null || Array.IndexOf(crit.tanningResult, item.itemID) == -1)
                    {
                        inventory += FormatCellItem(item) + ISPLIT;
                    }
                }
            }
            if (inventory.Length > Protocol.GAME_CRITTER_INVENTORY.Length)
            {
                inventory = inventory.Substring(0, inventory.Length - ISPLIT.Length);
            }
            inventory += Protocol.GAME_CRITTER_INVENTORY_END;
            return info + inventory;
        }

        public static string FormatCellItem(Item item)
        {
            return "" + item.itemID + VSPLIT +
                        item.name + VSPLIT +
                        item.longDesc + VSPLIT +
                        item.visualKey + VSPLIT +
                        (int)item.wearLocation + VSPLIT +
                        (int)item.attuneType + VSPLIT +
                        item.worldItemID;
        }

        public static string FormatSpellInfo(Spell spell)
        {
            string classTypes = "";

            for (int a = 0; a < spell.ClassTypes.Length; a++)
            {
                classTypes += (int)spell.ClassTypes[a] + " ";
            }
            classTypes = classTypes.Substring(0, classTypes.Length - 1);

            return "" + spell.SpellID + VSPLIT +
                        spell.SpellCommand + VSPLIT +
                        spell.Name + VSPLIT +
                        spell.Description + VSPLIT +
                        spell.RequiredLevel + VSPLIT +
                        spell.ManaCost + VSPLIT +
                        (int)spell.SpellType + VSPLIT +
                        (int)spell.TargetType + VSPLIT +
                        classTypes + VSPLIT +
                        spell.TrainingPrice + VSPLIT +
                        spell.IsBeneficial + VSPLIT +
                        spell.SoundFile;
        }

        public static string FormatEffectInfo(Effect effect)
        {
            string effectString = effect.effectType + VSPLIT +
                effect.effectAmount + VSPLIT +
                effect.duration + VSPLIT;

            if (effect.caster != null)
                effectString += effect.caster.Name;

            return effectString;
        }

        public static string FormatScoreInfo(Character score)
        {
            return "" + score.PlayerID + VSPLIT +
                        score.Name + VSPLIT +
                        (int)score.BaseProfession + VSPLIT +
                        score.classFullName + VSPLIT +
                        score.Level + VSPLIT +
                        score.Experience + VSPLIT +
                        score.Kills + VSPLIT +
                        score.RoundsPlayed + VSPLIT +
                        score.lastOnline + VSPLIT +
                        score.IsAnonymous + VSPLIT +
                        (int)score.ImpLevel + VSPLIT +
                        score.LandID;
        }

        public static string FormatCharGenInfo(Character newbie)
        {
            string startingEquipment = "";
            string startingSpells = "";
            string startingSkills = "";

            try
            {
                #region Starting Equipment
                if (newbie.RightHand != null)
                {
                    startingEquipment += newbie.RightHand.notes + " (Right Hand)" + VSPLIT;
                }

                if (newbie.LeftHand != null)
                {
                    startingEquipment += newbie.LeftHand.notes + " (Left Hand)" + VSPLIT;
                }

                foreach (Item item in newbie.wearing)
                {
                    startingEquipment += item.notes + " (Worn)" + VSPLIT;
                }

                foreach (Item item in newbie.beltList)
                {
                    startingEquipment += item.notes + " (Belt)" + VSPLIT;
                }

                foreach (Item item in newbie.sackList)
                {
                    startingEquipment += item.notes + " (Sack)" + VSPLIT;
                }

                startingEquipment = startingEquipment.Substring(0, startingEquipment.Length - VSPLIT.Length);


                #endregion

                #region Starting Spells
                if (newbie.spellList.Count > 0)
                {
                    foreach (int spellID in newbie.spellList.ints)
                    {
                        startingSpells += spellID + VSPLIT;
                    }
                    startingSpells = startingSpells.Substring(0, startingSpells.Length - VSPLIT.Length);
                }
                #endregion

                #region Starting Skills
                foreach (Globals.eSkillType skillType in Enum.GetValues(typeof(Globals.eSkillType)))
                {
                    if (skillType != Globals.eSkillType.None)
                    {
                        startingSkills += (int)skillType + " " + newbie.GetSkillExperience(skillType) + VSPLIT;
                    }
                }

                startingSkills = startingSkills.Substring(0, startingSkills.Length - VSPLIT.Length);
                #endregion

                return "" + newbie.race + ISPLIT +
                            (int)newbie.BaseProfession + ISPLIT +
                            (int)newbie.Alignment + ISPLIT +
                            startingEquipment + ISPLIT +
                            startingSpells + ISPLIT +
                            startingSkills;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return "";
            }
        }

        public static string FormatCharGenRollerResults(Character ch)
        {
            return ch.Strength + VSPLIT +
                ch.Dexterity + VSPLIT +
                ch.Intelligence + VSPLIT +
                ch.Wisdom + VSPLIT +
                ch.Constitution + VSPLIT +
                ch.Charisma + VSPLIT +
                ch.strengthAdd + VSPLIT +
                ch.dexterityAdd + VSPLIT +
                ch.HitsMax + VSPLIT +
                ch.StaminaMax + VSPLIT +
                ch.ManaMax;
        } 
        #endregion

        #region Check Command
        public static bool CheckAllCommand(Character ch, string command, string args)
        {
            #region Ping
            if (command == PING)
            {
                ch.WriteLine(PING);
                return true;
            } 
            #endregion

            return false;
        }

        public static bool CheckMenuCommand(Character ch, string command, string args)
        {
            if (CheckAllCommand(ch, command, args))
                return true;

            #region Set Protocol
            if (command == SET_PROTOCOL)
            {
                ch.protocol = DragonsSpineMain.APP_PROTOCOL;
                Menu.PrintMainMenu(ch);
                return true;
            }
            #endregion

            #region Return False If Protocol Not Set
            else if (ch.protocol != DragonsSpineMain.APP_PROTOCOL)
            {
                return false;
            }
            #endregion

            #region Set Client
            else if (command == SET_CLIENT)
            {
                ch.usingClient = true;
                return true;
            }
            #endregion

            #region Switch Character
            else if (command == SWITCH_CHARACTER)
            {
                if (PC.SwitchCharacter(ch, Convert.ToInt32(args)))
                {
                    Protocol.SendCurrentCharacterID(ch);
                }
                return true;
            }
            #endregion

            #region Delete Character
            else if (command == DELETE_CHARACTER)
            {
                int id = Convert.ToInt32(args);

                if (id == ch.PlayerID)
                {
                    Protocol.sendMessageBox(ch, "You cannot delete your active character.", "Delete Failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return true;
                }

                string[] playerlist = DAL.DBPlayer.GetCharacterList("Name", ch.accountID);

                bool accountMatch = false;

                foreach (string name in playerlist)
                {
                    if (PC.GetName(id) == name)
                    {
                        accountMatch = true;
                        break;
                    }
                }

                if (accountMatch)
                {
                    Protocol.sendMessageBox(ch, "You have deleted your character \"" + PC.GetName(id) + "\".", "Character Delete Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DAL.DBPlayer.deleteCharacter(id);
                }
                else if (ch.ImpLevel == Globals.eImpLevel.DEV)
                {
                    Protocol.sendMessageBox(ch, "You have deleted the character \"" + PC.GetName(id) + "\" belonging to account \"" + PC.GetPC(id).account + "\".", "Character Delete Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DAL.DBPlayer.deleteCharacter(id);
                }
                return true;
            }
            #endregion

            #region Go To Conference
            else if (command == GOTO_CONFERENCE)
            {
                ch.PCState = Globals.ePlayerState.CONFERENCE;
                ch.RemoveFromMenu();
                ch.AddToLimbo();
                //ch.PCState = Character.State.CONFERENCE;
                Conference.Header(ch, true);
            } 
            #endregion

            #region Go To Game
            else if (command == GOTO_GAME)
            {
                ch.RemoveFromMenu(); // remove the character from the menu list
                ch.PCState = Globals.ePlayerState.PLAYING; // change character state to playing
                ch.AddToWorld(); // add the character to the world list
                return true;
            }
            #endregion

            #region Go To CharGen
            else if (command == GOTO_CHARGEN)
            {
                if (DAL.DBPlayer.countCharacters(ch.accountID) >= Character.MAX_CHARS) // currently verified by the client -Eb 5/17/06
                {
                    Protocol.sendMessageBox(ch, "You have reached the maximum amount of characters allowed.  Try deleting an existing character.");
                    return true;
                }
                PC newchar = new PC();
                newchar.account = ch.account;
                newchar.accountID = ch.accountID;
                newchar.protocol = ch.protocol;
                newchar.usingClient = ch.usingClient;
                newchar.echo = ch.echo;
                newchar.confRoom = ch.confRoom;
                ch.RemoveFromMenu(); // remove the character from the menu list
                PC.LoadCharacter(ch, newchar); // load the new character
                ch.AddToCharGen(); // add the character to the character generation list
                ch.Write(Protocol.CHARGEN_ENTER);
                ch.PCState = Globals.ePlayerState.PROTO_CHARGEN;
                return true;
            }
            #endregion

            #region Get Scores
            else if (command == GET_SCORES)
            {
                Protocol.sendWorldScores(ch);
                return true;
            }
            #endregion

            #region Logout
            else if (command == LOGOUT)
            {
                Utils.Log(ch.GetLogString(), Utils.LogType.Logout);
                ch.RemoveFromMenu();
                ch.RemoveFromServer();
                return true;
            }
            #endregion

            return false;
        }

        public static bool CheckChatRoomCommand(Character ch, string command, string args)
        {
            if (ch.protocol != DragonsSpineMain.APP_PROTOCOL) { return false; }

            if (CheckAllCommand(ch, command, args))
                return true;

            #region Logout
            if (command == LOGOUT)
            {
                if (!ch.IsInvisible) // send message if character is not invisible
                {
                    ch.SendToAllInConferenceRoom(Conference.GetStaffTitle(ch) + ch.Name + " has left the world.", Protocol.TextType.Exit);
                }
                Utils.Log(ch.GetLogString(), Utils.LogType.Logout);
                ch.RemoveFromLimbo();
                ch.RemoveFromServer();
                return true;
            }
            #endregion

            #region Go To CharGen
            else if (command == GOTO_CHARGEN)
            {
                if (DAL.DBPlayer.countCharacters(ch.accountID) >= Character.MAX_CHARS) // currently verified by the client -Eb 5/17/06
                {
                    Protocol.sendMessageBox(ch, "You have reached the maximum amount of characters allowed.  Try deleting an existing character.");
                    return true;
                }
                if (!ch.IsInvisible) // send message if character is not invisible
                {
                    ch.SendToAllInConferenceRoom(Conference.GetStaffTitle(ch) + ch.Name + " has left for the character generator.", Protocol.TextType.Exit);
                }
                Utils.Log(ch.GetLogString(), Utils.LogType.Logout);
                PC newchar = new PC();
                newchar.account = ch.account;
                newchar.accountID = ch.accountID;
                newchar.protocol = ch.protocol;
                newchar.usingClient = ch.usingClient;
                newchar.echo = ch.echo;
                newchar.confRoom = ch.confRoom;
                ch.RemoveFromLimbo(); // remove the character from the menu list
                PC.LoadCharacter(ch, newchar); // load the new character
                ch.AddToCharGen(); // add the character to the character generation list
                ch.Write(Protocol.CHARGEN_ENTER);
                ch.PCState = Globals.ePlayerState.PROTO_CHARGEN;
            }
            #endregion

            #region Delete Character
            else if (command == DELETE_CHARACTER)
            {
                int id = Convert.ToInt32(args);

                if (id == ch.PlayerID)
                {
                    Protocol.sendMessageBox(ch, "You cannot delete your active character.", "Delete Failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return true;
                }

                string[] playerlist = DAL.DBPlayer.GetCharacterList("Name", ch.accountID);

                bool accountMatch = false;

                foreach (string name in playerlist)
                {
                    if (PC.GetName(id) == name)
                    {
                        accountMatch = true;
                        break;
                    }
                }

                if (accountMatch)
                {
                    Protocol.sendMessageBox(ch, "You have deleted your character \"" + PC.GetName(id) + "\".", "Character Delete Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DAL.DBPlayer.deleteCharacter(id);
                }
                else if (ch.ImpLevel == Globals.eImpLevel.DEV)
                {
                    Protocol.sendMessageBox(ch, "You have deleted the character \"" + PC.GetName(id) + "\" belonging to account \"" + PC.GetPC(id).account + "\".", "Character Delete Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DAL.DBPlayer.deleteCharacter(id);
                }
                return true;
            }
            #endregion

            #region Switch Character
            else if (command == SWITCH_CHARACTER)
            {
                if (PC.SwitchCharacter(ch, Convert.ToInt32(args)))
                {
                    Protocol.SendCurrentCharacterID(ch);
                }
                return true;
            }
            #endregion

            #region Go To Game
            else if (command == GOTO_GAME)
            {
                ch.RemoveFromLimbo();
                ch.PCState = Globals.ePlayerState.PLAYING;
                ch.AddToWorld();

                if (ch.IsAnonymous && !ch.IsInvisible)
                {
                    ch.SendToAllInConferenceRoom(Conference.GetStaffTitle(ch) + ch.Name + " has left for the lands.", Protocol.TextType.Exit);
                }
                else if (!ch.IsAnonymous && !ch.IsInvisible)
                {
                    ch.SendToAllInConferenceRoom(Conference.GetStaffTitle(ch) + ch.Name + " has left for " + World.GetFacetByID(ch.FacetID).GetLandByID(ch.LandID).GetMapByID(ch.MapID).ShortDesc + " (" + World.GetFacetByID(ch.FacetID)+").", Protocol.TextType.Exit);
                }
                return true;
            }
            #endregion

            #region Go To Menu
            else if (command == GOTO_MENU)
            {
                ch.PCState = Globals.ePlayerState.MENU;
                Menu.PrintMainMenu(ch);
            }
            #endregion

            #region Get Scores
            else if (command == GET_SCORES)
            {
                Protocol.sendWorldScores(ch);
                return true;
            }
            #endregion

            return false;
        }

        public static bool CheckCharGenCommand(Character ch, string command, string args)
        {
            if (CheckAllCommand(ch, command, args))
                return true;

            // Step 1: gender, race, class type and name verification
            // Step 2: roll stats or go back to step 1

            #region CharGen Receive (receive gender, homeland (race), class type and name from client)
            if (command == CHARGEN_RECEIVE)
            {
                string[] cArgs = args.Split(VSPLIT.ToCharArray()); // gender, homeland, classType, name
                bool genderOK = false;
                foreach (Globals.eGender gender in Enum.GetValues(typeof(Globals.eGender)))
                {
                    if (cArgs[0] == gender.ToString())
                    {
                        ch.gender = gender;
                        genderOK = true;
                    }
                }
                if (!genderOK)
                {
                    ch.Write(Protocol.CHARGEN_ERROR);
                    return true;
                }

                bool homelandOK = false;
                foreach (Globals.eHomeland homeland in Enum.GetValues(typeof(Globals.eHomeland)))
                {
                    if (cArgs[1] == homeland.ToString())
                    {
                        ch.race = homeland.ToString();
                        homelandOK = true;
                    }
                }
                if (!homelandOK)
                {
                    ch.Write(Protocol.CHARGEN_ERROR);
                    return true;
                }

                bool classTypeOK = false;
                foreach (Character.ClassType classType in Enum.GetValues(typeof(Character.ClassType)))
                {
                    if (cArgs[2] == Utils.FormatEnumString(classType.ToString()))
                    {
                        ch.BaseProfession = classType;
                        ch.classFullName = Utils.FormatEnumString(classType.ToString());
                        classTypeOK = true;
                    }
                }
                if (!classTypeOK)
                {
                    ch.Write(Protocol.CHARGEN_ERROR);
                    return true;
                }

                if (CharGen.CharacterNameDenied(ch, cArgs[3]))
                {
                    ch.Write(Protocol.CHARGEN_INVALIDNAME);
                    return true;
                }

                ch.Name = cArgs[3].Substring(0, 1).ToUpper() + cArgs[3].Substring(1, cArgs[3].Length - 1);
                ch.Write(Protocol.CHARGEN_ACCEPTED);
                ch.Write(CharGen.RollStats(ch));
                ch.PCState = Globals.ePlayerState.ROLLSTATS;
                return true;
            }
            #endregion

            #region Send Back To Step One
            else if (command == GOTO_CHARGEN)
            {
                ch.PCState = Globals.ePlayerState.PROTO_CHARGEN;
                return true;
            }
            #endregion

            #region Go To Menu
            else if (command == GOTO_MENU)
            {
                int lastPlayed = Account.GetLastPlayed(ch.accountID);

                if (lastPlayed > 0) // new account with no character cannot leave chargen
                {
                    PC pc1 = PC.GetPC(lastPlayed);
                    ch.RemoveFromCharGen();
                    ch.PlayerID = lastPlayed;
                    pc1.PlayerID = lastPlayed;
                    PC.LoadCharacter(ch, pc1); // fill in our ch with all the properties of lastplayed char.
                    ch.AddToMenu(); // add the character to the menu list
                    Utils.Log(ch.GetLogString(), Utils.LogType.Login);
                    ch.lastOnline = DateTime.Now; // set last online
                    PC.saveField(ch.PlayerID, "lastOnline", ch.lastOnline, null); // save last online
                    ch.PCState = Globals.ePlayerState.MENU; // set state to menu
                    Menu.PrintMainMenu(ch); // print main menu
                }
                return true;
            }
            #endregion

            #region Go To Conference
            else if (command == GOTO_CONFERENCE)
            {
                int lastPlayed = Account.GetLastPlayed(ch.accountID);

                if (lastPlayed > 0) // new account with no character cannot leave chargen
                {
                    PC pc1 = PC.GetPC(lastPlayed);
                    ch.RemoveFromCharGen();
                    ch.PlayerID = lastPlayed;
                    pc1.PlayerID = lastPlayed;
                    PC.LoadCharacter(ch, pc1); // fill in our ch with all the properties of lastplayed char.
                    ch.AddToLimbo(); // add the character to the menu list
                    Utils.Log(ch.GetLogString(), Utils.LogType.Login);
                    ch.lastOnline = DateTime.Now; // set last online
                    PC.saveField(ch.PlayerID, "lastOnline", ch.lastOnline, null); // save last online
                    ch.PCState = Globals.ePlayerState.CONFERENCE; // set state to menu
                    Conference.Header(ch, true);
                }
                return true;
            }
            #endregion

            #region Logout
            else if (command == LOGOUT)
            {
                Utils.Log("Account (" + ch.account + ") logged out from chargen.", Utils.LogType.Logout);
                ch.RemoveFromCharGen();
                ch.RemoveFromServer();
                return true;
            }
            #endregion

            return false;
        }

        public static bool CheckGameCommand(Character ch, string command, string args)
        {
            if (CheckAllCommand(ch, command, args))
                return true;

            #region Logout
            else if (command == LOGOUT)
            {
                Utils.Log(ch.GetLogString(), Utils.LogType.Logout);
                ch.RemoveFromWorld();
                ch.RemoveFromServer();
                return true;
            } 
            #endregion

            return false;
        }
        #endregion

        #region Character Updates
        public static void SendCharacterUpdate(PC pc, Character currentCharacter)
        {
            SendCharacterStats(pc, currentCharacter);
            SendCharacterSkills(pc, currentCharacter);
            SendCharacterRightHand(pc, currentCharacter);
            SendCharacterLeftHand(pc, currentCharacter);
            SendCharacterInventory(pc, currentCharacter);
            SendCharacterSack(pc, currentCharacter);
            SendCharacterBelt(pc, currentCharacter);
            SendCharacterRings(pc, currentCharacter);
            SendCharacterLocker(pc, currentCharacter);
            SendCharacterSpells(pc, currentCharacter);
            SendCharacterEffects(pc, currentCharacter);
            return;
        }

        public static void SendCharacterStats(PC pc, Character currentCharacter)
        {
            string message = Protocol.CHARACTER_STATS;
            try
            {
                message += pc.PlayerID + VSPLIT +
                    pc.Name + VSPLIT +
                    (int)pc.gender + VSPLIT +
                    pc.race + VSPLIT +
                    (int)pc.BaseProfession + VSPLIT +
                    pc.classFullName + VSPLIT +
                    (int)pc.Alignment + VSPLIT +
                    (int)pc.ImpLevel + VSPLIT +
                    pc.IsImmortal + VSPLIT +
                    pc.showStaffTitle + VSPLIT +
                    pc.receivePages + VSPLIT +
                    pc.receiveTells + VSPLIT +
                    Utils.ConvertIntArrayToString(pc.friendsList) + VSPLIT +
                    Utils.ConvertIntArrayToString(pc.ignoreList) + VSPLIT +
                    pc.friendNotify + VSPLIT +
                    pc.filterProfanity + VSPLIT +
                    pc.IsAncestor + VSPLIT +
                    pc.IsAnonymous + VSPLIT +
                    pc.LandID + VSPLIT +
                    pc.MapID + VSPLIT +
                    pc.X + VSPLIT +
                    pc.Y + VSPLIT +
                    pc.Stunned + VSPLIT +
                    pc.floating + VSPLIT +
                    pc.IsDead + VSPLIT +
                    pc.IsHidden + VSPLIT +
                    pc.IsInvisible + VSPLIT +
                    pc.HasNightVision + VSPLIT +
                    pc.HasFeatherFall + VSPLIT +
                    pc.CanBreatheWater + VSPLIT +
                    pc.IsBlind + VSPLIT +
                    pc.Poisoned + VSPLIT +
                    (int)pc.fighterSpecialization + VSPLIT +
                    pc.Level + VSPLIT +
                    pc.Experience + VSPLIT +
                    pc.Hits + VSPLIT +
                    pc.HitsMax + VSPLIT +
                    pc.Stamina + VSPLIT +
                    pc.StaminaMax + VSPLIT +
                    pc.Mana + VSPLIT +
                    pc.ManaMax + VSPLIT +
                    pc.Age + VSPLIT +
                    pc.RoundsPlayed + VSPLIT +
                    pc.Kills + VSPLIT +
                    pc.Deaths + VSPLIT +
                    pc.bankGold + VSPLIT +
                    pc.Strength + VSPLIT +
                    pc.Dexterity + VSPLIT +
                    pc.Intelligence + VSPLIT +
                    pc.Wisdom + VSPLIT +
                    pc.Constitution + VSPLIT +
                    pc.Charisma + VSPLIT +
                    pc.strengthAdd + VSPLIT +
                    pc.dexterityAdd + VSPLIT +
                    pc.encumbrance + VSPLIT +
                    pc.birthday + VSPLIT +
                    pc.lastOnline + VSPLIT +
                    pc.currentKarma + VSPLIT +
                    pc.currentMarks + VSPLIT +
                    pc.pvpNumKills + VSPLIT +
                    pc.pvpNumDeaths + VSPLIT +
                    Utils.ConvertListToString(pc.PlayersKilled) + VSPLIT +
                    Utils.ConvertListToString(pc.PlayersFlagged) + VSPLIT +
                    pc.knightRing + VSPLIT +
                    pc.dirPointer + VSPLIT +
                    pc.HitsAdjustment + VSPLIT +
                    pc.StaminaAdjustment + VSPLIT +
                    pc.ManaAdjustment + VSPLIT +
                    pc.visualKey + VSPLIT +
                    pc.HitsDoctored + VSPLIT +
                    pc.lastOnline.ToString();
                message += Protocol.CHARACTER_STATS_END;
                currentCharacter.Write(message);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return;
            }
            return;
        }

        public static void SendCharacterSkills(PC pc, Character currentCharacter)
        {
            Array skillTypes = Enum.GetValues(typeof(Globals.eSkillType));

            string message = Protocol.CHARACTER_SKILLS;

            for (int a = 1; a < skillTypes.Length; a++)
            {
                message += (int)skillTypes.GetValue(a) + " " + pc.GetSkillExperience((Globals.eSkillType)skillTypes.GetValue(a)) + VSPLIT;
            }
            message = message.Substring(0, message.Length - VSPLIT.Length);
            message += Protocol.CHARACTER_SKILLS_END;
            currentCharacter.Write(message);
        }

        public static void SendCharacterRightHand(PC pc, Character currentCharacter)
        {
            try
            {
                string message = CHARACTER_RIGHTHAND;
                if (pc.RightHand != null)
                {
                    message += FormatItemInfo(pc.RightHand);
                }
                currentCharacter.Write(message + CHARACTER_RIGHTHAND_END);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return;
            }
            return;
        }

        public static void SendCharacterLeftHand(PC pc, Character currentCharacter)
        {
            try
            {
                string message = CHARACTER_LEFTHAND;
                if (pc.LeftHand != null)
                {
                    message += FormatItemInfo(pc.LeftHand);
                }
                currentCharacter.Write(message + CHARACTER_LEFTHAND_END);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return;
            }
            return;
        }

        public static void SendCharacterInventory(PC pc, Character currentCharacter)
        {
            try
            {
                string message = CHARACTER_INVENTORY;
                foreach (Item item in pc.wearing)
                {
                    message += FormatItemInfo(item) + ISPLIT;
                }
                if (message.Length > CHARACTER_INVENTORY.Length) { message = message.Substring(0, message.Length - ISPLIT.Length); }
                currentCharacter.Write(message + CHARACTER_INVENTORY_END);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return;
            }
            return;
        }

        public static void SendCharacterSack(PC pc, Character currentCharacter)
        {
            try
            {
                string message = CHARACTER_SACK;
                foreach (Item item in pc.sackList)
                {
                    message += FormatItemInfo(item) + ISPLIT;
                }
                if (message.Length > CHARACTER_SACK.Length) { message = message.Substring(0, message.Length - ISPLIT.Length); }
                currentCharacter.Write(message + CHARACTER_SACK_END);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return;
            }
            return;
        }

        public static void SendCharacterBelt(PC pc, Character currentCharacter)
        {
            try
            {
                string message = CHARACTER_BELT;
                foreach (Item item in pc.beltList)
                {
                    message += FormatItemInfo(item) + ISPLIT;
                }
                if (message.Length > CHARACTER_BELT.Length) { message = message.Substring(0, message.Length - ISPLIT.Length); }
                currentCharacter.Write(message + CHARACTER_BELT_END);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return;
            }
            return;
        }

        public static void SendCharacterRings(PC pc, Character currentCharacter)
        {
            try
            {
                string message = CHARACTER_RINGS;
                Item item = null;
                for (int ring = 1; ring <= Character.MAX_RINGS; ring++)
                {
                    switch (ring)
                    {
                        case 1:
                            item = pc.RightRing1;
                            break;
                        case 2:
                            item = pc.RightRing2;
                            break;
                        case 3:
                            item = pc.RightRing3;
                            break;
                        case 4:
                            item = pc.RightRing4;
                            break;
                        case 5:
                            item = pc.LeftRing1;
                            break;
                        case 6:
                            item = pc.LeftRing2;
                            break;
                        case 7:
                            item = pc.LeftRing3;
                            break;
                        case 8:
                            item = pc.LeftRing4;
                            break;
                    }
                    if (item != null)
                    {
                        item.wearOrientation = (Globals.eWearOrientation)ring;
                        message += FormatItemInfo(item);
                    }
                    message += ISPLIT;
                }
                if (message.Length > CHARACTER_RINGS.Length) { message = message.Substring(0, message.Length - ISPLIT.Length); }
                currentCharacter.Write(message + CHARACTER_RINGS_END);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return;
            }
            return;
        }

        public static void SendCharacterLocker(PC pc, Character currentCharacter)
        {
            try
            {
                string message = CHARACTER_LOCKER;
                foreach (Item item in pc.lockerList)
                {
                    message += FormatItemInfo(item) + ISPLIT;
                }
                if (message.Length > CHARACTER_LOCKER.Length) { message = message.Substring(0, message.Length - ISPLIT.Length); }
                currentCharacter.Write(message + CHARACTER_LOCKER_END);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return;
            }
            return;
        }

        public static void SendCharacterSpells(PC pc, Character currentCharacter)
        {
            try
            {
                string message = Protocol.CHARACTER_SPELLS;
                for (int a = 0; a < pc.spellList.ints.Count; a++)
                {
                    message += pc.spellList.GetSpellID(a) + VSPLIT +
                        pc.spellList.GetString(a) + ISPLIT;
                }
                if (message.Length > Protocol.CHARACTER_SPELLS.Length) { message = message.Substring(0, message.Length - ISPLIT.Length); }
                message += Protocol.CHARACTER_SPELLS_END;
                currentCharacter.Write(message);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return;
            }
            return;
        }

        public static void SendCharacterEffects(PC pc, Character currentCharacter)
        {
            try
            {
                string message = Protocol.CHARACTER_EFFECTS;
                foreach (Effect effect in pc.effectList.Values)
                {
                    message += FormatEffectInfo(effect) + ISPLIT;
                }
                if (message.Length > Protocol.CHARACTER_EFFECTS.Length)
                {
                    message = message.Substring(0, message.Length - ISPLIT.Length);
                }
                message += Protocol.CHARACTER_EFFECTS_END;
                currentCharacter.Write(message);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return;
            }
            return;
        }

        public static void SendCurrentCharacterID(Character ch)
        {
            ch.Write(Protocol.CHARACTER_CURRENT + ch.PlayerID + Protocol.CHARACTER_CURRENT_END);
            if (ch.ImpLevel == Globals.eImpLevel.DEV)
            {
                if (!ch.sentImplementorInformation)
                {
                    Protocol.SendImplementorCharacterFields(ch);
                    ch.sentImplementorInformation = true;
                }
            }
        } 
        #endregion

        #region World Information
        public static void sendWorldLands(Character ch)
        {
            try
            {
                string message = Protocol.WORLD_LANDS;
                foreach (Land land in World.GetFacetByIndex(0).Lands)
                {
                    message += FormatLandInfo(land) + ISPLIT;
                }
                message = message.Substring(0, message.Length - ISPLIT.Length);
                message += Protocol.WORLD_LANDS_END;
                ch.WriteLine(message);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public static void sendWorldMaps(Character ch)
        {
            try
            {
                string message = Protocol.WORLD_MAPS;
                for (int a = 0; a < World.GetFacetByIndex(0).Lands.Count; a++)
                {
                    Land land = World.GetFacetByIndex(0).GetLandByIndex(a);
                    foreach (Map map in land.Maps)
                    {
                        message += FormatMapInfo(map) + ISPLIT;
                    }
                }
                message = message.Substring(0, message.Length - ISPLIT.Length);
                message += Protocol.WORLD_MAPS_END;
                ch.WriteLine(message);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public static void sendWorldNews(Character ch)
        {
            ch.Write(Protocol.NEWS + System.Configuration.ConfigurationManager.AppSettings["NEWS"] + Protocol.NEWS_END);
        }

        public static void sendWorldSpells(Character ch)
        {
            try
            {
                string message = Protocol.WORLD_SPELLS;
                foreach (Spell spell in Spell.spellDictionary.Values)
                {
                    message += FormatSpellInfo(spell) + ISPLIT;
                }
                message = message.Substring(0, message.Length - ISPLIT.Length);
                message += Protocol.WORLD_SPELLS_END;
                ch.Write(message);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public static void sendWorldVersion(Character ch)
        {
            try
            {
                string message = Protocol.VERSION_SERVER + DragonsSpineMain.APP_VERSION + Protocol.VERSION_SERVER_END;
                message += Protocol.VERSION_CLIENT + DragonsSpineMain.CLIENT_VERSION + Protocol.VERSION_CLIENT_END;
                ch.Write(message);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public static void sendWorldScores(Character ch)
        {
            try
            {
                System.Collections.ArrayList scores = DAL.DBWorld.p_getScores();
                string message = Protocol.WORLD_SCORES;
                for (int a = 0; a < MAX_SCORES; a++)
                {
                    message += FormatScoreInfo((Character)scores[a]) + ISPLIT;
                }
                message = message.Substring(0, message.Length - ISPLIT.Length);
                message += Protocol.WORLD_SCORES_END;
                ch.Write(message);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        public static void sendWorldCharGen(Character ch)
        {
            try
            {
                foreach (Character newbie in CharGen.Newbies)
                {
                    string message = Protocol.WORLD_CHARGEN_INFO;
                    message += FormatCharGenInfo(newbie);
                    message += Protocol.WORLD_CHARGEN_INFO_END;
                    if (message.Length > Protocol.WORLD_CHARGEN_INFO.Length + Protocol.WORLD_CHARGEN_INFO_END.Length)
                    {
                        ch.Write(message);
                    }
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        //public static void sendWorldItems(Character ch)
        //{
        //    try
        //    {
        //        string message = WORLD_ITEMS;
        //        foreach (Item item in Item.getItemCatalog())
        //        {
        //            message += formatItemInfo(item) + ISPLIT;
        //        }
        //        message = message.Substring(0, message.Length - ISPLIT.Length);
        //        message += WORLD_ITEMS_END;
        //        ch.Write(message);
        //    }
        //    catch (Exception e)
        //    {
        //        Utils.LogException(e);
        //    }
        //} 
        #endregion

        public static void SendImplementorCharacterFields(Character ch)
        {
            try
            {
                string message = IMP_CHARACTERFIELDS;
                System.Reflection.FieldInfo[] fieldInfo = typeof(Character).GetFields();
                foreach (System.Reflection.FieldInfo info in fieldInfo)
                {
                    message += info.Name + ISPLIT;
                }
                message = message.Substring(0, message.Length - ISPLIT.Length);
                message += IMP_CHARACTERFIELDS_END;
                ch.Write(message);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        #region Message Box
        public static void sendMessageBox(Character ch, string message)
        {
            ch.Write(Protocol.MESSAGEBOX + message + Protocol.MESSAGEBOX_END);
        }

        public static void sendMessageBox(Character ch, string message, string caption)
        {
            ch.Write(Protocol.MESSAGEBOX + message + VSPLIT + caption + Protocol.MESSAGEBOX_END);
        }

        public static void sendMessageBox(Character ch, string message, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            ch.Write(Protocol.MESSAGEBOX +
                message + VSPLIT +
                caption + VSPLIT +
                buttons.ToString() + VSPLIT +
                icon.ToString() +
                Protocol.MESSAGEBOX_END);
        } 
        #endregion

        public static void ShowMap(Character ch)
        {
            try
            {
                if (ch.IsPeeking && ch.CurrentCell != ch.PeekTarget.CurrentCell)
                {
                    ch.CurrentCell = ch.PeekTarget.CurrentCell;
                }

                if (ch.Map != null && ch.CurrentCell != null)
                    ch.Map.UpdateCellVisible(ch.CurrentCell); // update visible cells before displaying the map

                Cell[] cellArray = Cell.GetVisibleCellArray(ch.CurrentCell, 3);

                ch.Write(Protocol.GAME_NEW_ROUND);

                for (int j = 0; j < 49; j++)
                {
                    if (cellArray[j] == null || (ch.IsBlind && !ch.IsImmortal) || ((ch.CurrentCell.Effects.ContainsKey(Effect.EffectType.Darkness) ||
                        ch.CurrentCell.IsAlwaysDark) && !ch.HasNightVision))
                    {
                        ch.Write(Protocol.GAME_CELL + Protocol.GAME_CELL_END);
                    }
                    else
                    {
                        if (!ch.CurrentCell.visCells[j] && !ch.IsImmortal)
                        {
                            ch.Write(Protocol.GAME_CELL + Protocol.GAME_CELL_END);
                        }
                        else
                        {
                            ch.Write(Protocol.GAME_CELL + Protocol.FormatCellInfo(cellArray[j], ch) + Protocol.GAME_CELL_END);
                        }
                    }
                }

                ch.Write(ch.dirPointer + Protocol.GAME_POINTER_UPDATE);

                ch.Write(Protocol.GAME_END_ROUND);

                Protocol.SendCharacterStats((PC)ch, ch);
                Protocol.SendCharacterRightHand((PC)ch, ch);
                Protocol.SendCharacterLeftHand((PC)ch, ch);

                //string worldInfo = Protocol.GAME_WORLD_INFO;
                //worldInfo += ch.clientRoundTimer+VSPLIT+
                //    DragonsSpineMain.gameDayDesc+VSPLIT+
                //    DragonsSpineMain.gameTimeDesc+VSPLIT+
                //    DragonsSpineMain.moonPhaseDesc;
                //worldInfo += Protocol.GAME_WORLD_INFO_END;

                //send world information
                //ch.Write(worldInfo);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }
    }
}