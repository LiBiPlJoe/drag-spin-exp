using System;

namespace DragonsSpine
{
    public static class Sound
    {
        public enum CommonSound { 
            DogBark,
            MeleeMiss,
            UnarmedMiss,
            ThrownWeapon,
            MetalBlock,
            WoodBlock,
            HandBlock,
            DeathRevive,
            SpellFail,
            Splash,
            EatFood,
            DrinkBottle,
            OpenBottle,
            Smithy,
            EarthQuake,
            ThunderClap,
            MapPortal,
            BreakingGlass,
            OpenDoor,
            CloseDoor,
            NockCrossbow,
            NockBow,
            MaleHmm,
            MaleFumble,
            DeathMale,
            FallingMale,
            MaleSpellWarm,
            FemaleHmm,
            FemaleFumble,
            DeathFemale,
            FemaleSpellWarm,
            Beep,
            DeathMalePlayer,
            DeathFemalePlayer,
            FallingFemale,
            LevelUp,
            SkillUp,
            RecallReset }
        //TODO: put these into configuration settings

        public static string GetSoundForDamage(string damageAdjective)
        {// light, moderate, heavy, severe, fatal

            if(damageAdjective.StartsWith("fatal") || damageAdjective.StartsWith("severe"))
                return "0054";

            if (damageAdjective.StartsWith("heavy") || damageAdjective.StartsWith("moderate"))
                return "0053";

            return "0052";
        }

        public static string GetCommonSound(CommonSound commonSound)
        {
            switch (commonSound)
            {
                case CommonSound.DogBark:
                    return "0013";
                case CommonSound.MeleeMiss:
                    if (Rules.RollD(1, 2) == 1)
                    {
                        return "0045";
                    }
                    return "0046";
                case CommonSound.UnarmedMiss:
                    return "0047";
                case CommonSound.ThrownWeapon:
                    return "0048";
                case CommonSound.MetalBlock:
                    return "0049";
                case CommonSound.WoodBlock:
                    return "0050";
                case CommonSound.HandBlock:
                    return "0051";
                case CommonSound.DeathRevive:
                    return "0055";
                case CommonSound.SpellFail:
                    return "0056";
                case CommonSound.Splash:
                    return "0059";
                case CommonSound.DrinkBottle:
                    return "0061";
                case CommonSound.EatFood:
                    return "0062";
                case CommonSound.OpenBottle:
                    return "0063";
                case CommonSound.Smithy:
                    return "0064";
                case CommonSound.EarthQuake:
                    return "0065";
                case CommonSound.ThunderClap:
                    return "0066";
                case CommonSound.MapPortal:
                    return "0067";
                case CommonSound.BreakingGlass:
                    return "0071";
                case CommonSound.OpenDoor:
                    return "0073";
                case CommonSound.CloseDoor:
                    return "0074";
                case CommonSound.NockCrossbow:
                    return "0075";
                case CommonSound.NockBow:
                    return "0076";
                case CommonSound.MaleHmm:
                    return "0077";
                case CommonSound.MaleFumble:
                    return "0078";
                case CommonSound.DeathMale:
                    return "0079";
                case CommonSound.MaleSpellWarm:
                    return "0080";
                case CommonSound.FemaleHmm:
                    return "0081";
                case CommonSound.FemaleFumble:
                    return "0082";
                case CommonSound.DeathFemale:
                    return "0083";
                case CommonSound.FemaleSpellWarm:
                    return "0084";
                case CommonSound.Beep:
                    return "0089";
                case CommonSound.DeathMalePlayer:
                    return "0186";
                case CommonSound.FallingMale:
                    return "0187";
                case CommonSound.DeathFemalePlayer:
                    return "0205";
                case CommonSound.FallingFemale:
                    return "0218";
                case CommonSound.LevelUp:
                    return "0219";
                case CommonSound.SkillUp:
                    return "0220";
                case CommonSound.RecallReset:
                    return "0221";
                default:
                    return "";
            }
        }
    }
}
