namespace DragonsSpine
{
    using System;
    using System.Reflection;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;

    public class Spell
    {
        public static Dictionary<int, Spell> spellDictionary = new Dictionary<int, Spell>(); // global list of spells
        public static Dictionary<string, Spell> spellCommandDictionary = new Dictionary<string, Spell>();

        private const int MAGIC_WORDS_LENGTH = 4; // the total number of magic words per spell
        private const int BANISH_MULTI = 10;

        private static string[] magicWords = {"aazag","alla","alsi","anaku","angarru","anghizidda","anna","annunna","ardata","ashak",
            "baad","dingir","duppira","edin","enaa","endul","enmeshir","enn","ennul","esha","gallu","gidim","gish","ia","idpa","igigi",
            "ina","isa","khitim","kia","kielgallal","kima","ku","lalartu","limutuma","lini","ma","mardukka","masqim","mass","na","naa",
		    "namtar","nebo","nenlil","nergal","ninda","ningi","ninn","ninnda","ninnghizhidda","ninnme","nngi","nushi","qutri","raa","sagba",
			"shadu","shammash","shunu","shurrim","telal","uhddil","urruku","uruki","utug","utuk","utuq","ya","yu","zi","zumri","kanpa",
		    "ziyilqa","luubluyi","luudnin","luuppatar","xul","ssaratu","zu","barra","kunushi","tamatunu","ega","cuthalu","egura","asaru",
            "urma","muxxisha","akki","ilani","gishtugbi","arrflug"};

        #region Private Data
        int spellID; // each spell has a unique spellID
        bool beneficial; // used by npc's to assist in determining which spell to cast at a target
        Character.ClassType[] classTypes; // array of classType that can cast this spell
        Globals.eSpellType spellType; //
        Globals.eTargetType targetType; //
        int manaCost; // mana cost to cast the spell
        int requiredLevel;
        int trainingPrice; // purchase price
        string methodName; // method used
        string description; // description of the spell
        string spellCommand; // spell command
        string name; // name of the spell
        string soundFile; // sound file info
        #endregion

        #region Public Properties
        public int SpellID
        {
            get { return this.spellID; }
        }
        public bool IsBeneficial
        {
            get { return this.beneficial; }
        }
        public Character.ClassType[] ClassTypes
        {
            get { return this.classTypes; }
        }
        public Globals.eSpellType SpellType
        {
            get { return this.spellType; }
        }
        public Globals.eTargetType TargetType
        {
            get { return this.targetType; }
        }
        public int ManaCost
        {
            get { return this.manaCost; }
        }
        public int RequiredLevel
        {
            get { return this.requiredLevel; }
        }
        public int TrainingPrice
        {
            get { return this.trainingPrice; }
        }
        public string Description
        {
            get { return this.description; }
        }
        public string SpellCommand
        {
            get { return this.spellCommand; }
        }
        public string Name
        {
            get { return this.name; }
        }
        public string SoundFile
        {
            get { return this.soundFile; }
        }
        public MethodInfo Method
        {
            get { return typeof(Spell).GetMethod(this.methodName, BindingFlags.Instance | BindingFlags.NonPublic); }
        }
        public bool IsClassSpell(Character.ClassType classType)
        {
            if (Array.IndexOf(this.ClassTypes, classType) != -1)
            {
                return true;
            }
            return false;
        }
        #endregion

        #region Constructor
        public Spell(System.Data.DataRow dr)
        {
            this.spellID = Convert.ToInt32(dr["spellID"]);
            this.spellCommand = dr["command"].ToString();
            this.name = dr["name"].ToString();
            this.description = dr["description"].ToString();
            this.methodName = dr["methodName"].ToString();
            this.beneficial = Convert.ToBoolean(dr["beneficial"]);
            string[] classTypes = dr["classTypes"].ToString().Split(" ".ToCharArray());
            this.classTypes = new Character.ClassType[classTypes.Length];
            for (int a = 0; a < classTypes.Length; a++)
                this.ClassTypes[a] = (Character.ClassType)Convert.ToInt32(classTypes[a]);
            this.spellType = (Globals.eSpellType)Convert.ToInt32(dr["spellType"]);
            this.targetType = (Globals.eTargetType)Convert.ToInt32(dr["targetType"]);
            this.requiredLevel = Convert.ToInt32(dr["requiredLevel"]);
            this.trainingPrice = Convert.ToInt32(dr["trainingPrice"]);
            this.manaCost = Convert.ToInt32(dr["manaCost"]);
            this.soundFile = dr["soundFile"].ToString();
            this.methodName = dr["methodName"].ToString();
        }
        #endregion

        private Character FindAndConfirmTarget(Character caster, string name)
        {
            if (name.ToLower() == this.spellCommand.ToLower())
                name = caster.Name;

            Character target;

            int id = 0;

            try
            {
                id = Convert.ToInt32(name);
                if (caster.Group != null && caster.Group.IsGroupMember(name))
                {
                    target = Map.FindTargetInView(caster, id, true, true);
                }
                else
                {
                    target = Map.FindTargetInView(caster, id, true, false);
                }

            }
            catch
            {
                if (caster.Group != null && caster.Group.IsGroupMember(name))
                {
                    target = Map.FindTargetInView(caster, name, true, true);
                }
                else
                {
                    target = Map.FindTargetInView(caster, name, true, false);
                }
            }

            switch (caster.preppedSpell.targetType)
            {
                case Globals.eTargetType.Area_Effect:
                    if (target == null || name == null)
                    {
                        target = caster;
                    }
                    if (target.merchantType > Merchant.MerchantType.None)
                    {
                        caster.WriteToDisplay("You cannot cast spells at a merchant yet.");
                        target = null;
                    }
                    if (target.trainerType > 0)
                    {
                        caster.WriteToDisplay("You cannot cast spells at a trainer yet.");
                        target = null;
                    }
                    break;
                case Globals.eTargetType.Group:
                    // not implemented yet
                    break;
                case Globals.eTargetType.Point_Blank_Area_Effect:
                    target = caster;
                    break;
                case Globals.eTargetType.Self:
                    target = caster;
                    break;
                case Globals.eTargetType.Single:
                    if (target == null || name == null)
                        GenericFailMessage(caster, "You do not see " + name + " here.");
                    break;
                default:
                    caster.WriteToDisplay("ERROR: Inform the developers that you could not find and confirm a target for your spell.");
                    target = null;
                    break;
            }
            return target;
        }

        private static void GenericFailMessage(Character caster, string reason)
        {
            caster.WriteToDisplay("Your spell fails. " + reason);
            caster.EmitSound(Sound.GetCommonSound(Sound.CommonSound.SpellFail));
        }

        public static bool LoadSpells()
        {
            try
            {
                ArrayList spellsList = DAL.DBWorld.LoadSpells();

                foreach (Spell spell in spellsList)
                {
                    Spell.spellDictionary.Add(spell.SpellID, spell);
                    Spell.spellCommandDictionary.Add(spell.spellCommand, spell);
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return false;
            }
            return true;
        }

        public static string GetLogString(Spell spell)
        {
            return "[SpellID: " + spell.SpellID + "] " + spell.Name + " (" + spell.spellType + ", " + spell.targetType + ")";
        }

        private static int GetDirectDamageModifier(Character caster)
        {
            if (caster.IsWisdomCaster)
            {
                if (Rules.RollD(1, 20) == 20) // critical roll
                {
                    if (caster.IsPC) caster.WriteToDisplay("Your spell deals critical damage!");
                    return Rules.dice.Next((int)(caster.Wisdom / 2), caster.Wisdom + 1);
                }
                return Rules.dice.Next(0, (int)(caster.Wisdom / 2) + 1);
            }
            else if (caster.IsIntelligenceCaster)
            {
                if (Rules.RollD(1, 20) == 20) // critical roll
                {
                    if (caster.IsPC) caster.WriteToDisplay("Your spell deals critical damage!");
                    return Rules.dice.Next((int)(caster.Intelligence / 2), caster.Intelligence + 1);
                }
                return Rules.dice.Next(0, (int)(caster.Intelligence / 2) + 1);
            }
            return 0;
        }

        public static Spell GetSpell(int spellID)
        {
            if (spellDictionary.ContainsKey(spellID))
            {
                return spellDictionary[spellID];
            }
            return null;
        }

        public static Spell GetSpell(string spellCommand)
        {
            if (spellCommandDictionary.ContainsKey(spellCommand.ToLower()))
            {
                return spellCommandDictionary[spellCommand.ToLower()];
            }
            return null;
        }

        public static void TeachSpell(Character caster, string spellCommand)
        {
            Spell spell = GetSpell(spellCommand);

            string words = GenerateMagicWords();

            caster.spellList.Add(spell.SpellID, words);

            caster.WriteToDisplay(caster.TargetName + ": This incantation will cast the spell " + spell.Name + ". (" + spell.SpellCommand + ")");

            caster.WriteToDisplay(caster.TargetName + ": " + words);
        }

        public static string GenerateMagicWords()
        {
            string words = null;

            for (int a = 1; a <= MAGIC_WORDS_LENGTH; a++)
            {
                if (words == null)
                {
                    words = (magicWords[Rules.dice.Next(0, magicWords.Length - 1)]);
                }
                else
                {
                    words += " " + magicWords[Rules.dice.Next(0, magicWords.Length - 1)];
                }
            }
            return words;
        }

        public static void CastGenericAreaSpell(Cell center, string args, Effect.EffectType EffectType, int EffectPower, string spellName)
        {
            int AreaSize = 2;
            int XCordMod = 0;
            int YCordMod = 0;

            string EffectGraphic = "";

            string[] sArgs = args.Split(" ".ToCharArray());

            if (EffectPower == 0)
            {
                EffectPower = 15;
            }
            // check that they give us a starting cord
            //loop through the arguments to find the starting point

            ArrayList areaCellList = new ArrayList();
            int[] XCastMod = new int[] { 0, 0, 0 };
            int[] YCastMod = new int[] { 0, 0, 0 };

            // turn undead effects all undead on the current display map
            if (EffectType == Effect.EffectType.Turn_Undead)
            {
                AreaSize = 3;
            }

            if (EffectType == Effect.EffectType.Nitro)
            {
                AreaSize = 1;
                EffectType = Effect.EffectType.Concussion;
            }

            #region Create the list of effect cells
            if (AreaSize == 1)
            {
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y, center.Z));
            }
            else if (AreaSize > 2)
            {
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 2, center.Y + -2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 2, center.Y + -2, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 1, center.Y + -2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 1, center.Y + -2, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + -2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + -2, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + -2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + -2, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y + -2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y + -2, center.Z));

                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 2, center.Y + -1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 2, center.Y + -1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 1, center.Y + -1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 1, center.Y + -1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + -1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + -1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + -1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + -1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y + -1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y + -1, center.Z));

                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -2, center.Y, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -2, center.Y, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y, center.Z));

                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -2, center.Y + 1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -2, center.Y + 1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y + 1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y + 1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + 1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + 1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + 1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + 1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y + 1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y + 1, center.Z));

                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -2, center.Y + 2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -2, center.Y + 2, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y + 2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y + 2, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + 2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + 2, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + 2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + 2, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y + 2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y + 2, center.Z));


            }
            else
            {

                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 1, center.Y + -1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 1, center.Y + -1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + -1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + -1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + -1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + -1, center.Z));

                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y, center.Z));

                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y + 1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y + 1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + 1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + 1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + 1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + 1, center.Z));
            }
            #endregion

            #region Adjust EffectPower based on AreaSize and EffectType
            if (AreaSize >= 3)
            {
                if (EffectType != Effect.EffectType.Turn_Undead)
                {
                    EffectPower = (int)(EffectPower / 2);
                }
            }
            else if (AreaSize == 1)
            {
                EffectPower = EffectPower * 2;
            }
            #endregion

            if (EffectType == Effect.EffectType.Concussion)
            {
                #region Concussion
                Cell centerCell = Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + XCordMod, center.Y + YCordMod, center.Z);
                Cell[] cellArray = Cell.GetVisibleCellArray(centerCell, AreaSize);

                foreach (Cell cell in areaCellList)
                {
                    Rules.DoConcussionDamage(cell, EffectPower, null);
                    //chr.WriteToDisplay("You have been hit by an explosion!");
                }

                ArrayList brokenWallsList = new ArrayList();

                for (int j = 0; j < cellArray.Length; j++)
                {
                    if (cellArray[j] != null)
                    {
                        if ((cellArray[j].CellGraphic == "[]" || cellArray[j].CellGraphic == "SD") && !cellArray[j].IsMagicDead)
                        {
                            if (Rules.RollD(1, 100) < 30)
                            {
                                brokenWallsList.Add(cellArray[j]);
                            }
                        }
                    }
                }
                if (brokenWallsList.Count > 0)
                {
                    Effect effect = new Effect(Effect.EffectType.Illusion, "[_", 1, -1, null, brokenWallsList);
                }
                #endregion
            }
            else if (EffectType == Effect.EffectType.Fire)
            {
                #region Fireball
                Cell centerCell = Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + XCordMod, center.Y + YCordMod, center.Z);
                //Cell[] cellArray = Cell.GetVisibleCellArray(centerCell, 3);

                EffectGraphic = "**";

                Effect effect = new Effect(EffectType, EffectGraphic, EffectPower, 2, null, areaCellList);

                foreach (Cell cell in areaCellList)
                {
                    Effect.DoAreaEffect(cell, effect);

                    for (int m = 0; m < cell.Characters.Count; m++)
                    {
                        Character chr = (Character)cell.Characters[m];
                        if (!chr.IsInvisible && !chr.IsImmortal && !chr.IsDead)
                        {
                            chr.WriteToDisplay("You have been hit by a fireball!");
                        }
                    }
                }
                #endregion
            }
            else if (EffectType == Effect.EffectType.Ice)
            {
                #region Icestorm
                Cell centerCell = Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + XCordMod, center.Y + YCordMod, center.Z);
                //Cell[] cellArray = Cell.GetVisibleCellArray(centerCell, 3);
                foreach (Cell cell in areaCellList)
                {
                    for (int m = 0; m < cell.Characters.Count; m++)
                    {
                        Character chr = (Character)cell.Characters[m];
                        if (!chr.IsDead)
                        {
                            Rules.DoSpellDamage(null, chr, null, EffectPower, "ice");
                            chr.WriteToDisplay("You have been hit by a raging ice storm!");
                        }
                    }
                }

                EffectGraphic = "~.";

                Effect effect = new Effect(EffectType, EffectGraphic, EffectPower, 1, null, areaCellList);
                #endregion
            }
            else
            {
                string spellMsg = "none";
                int spellDuration = 1;

                switch (EffectType)
                {
                    case Effect.EffectType.Light:
                        EffectGraphic = "";
                        spellMsg = "The area is illuminated by a burst of magical " + spellName.ToLower() + "!";
                        break;
                    case Effect.EffectType.Turn_Undead:
                        EffectGraphic = "";
                        spellMsg = "You feel a strong wind race through the area.";
                        spellDuration = 0;
                        break;
                    case Effect.EffectType.Darkness:
                        EffectGraphic = "";
                        spellMsg = "The area is covered in a shroud of magical " + spellName.ToLower() + ".";
                        break;
                    default:
                        EffectGraphic = "";
                        break;
                }
                if (spellMsg != "none")
                {
                    foreach (Cell cell in areaCellList)
                    {
                        foreach (Character chr in cell.Characters)
                        {
                            chr.WriteToDisplay(spellMsg);
                        }
                    }
                }
                Effect effect = new Effect(EffectType, EffectGraphic, EffectPower, spellDuration, null, areaCellList);
            }
        }

        public static void CastGenericAreaSpell(Cell center, string args, Effect.EffectType EffectType, int EffectPower, string spellName, Character caster)
        {
            int AreaSize = 2;
            int XCordMod = 0;
            int YCordMod = 0;

            string[] sArgs = args.Split(" ".ToCharArray());

            if (EffectPower == 0)
            {
                EffectPower = 15;
            }

            ArrayList areaCellList = new ArrayList();
            #region Set the size of the effect based on type
            // turn undead effects all undead on the current display map
            if (EffectType == Effect.EffectType.Turn_Undead)
            {
                AreaSize = 3;
            }

            if (EffectType == Effect.EffectType.Nitro)
            {
                AreaSize = 1;
                EffectType = Effect.EffectType.Concussion;
            }
            #endregion
            #region Create the list of effect cells
            if (AreaSize == 1)
            {
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y, center.Z));
            }
            else if (AreaSize > 2)
            {
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 2, center.Y + -2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 2, center.Y + -2, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 1, center.Y + -2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 1, center.Y + -2, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + -2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + -2, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + -2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + -2, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y + -2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y + -2, center.Z));

                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 2, center.Y + -1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 2, center.Y + -1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 1, center.Y + -1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 1, center.Y + -1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + -1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + -1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + -1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + -1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y + -1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y + -1, center.Z));

                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -2, center.Y, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -2, center.Y, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y, center.Z));

                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -2, center.Y + 1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -2, center.Y + 1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y + 1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y + 1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + 1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + 1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + 1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + 1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y + 1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y + 1, center.Z));

                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -2, center.Y + 2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -2, center.Y + 2, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y + 2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y + 2, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + 2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + 2, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + 2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + 2, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y + 2, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 2, center.Y + 2, center.Z));


            }
            else
            {

                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 1, center.Y + -1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X - 1, center.Y + -1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + -1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + -1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + -1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + -1, center.Z));

                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y, center.Z));

                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y + 1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + -1, center.Y + 1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + 1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X, center.Y + 1, center.Z));
                if (!Map.IsSpellPathBlocked(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + 1, center.Z)))
                    areaCellList.Add(Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + 1, center.Y + 1, center.Z));
            }
            #endregion

            #region Adjust EffectPower based on AreaSize and EffectType
            if (AreaSize >= 3)
            {
                if (EffectType != Effect.EffectType.Turn_Undead)
                {
                    EffectPower = (int)(EffectPower / 2);
                }
            }
            else if (AreaSize == 1)
            {
                EffectPower = EffectPower * 2;
            }
            #endregion

            if (EffectType == Effect.EffectType.Concussion)
            {
                #region Concussion
                Cell centerCell = Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + XCordMod, center.Y + YCordMod, center.Z);
                Cell[] cellArray = Cell.GetVisibleCellArray(centerCell, AreaSize);

                foreach (Cell cell in areaCellList)
                {
                    Rules.DoConcussionDamage(cell, EffectPower, caster);
                    //chr.WriteToDisplay("You have been hit by an explosion!");
                }

                ArrayList brokenWallsList = new ArrayList();

                for (int j = 0; j < cellArray.Length; j++)
                {
                    if (cellArray[j] != null)
                    {
                        if ((cellArray[j].CellGraphic == "[]" || cellArray[j].CellGraphic == "SD") && !cellArray[j].IsMagicDead)
                        {
                            if (Rules.RollD(1, 100) < 30)
                            {
                                brokenWallsList.Add(cellArray[j]);
                            }
                        }
                    }
                }
                if (brokenWallsList.Count > 0)
                {
                    Effect effect = new Effect(Effect.EffectType.Illusion, "[_", 1, -1, null, brokenWallsList);
                }
                #endregion

            }
            else if (EffectType == Effect.EffectType.Fire)
            {
                #region Fireball
                Cell centerCell = Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + XCordMod, center.Y + YCordMod, center.Z);



                centerCell.SendShout("a whoosh of fire!");

                Effect effect = new Effect(EffectType, "**", EffectPower, 2, caster, areaCellList);

                foreach (Cell cell in areaCellList)
                {
                    //Effect.DoAreaEffect(cell, effect);

                    for (int m = 0; m < cell.Characters.Count; m++)
                    {
                        Character chr = cell.Characters[m];
                        if (!chr.IsInvisible && !chr.IsImmortal && !chr.IsDead)
                        {
                            chr.WriteToDisplay("You have been hit by a fireball!");
                        }
                    }
                }
                #endregion
            }
            else if (EffectType == Effect.EffectType.Ice)
            {
                #region Icestorm
                Cell centerCell = Cell.GetCell(center.FacetID, center.LandID, center.MapID, center.X + XCordMod, center.Y + YCordMod, center.Z);

                //if (this.SoundFile != "") { centerCell.EmitSound(this.SoundFile); }

                centerCell.SendShout("the pounding of giant ice pellets!");

                Effect effect = new Effect(EffectType, "~.", EffectPower, 2, caster, areaCellList);

                foreach (Cell cell in areaCellList)
                {
                    Effect.DoAreaEffect(cell, effect);
                    for (int m = 0; m < cell.Characters.Count; m++)
                    {
                        Character chr = cell.Characters[m];
                        if (!chr.IsInvisible && !chr.IsImmortal && !chr.IsDead)
                        {
                            chr.WriteToDisplay("You have been hit by a raging ice storm!");
                        }
                    }
                }
                #endregion
            }
            else if (EffectType == Effect.EffectType.Lightning)
            {

            }
        }

        public void WarmSpell(Character caster)
        {
            if (caster.IsUndead || caster.race == "")
            {
                if (caster.attackSound != "")
                {
                    caster.EmitSound(caster.attackSound);
                }
                else
                {
                    if (caster.gender == Globals.eGender.Female)
                    {
                        caster.EmitSound(Sound.GetCommonSound(Sound.CommonSound.FemaleSpellWarm));
                    }
                    else
                    {
                        caster.EmitSound(Sound.GetCommonSound(Sound.CommonSound.MaleSpellWarm));
                    }
                }
            }
            else
            {
                if (caster.gender == Globals.eGender.Female)
                {
                    caster.EmitSound(Sound.GetCommonSound(Sound.CommonSound.FemaleSpellWarm));
                }
                else
                {
                    caster.EmitSound(Sound.GetCommonSound(Sound.CommonSound.MaleSpellWarm));
                }
            }

            caster.WriteToDisplay("You warm the spell " + this.Name + ".");
        }

        public void CastSpell(Character caster, string args)
        {
            try
            {
                Object[] obj = new Object[2];
                obj[0] = caster;
                obj[1] = args;

                #region Spell Cast Logging
                if (caster.IsPC)
                {
                    if (caster.preppedSpell.IsBeneficial)
                    {
                        Utils.Log(caster.GetLogString() + " cast " + GetLogString(this) + " with args (" + args + ")", Utils.LogType.SpellBeneficialFromPlayer);
                    }
                    else
                    {
                        Utils.Log(caster.GetLogString() + " cast " + GetLogString(this) + " with args (" + args + ")", Utils.LogType.SpellHarmfulFromPlayer);
                    }
                }
                else
                {
                    if (caster.preppedSpell.IsBeneficial)
                    {
                        Utils.Log(caster.GetLogString() + " cast " + GetLogString(this) + " with args (" + args + ")", Utils.LogType.SpellBeneficialFromCreature);
                    }
                    else
                    {
                        Utils.Log(caster.GetLogString() + " cast " + GetLogString(this) + " with args (" + args + ")", Utils.LogType.SpellHarmfulFromCreature);
                    }
                }
                #endregion
                this.Method.Invoke(this, obj);
            }
            catch (Exception e)
            {
                Utils.Log("Failure at spell.CastSpell(" + caster.GetLogString() + ", " + args + ")", Utils.LogType.SystemFailure);
                Utils.LogException(e);
            }
        }

        private void CastGenericAreaSpell(Character caster, string args, Effect.EffectType EffectType, int EffectPower, string spellName)
        {
            int AreaSize = 2;
            int XCordMod = 0;
            int YCordMod = 0;
            int argCount = 0;

            String[] sArgs = args.Split(" ".ToCharArray());
            if (EffectPower == 0)
            {
                EffectPower = 15;
            }
            // check that they give us a starting cord
            if (sArgs[0] == null)
            {
            }
            else
            {
                if (sArgs.Length > 1 && sArgs[1] == "at")
                {
                    Character target = FindAndConfirmTarget(caster, sArgs[2]);
                    if (target != null)
                    {
                        if (caster.Y > target.Y)
                        {
                            YCordMod -= (caster.Y - target.Y);
                        }
                        else
                        {
                            YCordMod += target.Y - caster.Y;
                        }
                        if (caster.X > target.X)
                        {
                            XCordMod -= (caster.X - target.X);
                        }
                        else
                        {
                            XCordMod += (target.X - caster.X);
                        }
                    }
                }

                //loop through the arguments to find the starting point
                while (argCount < sArgs.Length)
                {
                    switch (sArgs[argCount])
                    {
                        case "north":
                        case "n":
                            YCordMod -= 1;
                            break;
                        case "south":
                        case "s":
                            YCordMod += 1;
                            break;
                        case "west":
                        case "w":
                            XCordMod -= 1;
                            break;
                        case "east":
                        case "e":
                            XCordMod += 1;
                            break;
                        case "northeast":
                        case "ne":
                            YCordMod -= 1;
                            XCordMod += 1;
                            break;
                        case "northwest":
                        case "nw":
                            YCordMod -= 1;
                            XCordMod -= 1;
                            break;
                        case "southeast":
                        case "se":
                            YCordMod += 1;
                            XCordMod += 1;
                            break;
                        case "southwest":
                        case "sw":
                            YCordMod += 1;
                            XCordMod -= 1;
                            break;
                        case "1":
                            AreaSize = 1;
                            break;
                        case "3":
                        case "4":
                        case "5":
                        case "6":
                            AreaSize = 3;
                            break;
                        default:
                            break;
                    }
                    argCount++;

                }

                ArrayList areaCellList = new ArrayList();
                int[] XCastMod = new int[] { 0, 0, 0 };
                int[] YCastMod = new int[] { 0, 0, 0 };

                //turn undead effects all undead on the current map
                if (EffectType == Effect.EffectType.Turn_Undead)
                {
                    AreaSize = 3;
                }

                //OK now we have the starting point, and the direction, create all the effects needed
                #region Create the list of effect cells
                if (AreaSize == 1)
                {
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod, caster.Z));
                }
                else if (AreaSize > 2)
                {
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod - 2, caster.Y + YCordMod + -2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod - 2, caster.Y + YCordMod + -2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod - 1, caster.Y + YCordMod + -2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod - 1, caster.Y + YCordMod + -2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod + -2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod + -2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 1, caster.Y + YCordMod + -2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 1, caster.Y + YCordMod + -2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 2, caster.Y + YCordMod + -2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 2, caster.Y + YCordMod + -2, caster.Z));

                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod - 2, caster.Y + YCordMod + -1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod - 2, caster.Y + YCordMod + -1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod - 1, caster.Y + YCordMod + -1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod - 1, caster.Y + YCordMod + -1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod + -1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod + -1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 1, caster.Y + YCordMod + -1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 1, caster.Y + YCordMod + -1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 2, caster.Y + YCordMod + -1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 2, caster.Y + YCordMod + -1, caster.Z));

                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + -2, caster.Y + YCordMod, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + -2, caster.Y + YCordMod, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + -1, caster.Y + YCordMod, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + -1, caster.Y + YCordMod, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 1, caster.Y + YCordMod, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 1, caster.Y + YCordMod, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 2, caster.Y + YCordMod, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 2, caster.Y + YCordMod, caster.Z));

                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + -2, caster.Y + YCordMod + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + -2, caster.Y + YCordMod + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + -1, caster.Y + YCordMod + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + -1, caster.Y + YCordMod + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 1, caster.Y + YCordMod + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 1, caster.Y + YCordMod + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 2, caster.Y + YCordMod + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 2, caster.Y + YCordMod + 1, caster.Z));

                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + -2, caster.Y + YCordMod + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + -2, caster.Y + YCordMod + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + -1, caster.Y + YCordMod + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + -1, caster.Y + YCordMod + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 1, caster.Y + YCordMod + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 1, caster.Y + YCordMod + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 2, caster.Y + YCordMod + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 2, caster.Y + YCordMod + 2, caster.Z));


                }
                else
                {

                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod - 1, caster.Y + YCordMod + -1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod - 1, caster.Y + YCordMod + -1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod + -1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod + -1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 1, caster.Y + YCordMod + -1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 1, caster.Y + YCordMod + -1, caster.Z));

                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + -1, caster.Y + YCordMod, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + -1, caster.Y + YCordMod, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 1, caster.Y + YCordMod, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 1, caster.Y + YCordMod, caster.Z));

                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + -1, caster.Y + YCordMod + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + -1, caster.Y + YCordMod + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 1, caster.Y + YCordMod + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod + 1, caster.Y + YCordMod + 1, caster.Z));
                }
                #endregion
                //adjust the power of the area spell according to area size - no adjustment if normal
                if (AreaSize >= 3)
                {
                    if (EffectType != Effect.EffectType.Turn_Undead)
                    {
                        EffectPower = (int)(EffectPower / 2);
                    }
                }
                else if (AreaSize == 1)
                {
                    EffectPower = EffectPower * 2;
                }

                if (EffectType == Effect.EffectType.Concussion)
                {
                    #region Concussion
                    Cell centerCell = Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod, caster.Z);
                    Cell[] cellArray = Cell.GetVisibleCellArray(centerCell, AreaSize);

                    foreach (Cell cell in areaCellList)
                    {
                        Rules.DoConcussionDamage(cell, EffectPower, caster);
                        //chr.WriteToDisplay("You have been hit by an explosion!");
                    }

                    ArrayList brokenWallsList = new ArrayList();

                    for (int j = 0; j < cellArray.Length; j++)
                    {
                        if (cellArray[j] != null)
                        {
                            if ((cellArray[j].CellGraphic == "[]" || cellArray[j].CellGraphic == "SD") && !cellArray[j].IsMagicDead)
                            {
                                if (Rules.RollD(1, 100) < 30)
                                {
                                    brokenWallsList.Add(cellArray[j]);
                                }
                            }
                        }
                    }
                    if (brokenWallsList.Count > 0)
                    {
                        Effect effect = new Effect(Effect.EffectType.Illusion, "[_", 1, -1, null, brokenWallsList);
                    }
                    #endregion
                }
                else if (EffectType == Effect.EffectType.Fire)
                {
                    #region Fireball
                    Cell centerCell = Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod, caster.Z);

                    if (this.SoundFile != "") { centerCell.EmitSound(this.SoundFile); }

                    centerCell.SendShout("a whoosh of fire!");

                    Effect effect = new Effect(EffectType, "**", EffectPower, 2, caster, areaCellList);

                    foreach (Cell cell in areaCellList)
                    {
                        //Effect.DoAreaEffect(cell, effect);

                        for (int m = 0; m < cell.Characters.Count; m++)
                        {
                            Character chr = cell.Characters[m];
                            if (!chr.IsInvisible && !chr.IsImmortal && !chr.IsDead)
                            {
                                chr.WriteToDisplay("You have been hit by a fireball!");
                            }
                        }
                    }
                    #endregion
                }
                else if (EffectType == Effect.EffectType.Ice)
                {
                    #region Icestorm
                    Cell centerCell = Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod, caster.Z);

                    if (this.SoundFile != "") { centerCell.EmitSound(this.SoundFile); }

                    centerCell.SendShout("the pounding of giant ice pellets!");

                    Effect effect = new Effect(EffectType, "~.", EffectPower, 2, caster, areaCellList);

                    foreach (Cell cell in areaCellList)
                    {
                        //Effect.DoAreaEffect(cell, effect);
                        for (int m = 0; m < cell.Characters.Count; m++)
                        {
                            Character chr = cell.Characters[m];
                            if (!chr.IsInvisible && !chr.IsImmortal && !chr.IsDead)
                            {
                                chr.WriteToDisplay("You have been hit by a raging ice storm!");
                            }
                        }
                    }
                    #endregion
                }
                else if (EffectType == Effect.EffectType.Turn_Undead)
                {
                    #region Turn Undead
                    Cell centerCell = Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod, caster.Z);

                    if (this.SoundFile != "") { centerCell.EmitSound(this.SoundFile); }

                    Effect effect = new Effect(EffectType, "", EffectPower, 1, caster, areaCellList);

                    foreach (Cell cell in areaCellList)
                    {
                        //Effect.DoAreaEffect(cell, effect);
                        for (int m = 0; m < cell.Characters.Count; m++)
                        {
                            Character chr = cell.Characters[m];
                            if (!chr.IsDead)
                            {
                                chr.WriteToDisplay("You feel a strong wind race through the area.");
                            }
                        }
                    }
                    #endregion
                }
                else if (EffectType == Effect.EffectType.Light)
                {
                    #region Light
                    Cell centerCell = Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod, caster.Z);

                    if (this.SoundFile != "") { centerCell.EmitSound(this.SoundFile); }

                    Effect effect = new Effect(EffectType, "", EffectPower, Skills.GetSkillLevel(caster.magic), caster, areaCellList);

                    foreach (Cell cell in areaCellList)
                    {
                        //Effect.DoAreaEffect(cell, effect);
                        for (int m = 0; m < cell.Characters.Count; m++)
                        {
                            Character chr = cell.Characters[m];
                            if (!chr.IsDead)
                            {
                                chr.WriteToDisplay("The area is illuminated by a burst of magical light!");
                            }
                        }
                    }
                    #endregion
                }
                else if (EffectType == Effect.EffectType.Darkness)
                {
                    #region Darkness
                    Cell centerCell = Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + XCordMod, caster.Y + YCordMod, caster.Z);

                    if (this.SoundFile != "") { centerCell.EmitSound(this.SoundFile); }

                    Effect effect = new Effect(EffectType, "??", 0, EffectPower, caster, areaCellList);

                    foreach (Cell cell in areaCellList)
                    {
                        //Effect.DoAreaEffect(cell, effect);
                        for (int m = 0; m < cell.Characters.Count; m++)
                        {
                            Character chr = cell.Characters[m];
                            if (!chr.IsDead)
                            {
                                chr.WriteToDisplay("The area is covered in a shroud of magical darkness!");
                            }
                        }
                    }
                    #endregion
                }
            }
        }

        private void CastGenericConeSpell(Character caster, string args)
        {
            Effect.EffectType EffectType = Effect.EffectType.None;
            string EffectGraphic = "  ";
            int EffectPower = 1;
            string spellMsg = "";
            string spellEmote = "";
            string direction = "";

            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = new Character();

            // the cells that will be effected by the cone
            ArrayList areaCellList = new ArrayList();

            // get the immediate direction of the cone
            Cell cell = Map.GetCellRelevantToCell(caster.CurrentCell, direction, false);

            //dragon's breath cones
            if (sArgs[0] == "drbreath")
            {
                switch (sArgs[1].ToLower())
                {
                    case "ice":
                        EffectType = Effect.EffectType.Dragon__s_Breath_Ice;
                        EffectGraphic = "~.";
                        EffectPower = Skills.GetSkillLevel(caster.magic) * 4;
                        spellMsg = "You are hit by a frigid blast of Dragon's Breath!";
                        spellEmote = "breathes a blue cone of ice!";
                        direction = sArgs[2];
                        break;
                    case "fire":
                    default:
                        EffectType = Effect.EffectType.Dragon__s_Breath_Fire;
                        EffectGraphic = "**";
                        EffectPower = Skills.GetSkillLevel(caster.magic) * 4;
                        spellMsg = "You are hit by searing Dragon's Breath! Your skin blisters and burns!";
                        spellEmote = "breathes fire!";
                        direction = sArgs[1];
                        if (sArgs[1] == "fire")
                        {
                            direction = sArgs[2];
                        }
                        break;
                }
            }
            //if cone is cast at something figure out the direction
            if (sArgs[1] == "at" || sArgs[2] == "at")
            {
                target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);
                if (target.X < caster.X && target.Y < caster.Y) { direction = "nw"; }
                if (target.X < caster.X && target.Y > caster.Y) { direction = "sw"; }
                if (target.X < caster.X && target.Y == caster.Y) { direction = "w"; }
                if (target.X > caster.X && target.Y == caster.Y) { direction = "e"; }
                if (target.X > caster.X && target.Y < caster.Y) { direction = "ne"; }
                if (target.X > caster.X && target.Y > caster.Y) { direction = "se"; }
                if (target.X == caster.X && target.Y < caster.Y) { direction = "n"; }
                if (target.X == caster.X && target.Y > caster.Y) { direction = "s"; }
                if (target.X == caster.X && target.Y == caster.Y)
                {
                    switch (caster.dirPointer)
                    {
                        case "^ ": direction = "n"; break;
                        case "v ": direction = "s"; break;
                        case "> ": direction = "e"; break;
                        case "< ": direction = "w"; break;
                        default:
                            switch (Rules.dice.Next(3) + 1)
                            {
                                case 1: direction = "n"; break;
                                case 2: direction = "s"; break;
                                case 3: direction = "e"; break;
                                case 4: direction = "w"; break;
                            }
                            break;
                    }
                    areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X, caster.Y, caster.Z));
                }
            }

            #region Cone Area Effect
            switch (direction)
            {
                case "north":
                case "n":
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y - 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y - 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y - 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y - 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y - 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y - 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y - 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y - 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y - 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y - 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y - 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y - 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y - 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y - 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y - 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y - 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y - 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y - 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y - 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y - 3, caster.Z));
                    break;
                case "south":
                case "s":
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y + 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y + 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y + 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y + 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y + 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y + 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y + 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y + 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y + 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y + 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y + 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y + 3, caster.Z));
                    break;
                case "west":
                case "w":
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y - 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y - 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y - 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y - 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y - 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y - 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y + 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y + 3, caster.Z));
                    break;
                case "east":
                case "e":
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y - 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y - 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y - 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y - 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y - 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y - 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y + 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y + 3, caster.Z));
                    break;
                case "northwest":
                case "nw":
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y - 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y - 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y - 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y - 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y - 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y - 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y - 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y - 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y - 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y - 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y - 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y - 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 1, caster.Z));
                    break;
                case "northeast":
                case "ne":
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y - 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y - 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y - 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y - 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y - 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y - 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y - 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y - 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y - 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y - 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y - 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y - 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 1, caster.Z));
                    break;
                case "southwest":
                case "sw":
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y + 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y + 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 3, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y + 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y + 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 2, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y + 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y + 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 1, caster.Z));
                    break;
                case "southeast":
                case "se":
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y + 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y + 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 3, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y + 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y + 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 2, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y + 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y + 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 3, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 3, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 2, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 2, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 1, caster.Z));
                    break;
            }
            #endregion

            if (caster.race != "")
            {
                caster.SendToAllInSight(caster.Name + " " + spellEmote);
            }
            else
            {
                caster.SendToAllInSight("The " + caster.Name + " " + spellEmote);
            }

            if (spellMsg != "")
            {
                foreach (Cell bCell in areaCellList)
                {
                    foreach (Character chr in bCell.Characters)
                    {
                        if (chr != caster)
                        {
                            chr.WriteToDisplay(spellMsg);
                        }
                    }
                }
            }

            EffectPower = EffectPower - Rules.dice.Next(-10, 10);
            if (EffectPower < 1) { EffectPower = 5; }
            Effect effect = new Effect(EffectType, EffectGraphic, EffectPower, 1, caster, areaCellList);
            return;
        }

        private void CastGenericWallSpell(Character caster, string args, Effect.EffectType EffectType, int EffectPower)
        {
            string EffectGraphic = "  ";
            string spellMsg = "";
            string spellShout = "";
            bool spellStun = false;

            ArrayList areaCellList = new ArrayList(); // the cells that will be effected by the wall

            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            Cell cell = null;

            string castDirection = "";

            if (target != null)
            {
                cell = target.CurrentCell;
                if (cell.X > caster.X && cell.Y > caster.Y)
                {
                    castDirection = "se";
                }
                else if (cell.X > caster.X && cell.Y == caster.Y)
                {
                    castDirection = "e";
                }
                else if (cell.X > caster.X && cell.Y < caster.Y)
                {
                    castDirection = "ne";
                }
                else if (cell.X == caster.X && cell.Y == caster.Y)
                {
                    switch (Rules.RollD(1, 8))
                    {
                        case 1:
                            castDirection = "n";
                            break;
                        case 2:
                            castDirection = "s";
                            break;
                        case 3:
                            castDirection = "e";
                            break;
                        case 4:
                            castDirection = "w";
                            break;
                        case 5:
                            castDirection = "ne";
                            break;
                        case 6:
                            castDirection = "se";
                            break;
                        case 7:
                            castDirection = "nw";
                            break;
                        case 8:
                            castDirection = "sw";
                            break;
                    }
                }
                else if (cell.X == caster.X && cell.Y > caster.Y)
                {
                    castDirection = "s";
                }
                else if (cell.X == caster.X && cell.Y < caster.Y)
                {
                    castDirection = "n";
                }
                else if (cell.X < caster.X && cell.Y > caster.Y)
                {
                    castDirection = "sw";
                }
                else if (cell.X < caster.X && cell.Y == caster.Y)
                {
                    castDirection = "w";
                }
                else if (cell.X < caster.X && cell.Y < caster.Y)
                {
                    castDirection = "nw";
                }
            }
            else
            {
                cell = Map.GetCellRelevantToCell(caster.CurrentCell, args, false); // get the center cell, where the firewall starts
                castDirection = sArgs[sArgs.Length - 1];
            }

            areaCellList.Add(cell);
            switch (castDirection)
            {
                case "n":
                case "s":
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y, caster.Z));
                    break;
                case "w":
                case "e":
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 1, caster.Z));
                    break;
                case "ne":
                case "sw":
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y + 1, caster.Z));
                    break;
                case "se":
                case "nw":
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y + 1, caster.Z));
                    break;
                default:
                    areaCellList.Remove(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y - 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y - 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X - 1, cell.Y + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X, cell.Y + 1, caster.Z));
                    if (!Map.IsSpellPathBlocked(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y + 1, caster.Z)))
                        areaCellList.Add(Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, cell.X + 1, cell.Y + 1, caster.Z));
                    break;
            }

            switch (EffectType)
            {
                case Effect.EffectType.Fire:
                    EffectGraphic = "**";
                    spellMsg = "You are burned by a wall of fire!";
                    spellShout = "a whoosh of fire.";
                    break;
                case Effect.EffectType.Ice:
                    EffectGraphic = "~.";
                    spellMsg = "You are consumed in a wall of ice!";
                    spellShout = "a loud hiss and the sound of ice cracking.";
                    spellStun = true;
                    break;
                case Effect.EffectType.Light:
                    EffectGraphic = "";
                    spellMsg = "The area is illuminated by a wall of magical light!";
                    break;
                case Effect.EffectType.Dragon__s_Breath_Fire:
                    EffectGraphic = "**";
                    spellMsg = "You are surrounded by a wall of searing dragon's breath!";
                    spellShout = "a whoosh of fire.";
                    break;
                case Effect.EffectType.Darkness:
                    EffectGraphic = "";
                    spellMsg = "The area is covered in a wall of magical darkness!";
                    break;
            }
            // generic shout message
            if (spellShout != "")
            {
                cell.SendShout(spellShout);
            }
            cell.EmitSound(this.soundFile);
            //message to those in the area of effect, and stun for ice wall
            if (spellMsg != "")
            {
                foreach (Cell c in areaCellList)
                {
                    for (int a = 0; a < c.Characters.Count; a++)
                    {
                        Character chr = c.Characters[a];
                        if (!chr.IsDead)
                        {
                            Rules.DoSpellDamage(caster, chr, null, EffectPower, EffectType.ToString().ToLower());
                            chr.WriteToDisplay(spellMsg);
                            if (spellStun)
                            {
                                if (Rules.DoSpellDamage(caster, chr, null, 0, "stun") == 1)
                                {
                                    chr.Stunned += (short)Rules.RollD(1, 4);
                                }
                            }
                        }

                    }
                }
            }
            // create the wall
            Effect effect = new Effect(EffectType, EffectGraphic, EffectPower, Rules.RollD(10, 10), caster, areaCellList);
            return;

        }

        #region Wizard Spells
        private void castMagicMissile(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            this.GenericCastMessage(caster, target, true);
            
            if (Rules.DoSpellDamage(caster, target, null, (Skills.GetSkillLevel(caster.magic) * 3) + GetDirectDamageModifier(caster), "magic missile") == 1)
            {
                Rules.GiveKillExp(caster, target);
                Skills.GiveSkillExp(caster, target, Globals.eSkillType.Magic);
            }
        }

        private void castShield(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            GenericCastMessage(caster, target, false);

            int shieldEffect = 0;
            int shieldDuration = 0;
            if (Skills.GetSkillLevel(caster.magic) < 5) { shieldEffect = 1; shieldDuration = 30; }
            else if (Skills.GetSkillLevel(caster.magic) >= 5 && Skills.GetSkillLevel(caster.magic) <= 10) { shieldEffect = 3; shieldDuration = 60; }
            else if (Skills.GetSkillLevel(caster.magic) > 10 && Skills.GetSkillLevel(caster.magic) <= 15) { shieldEffect = 6; shieldDuration = 90; }
            else { shieldEffect = 9; shieldDuration = 120; }
            Effect.CreateCharacterEffect(Effect.EffectType.Shield, shieldEffect, target, Skills.GetSkillLevel(caster.magic) * shieldDuration, caster);
            target.EmitSound(this.SoundFile);
            target.WriteToDisplay("You are surrounded by the blue glow of a " + this.Name + " spell.");
            return;
        }

        private void castBonfire(Character caster, string args)
        {
            // clean up the args
            args = args.Replace("bonfire", "");
            args = args.Trim();

            string[] sArgs = args.Split(" ".ToCharArray());

            if (Map.IsSpellPathBlocked(Map.GetCellRelevantToCell(caster.CurrentCell, args, true)))
            {
                Spell.GenericFailMessage(caster, null);
                return;
            }

            if (Map.GetCellRelevantToCell(caster.CurrentCell, args, true) != null)
            {
                Effect effect = new Effect(Effect.EffectType.Fire, "**", (int)(Skills.GetSkillLevel(caster.magic) * 2.5), (int)(Skills.GetSkillLevel(caster.magic) * 1.5), caster, Map.GetCellRelevantToCell(caster.CurrentCell, args, false));
                Map.GetCellRelevantToCell(caster.CurrentCell, args, true).SendShout("a roaring fire.");
            }

            caster.WriteToDisplay("You create a magical " + this.Name + ".");
        }

        private void castCreateWeb(Character caster, string args)
        {
            ArrayList cells = new ArrayList();
            cells.Add(Map.GetCellRelevantToCell(caster.CurrentCell, args, false));
            Effect effect = new Effect(Effect.EffectType.Web, "ww", Skills.GetSkillLevel(caster.magic) * 2, Skills.GetSkillLevel(caster.magic) * 2, caster, cells);
            caster.WriteToDisplay("You cast " + this.Name + ".");
            return;
        }

        private void castFireBall(Character caster, string args)
        {
            caster.WriteToDisplay("You cast " + this.Name + ".");
            CastGenericAreaSpell(caster, args, Effect.EffectType.Fire, Skills.GetSkillLevel(caster.magic) * 4, this.Name);
            return;
        }

        private void castFireWall(Character caster, string args)
        {
            caster.WriteToDisplay("You cast " + this.Name + ".");
            CastGenericWallSpell(caster, args, Effect.EffectType.Fire, (int)(Skills.GetSkillLevel(caster.magic) * 4));
        }

        private void castIceStorm(Character caster, string args)
        {
            caster.WriteToDisplay("You cast " + this.Name + ".");
            CastGenericAreaSpell(caster, args, Effect.EffectType.Ice, Skills.GetSkillLevel(caster.magic) * 5, this.Name);
        }

        private void castConcussion(Character caster, string args)
        {
            caster.WriteToDisplay("You cast " + this.Name + ".");
            CastGenericAreaSpell(caster, args, Effect.EffectType.Concussion, (int)(Skills.GetSkillLevel(caster.magic) * 6), this.Name);
        }

        private void castDispel(Character caster, string args)
        {
            caster.WriteToDisplay("You cast " + this.Name + ".");
            int xpos = 0;
            int ypos = 0;

            //clean out the command name
            args = args.Replace(this.SpellCommand, "");
            args = args.Trim();
            string[] sArgs = args.Split(" ".ToCharArray());
            #region Get the direction
            switch (sArgs[0])
            {
                case "south":
                case "s":
                    ypos++;
                    break;
                case "southeast":
                case "se":
                    ypos++;
                    xpos++;
                    break;
                case "southwest":
                case "sw":
                    ypos++;
                    xpos--;
                    break;
                case "west":
                case "w":
                    xpos--;
                    break;
                case "east":
                case "e":
                    xpos++;
                    break;
                case "northeast":
                case "ne":
                    ypos--;
                    xpos++;
                    break;
                case "northwest":
                case "nw":
                    ypos--;
                    xpos--;
                    break;
                case "north":
                case "n":
                    ypos--;
                    break;
                default:
                    break;
            }
            #endregion
            Cell cell = Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + xpos, caster.Y + ypos, caster.Z);

            foreach (Effect effect in cell.Effects.Values)
            {
                effect.StopAreaEffect();
            }
            return;
        }

        private void castIllusion(Character caster, string args)
        {
            //clean out the command name
            args = args.Replace(this.SpellCommand, "");
            args = args.Trim();

            string[] sArgs = args.Split(" ".ToCharArray());

            Cell cell = Map.GetCellRelevantToCell(caster.CurrentCell, args, false);

            if (cell == null)
            {
                caster.WriteToDisplay("Illusion spell format: CAST <illusion type> <direction>");
                return;
            }

            if (cell.IsMagicDead)
            {
                caster.WriteToDisplay("Your spell fails.");
                caster.EmitSound(Sound.GetCommonSound(Sound.CommonSound.SpellFail));
                return;
            }

            Effect effect;

            switch (sArgs[0].ToLower())
            {
                case "wall":
                    GenericCastMessage(caster, null, true);
                    effect = new Effect(Effect.EffectType.Illusion, "[]", 1, 200, caster, cell);
                    break;
                case "fire":
                    GenericCastMessage(caster, null, true);
                    effect = new Effect(Effect.EffectType.Illusion, "**", 1, 200, caster, cell);
                    break;
                case "bridge":
                    GenericCastMessage(caster, null, true);
                    effect = new Effect(Effect.EffectType.Illusion, "::", 1, 200, caster, cell);
                    break;
                case "empty":
                    GenericCastMessage(caster, null, true);
                    effect = new Effect(Effect.EffectType.Illusion, ". ", 1, 200, caster, cell);
                    break;
                case "water":
                    GenericCastMessage(caster, null, true);
                    effect = new Effect(Effect.EffectType.Illusion, "~~", 1, 200, caster, cell);
                    break;
                default:
                    caster.WriteToDisplay(sArgs[0] + " is not a valid illusion type.");
                    caster.WriteToDisplay("Valid Illusions: wall | fire | bridge | empty | water");
                    break;
            }
            return;
        }

        private void castDisintegrate(Character caster, string args)
        {
            // clean up the args
            args = args.Replace(this.spellCommand, "");
            args = args.Trim();

            string[] sArgs = args.Split(" ".ToCharArray());

            Cell tCell = Map.GetCellRelevantToCell(caster.CurrentCell, args, true);

            if (tCell != null)
            {
                // destroy all but attuned items
                foreach (Item item in new List<Item>(tCell.Items))
                {
                    if (item.attunedID != 0)
                    {
                        tCell.Remove(item);
                    }
                }

                // do spell damage
                foreach (Character chr in new List<Character>(tCell.Characters))
                {
                    if (Rules.DoSpellDamage(caster, chr, null, Skills.GetSkillLevel(caster.magic) * 4, "disintegrate") == 1)
                        Rules.GiveAEKillExp(caster, chr);
                }

                // destroy walls for a while
                if (!tCell.IsMagicDead && tCell.DisplayGraphic == "[]")
                {
                    string newDispGraphic = "[_";
                    if (Rules.RollD(1, 20) >= 10)
                        newDispGraphic = "_]";

                    Effect effect = new Effect(Effect.EffectType.Illusion, newDispGraphic, 0, (int)Skills.GetSkillLevel(caster.magic) * 6, caster, tCell);
                    tCell.SendShout("a wall crumbling.");
                }
            }

            caster.WriteToDisplay("You cast " + this.Name + ".");
        }

        private void castPeek(Character caster, string args)
        {
            try
            {
                // make sure they have the correct compent, otherwise BOOM.

                if (caster.ImpLevel < Globals.eImpLevel.GM)
                {
                    Item item = caster.RightHand;
                    if (item == null)
                    {
                        caster.WriteToDisplay("Your spell fails.");
                        caster.EmitSound(Sound.GetCommonSound(Sound.CommonSound.SpellFail));
                        return;
                    }

                    if (item.itemID != Item.ID_TIGERSEYE)
                    {
                        caster.RightHand = null;
                        this.CastGenericAreaSpell(caster, null, Effect.EffectType.Concussion, 10, "");
                        return;
                    }
                }

                string[] sArgs = args.Split(" ".ToCharArray());

                //find and confirm the target
                string tName = sArgs[2];

                Character target = null;
                // find the FIRST match.
                foreach (Character ch in Character.pcList)
                {
                    if (ch.MapID == caster.MapID && ch.Name.ToLower() == tName.ToLower())
                    {
                        target = ch;
                        break;
                    }
                }

                if (target == null)
                {
                    foreach (Character ch in Character.NPCList)
                    {
                        if (ch.MapID == caster.MapID && ch.Name.ToLower() == tName.ToLower())
                        {
                            target = ch;
                            break;
                        }

                    }
                }

                if (target == null)
                {
                    caster.WriteToDisplay("You cannot sense your target.");
                    return;
                }
                else
                {
                    if (!caster.IsInvisible)
                    {
                        target.WriteToDisplay("Your eyes tingle for a moment.");
                    }
                }

                Effect.CreateCharacterEffect(Effect.EffectType.Peek, 1, caster, 3, caster);

            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        private void castIceSpear(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            GenericCastMessage(caster, target, true);          

            if (Rules.DoSpellDamage(caster, target, null, Skills.GetSkillLevel(caster.magic) * 14 + GetDirectDamageModifier(caster), "icespear") == 1)
            {
                Rules.GiveKillExp(caster, target);
                Skills.GiveSkillExp(caster, target, Globals.eSkillType.Magic);
            }
        }

        private void castFireBolt(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            GenericCastMessage(caster, target, true);        

            if (Rules.DoSpellDamage(caster, target, null, Skills.GetSkillLevel(caster.magic) * 12 + GetDirectDamageModifier(caster), this.name) == 1)
            {
                Rules.GiveKillExp(caster, target);
                Skills.GiveSkillExp(caster, target, Globals.eSkillType.Magic);
            }
        }

        private void castWhirlWind(Character caster, string args)
        {
            caster.WriteToDisplay("You cast " + this.Name + ".");
            if (args == null)
                args = "";
            Effect effect = new Effect(Effect.EffectType.Whirlwind, "&&", caster.Level, 5, caster, Map.GetCellRelevantToCell(caster.CurrentCell, args, true));
        }

        private void castFireStorm(Character caster, string args)
        {
            caster.WriteToDisplay("You cast " + this.Name + ".");
            if (args == null)
                args = "";
            CastGenericAreaSpell(caster, args, Effect.EffectType.Fire, Skills.GetSkillLevel(caster.magic) * 4, this.Name);

            Effect effect = new Effect(Effect.EffectType.Fire_Storm, "**", caster.Level, 5, caster, Map.GetCellRelevantToCell(caster.CurrentCell, args, true));
        }

        private void castDragonsBreath(Character caster, string args)
        {
            caster.WriteToDisplay("You cast " + this.Name + "!");
            switch (caster.species)
            {
                case Globals.eSpecies.FireDragon:
                    args = args.Replace("drbreath", "drbreath fire");
                    break;
                case Globals.eSpecies.IceDragon:
                    args = args.Replace("drbreath", "drbreath ice");
                    break;
                default:
                    break;
            }
            this.CastGenericConeSpell(caster, args);
            return;
        }

        private void castBlizzard(Character caster, string args)
        {
            caster.WriteToDisplay("You cast " + this.Name + ".");
        }

        private void castLightningLance(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            GenericCastMessage(caster, target, true);
            
            if (Rules.DoSpellDamage(caster, target, null, Skills.GetSkillLevel(caster.magic) * 12, "lightning") == 1)
            {
                Rules.GiveKillExp(caster, target);
                Skills.GiveSkillExp(caster, target, Globals.eSkillType.Magic);
            }
        }

        private void castLightningStorm(Character caster, string args)
        {
            caster.WriteToDisplay("You cast " + this.Name + ".");
        }

        private void castLava(Character caster, string args)
        {
            caster.WriteToDisplay("You cast " + this.Name + ".");
        }
        #endregion

        #region Thaumaturge Spells

        private void castCurse(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            int dmgMultiplier = 4;
            if (caster.IsPC) dmgMultiplier = 5;

            GenericCastMessage(caster, target, true);          
            
            if (Rules.DoSpellDamage(caster, target, null, Skills.GetSkillLevel(caster.magic) * dmgMultiplier + GetDirectDamageModifier(caster), "curse") == 1)
            {
                Rules.GiveKillExp(caster, target);
                Skills.GiveSkillExp(caster, target, Globals.eSkillType.Magic);
            }
        }

        private void castTurnUndead(Character caster, string args)
        {
            caster.WriteToDisplay("You cast " + this.Name + ".");
            this.CastGenericAreaSpell(caster, args, Effect.EffectType.Turn_Undead, Skills.GetSkillLevel(caster.magic) * 10, this.Name);
            return;
        }

        private void castLightningBolt(Character caster, string args)
        {
            try
            {
                //int xpos = 0;
                //int ypos = 0;

                args = args.Trim();

                Cell cell = Map.GetCellRelevantToCell(caster.CurrentCell, args, false);

                string[] sArgs = args.Split(" ".ToCharArray());

                if (sArgs.Length > 1 && sArgs[1].ToLower() == "at" || sArgs.Length > 2 && sArgs[2].ToLower() == "at")
                {
                    if (sArgs.Length > 2)
                    {
                        Character target = Map.FindTargetInView(caster, sArgs[2], false, false);
                        if (target != null)
                        {
                            cell = target.CurrentCell;
                        }
                        else if (sArgs.Length > 3)
                        {
                            target = Map.FindTargetInView(caster, sArgs[3], false, false);
                            if (target != null)
                            {
                                cell = target.CurrentCell;
                            }
                        }
                    }
                }

                if (cell == null)
                {
                    caster.WriteToDisplay("Your spell fails.");
                    caster.EmitSound(Sound.GetCommonSound(Sound.CommonSound.SpellFail));
                    return;
                }
                Creature pathTest = new Creature();
                pathTest.CurrentCell = caster.CurrentCell;
                if (!pathTest.BuildMoveList(cell.X, cell.Y, cell.Z))
                    cell = caster.CurrentCell;
                pathTest.RemoveFromWorld();
                cell.SendShout("a thunder clap!");
                cell.EmitSound(Sound.GetCommonSound(Sound.CommonSound.ThunderClap));
                ArrayList theAffected = new ArrayList();
                if (cell.Characters.Count > 0)
                {
                    for (int m = 0; m < cell.Characters.Count; m++)
                    {
                        Character ch = (Character)cell.Characters[m];
                        theAffected.Add(ch);

                    }
                }
                if (theAffected.Count > 0)
                {
                    int dmgMultiplier = 6;
                    if (caster.IsPC) dmgMultiplier = 8;
                    foreach (Character affected in theAffected)
                    {
                        affected.WriteToDisplay("You have been hit by a " + this.Name + "!");
                        if (Rules.DoSpellDamage(caster, affected, null, Skills.GetSkillLevel(caster.magic) * dmgMultiplier + GetDirectDamageModifier(caster), "lightning") == 1)
                        {
                            Rules.GiveAEKillExp(caster, affected);
                            Skills.GiveSkillExp(caster, affected, Globals.eSkillType.Magic);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
            return;
        }

        private void castBanish(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            if (target.IsSummoned)
            {
                GenericCastMessage(caster, target, true);
                target.Age += Skills.GetSkillLevel(caster.magic) * Spell.BANISH_MULTI;
            }
            else
            {
                GenericFailMessage(caster, "Your target cannot be banished.");                
            }
        }

        private void castDeath(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            int dmgMultiplier = 8;
            if (caster.IsPC) dmgMultiplier = 9;

            this.GenericCastMessage(caster, target, true);
            
            if (Rules.DoSpellDamage(caster, target, null, Skills.GetSkillLevel(caster.magic) * dmgMultiplier + GetDirectDamageModifier(caster), this.name.ToLower()) == 1)
            {
                Skills.GiveSkillExp(caster, target, Globals.eSkillType.Magic);
                Rules.GiveKillExp(caster, target);
            }
        }

        private void castRaiseDead(Character caster, string args)
        {
            bool match = false;
            if (caster.CurrentCell.Items.Count > 0)
            {
                int cellCount = caster.CurrentCell.Items.Count;
                Item corpse;
                for (int x = 0; x < cellCount; x++)
                {
                    corpse = caster.CurrentCell.Items[x];
                    if (corpse.itemType == Globals.eItemType.Corpse)
                    {
                        foreach (Character player in Character.pcList)
                        {
                            if (player.Name == corpse.special)
                            {
                                if (player.IsDead)
                                {
                                    player.IsDead = false;
                                    player.IsInvisible = false;
                                    player.CurrentCell = caster.CurrentCell;
                                    player.Hits = (int)(player.HitsMax / 3);
                                    player.Stamina = (int)(player.StaminaMax / 3);
                                    if (player.ManaMax > 0)
                                    {
                                        player.Mana = (int)(player.ManaMax / 3);
                                    }
                                    player.WriteToDisplay("You have been raised from the dead.");
                                    player.SendSound(Sound.GetCommonSound(Sound.CommonSound.DeathRevive));
                                    match = true;
                                }
                            }
                        }
                        if (match)
                        {
                            caster.CurrentCell.Items.RemoveAt(x);
                        }
                        continue;
                    }
                }
            }
            if (!match)
            {
                caster.WriteToDisplay("There is nothing here to raise from the dead.");
            }
        }

        private void castResistFear(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            GenericCastMessage(caster, target, true);

            GenericEnchantMessage(target, this.Name);

            Effect.CreateCharacterEffect(Effect.EffectType.Resist_Fear, Skills.GetSkillLevel(caster.magic) * 3, target, Skills.GetSkillLevel(caster.magic) * 20, caster);
        }

        private void castCreateSnake(Character caster, string args)
        {
            return;
        }

        private void castSummonPhantasm(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            int power = 3; // default power
            int count = sArgs.Length;
            Item armor = Item.CopyItemFromDictionary(8015);
            #region Set the power of the spell - Using a try/catch to set defaults if invalid sArg[1]
            try
            {
                power = Convert.ToInt32(sArgs[1]);
                if (power > 5)
                    power = 5;
            }
            catch (Exception)
            {
                power = 3;
            }
            #endregion
            #region Setup the summon NPC
            NPC summoned = NPC.CreateNPC(Item.ID_SUMMONEDMOB, caster.FacetID, caster.LandID, caster.MapID, caster.X, caster.Y, caster.Z);
            summoned.Alignment = caster.Alignment;
            summoned.HitsMax = Rules.RollD(Skills.GetSkillLevel(caster.magic), 12);
            summoned.Hits = summoned.HitsMax;
            summoned.aiType = NPC.AIType.Summoned;
            summoned.Age = 0;
            summoned.special = "despawn";
            summoned.species = Globals.eSpecies.Unknown;
            summoned.WearItem(armor);
            summoned.canCommand = true;
            summoned.IsMobile = false;
            summoned.IsSummoned = true;
            summoned.IsUndead = false;
            summoned.Level = (short)Skills.GetSkillLevel(caster.magic);
            summoned.animal = false;
            summoned.shortDesc = "phantasm";
            summoned.longDesc = "a phantasm";
            summoned.classFullName = "Fighter";
            summoned.attackString1 = "";
            summoned.attackString2 = "";
            summoned.attackString3 = "";
            summoned.attackString4 = "";
            summoned.attackString5 = "";
            summoned.attackString6 = "";
            summoned.blockString1 = "";
            summoned.blockString2 = "";
            summoned.blockString3 = "";
            summoned.RoundsRemaining = Skills.GetSkillLevel(caster.magic)*2;
            summoned.BaseProfession = Character.ClassType.Fighter;
            summoned.mace = Skills.GetSkillToNext(summoned.Level);
            summoned.bow = Skills.GetSkillToNext(summoned.Level);
            summoned.unarmed = Skills.GetSkillToNext(summoned.Level);
            summoned.twoHanded = Skills.GetSkillToNext(summoned.Level);
            summoned.sword = Skills.GetSkillToNext(summoned.Level);
            summoned.magic = Skills.GetSkillToNext(summoned.Level);
            summoned.shuriken = Skills.GetSkillToNext(summoned.Level);
            summoned.staff = Skills.GetSkillToNext(summoned.Level);
            summoned.rapier = Skills.GetSkillToNext(summoned.Level);
            summoned.dagger = Skills.GetSkillToNext(summoned.Level);
            summoned.flail = Skills.GetSkillToNext(summoned.Level);
            summoned.halberd = Skills.GetSkillToNext(summoned.Level);
            summoned.threestaff = Skills.GetSkillToNext(summoned.Level);
            summoned.wearing.Add(armor);
            switch (power)
            {
                case 4: // normal djinn
                    summoned.Name = "djinn";
                    break;
                case 5: // djinn that can cast heal
                    summoned.Name = "djinn";
                    string chant = Spell.GenerateMagicWords();
                    summoned.spellList.Add(9, chant);
                    summoned.castMode = NPC.CastMode.Limited;
                    summoned.ManaMax = 50;
                    summoned.Mana = 50;
                    break;
                default: // normal phantasm
                    summoned.Name = "phantasm";
                    break;
            }
            #endregion
            Utils.Log("SummonPhantasm: Power: " + power + " [args = 0: " + sArgs[0] + " 1: " + sArgs[1] + " TTL: " + summoned.special, Utils.LogType.Unknown);
            return;
        }

        private void castResistBlind(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            GenericCastMessage(caster, target, true);

            GenericEnchantMessage(target, this.Name);

            Effect.CreateCharacterEffect(Effect.EffectType.Resist_Blind, Skills.GetSkillLevel(caster.magic) * 3, target, Skills.GetSkillLevel(caster.magic) * 20, caster);
        }

        private void castSummonDemon(Character caster, string args)
        {
            String[] sArgs = args.Split(" ".ToCharArray());
            int power = 3; // default power
            int count = sArgs.Length;
            Item armor = Item.CopyItemFromDictionary(8015);
            #region Set the power of the spell - Using a try/catch to set defaults if invalid sArg[1]
            try
            {
                power = Convert.ToInt32(sArgs[1]);
                if (power > 5)
                    power = 5;
            }
            catch (Exception)
            {
                power = Rules.RollD(1, 5);
            }
            #endregion
            #region Setup the summon NPC
            NPC summoned = NPC.CreateNPC(Item.ID_SUMMONEDMOB, caster.FacetID, caster.LandID, caster.MapID, caster.X, caster.Y, caster.Z);
            summoned.Alignment = caster.Alignment;
            summoned.HitsMax = Rules.RollD(Skills.GetSkillLevel(caster.magic), 12);
            summoned.Hits = summoned.HitsMax;
            summoned.aiType = NPC.AIType.Summoned;
            summoned.Age = 0;
            //summoned.special = Skills.GetSkillLevel(caster.magic).ToString();
            summoned.RoundsRemaining = Skills.GetSkillLevel(caster.magic);
            summoned.WearItem(armor);
            summoned.canCommand = false;
            summoned.IsMobile = true;
            summoned.IsSummoned = true;
            summoned.IsUndead = false;
            summoned.Level = (short)Skills.GetSkillLevel(caster.magic);
            summoned.animal = false;
            summoned.species = Globals.eSpecies.Demon;
            summoned.shortDesc = "demon";
            summoned.longDesc = "a demon";
            summoned.classFullName = "Fighter";
            summoned.attackString1 = "";
            summoned.attackString2 = "";
            summoned.attackString3 = "";
            summoned.attackString4 = "";
            summoned.attackString5 = "";
            summoned.attackString6 = "";
            summoned.blockString1 = "";
            summoned.blockString2 = "";
            summoned.blockString3 = "";
            summoned.mace = Skills.GetSkillToNext(summoned.Level);
            summoned.bow = Skills.GetSkillToNext(summoned.Level);
            summoned.unarmed = Skills.GetSkillToNext(summoned.Level);
            summoned.twoHanded = Skills.GetSkillToNext(summoned.Level);
            summoned.sword = Skills.GetSkillToNext(summoned.Level);
            summoned.magic = Skills.GetSkillToNext(summoned.Level);
            summoned.shuriken = Skills.GetSkillToNext(summoned.Level);
            summoned.staff = Skills.GetSkillToNext(summoned.Level);
            summoned.rapier = Skills.GetSkillToNext(summoned.Level);
            summoned.dagger = Skills.GetSkillToNext(summoned.Level);
            summoned.flail = Skills.GetSkillToNext(summoned.Level);
            summoned.halberd = Skills.GetSkillToNext(summoned.Level);
            summoned.threestaff = Skills.GetSkillToNext(summoned.Level);
            summoned.BaseProfession = Character.ClassType.Fighter;
            summoned.wearing.Add(armor);
            switch (power)
            {
                case 4: // normal demon
                    summoned.Name = "demon";
                    string chant = Spell.GenerateMagicWords();
                    summoned.spellList.Add(1, chant);
                    summoned.castMode = NPC.CastMode.Limited;
                    summoned.Mana = 50;
                    summoned.ManaMax = 50;
                    break;
                case 5: // demon that can cast death
                    summoned.Name = "demon";
                    string dchant = Spell.GenerateMagicWords();
                    summoned.spellList.Add(14, dchant);
                    summoned.castMode = NPC.CastMode.Limited;
                    summoned.Mana = 50;
                    summoned.ManaMax = 50;
                    break;
                default: // normal phantasm
                    summoned.Name = "demon";
                    break;
            }
            #endregion
            Utils.Log("SummonDemon: Power: " + power + " [args = 0: " + sArgs[0] + " 1: " + sArgs[1] + " TTL: " + summoned.special, Utils.LogType.Unknown);
            return;
        }

        private void castFear(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            if (Rules.DoSpellDamage(caster, target, null, Skills.GetSkillLevel(caster.magic) * 2, "fear") == 0)
            {
                if (target.race != "")
                {
                    caster.WriteToDisplay(target.Name + " resists your " + this.Name + " spell!");
                }
                else
                {
                    caster.WriteToDisplay("The " + target.Name + " resists your " + this.Name + " spell!");
                }
                target.WriteToDisplay("You resist a " + this.Name + " spell!");
                return;
            }
            else
            {
                if (target.race != "")
                {
                    caster.WriteToDisplay("You cast " + this.Name + " at " + target.Name + ".");
                }
                else
                {
                    caster.WriteToDisplay("You cast " + this.Name + " at the " + target.Name + ".");
                }
                target.WriteToDisplay("You have been hit by a " + this.Name + " spell!");
                Effect.CreateCharacterEffect(Effect.EffectType.Fear, 0, target, Rules.RollD(1, (int)(Skills.GetSkillLevel(caster.magic) / 2))+1, caster);
            }
            return;
        }

        private void castResistLightning(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            GenericCastMessage(caster, target, true);

            GenericEnchantMessage(target, this.Name);

            Effect.CreateCharacterEffect(Effect.EffectType.Resist_Lightning, Skills.GetSkillLevel(caster.magic) * 3, target, Skills.GetSkillLevel(caster.magic) * 20, caster);
        }

        private void castProtectPoison(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            GenericCastMessage(caster, target, true);

            GenericEnchantMessage(target, this.Name);

            Effect.CreateCharacterEffect(Effect.EffectType.Protection_from_Poison, Skills.GetSkillLevel(caster.magic) * 5, target, Skills.GetSkillLevel(caster.magic) * 20, caster);
        }

        private void castPoisonCloud(Character caster, string args)
        {
            return;
        }

        private void castResistStun(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            GenericCastMessage(caster, target, true);

            GenericEnchantMessage(target, this.Name);

            Effect.CreateCharacterEffect(Effect.EffectType.Resist_Stun, Skills.GetSkillLevel(caster.magic) * 3, target, Skills.GetSkillLevel(caster.magic) * 20, caster);
        }

        private void castResistDeath(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            GenericCastMessage(caster, target, true);

            GenericEnchantMessage(target, this.Name);

            Effect.CreateCharacterEffect(Effect.EffectType.Resist_Death, Skills.GetSkillLevel(caster.magic) * 3, target, Skills.GetSkillLevel(caster.magic) * 20, caster);
        }

        private void castProtectBlindFear(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            GenericCastMessage(caster, target, true);

            GenericEnchantMessage(target, this.Name);

            Effect.CreateCharacterEffect(Effect.EffectType.Protection_from_Blind_and_Fear, Skills.GetSkillLevel(caster.magic) * 5, target, Skills.GetSkillLevel(caster.magic) * 30, caster);
        }

        private void castProtectStunDeath(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            GenericCastMessage(caster, target, true);

            GenericEnchantMessage(target, this.Name);

            Effect.CreateCharacterEffect(Effect.EffectType.Protection_from_Stun_and_Death, Skills.GetSkillLevel(caster.magic) * 5, target, Skills.GetSkillLevel(caster.magic) * 30, caster);
        }

        private void castBlind(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            int chance = 100;
            int roll = 0;

            GenericCastMessage(caster, target, true);
            chance -= target.BlindResistance;
            roll = Rules.dice.Next(100) + 1;
            if (roll < chance)
            {
                target.WriteToDisplay("You have been blinded!");
                Effect.CreateCharacterEffect(Effect.EffectType.Blind, 0, target, ((int)Skills.GetSkillLevel(caster.magic) / 2) + Rules.dice.Next(-3, 3), caster);
                return;
            }
            target.WriteToDisplay("You resist the " + this.Name + " spell.");
        }

        private void castStun(Character caster, string args)
        {
            try
            {
                string[] sArgs = args.Split(" ".ToCharArray());

                Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

                if (target == null) { return; }

                if (target.Stunned > 1)
                {
                    return;
                }

                GenericCastMessage(caster, target, true);

                if (Rules.DoSpellDamage(caster, target, null, 0, "stun") == 1)
                {
                    target.WriteToDisplay("You are stunned!");
                    if (target.race != "")
                    {
                        target.SendToAllInSight(target.Name + " is stunned.");
                    }
                    else
                    {
                        target.SendToAllInSight("The " + target.Name + " is stunned.");
                    }
                    if (target.preppedSpell != null)
                    {
                        target.preppedSpell = null;
                        target.WriteToDisplay("Your spell has been lost.");
                    }
                    //stun duration is random rounds from 1 to magic skill level divided by 2
                    target.Stunned = (short)(Rules.dice.Next(1, (int)(Skills.GetSkillLevel(caster.magic) / 2))+1);
                    return;
                }
                target.WriteToDisplay("You resist the " + this.Name + " spell.");
            }
            catch (Exception e)
            {
                Utils.Log("Error in Stun Spell. " + caster.GetLogString(), Utils.LogType.Unknown);
                Utils.LogException(e);
                return;
            }
            
        }

        #endregion

        private void castBreatheWater(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) return;

            GenericCastMessage(caster, target, true);

            GenericEnchantMessage(target, this.Name);

            Effect.CreateCharacterEffect(Effect.EffectType.Breathe_Water, 1, target, Skills.GetSkillLevel(caster.magic) * 30, caster);
        }

        private void castCloseOpenDoor(Character caster, string args)
        {
            Cell cell = Map.GetCellRelevantToCell(caster.CurrentCell, args, true);
            string newGraphic = cell.DisplayGraphic;
            bool spellSuccess = false;
            string soundFile = "";

            switch (cell.CellGraphic)
            {
                case "| ":
                    GenericCastMessage(caster, null, true);
                    newGraphic = "/ ";
                    soundFile = Sound.GetCommonSound(Sound.CommonSound.OpenDoor);
                    spellSuccess = true;
                    break;
                case "--":
                    GenericCastMessage(caster, null, true);
                    newGraphic = "\\ ";
                    soundFile = Sound.GetCommonSound(Sound.CommonSound.OpenDoor);
                    spellSuccess = true;
                    break;
                case "/ ":
                    GenericCastMessage(caster, null, true);
                    newGraphic = "| ";
                    soundFile = Sound.GetCommonSound(Sound.CommonSound.CloseDoor);
                    spellSuccess = true;
                    break;
                case "\\ ":
                    GenericCastMessage(caster, null, true);
                    newGraphic = "--";
                    soundFile = Sound.GetCommonSound(Sound.CommonSound.CloseDoor);
                    spellSuccess = true;
                    break;
                default:
                    caster.WriteToDisplay("There is no door there.");
                    break;
            }

            caster.EmitSound(this.SoundFile);

            if (spellSuccess)
            {
                cell.CellGraphic = newGraphic;
                cell.DisplayGraphic = newGraphic;
                cell.EmitSound(soundFile);
            }
        }

        private void castProtectFire(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            GenericCastMessage(caster, target, true);

            GenericEnchantMessage(target, this.Name);

            Effect.CreateCharacterEffect(Effect.EffectType.Protection_from_Fire, Skills.GetSkillLevel(caster.magic) * 5, target, Skills.GetSkillLevel(caster.magic) * 30, caster);
        }

        private void castDarkness(Character caster, string args)
        {
            this.GenericCastMessage(caster, null, true);
            this.CastGenericAreaSpell(caster, args, Effect.EffectType.Darkness, Skills.GetSkillLevel(caster.magic) * 3, this.Name);
        }

        private void castProtectCold(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            GenericCastMessage(caster, target, true);

            GenericEnchantMessage(target, this.Name);

            Effect.CreateCharacterEffect(Effect.EffectType.Protection_from_Cold, Skills.GetSkillLevel(caster.magic) * 5, target, Skills.GetSkillLevel(caster.magic) * 20, caster);
        }

        private void castLight(Character caster, string args)
        {
            GenericCastMessage(caster, null, true);
            CastGenericAreaSpell(caster, args, Effect.EffectType.Light, Skills.GetSkillLevel(caster.magic) * 6, this.Name);
        }

        private void castLocate(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            List<Character> locatedTargets = new List<Character>();

            foreach (Character ch in Character.allCharList)
            {
                if (ch.FacetID == caster.FacetID && ch.LandID == caster.LandID && ch.MapID == caster.MapID)
                {
                    if (ch.Name.ToLower().StartsWith(sArgs[sArgs.Length - 1].ToLower()))
                    {
                        if (!ch.IsInvisible) // do not add invisible targets (implementors)
                        {
                            locatedTargets.Add(ch);
                        }
                    }
                }
            }

            caster.EmitSound(this.SoundFile);

            if (locatedTargets.Count < 1)
            {
                caster.WriteToDisplay("You cannot sense your target.");
                return;
            }

            Character target = locatedTargets[0];

            foreach (Character ch in locatedTargets)
            {
                if (Cell.GetCellDistance(caster.X, caster.Y, target.X, target.Y) >
                    Cell.GetCellDistance(caster.X, caster.Y, ch.X, ch.Y))
                {
                    target = ch;
                }
            }

            string directionString = Map.GetDirection(caster.CurrentCell, target.CurrentCell).ToString().ToLower();

            if (directionString.ToLower() == "none")
            {
                caster.WriteToDisplay("Your target is directly in front of you.");
                return;
            }

            if (target.Z == caster.Z)
            {
                #region Distance Information

                int distance = Cell.GetCellDistance(caster.X, caster.Y, target.X, target.Y);
                string distanceString = "";
                if (distance <= 6)
                {
                    distanceString = "very close!";
                }
                else if (distance > 6 && distance <= 12)
                {
                    distanceString = "fairly close.";
                }
                else if (distance > 12 && distance <= 18)
                {
                    distanceString = "close.";
                }
                else if (distance > 18 && distance <= 24)
                {
                    distanceString = "far away.";
                }
                else if (distance > 24)
                {
                    distanceString = "very far away.";
                }
                #endregion

                caster.WriteToDisplay("You sense your that your target is to the " + directionString + " and " + distanceString);
            }
            else
            {
                #region Height Information
                string heightString = "";
                int heightDifference = 0;
                if (target.Z > caster.Z)
                {
                    heightDifference = Math.Abs(Math.Abs(caster.Z) - Math.Abs(target.Z));

                    if (heightDifference > 0 && heightDifference <= 60)
                    {
                        heightString = "above you";
                    }
                    else if (heightDifference > 60 && heightDifference <= 140)
                    {
                        heightString = "far above you";
                    }
                    else
                    {
                        heightString = "very far above you";
                    }
                }
                else
                {
                    heightDifference = Math.Abs(Math.Abs(target.Z) - Math.Abs(caster.Z));
                    if (heightDifference > 0 && heightDifference <= 60)
                    {
                        heightString = "below you";
                    }
                    else if (heightDifference > 60 && heightDifference <= 140)
                    {
                        heightString = "far below you";
                    }
                    else
                    {
                        heightString = "very far below you";
                    }
                }
                #endregion

                caster.WriteToDisplay("You sense that your target is " + heightString + " and to the " + directionString + ".");
            }

        }

        private void castStrength(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            GenericCastMessage(caster, target, true);

            GenericEnchantMessage(target, this.Name);

            if (caster.BaseProfession == Character.ClassType.Thaumaturge)
            {
                Effect.CreateCharacterEffect(Effect.EffectType.Temporary_Strength, 3, target, Skills.GetSkillLevel(caster.magic) * 20, caster);
            }
            else
            {
                Effect.CreateCharacterEffect(Effect.EffectType.Temporary_Strength, 3, target, 180, caster);
            }
        }

        private void castCure(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            int cureAmount = 0;
            int pctHitsLeft = (int)(((float)target.Hits / (float)target.HitsFull) * 100);

            if (caster.BaseProfession != Character.ClassType.Thaumaturge)
            {
                if (pctHitsLeft < 75)
                {
                    cureAmount = (int)((target.HitsFull - target.Hits) * .80);
                }
                else { target.Hits = target.HitsFull; }
            }
            else
            {
                if (pctHitsLeft < 50)
                {
                    int criticalHeal = Rules.RollD(1, 100);
                    if (criticalHeal < Skills.GetSkillLevel(caster.magic))
                    {
                        Skills.GiveSkillExp(caster, Globals.eSkillType.Magic, (caster.Level - criticalHeal) * 10);
                        target.Hits = target.HitsFull;
                    }
                    else { cureAmount = (int)((target.HitsFull - target.Hits) * .80); }
                }
                else { target.Hits = target.HitsFull; }
            }

            target.Hits += cureAmount;

            if (target.Hits > target.HitsFull) { target.Hits = target.HitsFull; }

            GenericCastMessage(caster, target, true);

            if (caster.BaseProfession == Character.ClassType.Knight && target != caster)
                target.SendToAllInSight(target.Name + " is surrounded by a pale blue glow from " + caster.Name + "'s outstretched hand.");
            target.WriteToDisplay("You have been healed.");
        }

        private void castNeutralizePoison(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            this.GenericCastMessage(caster, target, true);

            if (target.Poisoned > 0)
            {
                target.WriteToDisplay("The poison has been neutralized.");
                target.Poisoned = 0;
            }
        }

        private void castPortal(Character caster, string args)
        {
            int xpos = 0;
            int ypos = 0;

            //clean out the command name
            args = args.Replace(this.SpellCommand, "");
            args = args.Trim();
            string[] sArgs = args.Split(" ".ToCharArray());

            switch (sArgs[0])
            {
                case "south":
                case "s":
                    ypos++;
                    break;
                case "southeast":
                case "se":
                    ypos++;
                    xpos++;
                    break;
                case "southwest":
                case "sw":
                    ypos++;
                    xpos--;
                    break;
                case "west":
                case "w":
                    xpos--;
                    break;
                case "east":
                case "e":
                    xpos++;
                    break;
                case "northeast":
                case "ne":
                    ypos--;
                    xpos++;
                    break;
                case "northwest":
                case "nw":
                    ypos--;
                    xpos--;
                    break;
                case "north":
                case "n":
                    ypos--;
                    break;
                default:
                    break;
            }

            Cell cell = Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + xpos, caster.Y + ypos, caster.Z);
            
            ArrayList cells = new ArrayList();

            cells.Add(cell);

            if (cell.DisplayGraphic == "[]" && !cell.IsMagicDead)
            {
                GenericCastMessage(caster, null, true);
                Effect effect = new Effect(Effect.EffectType.Illusion, ". ", 1, 2, caster, cells);
            }
            else
            {
                GenericFailMessage(caster, "");
                return;
            }
        }

        private void castProtectFireIce(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            GenericCastMessage(caster, target, true);

            GenericEnchantMessage(target, this.Name);

            Effect.CreateCharacterEffect(Effect.EffectType.Protection_from_Fire_and_Ice, Skills.GetSkillLevel(caster.magic) * 5, target, Skills.GetSkillLevel(caster.magic) * 30, caster);
        }

        private void castWizardEye(Character caster, string args)
        {
            Effect.CreateCharacterEffect(Effect.EffectType.Wizard_Eye, 1, caster, 10, caster);
        }

        #region Knight Spells
        private void castBless(Character caster, string args)
        {
            string[] sArgs = args.Split(" ".ToCharArray());

            Character target = FindAndConfirmTarget(caster, sArgs[sArgs.Length - 1]);

            if (target == null) { return; }

            // the Bless spell can only be cast by knights on those that are lawful
            if (target.Alignment != caster.Alignment)
            {
                GenericFailMessage(caster, "You may only bless other " + caster.Alignment.ToString().ToLower() + " characters.");
                return;
            }

            GenericCastMessage(caster, target, true);

            GenericEnchantMessage(target, this.Name);

            Effect.CreateCharacterEffect(Effect.EffectType.Bless, 0, target, 180, caster);
        }
        #endregion

        #region Thief Spells
        private void castFeatherFall(Character caster, string args)
        {
            // currently thieves may only enchant themselves with feather fall

            GenericEnchantMessage(caster, this.Name);
            Effect.CreateCharacterEffect(Effect.EffectType.Feather_Fall, 1, caster, Skills.GetSkillLevel(caster.magic), caster);
        }

        private void castFindSecretDoor(Character caster, string args)
        {

            caster.EmitSound(this.SoundFile);

            int bitcount = 0;
            //loop through all visible cells
            for (int ypos = -3; ypos <= 3; ypos += 1)
            {
                for (int xpos = -3; xpos <= 3; xpos += 1)
                {
                    Cell cell = Cell.GetCell(caster.FacetID, caster.LandID, caster.MapID, caster.X + xpos, caster.Y + ypos, caster.Z);
                    if (caster.CurrentCell.visCells[bitcount] && cell.IsSecretDoor)
                    {
                        if (cell.Effects.ContainsKey(Effect.EffectType.Hide_Door))
                        {
                            cell.Effects[Effect.EffectType.Hide_Door].StopAreaEffect();
                        }
                        else
                        {
                            Effect effect = new Effect(Effect.EffectType.Find_Secret_Door, ". ", 0, (int)(Skills.GetSkillLevel(caster.magic) / 2) + 10, caster, cell);
                            cell.EmitSound(Sound.GetCommonSound(Sound.CommonSound.OpenDoor));
                        }
                    }
                    bitcount++;
                }
            }
        }

        private void castHideDoor(Character caster, string args)
        {
            Cell cell = Map.GetCellRelevantToCell(caster.CurrentCell, args, true);
            bool spellSuccess = false;

            switch (cell.CellGraphic)
            {
                case "| ":
                case "--":
                case "/ ":
                case "\\ ":
                    caster.WriteToDisplay("You use your magic to conceal the door.");
                    spellSuccess = true;
                    break;
                case ". ":
                    if (cell.IsSecretDoor &&
                        (cell.Effects.ContainsKey(Effect.EffectType.Find_Secret_Door) || cell.Effects.ContainsKey(Effect.EffectType.Find_Secret_Rockwall)))
                    {
                        if (cell.Effects.ContainsKey(Effect.EffectType.Find_Secret_Door))
                        {
                            caster.WriteToDisplay("You use your magic to close the secret door.");
                            cell.Effects[Effect.EffectType.Find_Secret_Door].StopAreaEffect();
                        }

                        if (cell.Effects.ContainsKey(Effect.EffectType.Find_Secret_Rockwall))
                        {
                            caster.WriteToDisplay("You use your magic to conceal the secret door.");
                            cell.Effects[Effect.EffectType.Find_Secret_Rockwall].StopAreaEffect();
                        }

                        spellSuccess = true;

                    }
                    break;
                default:
                    caster.WriteToDisplay("Your spell fails.");
                    caster.EmitSound(Sound.GetCommonSound(Sound.CommonSound.SpellFail));
                    break;
            }
            if (spellSuccess)
            {
                cell.IsSecretDoor = true;
                Effect effect = new Effect(Effect.EffectType.Hide_Door, "[]", 0, (int)(Skills.GetSkillLevel(caster.magic) / 2), caster, cell);
            }
            return;
        }

        private void castHideInShadows(Character caster, string args)
        {
            if (Map.isCharNextToWall(caster))
            {
                if (!Rules.BreakHideSpell(caster))
                {
                    Effect.CreateCharacterEffect(Effect.EffectType.Hide_In_Shadows, 0, caster, 0, caster);
                    caster.WriteToDisplay("You fade into the shadows.");
                }
                else
                {
                    GenericFailMessage(caster, "");
                }
            }
            else
            {
                caster.WriteToDisplay("You must be in the shadows to hide.");
            }
            return;
        }

        private void castIdentify(Character caster, string args)
        {
            try
            {
                string[] sArgs = args.Split(" ".ToCharArray());

                Item iditem = new Item();

                if (caster.RightHand != null)
                {
                    iditem = caster.RightHand;
                }
                else if (caster.LeftHand != null)
                {
                    iditem = caster.LeftHand;
                }
                else
                {
                    iditem = Item.FindItemOnGround(sArgs[sArgs.Length - 1], caster.FacetID, caster.LandID, caster.MapID, caster.X, caster.Y, caster.Z);
                    if (iditem == null)
                    {
                        caster.WriteToDisplay("You must hold the item in your hands or be standing next to it.");
                        return;
                    }
                }

                string itmeffect = "";
                string itmenchantment = "";
                string itmcharges = "";
                string itmspecial = "";
                string itmalign = "";
                string itmattuned = "";

                if (iditem.spell > 0)
                {
                    if (iditem.charges == 0) { itmcharges = " There are no charges remaining."; }
                    if (iditem.charges > 1 && iditem.charges < 100) { itmcharges = " There are " + iditem.charges + " charges remaining."; }
                    if (iditem.charges == 1) { itmcharges = " There is 1 charge remaining."; }
                    if (iditem.charges == -1) { itmcharges = " The " + iditem.name + " has unlimited charges."; }

                    itmenchantment = " It contains the spell of " + Spell.GetSpell(iditem.spell).Name + "." + itmcharges;
                }

                System.Text.StringBuilder sb = new System.Text.StringBuilder(40);

                if (iditem.baseType == Globals.eItemBaseType.Figurine)
                {
                    sb.AppendFormat(" The {0}'s avatar has " + iditem.figExp + " experience.", iditem.name);
                }
                if (iditem.combatAdds > 0)
                {
                    sb.AppendFormat(" The combat adds are {0}.", iditem.combatAdds);
                }
                if (iditem.silver)
                {
                    sb.AppendFormat(" The {0} is silver.", iditem.name);
                }
                if (iditem.blueglow)
                {
                    sb.AppendFormat(" The {0} is emitting a faint blue glow.", iditem.name);
                }

                itmspecial = sb.ToString();

                //item effects

                if (iditem.effectType.Length > 0)
                {
                    string[] itmEffectType = iditem.effectType.Split(" ".ToCharArray());
                    string[] itmEffectAmount = iditem.effectAmount.Split(" ".ToCharArray());

                    if (itmEffectType.Length == 1 && Effect.GetEffectName((Effect.EffectType)Convert.ToInt32(itmEffectType[0])) != "")
                    {
                        if (iditem.baseType == Globals.eItemBaseType.Bottle)
                        {
                            itmeffect = " Inside the bottle is a potion of " + Effect.GetEffectName((Effect.EffectType)Convert.ToInt32(itmEffectType[0])) + ".";
                        }
                        else
                        {
                            itmeffect = " The " + iditem.name + " contains the enchantment of " + Effect.GetEffectName((Effect.EffectType)Convert.ToInt32(itmEffectType[0])) + ".";
                        }
                    }
                    else
                    {
                        ArrayList itemEffectList = new ArrayList();

                        for (int a = 0; a < itmEffectType.Length; a++)
                        {
                            Effect.EffectType effectType = (Effect.EffectType)Convert.ToInt32(itmEffectType[a]);
                            if (Effect.GetEffectName(effectType).ToLower() != "unknown")
                            {
                                itemEffectList.Add(Effect.GetEffectName(effectType));
                            }
                        }

                        if (itemEffectList.Count > 0)
                        {
                            if (itemEffectList.Count > 1)
                            {
                                itmeffect = " The " + iditem.name + " contains the enchantments of";
                                for (int a = 0; a < itemEffectList.Count; a++)
                                {
                                    if (a != itemEffectList.Count - 1)
                                    {
                                        itmeffect += " " + (string)itemEffectList[a] + ",";
                                    }
                                    else
                                    {
                                        itmeffect += " and " + (string)itemEffectList[a] + ".";
                                    }
                                }
                            }
                            else if (itemEffectList.Count == 1)
                            {
                                if (iditem.baseType == Globals.eItemBaseType.Bottle)
                                {
                                    itmeffect = " Inside the bottle is a potion of " + Effect.GetEffectName((Effect.EffectType)Convert.ToInt32(itmEffectType[0])) + ".";
                                }
                                else
                                {
                                    itmeffect = " The " + iditem.name + " contains the enchantment of " + Effect.GetEffectName((Effect.EffectType)Convert.ToInt32(itmEffectType[0])) + ".";
                                }
                            }
                        }
                    }
                }
                //item alignment
                if (iditem.alignment != Globals.eAlignment.None)
                {
                    string aligncolor = "";
                    switch (iditem.alignment)
                    {
                        case Globals.eAlignment.Lawful:
                            aligncolor = "white";
                            break;
                        case Globals.eAlignment.Neutral:
                            aligncolor = "green";
                            break;
                        case Globals.eAlignment.Chaotic:
                            aligncolor = "purple";
                            break;
                        case Globals.eAlignment.Evil:
                            aligncolor = "red";
                            break;
                        case Globals.eAlignment.Amoral:
                            aligncolor = "yellow";
                            break;
                        default:
                            break;
                    }
                    itmalign = " The " + iditem.name + " briefly pulses with a " + aligncolor + " glow.";
                }
                //item attuned
                if (iditem.attunedID != 0)
                {
                    if (iditem.attunedID > 0)
                    {
                        if (iditem.attunedID == caster.PlayerID)
                        {
                            itmattuned = " The " + iditem.name + " is soulbound to you.";
                        }
                        //if the caster's skill level is level 10+ give attuned name info
                        else if (Skills.GetSkillLevel(caster.magic) >= 10)
                        {
                            itmattuned = " The " + iditem.name + " is soulbound to " + PC.GetName(iditem.attunedID) + ".";
                        }
                        else
                        {
                            itmattuned = " The " + iditem.name + " is soulbound to another individual.";
                        }
                    }
                    else
                    {
                        itmattuned = " The " + iditem.name + " is soulbound to another being.";
                    }
                }
                //iditem.identified[iditem.identified.Length - 1] = caster.playerID;
                caster.WriteToDisplay("You are looking at " + iditem.longDesc + "." + itmeffect + itmenchantment + itmspecial + itmalign + itmattuned);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        private void castMakeRecall(Character caster, string args)
        {
            if (caster.RightHand != null && caster.RightHand.itemID == Item.ID_GOLDRING)
            {
                caster.RightHand = null;
                caster.RightHand = Item.CopyItemFromDictionary(Item.ID_RECALLRING);
                caster.WriteToDisplay("You cast the spell " + this.Name + ".");
            }
            else if (caster.LeftHand != null && caster.LeftHand.itemID == Item.ID_GOLDRING)
            {
                caster.LeftHand = null;
                caster.LeftHand = Item.CopyItemFromDictionary(Item.ID_RECALLRING);
                caster.WriteToDisplay("You cast the spell " + this.Name + ".");
            }
            else
            {
                if (System.Configuration.ConfigurationManager.AppSettings["RequireMakeRecallReagent"].ToLower() == "false")
                {
                    if (caster.RightHand == null)
                        caster.RightHand = Item.CopyItemFromDictionary(Item.ID_RECALLRING);
                    else if (caster.LeftHand == null)
                        caster.LeftHand = Item.CopyItemFromDictionary(Item.ID_RECALLRING);
                    else caster.CurrentCell.Add(Item.CopyItemFromDictionary(Item.ID_RECALLRING));
                }
                else
                {

                    if (caster.RightHand != null)
                    {
                        caster.WriteToDisplay("Your " + caster.RightHand.name + " explodes!");
                        caster.RightHand = null;
                        Rules.DoSpellDamage(null, caster, null, Rules.dice.Next(1, 20), "concussion");
                    }
                    else if (caster.LeftHand != null)
                    {
                        caster.WriteToDisplay("Your " + caster.LeftHand.name + " explodes!");
                        caster.LeftHand = null;
                        Rules.DoSpellDamage(null, caster, null, Rules.dice.Next(1, 20), "concussion");
                    }
                    else
                    {
                        GenericFailMessage(caster, "");
                    }
                }
            }
        }

        private void castNightVision(Character caster, string args)
        {
            // currently thieves may only enchant themselves with night vision

            GenericEnchantMessage(caster, this.Name);

            Effect.CreateCharacterEffect(Effect.EffectType.Night_Vision, 1, caster, Skills.GetSkillLevel(caster.magic) * 20, caster);
        }

        private void castSpeed(Character caster, string args)
        {
            return;
        }

        private void castVenom(Character caster, string args)
        {
            return;
        }
        #endregion

        private void GenericCastMessage(Character caster, Character target, bool emitSound)
        {
            if (target == null)
            {
                caster.WriteToDisplay("You cast " + this.Name + ".");
                if (emitSound)
                {
                    caster.EmitSound(this.SoundFile);
                }
            }
            else if (target != caster)
            {
                if (target.race != "")
                {
                    caster.WriteToDisplay("You cast " + this.Name + " at " + target.Name + ".");
                }
                else
                {
                    caster.WriteToDisplay("You cast " + this.Name + " at the " + target.Name + ".");
                }
                if (emitSound)
                {
                    target.EmitSound(this.SoundFile);
                }
            }
            else if (emitSound)
            {
                caster.EmitSound(this.SoundFile);
            }
        }
        
        private void GenericEnchantMessage(Character target, string enchantName)
        {
            target.WriteToDisplay("You have been enchanted with " + enchantName + "!");
            target.EmitSound(this.SoundFile);
        }        
    }
}
