using System;

namespace DragonsSpine.Commands
{
    public class GameCommand
    {
        #region Private Data
        /// <summary>
        /// Holds the string game command.
        /// </summary>
        private readonly string m_command;

        /// <summary>
        /// Holds the description of the game command.
        /// </summary>
        private readonly string m_description;

        /// <summary>
        /// Holds the minimum priv level that may use this game command.
        /// </summary>
        private readonly int m_privLevel;

        /// <summary>
        /// Holds examples of game command usages.
        /// </summary>
        private readonly string[] m_usages;

        /// <summary>
        /// Holds the handler for this game command.
        /// </summary>
        private readonly ICommandHandler m_commandHandler;

        /// <summary>
        ///  Holds the weight of the command.
        /// </summary>
        private readonly int m_commandWeight;
        #endregion

        #region Public Properties
        public string Command
        {
            get { return m_command; }
        }

        public int Weight
        {
            get { return m_commandWeight; }
        }

        public string Description
        {
            get { return m_description; }
        }

        public int PrivLevel
        {
            get { return m_privLevel; }
        }

        public string[] Usages
        {
            get { return m_usages; }
        }

        public ICommandHandler Handler
        {
            get { return m_commandHandler; }
        }
        #endregion

        public GameCommand(string command, string description, int privLevel, string[] usages, int weight, ICommandHandler handler)
        {
            m_command = command;
            m_description = description;
            m_privLevel = privLevel;
            m_usages = usages;
            m_commandHandler = handler;
            m_commandWeight = weight;
        }
    }
}