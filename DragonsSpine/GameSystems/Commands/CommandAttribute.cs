using System;
using System.Collections.Generic;
using System.Text;

namespace DragonsSpine.Commands
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class CommandAttribute : Attribute
    {
        #region Private Data
        /// <summary>
        /// Holds the string command.
        /// </summary>
        private string m_command;

        /// <summary>
        /// Holds the description of the command.
        /// </summary>
        private string m_description;

        /// <summary>
        /// Holds the minimum priv level that may use this command.
        /// </summary>
        private int m_privLevel;

        /// <summary>
        /// Holds command aliases.
        /// </summary>
        private string[] m_aliases;

        /// <summary>
        /// Holds examples of command usages.
        /// </summary>
        private string[] m_usages;

        /// <summary>
        /// Holds the weight of the command.
        /// </summary>
        private int m_commandWeight;
        #endregion

        #region Public Properties
        public string Command
        {
            get { return m_command; }
        }

        public int Weight
        {
            get { return m_commandWeight; }
        }

        public string Description
        {
            get { return m_description; }
        }

        public int PrivLevel
        {
            get { return m_privLevel; }
        }

        public string[] Aliases
        {
            get { return m_aliases; }
        }

        public string[] Usages
        {
            get { return m_usages; }
        }
        #endregion

        /// <summary>
        /// Constructs a new CommandAttribute.
        /// </summary>
        /// <param name="command">Command.</param>
        /// <param name="description">Description.</param>
        /// <param name="privLevel">Privilege Level.</param>
        /// <param name="aliases">Aliases.</param>
        /// <param name="weight">Command Weight</param>
        /// <param name="usages">Usages.</param>
        /// <param name="conferenceCommand">Can be used from conference rooms.</param>
        public CommandAttribute(string command, string description, int privLevel, string[] aliases, int weight, params string[] usages)
        {
            m_command = command;
            m_description = description;
            m_privLevel = privLevel;
            m_aliases = aliases;
            m_usages = usages;
            m_commandWeight = weight;
        }

        public CommandAttribute(string command, string description, int privLevel, int weight, params string[] usages)
            : this(command, description, privLevel, null, weight, usages)
        {
        }
    }
}
