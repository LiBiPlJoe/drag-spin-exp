using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Text;
using System.CodeDom.Compiler;
using Microsoft.VisualBasic;
using Microsoft.CSharp;
using DragonsSpine.Commands;

namespace DragonsSpine
{
    public static class ScriptManager
    {
        /// <summary>
        /// Holds all game commands.
        /// </summary>
        private static Dictionary<string, GameCommand> m_commands = new Dictionary<string, GameCommand>();

        public static Dictionary<string, GameCommand> CommandDict
        {
            get { return m_commands; }
        }

        /// <summary>
        /// Holds script assemblies.
        /// </summary>
        private static ArrayList m_scripts = new ArrayList();
        /// <summary>
        /// Gets all script assemblies.
        /// </summary>
        public static ArrayList Scripts
        {
            get { return m_scripts; }
        }

        /// <summary>
        /// Compiles the scripts into an assembly.
        /// </summary>
        /// <param name="compileVB">True if the source files will be in VB.NET.</param>
        /// <param name="path">Path to the source files.</param>
        /// <param name="dllName">Name of the assembly to be generated.</param>
        /// <param name="asm_names">References to other assemblies.</param>
        /// <returns>True if successful.</returns>
        public static bool CompileScripts(bool compileVB, string path, string dllName, string[] asm_names)
        {
            if (!path.EndsWith(@"\") && !path.EndsWith(@"/"))
            {
                path = path + "/";
            }

            // Clear assemblies.
            m_scripts.Clear();

            // Check if there are any scripts, if no scripts exist, that is fine as well
            ArrayList files = ParseDirectory(new DirectoryInfo(path), compileVB ? "*.vb" : "*.cs", true);

            if (files.Count == 0)
            {
                return true;
            }

            // We need a recompile, if the dll exists, delete it firsthand
            if (File.Exists(dllName))
            {
                File.Delete(dllName);
            }

            CompilerResults res = null;

            try
            {
                CodeDomProvider compiler;

                if (compileVB)
                {
                    compiler = new VBCodeProvider();
                }
                else
                {
                    compiler = new CSharpCodeProvider();
                }

                CompilerParameters param = new CompilerParameters(asm_names, dllName, true);
                param.GenerateExecutable = false;
                param.GenerateInMemory = false;
                param.WarningLevel = 2;
                param.CompilerOptions = @"/lib:." + Path.DirectorySeparatorChar + "lib";

                string[] filepaths = new string[files.Count];

                for (int i = 0; i < files.Count; i++)
                {
                    filepaths[i] = ((FileInfo)files[i]).FullName;
                }

                res = compiler.CompileAssemblyFromFile(param, filepaths);

                // After compiling, collect.
                GC.Collect();

                if (res.Errors.HasErrors)
                {
                    foreach (CompilerError err in res.Errors)
                    {
                        if (err.IsWarning) continue;

                        StringBuilder builder = new StringBuilder();
                        builder.Append("   ");
                        builder.Append(err.FileName);
                        builder.Append(" Line:");
                        builder.Append(err.Line);
                        builder.Append(" Col:");
                        builder.Append(err.Column);
                        Utils.Log("Compiler Errors: " + builder.ToString(), Utils.LogType.SystemFailure);
                    }

                    return false;
                }

                if (!m_scripts.Contains(res.CompiledAssembly))
                {
                    m_scripts.Add(res.CompiledAssembly);
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                m_scripts.Clear();
            }
            return true;
        }

        /// <summary>
        /// Parses a directory for all source files.
        /// </summary>
        /// <param name="path">The root directory to start the search in.</param>
        /// <param name="filter">A filter representing the types of files to search for.</param>
        /// <param name="deep">True if subdirectories should be included.</param>
        /// <returns>An ArrayList containing FileInfo's for all files in the path.</returns>
        private static ArrayList ParseDirectory(DirectoryInfo path, string filter, bool deep)
        {
            ArrayList files = new ArrayList();

            if (!path.Exists)
            {
                return files;
            }

            files.AddRange(path.GetFiles(filter));

            if (deep)
            {
                foreach (DirectoryInfo subdir in path.GetDirectories())
                {
                    files.AddRange(ParseDirectory(subdir, filter, deep));
                }
            }

            return files;
        }

        /// <summary>
        /// Searches the script assembly for all command handlers.
        /// </summary>
        /// <returns>True if succeeded.</returns>
        public static bool LoadCommands()
        {
            m_commands.Clear();
            #region Load Commands from Scripts
            foreach (Assembly script in Scripts)
            {
                // Step through each type in the assembly.
                foreach (Type type in script.GetTypes())
                {
                    if (!type.IsClass) continue;

                    if (type.GetInterface("DragonsSpine.Commands.ICommandHandler") == null) continue;

                    try
                    {
                        object[] objs = type.GetCustomAttributes(typeof(CommandAttribute), false);

                        foreach (CommandAttribute attrib in objs)
                        {
                            ICommandHandler handler = (ICommandHandler)Activator.CreateInstance(type);

                            GameCommand cmd = new GameCommand(attrib.Command, attrib.Description, attrib.PrivLevel,
                                attrib.Usages, attrib.Weight, handler);

                            // Add the command to the command dictionary. Key = command string.
                            m_commands.Add(attrib.Command, cmd);

                            if (attrib.Aliases != null)
                            {
                                foreach (string alias in attrib.Aliases)
                                {
                                    if (!m_commands.ContainsKey(alias))
                                    {
                                        m_commands.Add(alias, cmd);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Utils.LogException(e);
                    }
                }
            }
            #endregion
            #region Load Static Commands
            Assembly asm = Assembly.GetAssembly(typeof(GameCommand));

            foreach (Type type in asm.GetTypes())
            {
                object[] objs = type.GetCustomAttributes(typeof(CommandAttribute), false);

                foreach (CommandAttribute attrib in objs)
                {
                    ICommandHandler handler = (ICommandHandler)Activator.CreateInstance(type);

                    GameCommand cmd = new GameCommand(attrib.Command, attrib.Description, attrib.PrivLevel,
                        attrib.Usages, attrib.Weight , handler);

                    // Add the command to the command dictionary. Key = command string.
                    m_commands.Add(attrib.Command, cmd);

                    if (attrib.Aliases != null)
                    {
                        foreach (string alias in attrib.Aliases)
                        {
                            if (!m_commands.ContainsKey(alias))
                            {
                                m_commands.Add(alias, cmd);
                            }
                        }
                    }
                }
            }
            #endregion
            Utils.Log("Command scripts loaded, " + m_commands.Count + " total.", Utils.LogType.SystemGo);
            return true;
        }
    }
}
