using System;
using System.Collections.Generic;
using System.Text;

namespace DragonsSpine
{
    public class MailAttachment
    {
        long m_mailID;
        int m_itemID;
        int m_attunedID;
        string m_special;
        double m_coinValue;
        int m_charges;
        Globals.eAttuneType m_attuneType;
        long m_figExp;
        double m_paymentRequested;
        DateTime m_timeCreated;
        string m_whoCreated;

        public MailAttachment(System.Data.DataRow dr)
        {
            m_mailID = Convert.ToInt64(dr["mailID"]);
            m_itemID = Convert.ToInt32(dr["itemID"]);
            m_attunedID = Convert.ToInt32(dr["attunedID"]);
            m_special = dr["special"].ToString();
            m_coinValue = Convert.ToDouble(dr["coinValue"]);
            m_charges = Convert.ToInt32(dr["charges"]);
            m_attuneType = (Globals.eAttuneType)Convert.ToInt32(dr["attuneType"]);
            m_figExp = Convert.ToInt64(dr["figExp"]);
            m_paymentRequested = Convert.ToDouble(dr["paymentRequested"]);
            m_timeCreated = Convert.ToDateTime(dr["timeCreated"]);
            m_whoCreated = dr["whoCreated"].ToString();
        }
    }
}
