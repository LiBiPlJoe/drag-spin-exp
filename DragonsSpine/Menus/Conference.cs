namespace DragonsSpine
{
    using System;
    using System.Net;
    using System.Net.Sockets;
    using System.Collections;
    using System.Collections.Generic;

    public class Conference
    {
        public static string[] rooms = { "A", "B", "C", "D", "E", "F", "GM", "DEV" };
        public static int[] roomsLevel = { 0, 0, 0, 0, 0, 0, 1, 10 };

        public static string[] ProfanityArray = {"fuck","shit","dick","cunt","pussy","fag","prick","penis","crap","asshole","butt","tits",
												   "boobs","jackoff","suck","sex","intercourse","anal","masturbate","masterbate",
												   "wtf","bitch","cock"};


        public static void Header(Character ch, bool enterMessage)
        {
            string confroom = rooms[ch.confRoom];
            ArrayList room_players = getAllInRoom(ch);

            if (ch.protocol == DragonsSpineMain.APP_PROTOCOL)
            {
                String roomsList = "";
                for (int a = 0; a < rooms.Length; a++)
                {
                    roomsList += rooms[a] + Protocol.VSPLIT;
                }
                roomsList = roomsList.Substring(0, roomsList.Length - Protocol.VSPLIT.Length);

                if (enterMessage)
                {
                    ch.Write(Protocol.CONF_ENTER);
                }
                // send conference info -- current room letter, room players count, room letters list
                ch.Write(Protocol.CONF_INFO + "Room " + rooms[ch.confRoom] + Protocol.ISPLIT + room_players.Count + Protocol.ISPLIT + roomsList + Protocol.CONF_INFO_END);
            }
            else
            {
                Map.clearMap(ch);
            }

            ch.WriteLine("You are in Conference Room " + rooms[ch.confRoom] + ".", Protocol.TextType.Header);

            switch (room_players.Count)
            {
                case 0:
                    ch.WriteLine("You are the only player present.", Protocol.TextType.Header);
                    break;
                case 1:
                    ch.WriteLine("There is one player in the room.", Protocol.TextType.Header);
                    break;
                default:
                    ch.WriteLine("There are " + room_players.Count.ToString() + " players in the room.", Protocol.TextType.Header);
                    break;
            }

            ch.WriteLine("Type /help for a list of commands.", Protocol.TextType.Header);

            if (ch.IsInvisible)
            {
                ch.WriteLine("******* You are invisible. *******", Protocol.TextType.System);
            }

            if (!ch.IsInvisible && enterMessage)
            {
                ch.SendToAllInConferenceRoom(GetStaffTitle(ch) + ch.Name + " has entered the room.", Protocol.TextType.Enter);
            }

            Protocol.UpdateUserLists();
        }

        public static void ChatCommands(Character ch, string command, string args)
        {
            string all = command + " " + args;
            bool match = false;

            if (ch == null)
            {
                goto slashCommand;
            }

            if (all.ToLower() == "/echo on") { ch.echo = true; ch.WriteLine("Echo Enabled.", Protocol.TextType.Status); return; }
            if (all.ToLower() == "/echo off") { ch.echo = false; ch.WriteLine("Echo disabled.", Protocol.TextType.Status); return; }

            if (command.StartsWith(Convert.ToString((char)27)) || command.IndexOf((char)27) != -1)
            {
                if (!Protocol.CheckChatRoomCommand(ch, command, args))
                {
                    //to prevent users from sending protocol messages
                    Utils.Log(ch.GetLogString() + " attempted to send conference room message with protocol. Command: " + command + args, Utils.LogType.SystemWarning);
                    SendInvalidCommand(ch);
                }
            }
        slashCommand:
            if (command.StartsWith("/"))
            {
                switch (command.ToLower())
                {
                    #region /time
                    case "/time":
                        ch.WriteLine("The time is now " + DateTime.Now.ToString() + ".", Protocol.TextType.Status);
                        break;
                    #endregion
                    #region /scores
                    case "/topten":
                    case "/scores":
                    case "/score":
                        short amount = 10; // default amount will be 10 rows
                        bool devRequest = false;
                        bool pvp = false;
                        List<string> list = new List<string>();

                        if (ch.ImpLevel >= Globals.eImpLevel.GM) { devRequest = true; } // turn devrequest on if impLevel >= GM

                        try
                        {
                            if (args == null) // default to all classes list of 10
                            {
                                list = World.GetScoresList("all", amount, devRequest, pvp);

                                foreach (string line in list)
                                    ch.WriteLine(line, Protocol.TextType.Listing);

                                //ch.WriteLine("", Protocol.TextType.Listing);
                                //ch.WriteLine(World.GetScoresList("all", amount, devRequest, pvp), Protocol.TextType.Listing);
                                //ch.WriteLine("", Protocol.TextType.Listing);
                                break;
                            }
                            else
                            {
                                string[] scoreArgs = args.Split(" ".ToCharArray());

                                if (scoreArgs[0] == "me" || scoreArgs[0] == "my" || scoreArgs[0].ToLower() == ch.Name.ToLower()) // get my score
                                {
                                    if (scoreArgs.Length > 1 && scoreArgs[1].ToLower() == "pvp") { pvp = true; }
                                    list = World.GetScoresList(ch.Name, 1, true, pvp);
                                }
                                else
                                {
                                    if (scoreArgs.Length > 3)
                                    {
                                        ch.WriteLine("Formats: /scores, /scores me, /scores <class>, /scores <class> <amount>, /scores <name>", Protocol.TextType.Help);
                                        break;
                                    }
                                    try
                                    {
                                        if (scoreArgs.Length >= 2 && scoreArgs[1] == "all") { amount = 100; }
                                        else if (scoreArgs.Length >= 2) { amount = Convert.ToInt16(scoreArgs[1]); }
                                    }
                                    catch (Exception ex)
                                    {
                                        Utils.LogException(ex);
                                        amount = 10;
                                        pvp = false;
                                        scoreArgs[0] = "all";
                                    }

                                    if (amount < 1) { amount = 1; }

                                    list = World.GetScoresList(scoreArgs[0].ToLower(), amount, devRequest, pvp);
                                }
                            }

                            ch.WriteLine("", Protocol.TextType.Listing);
                            foreach (string line in list)
                                ch.WriteLine(line, Protocol.TextType.Listing);
                            ch.WriteLine("", Protocol.TextType.Listing);
                        }
                        catch (Exception e)
                        {
                            Utils.LogException(e);
                            ch.WriteLine("Formats: /scores, /scores me, /scores <class>, /scores <class> <amount>, /scores <name>", Protocol.TextType.Error);
                        }
                        break;
                    #endregion
                    #region /list
                    case "/list":
                        ch.WriteLine("", Protocol.TextType.Listing);
                        ch.WriteLine(PC.GetCharacterList(ch.accountID, false, ch), Protocol.TextType.Listing);
                        ch.WriteLine("", Protocol.TextType.Listing);
                        break;
                    #endregion
                    #region /lottery
                    case "/lottery":
                        ch.WriteLine("", Protocol.TextType.Listing);
                        if (World.Lottery == null)
                        {
                            World.Lottery = new long[World.GetFacetByIndex(0).Lands.Count];
                        }
                        foreach (Land land in World.GetFacetByIndex(0).Lands)
                        {
                            if (land.ShortDesc != "UW")
                            {
                                ch.WriteLine(land.Name + " lottery jackpot is currently " + World.Lottery[land.LandID] + " gold coins.", Protocol.TextType.Listing);
                            }
                        }
                        ch.WriteLine("", Protocol.TextType.Listing);
                        break;
                    #endregion
                    #region /switch
                    case "/switch":
                        if (args == null) { ch.WriteLine("Usage: /switch #   Use /list to see a list of your characters.", Protocol.TextType.Error); break; }
                        else
                        {
                            string[] sArgs = args.Split(" ".ToCharArray());

                            string oldName = ch.Name;
                            bool wasInvisible = ch.IsInvisible;

                            string oldLogString = ch.GetLogString();
                            if (PC.SelectNewCharacter(ch, Convert.ToInt32(sArgs[0])))
                            {
                                Utils.Log(oldLogString, Utils.LogType.Logout);
                                Utils.Log(ch.GetLogString(), Utils.LogType.Login);
                                if (!wasInvisible && !ch.IsInvisible) { ch.SendToAllInConferenceRoom(oldName + " has switched to " + ch.Name + ".", Protocol.TextType.Enter); } // do not send if switched player was invisible
                                else if (wasInvisible && !ch.IsInvisible) // but instead send a message that the new character entered (if visible)
                                {
                                    ch.SendToAllInConferenceRoom(GetStaffTitle(ch) + ch.Name + " has entered the room.", Protocol.TextType.Enter);
                                }
                                else if (!wasInvisible && ch.IsInvisible)
                                {
                                    ch.SendToAllInConferenceRoom(GetStaffTitle(ch) + ch.Name + " has left the world.", Protocol.TextType.Exit);
                                }
                                ch.WriteLine("You have switched to your character named " + ch.Name + ".", Protocol.TextType.Status);
                                ch.lastOnline = DateTime.Now;//.AddSeconds(DateTime.Now.Second); // set last online
                                PC.saveField(ch.PlayerID, "lastOnline", ch.lastOnline, null);
                            }
                            else
                            {
                                ch.WriteLine("Invalid character.", Protocol.TextType.Error);
                            }
                        }
                        break;
                    #endregion
                    #region /filter
                    case "/filter":
                        if (ch.filterProfanity)
                        {
                            ch.filterProfanity = false;
                            ch.WriteLine("Your profanity filter is now OFF.", Protocol.TextType.Status);
                        }
                        else
                        {
                            ch.filterProfanity = true;
                            ch.WriteLine("Your profanity filter is now ON.", Protocol.TextType.Status);
                        }
                        PC.saveField(ch.PlayerID, "filterProfanity", ch.filterProfanity, null);
                        break;
                    #endregion
                    #region /afk
                    case "/away":
                    case "/afk":
                        if (ch.afk)
                        {
                            ch.ResetAFK();
                            ch.WriteLine("You are no longer A.F.K. (Away From Keyboard).", Protocol.TextType.Status);
                        }
                        else
                        {
                            ch.afk = true;
                            if (args != null && args != "" && args.IndexOf((char)27) == -1) // do not allow escape characters
                            {
                                if (args.Length > 255) { args = args.Substring(0, 255); } // limit length of afk message
                                ch.afkMessage = args;
                                ch.WriteLine("You are now A.F.K. (Away From Keyboard).", Protocol.TextType.Status);
                            }
                            else
                            {
                                ch.WriteLine("You are now A.F.K. (Away From Keyboard).", Protocol.TextType.Status);
                            }
                        }
                        break;
                    #endregion
                    #region /echo
                    case "/echo":
                        if (ch.echo)
                        {
                            ch.echo = false;
                            ch.WriteLine("Echo disabled.", Protocol.TextType.Status);
                        }
                        else
                        {
                            ch.echo = true;
                            ch.WriteLine("Echo enabled.", Protocol.TextType.Status);
                        }
                        PC.saveField(ch.PlayerID, "echo", ch.echo, null);
                        break;
                    #endregion
                    #region /anon
                    case "/anon":
                    case "/anonymous":
                        if (ch.IsAnonymous)
                        {
                            ch.IsAnonymous = false;
                            ch.WriteLine("You are no longer anonymous.", Protocol.TextType.Status);
                        }
                        else
                        {
                            ch.IsAnonymous = true;
                            ch.WriteLine("You are now anonymous.", Protocol.TextType.Status);
                        }
                        PC.saveField(ch.PlayerID, "anonymous", ch.IsAnonymous, null);
                        break;
                    #endregion
                    #region /enter
                    case "/ent":
                    case "/play":
                    case "/enter":
                        if (DragonsSpineMain.ServerStatus < DragonsSpineMain.ServerState.Locked || ch.ImpLevel >= Globals.eImpLevel.GM)
                        {
                            ch.PCState = Globals.ePlayerState.PLAYING;
                            ch.RemoveFromLimbo();
                            ch.AddToWorld();
                            Map.clearMap(ch);
                            if (ch.protocol == "old-kesmai") { ch.Write(Map.KP_ENHANCER_ENABLE + Map.KP_ERASE_TO_END); ch.updateAll = true; }

                            if (ch.IsAnonymous && !ch.IsInvisible)
                            {
                                ch.SendToAllInConferenceRoom(GetStaffTitle(ch) + ch.Name + " has left for the lands.", Protocol.TextType.Exit);
                            }
                            else if (!ch.IsAnonymous && !ch.IsInvisible)
                            {
                                ch.SendToAllInConferenceRoom(GetStaffTitle(ch) + ch.Name + " has left for " + ch.Map.ShortDesc + ".", Protocol.TextType.Exit);
                            }
                        }
                        else
                        {
                            if (ch.usingClient)
                            {
                                Protocol.sendMessageBox(ch, "The game world is currently locked.", "World Locked", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                            }
                            else
                            {
                                ch.WriteLine("The game world is currently locked.", Protocol.TextType.System);
                            }
                        }
                        break;
                    #endregion
                    #region /exit
                    case "/quit":
                    case "/exit":
                        if (!ch.IsInvisible) // send exit message if character is not invisible
                        {
                            ch.SendToAllInConferenceRoom(GetStaffTitle(ch) + ch.Name + " has left the world.", Protocol.TextType.Exit);
                        }

                        Utils.Log(ch.GetLogString(), Utils.LogType.Logout); // log the logout

                        ch.RemoveFromLimbo();
                        ch.RemoveFromServer();
                        break;
                    #endregion
                    #region /room
                    case "/room":
                        if (args == "" || args == null)
                        {
                            Header(ch, false);
                            break;
                        }

                        for (int a = 0; a < rooms.Length; a++)
                        {
                            if (args.ToUpper() == rooms[a].ToUpper())
                            {
                                if (ch.ImpLevel >= (Globals.eImpLevel)roomsLevel[a])
                                {
                                    match = true;
                                    if (!ch.IsInvisible)
                                    {
                                        ch.SendToAllInConferenceRoom(GetStaffTitle(ch) + ch.Name + " has left the room.", Protocol.TextType.Exit);
                                    }
                                    ch.confRoom = a;
                                    PC.saveField(ch.PlayerID, "confRoom", ch.confRoom, null);
                                    break;
                                }
                            }
                        }
                        if (match)
                        {
                            Header(ch, true);
                        }
                        else
                        {
                            ch.WriteLine("Invalid Room.", Protocol.TextType.Error);
                        }
                        break;
                    #endregion
                    #region /notify
                    case "/notify":
                        if (ch.friendNotify)
                        {
                            ch.friendNotify = false;
                            ch.WriteLine("You will now be notified when your friends list log on and off.", Protocol.TextType.Status);
                        }
                        else
                        {
                            ch.friendNotify = true;
                            ch.WriteLine("You will no longer be notified when your friends log on and off.", Protocol.TextType.Status);
                        }
                        PC.saveField(ch.PlayerID, "friendNotify", ch.friendNotify, null);
                        break;
                    #endregion
                    #region /macro
                    case "/macro":
                    case "/macros":
                        if (args == null)
                        {
                            if (ch.macros.Count == 0)
                            {
                                ch.WriteLine("You do not have any macros set.", Protocol.TextType.Error);
                                break;
                            }
                            else
                            {
                                ch.WriteLine("", Protocol.TextType.Listing);
                                ch.WriteLine("Macro List", Protocol.TextType.Listing);
                                ch.WriteLine("----------", Protocol.TextType.Listing);
                                for (int a = 0; a < ch.macros.Count; a++)
                                {
                                    ch.WriteLine(a + " = " + ch.macros[a].ToString(), Protocol.TextType.Listing);
                                }
                            }
                        }
                        else
                        {
                            int macrosIndex;

                            try
                            {
                                macrosIndex = Convert.ToInt32(args.Substring(0, 1));
                            }
                            catch (Exception e)
                            {
                                Utils.LogException(e);
                                ch.WriteLine("Format: /macro # <text>, where # can range from 0 to " + Character.MAX_MACROS + ".", Protocol.TextType.Status);
                                break;
                            }

                            if (macrosIndex > Character.MAX_MACROS)
                            {
                                ch.WriteLine("The current maximum amount of macros each character may have is " + (Character.MAX_MACROS + 1) + ".", Protocol.TextType.Status);
                                break;
                            }

                            if (macrosIndex < 0)
                            {
                                ch.WriteLine("Format: /macro # <text>, where # can range from 0 to " + Character.MAX_MACROS + ".", Protocol.TextType.Status);
                                break;
                            }

                            if (ch.macros.Count >= macrosIndex + 1)
                            {
                                if (args.Substring(2).IndexOf(Protocol.ISPLIT) != -1)
                                {
                                    ch.WriteLine("Your macro contains an illegal character. The character " + Protocol.ISPLIT + " is reserved.", Protocol.TextType.Status);
                                    break;
                                }
                                ch.macros[macrosIndex] = args.Substring(2);
                                ch.WriteLine("Macro " + macrosIndex + " has been set to \"" + args.Substring(2) + "\".", Protocol.TextType.Status);
                            }
                            else
                            {
                                if (args.Substring(2).IndexOf(Protocol.ISPLIT) != -1)
                                {
                                    ch.WriteLine("Your macro contains an illegal character. The character " + Protocol.ISPLIT + " is reserved.", Protocol.TextType.Status);
                                    break;
                                }
                                ch.macros.Add(args.Substring(2));
                                ch.WriteLine("Macro " + macrosIndex + " has been set to \"" + args.Substring(2) + "\".", Protocol.TextType.Status);
                            }
                        }
                        break;
                    #endregion
                    #region /friend
                    case "/friend":
                    case "/friends":
                        if (args != null)
                        {
                            int friendsCount;
                            for (friendsCount = 0; friendsCount <= Character.MAX_FRIENDS; friendsCount++)
                            {
                                if (ch.friendsList[friendsCount] == 0) { break; }
                            }
                            if (friendsCount >= Character.MAX_FRIENDS) // friends list has a maximum length
                            {
                                ch.WriteLine("Your friends list is full.", Protocol.TextType.Error);
                                break;
                            }
                            int friendID = DAL.DBPlayer.getPlayerID(args); // attempt to retrieve the friend's playerID
                            if (friendID == -1) // player ID not found
                            {
                                ch.WriteLine("That player was not found.", Protocol.TextType.Error);
                                break;
                            }
                            if (friendID == ch.PlayerID) // players cannot add themselves to their friends list
                            {
                                ch.WriteLine("You cannot add your own name to your friend's list.", Protocol.TextType.Error);
                                break;
                            }
                            string friend = PC.GetName(friendID);
                            for (int a = 0; a < ch.friendsList.Length; a++) // check if player ID is already on friends list
                            {
                                if (ch.friendsList[a] == 0)
                                {
                                    break;
                                }
                                else if (ch.friendsList[a] == friendID) // if player ID exists, remove from friends list
                                {
                                    match = true;
                                    ch.friendsList[a] = 0;
                                    ch.WriteLine(friend + " has been removed from your friends list.", Protocol.TextType.Status);
                                    PC.saveField(ch.PlayerID, "friendsList", Utils.ConvertIntArrayToString(ch.friendsList), null);
                                    break;
                                }
                            }
                            if (!match) // add friend's player ID to first available array location
                            {
                                ch.friendsList[friendsCount] = friendID;
                                ch.WriteLine(friend + " has been added to your friends list.", Protocol.TextType.Status);
                                PC.saveField(ch.PlayerID, "friendsList", Utils.ConvertIntArrayToString(ch.friendsList), null);
                                break;
                            }
                            if (ch.protocol == DragonsSpineMain.APP_PROTOCOL)
                            {
                                Protocol.SendCharacterStats((PC)ch, ch);
                                Protocol.SendUserList(ch);
                            }
                        }
                        else // if args are null display ignore list
                        {
                            ch.WriteLine("", Protocol.TextType.Listing);
                            ch.WriteLine("Friends List", Protocol.TextType.Listing);
                            ch.WriteLine("-----------", Protocol.TextType.Listing);
                            for (int a = 0; a < ch.friendsList.Length; a++)
                            {
                                if (ch.friendsList[a] == 0)
                                {
                                    break;
                                }
                                ch.WriteLine(a + 1 + ". " + PC.GetName(ch.friendsList[a]), Protocol.TextType.Listing);
                            }
                            ch.WriteLine("", Protocol.TextType.Listing);
                        }
                        break;
                    #endregion
                    #region /ignore
                    case "/ignore":
                        if (args != null)
                        {
                            int ignoreCount;
                            for (ignoreCount = 0; ignoreCount <= Character.MAX_IGNORE; ignoreCount++) // get ignore list count
                            {
                                if (ch.ignoreList[ignoreCount] == 0)
                                {
                                    break;
                                }
                            }
                            if (ignoreCount >= Character.MAX_IGNORE) // ignore list has a maximum length
                            {
                                ch.WriteLine("Your ignore list is full.", Protocol.TextType.Error);
                                break;
                            }

                            int ignoreID = DAL.DBPlayer.getPlayerID(args); // attempt to retrieve the ignored player's ID
                            if (ignoreID == -1) // player ID not found
                            {
                                ch.WriteLine("That player was not found.", Protocol.TextType.Error);
                                break;
                            }
                            if (ignoreID == ch.PlayerID) // players cannot ignore themselves
                            {
                                ch.WriteLine("You cannot ignore yourself.", Protocol.TextType.Error);
                                break;
                            }
                            string ignored = PC.GetName(ignoreID);
                            //string ignored = DAL.DBPlayer.GetPlayerNameByID(ignoreID); // ignored player's name
                            if ((Globals.eImpLevel)DAL.DBPlayer.getPlayerField(ignoreID, "ImpLevel", ch.ImpLevel.GetType()) >= Globals.eImpLevel.GM) // cannot ignore staff member
                            {
                                ch.WriteLine("You cannot ignore a " + DragonsSpineMain.APP_NAME + " staff member.", Protocol.TextType.Error);
                                break;
                            }
                            for (int a = 0; a < ch.ignoreList.Length; a++) // check if player ID is already ignored
                            {
                                if (ch.ignoreList[a] == 0)
                                {
                                    break;
                                }
                                else if (ch.ignoreList[a] == ignoreID) // if player ID exists, remove from ignore list
                                {
                                    match = true;
                                    ch.ignoreList[a] = 0;
                                    ch.WriteLine(ignored + " has been removed from your ignore list.", Protocol.TextType.Status);
                                    PC.saveField(ch.PlayerID, "ignoreList", Utils.ConvertIntArrayToString(ch.ignoreList), null);
                                    break;
                                }
                            }
                            if (!match) // add ignored player ID to first available array location
                            {
                                ch.ignoreList[ignoreCount] = ignoreID;
                                ch.WriteLine(ignored + " has been added to your ignore list.", Protocol.TextType.Status);
                                PC.saveField(ch.PlayerID, "ignoreList", Utils.ConvertIntArrayToString(ch.ignoreList), null);
                                break;
                            }
                        }
                        else // if args are null display ignore list
                        {
                            ch.WriteLine("", Protocol.TextType.Listing);
                            ch.WriteLine("Ignore List", Protocol.TextType.Listing);
                            ch.WriteLine("-----------", Protocol.TextType.Listing);
                            for (int a = 0; a < ch.ignoreList.Length; a++)
                            {
                                if (ch.ignoreList[a] == 0)
                                {
                                    break;
                                }
                                ch.WriteLine(a + 1 + ". " + PC.GetName(ch.ignoreList[a]), Protocol.TextType.Listing);
                            }
                            ch.WriteLine("", Protocol.TextType.Listing);
                        }
                        break;
                    #endregion
                    #region /page
                    case "/page":
                        // if args are null then turn paging off or on
                        if (args == null)
                        {
                            if (ch.receivePages)
                            {
                                ch.receivePages = false;
                                ch.WriteLine("You will no longer receive pages.", Protocol.TextType.Status);
                            }
                            else
                            {
                                ch.receivePages = true;
                                ch.WriteLine("You will now receive pages.", Protocol.TextType.Status);
                            }
                            PC.saveField(ch.PlayerID, "receivePages", ch.receivePages, null);
                            break;
                        }
                        // if args are not null, search for the player and send them a page
                        try
                        {
                            Character receiver = PC.getOnline(PC.GetPlayerID(args));
                            if (receiver != null)
                            {
                                if (receiver.IsInvisible)
                                {
                                    ch.WriteLine("That player was not found.", Protocol.TextType.Status);
                                    return;
                                }

                                if (receiver.receivePages)
                                {
                                    if (Array.IndexOf(receiver.ignoreList, ch.PlayerID) == -1)
                                    {
                                        receiver.WriteLine(GetStaffTitle(ch) + ch.Name + " would like to speak to you in Conference " + GetUserLocation(ch) + ".", Protocol.TextType.Page);
                                    }
                                    ch.WriteLine(GetStaffTitle(receiver) + receiver.Name + " has been paged.", Protocol.TextType.Status);
                                }
                                else
                                {
                                    ch.WriteLine(GetStaffTitle(receiver) + receiver.Name + " has disabled their pager.", Protocol.TextType.Status);
                                }
                            }
                            else
                            {
                                ch.WriteLine("That player was not found.", Protocol.TextType.Status);
                            }
                        }
                        catch
                        {
                            ch.WriteLine("An error occured while processing your page request.", Protocol.TextType.Error);
                            Utils.Log("ChatCommands(" + ch.GetLogString() + ", " + command + ", " + args + ")", Utils.LogType.CommandFailure);
                        }
                        break;
                    #endregion
                    #region /displaycombatdamage
                    case "/displaycombatdamage":
                        if (ch.displayCombatDamage)
                        {
                            ch.displayCombatDamage = false;
                            ch.WriteLine("You will no longer see combat damage statistics.", Protocol.TextType.Status);
                        }
                        else
                        {
                            ch.displayCombatDamage = true;
                            ch.WriteLine("You will now see combat damage statistics.", Protocol.TextType.Status);
                        }
                        if (System.Configuration.ConfigurationManager.AppSettings["DisplayCombatDamage"].ToLower() == "false")
                            ch.WriteLine("** Combat damage statistic information is currently disabled.", Protocol.TextType.Status);
                        break; 
                    #endregion
                    #region /tell
                    case "/tell":
                        // if args are null then turn tells off or on
                        if (args == null)
                        {
                            if (ch.receivePages)
                            {
                                ch.receivePages = false;
                                ch.WriteLine("You will no longer receive tells.", Protocol.TextType.Status);
                            }
                            else
                            {
                                ch.receivePages = true;
                                ch.WriteLine("You will now receive tells.", Protocol.TextType.Status);
                            }
                            PC.saveField(ch.PlayerID, "receiveTells", ch.receiveTells, null);
                            break;
                        }

                        // if args are not null, find the player and send them the message
                        String[] newargs = args.Split(" ".ToCharArray());
                        string message = args.Replace(newargs[0], ": ");
                        foreach (Character receiver in Character.menuList) // currently only receivers using the protocol can receive messages while at the menu
                        {
                            if (receiver.Name.ToLower().Equals(newargs[0].ToLower()) && !receiver.IsInvisible)
                            {
                                match = true;
                                if (receiver.protocol == DragonsSpineMain.APP_PROTOCOL)
                                {
                                    if (receiver.receiveTells) // receiver has tells turned on
                                    {
                                        // let the ch know the tell was sent, even if the ch is ignored by the receiver....
                                        ch.WriteLine("You tell " + GetStaffTitle(receiver) + receiver.Name + message, Protocol.TextType.Private);
                                        if (receiver.afk)
                                        {
                                            string afkmsg = receiver.afkMessage;
                                            if (ch.filterProfanity)
                                            {
                                                afkmsg = FilterProfanity(afkmsg);
                                            }
                                            ch.WriteLine("(" + GetUserLocation(receiver) + ") " + GetStaffTitle(receiver) + receiver.Name + "(AFK) tells you: " + afkmsg, Protocol.TextType.Private);
                                        }
                                        if (Array.IndexOf(receiver.ignoreList, ch.PlayerID) == -1) // is the tell sender ignored by the receiver
                                        {
                                            if (ch.IsInvisible)
                                            {
                                                receiver.WriteLine(GetStaffTitle(ch) + ch.Name + " tells you" + message, Protocol.TextType.Private);
                                            }
                                            else
                                            {
                                                receiver.WriteLine("(" + GetUserLocation(ch) + ") " + GetStaffTitle(ch) + ch.Name + " tells you" + message, Protocol.TextType.Private);
                                                if (receiver.afk)
                                                {
                                                    string afkmsg = receiver.afkMessage;
                                                    if (receiver.filterProfanity)
                                                    {
                                                        afkmsg = FilterProfanity(afkmsg);
                                                    }
                                                    receiver.WriteLine("You tell " + GetStaffTitle(ch) + ch.Name + ": " + receiver.afkMessage, Protocol.TextType.Private);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ch.WriteLine(GetStaffTitle(receiver) + receiver.Name + " has private messages turned off.", Protocol.TextType.Status);
                                    }
                                }
                                else
                                {
                                    ch.WriteLine(GetStaffTitle(receiver) + receiver.Name + " cannot receive a private message while at the menu.", Protocol.TextType.Error);
                                }
                            }
                        }
                        foreach (Character receiver in Character.confList)
                        {
                            if (receiver.Name.ToLower().Equals(newargs[0].ToLower()) && !receiver.IsInvisible)
                            {
                                match = true;
                                if (receiver.receiveTells) // receiver has tells turned on
                                {
                                    // let the ch know the tell was sent, even if the ch is ignored by the receiver....
                                    ch.WriteLine("You tell " + GetStaffTitle(receiver) + receiver.Name + message, Protocol.TextType.Private);
                                    if (receiver.afk)
                                    {
                                        string afkmsg = receiver.afkMessage;
                                        if (ch.filterProfanity)
                                        {
                                            afkmsg = FilterProfanity(afkmsg);
                                        }
                                        ch.WriteLine("(" + GetUserLocation(receiver) + ") " + GetStaffTitle(receiver) + receiver.Name + "(AFK) tells you: " + afkmsg, Protocol.TextType.Private);
                                    }
                                    if (Array.IndexOf(receiver.ignoreList, ch.PlayerID) == -1) // is the tell sender ignored by the receiver
                                    {
                                        if (ch.IsInvisible)
                                        {
                                            receiver.WriteLine(GetStaffTitle(ch) + ch.Name + " tells you" + message, Protocol.TextType.Private);
                                        }
                                        else
                                        {
                                            receiver.WriteLine("(" + GetUserLocation(ch) + ") " + GetStaffTitle(ch) + ch.Name + " tells you" + message, Protocol.TextType.Private);
                                            if (receiver.afk)
                                            {
                                                string afkmsg = receiver.afkMessage;
                                                if (receiver.filterProfanity)
                                                {
                                                    afkmsg = FilterProfanity(afkmsg);
                                                }
                                                receiver.WriteLine("You tell " + GetStaffTitle(ch) + ch.Name + ": " + receiver.afkMessage, Protocol.TextType.Private);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    ch.WriteLine(GetStaffTitle(receiver) + receiver.Name + " has private messages turned off.", Protocol.TextType.Status);
                                }
                            }
                        }
                        if (!match)
                            foreach (Character receiver in Character.pcList) // in game...
                            {
                                if (receiver.Name.ToLower().Equals(newargs[0].ToLower()) && !receiver.IsInvisible)
                                {
                                    match = true;
                                    if (receiver.receiveTells) // receiver has tells turned on
                                    {
                                        // let the ch know the tell was sent, even if the ch is ignored by the receiver....
                                        ch.WriteLine("You tell " + receiver.Name + message, Protocol.TextType.Private);
                                        if (receiver.afk)
                                        {
                                            string afkmsg = receiver.afkMessage;
                                            if (ch.filterProfanity)
                                            {
                                                afkmsg = FilterProfanity(afkmsg);
                                            }
                                            ch.WriteLine("(" + GetUserLocation(receiver) + ") " + GetStaffTitle(receiver) + receiver.Name + "(AFK) tells you: " + afkmsg, Protocol.TextType.Private);
                                        }
                                        if (Array.IndexOf(receiver.ignoreList, ch.PlayerID) == -1)
                                        {
                                            if (ch.IsInvisible)
                                            {
                                                receiver.WriteLine(GetStaffTitle(ch) + ch.Name + " tells you" + message, Protocol.TextType.Private);
                                            }
                                            else
                                            {
                                                receiver.WriteLine("(" + GetUserLocation(ch) + ") " + GetStaffTitle(ch) + ch.Name + " tells you" + message, Protocol.TextType.Private);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ch.WriteLine(GetStaffTitle(receiver) + receiver.Name + " has private messages turned off.", Protocol.TextType.Status);
                                    }
                                }
                            }
                        if (!match)
                        {
                            ch.WriteLine("That player was not found.", Protocol.TextType.Status);
                        }
                        break;
                    #endregion
                    #region /who
                    case "/who":
                    case "/ust":
                    case "/users":
                        int count = 0;
                        string nameOfChar = "";
                        string nameOfMap = "";
                        string nameOfRoom = "";
                        string nameOfClass = "";
                        bool isAnonymous = false;
                        string anonymous = "";
                        bool isVisible = true;
                        string visibility = "";

                        ch.WriteLine("", Protocol.TextType.Listing);
                        ch.WriteLine("  Player Name              Location            Stats", Protocol.TextType.Listing);
                        try
                        {
                            foreach (Character chr in Character.menuList)
                            {
                                isAnonymous = chr.IsAnonymous;
                                isVisible = !chr.IsInvisible;
                                //make sure all players are visible to impLevel 5+
                                if (ch.ImpLevel >= Globals.eImpLevel.GM)
                                {
                                    isAnonymous = false;
                                    isVisible = true;
                                    // flag an impLevel that is anonymous or invisible
                                    anonymous = "";
                                    if (chr.IsAnonymous) { anonymous = "[ANON]"; }
                                    else if (chr.ImpLevel > Globals.eImpLevel.USER) { anonymous = GetStaffTitle(chr); }
                                    else { anonymous = ""; }
                                    if (chr.IsInvisible) { visibility = " [INVIS]"; }
                                    else { visibility = ""; }
                                }
                                if (chr.IsPC && isVisible)
                                {
                                    nameOfChar = GetStaffTitle(chr) + chr.Name;
                                    if (chr.afk) { nameOfChar = nameOfChar + "(AFK)"; }

                                    nameOfRoom = Conference.GetUserLocation(chr);
                                    if (chr.Name == "Nobody")
                                    {
                                        nameOfChar = "Nobody";
                                        nameOfRoom = "CharGen";
                                    }
                                    if (isAnonymous)
                                    {
                                        nameOfClass = "ANONYMOUS";
                                    }
                                    else
                                    {
                                        nameOfClass = "[" + chr.Level.ToString() + "] " + chr.classFullName;
                                    }
                                    count++;
                                    //ch.WriteLine(count + " " + nameOfChar + spaces.Substring(0, spaces.Length - nameOfChar.Length) + nameOfRoom + spaces.Substring(0, spaces.Length - nameOfRoom.Length) + nameOfClass + " " + anonymous + visibility, Protocol.TextType.Listing);
                                    ch.WriteLine(count + " " + nameOfChar.PadRight(25, ' ') + nameOfRoom.PadRight(20, ' ') + nameOfClass.PadRight(20, ' ') + anonymous + visibility, Protocol.TextType.Listing);
                                }
                            }
                            //get everyone in conference rooms
                            foreach (Character chr in Character.confList)
                            {
                                isAnonymous = chr.IsAnonymous;
                                isVisible = !chr.IsInvisible;
                                //make sure all players are visible to impLevel 5+
                                if (ch.ImpLevel >= Globals.eImpLevel.GM)
                                {
                                    isAnonymous = false;
                                    isVisible = true;
                                    //flag an impLevel that is anonymous or invisible
                                    anonymous = "";
                                    if (chr.IsAnonymous) { anonymous = "[ANON]"; }
                                    else if (chr.ImpLevel > Globals.eImpLevel.USER) { anonymous = GetStaffTitle(chr); }
                                    else { anonymous = ""; }
                                    if (chr.IsInvisible) { visibility = " [INVIS]"; }
                                    else { visibility = ""; }
                                }
                                if (chr.IsPC && isVisible)
                                {
                                    nameOfChar = GetStaffTitle(chr) + chr.Name;
                                    if (chr.afk) { nameOfChar = nameOfChar + "(AFK)"; }
                                    nameOfRoom = Conference.GetUserLocation(chr);
                                    if (chr.Name == "Nobody")
                                    {
                                        nameOfChar = "Nobody";
                                        nameOfRoom = "CharGen";
                                    }
                                    if (isAnonymous)
                                    {
                                        nameOfClass = "ANONYMOUS";
                                    }
                                    else
                                    {
                                        nameOfClass = "[" + chr.Level.ToString() + "] " + chr.classFullName;
                                    }
                                    count++;
                                    ch.WriteLine(count + " " + nameOfChar.PadRight(25, ' ') + nameOfRoom.PadRight(20, ' ') + nameOfClass.PadRight(20, ' ') + anonymous + visibility, Protocol.TextType.Listing);
                                }
                            }
                            // get everyone in the game
                            foreach (Character chr in Character.pcList)
                            {
                                isAnonymous = chr.IsAnonymous;
                                isVisible = !chr.IsInvisible;
                                // make sure all players are visible to impLevel 5+
                                if (ch.ImpLevel >= Globals.eImpLevel.GM)
                                {
                                    isAnonymous = false;
                                    isVisible = true;
                                    anonymous = "";
                                    // flag an impLevel that is anonymous or invisible
                                    if (chr.IsAnonymous) { anonymous = "[ANON]"; }
                                    else if (chr.ImpLevel > Globals.eImpLevel.USER) { anonymous = GetStaffTitle(chr); }
                                    else { anonymous = ""; }
                                    if (chr.IsInvisible) { visibility = "[INVIS]"; }
                                    else { visibility = ""; }
                                }
                                if (chr.IsPC && isVisible)
                                {
                                    nameOfChar = GetStaffTitle(chr) + chr.Name;
                                    if (chr.afk) { nameOfChar = nameOfChar + "(AFK)"; }
                                    if (isAnonymous)
                                    {
                                        nameOfMap = "ANONYMOUS";
                                        nameOfClass = "ANONYMOUS";
                                    }
                                    else
                                    {
                                        nameOfMap = chr.Map.Name;
                                        nameOfClass = "[" + chr.Level.ToString() + "] " + chr.classFullName;
                                    }
                                    count++;
                                    string final = count + " " + nameOfChar.PadRight(25) + nameOfMap.PadRight(20) + nameOfClass.PadRight(20);
                                    if (ch.IsDead && !isAnonymous)
                                    {
                                        final += " (dead)";
                                    }
                                    final += " " + anonymous + visibility;
                                    ch.WriteLine(final, Protocol.TextType.Listing);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Utils.Log("ChatCommands(" + ch.Name + ", " + command + ", " + args + ") " + e.Message + "  Stack: " + e.StackTrace, Utils.LogType.Exception);
                        }
                        if (count > 1)
                        {
                            ch.WriteLine("There are " + count.ToString() + " players in " + DragonsSpineMain.APP_NAME + ".", Protocol.TextType.Listing);
                        }
                        else
                        {
                            ch.WriteLine("There is 1 player in " + DragonsSpineMain.APP_NAME + ".", Protocol.TextType.Listing);
                        }
                        ch.WriteLine("", Protocol.TextType.Listing);
                        break;
                    #endregion
                    #region /help
                    case "/help":
                        ch.WriteLine("", Protocol.TextType.Help);
                        ch.WriteLine("Conference Room Commands", Protocol.TextType.Help);
                        ch.WriteLine("------------------------", Protocol.TextType.Help);
                        ch.WriteLine("  /play - Enter the game.", Protocol.TextType.Help);
                        ch.WriteLine("  /exit - Disconnect from " + DragonsSpineMain.APP_NAME + ".", Protocol.TextType.Help);
                        ch.WriteLine("  /list - List your current characters.", Protocol.TextType.Help);
                        ch.WriteLine("  /switch # - Switch to character #.", Protocol.TextType.Help);
                        ch.WriteLine("  /scores or /topten - Get player rankings.", Protocol.TextType.Help);
                        ch.WriteLine("     /scores me - Your current score.", Protocol.TextType.Help);
                        ch.WriteLine("     /scores <class> <amount>", Protocol.TextType.Help);
                        ch.WriteLine("     /scores <class>", Protocol.TextType.Help);
                        ch.WriteLine("     /scores <player>", Protocol.TextType.Help);
                        ch.WriteLine("	   /scores all <amount>", Protocol.TextType.Help);
                        ch.WriteLine("  /page - Toggle paging.", Protocol.TextType.Help);
                        ch.WriteLine("     /page <name> - Page someone in the game.", Protocol.TextType.Help);
                        ch.WriteLine("  /tell - Toggle private tells.", Protocol.TextType.Help);
                        ch.WriteLine("     /tell <name> <message> - Send a private tell.", Protocol.TextType.Help);
                        ch.WriteLine("  /friend - View your friends list.", Protocol.TextType.Help);
                        ch.WriteLine("     /friend <name> - Add or remove a player from your friends list.", Protocol.TextType.Help);
                        ch.WriteLine("  /notify - Toggle friend notification.", Protocol.TextType.Help);
                        ch.WriteLine("  /ignore - View your ignore list.", Protocol.TextType.Help);
                        ch.WriteLine("     /ignore <name> - Add or remove a player from your ignore list.", Protocol.TextType.Help);
                        ch.WriteLine("  /users - Shows everyone in the game.", Protocol.TextType.Help);
                        ch.WriteLine("  /menu - Return to the main menu.", Protocol.TextType.Help);
                        ch.WriteLine("  /afk - Toggle A.F.K. (Away From Keyboard).", Protocol.TextType.Help);
                        ch.WriteLine("     /afk <message> - Use personal A.F.K. message.", Protocol.TextType.Help);
                        ch.WriteLine("  /anon - Toggle anonymous.", Protocol.TextType.Help);
                        ch.WriteLine("  /filter - Toggle profanity filter.", Protocol.TextType.Help);
                        ch.WriteLine("  /rename <new name> - Change your character's name.", Protocol.TextType.Help);
                        ch.WriteLine("  /echo - Toggle command echo.", Protocol.TextType.Help);
                        ch.WriteLine("  /macro - View your macros list.", Protocol.TextType.Help);
                        ch.WriteLine("     /macro # <text> - Set # macro, where # macro is between 0 and " + Character.MAX_MACROS + ".", Protocol.TextType.Help);
                        ch.WriteLine("  /lottery - View current lottery jackpots.", Protocol.TextType.Help);
                        ch.WriteLine("  /commands - View a full list of available game commands.", Protocol.TextType.Help);
                        ch.WriteLine("  /displaycombatdamage - Toggle combat damage statistics.", Protocol.TextType.Help);
                        ch.WriteLine("  /help - This help list.", Protocol.TextType.Help);
                        ch.WriteLine("", Protocol.TextType.Help);
                        if (ch.ImpLevel > Globals.eImpLevel.USER)
                        {
                            ch.WriteLine("Staff Commands", Protocol.TextType.Help);
                            ch.WriteLine("  /stafftitle - Toggle staff title.", Protocol.TextType.Help);
                            ch.WriteLine("", Protocol.TextType.Help);
                        }
                        if (ch.ImpLevel >= Globals.eImpLevel.GM)
                        {
                            ch.WriteLine("GM Commands", Protocol.TextType.Help);
                            ch.WriteLine("  /invis - Toggle invisibility.", Protocol.TextType.Help);
                            ch.WriteLine("  /announce - Send announcement to all, anonymously.", Protocol.TextType.Help);
                            ch.WriteLine("  /selfannounce - Send announcement to all, includes your name.", Protocol.TextType.Help);
                            ch.WriteLine("  /immortal - Toggle immortality.", Protocol.TextType.Help);
                            ch.WriteLine("  /ban <name> <# of days> - Ban a player (includes their account)", Protocol.TextType.Help);
                            ch.WriteLine("  /boot <name> - Disconnects a player from the server.", Protocol.TextType.Help);
                            ch.WriteLine("  /rename <old> <new> - Change a player's name.", Protocol.TextType.Help);
                            ch.WriteLine("  /restock - Restock all merchant inventory items.", Protocol.TextType.Help);
                            ch.WriteLine("  /clearstores - Clears all non original merchant inventory items.", Protocol.TextType.Help);
                            ch.WriteLine("", Protocol.TextType.Help);
                        }
                        if (ch.ImpLevel >= Globals.eImpLevel.DEV)
                        {
                            ch.WriteLine("DEV Commands", Protocol.TextType.Help);
                            ch.WriteLine("  /bootplayers - Force all players to quit.", Protocol.TextType.Help);
                            ch.WriteLine("  /lockserver - Locks the server.", Protocol.TextType.Help);
                            ch.WriteLine("  /unlockserver - Unlocks the server.", Protocol.TextType.Help);
                            ch.WriteLine("  /implevel <name> <impLevel#> - Set a player's implevel.", Protocol.TextType.Help);
                            ch.WriteLine("  /getf <name> <field#> - Get a player's field value.", Protocol.TextType.Help);
                            ch.WriteLine("  /setf <name> <field#> <value> <notify> - Set a player's field value.", Protocol.TextType.Help);
                            ch.WriteLine("  /listf - Lists the fields that may be modified from conference.", Protocol.TextType.Help);
                            ch.WriteLine("  /purgeaccount <account> - Purge a players account.", Protocol.TextType.Help);
                            ch.WriteLine("", Protocol.TextType.Help);
                        }
                        ch.WriteLine("", Protocol.TextType.Help);
                        break;
                    #endregion
                    #region /menu
                    case "/menu":
                        ch.RemoveFromLimbo();
                        ch.AddToMenu();
                        if (!ch.IsInvisible)
                        {
                            ch.SendToAllInConferenceRoom(GetStaffTitle(ch) + ch.Name + " has exited to the menu.", Protocol.TextType.Exit);
                        }
                        ch.PCState = Globals.ePlayerState.MENU;
                        Menu.PrintMainMenu(ch);
                        break;
                    #endregion
                    #region /commands
                    case "/comm":
                    case "/commands":
                        Command cmd = new Command(ch);
                        cmd.commands("");
                        break;
                    #endregion
                    // Staff Commands
                    #region /stafftitle
                    case "/stafftitle":
                        if (ch.ImpLevel > Globals.eImpLevel.USER)
                        {
                            if (ch.showStaffTitle)
                            {
                                ch.showStaffTitle = false;
                                ch.WriteLine("Your staff title is now OFF.", Protocol.TextType.Status);

                            }
                            else
                            {
                                ch.showStaffTitle = true;
                                ch.WriteLine("Your staff title is now ON.", Protocol.TextType.Status);
                            }
                            Protocol.UpdateUserLists();
                            PC.saveField(ch.PlayerID, "showStaffTitle", ch.showStaffTitle, null);
                        }
                        else { Conference.SendInvalidCommand(ch); }
                        break;
                    #endregion
                    // GM Commands
                    #region /announce
                    case "/announce":
                        if (ch == null) // coming from local server window
                        {
                            int a;
                            for (a = 0; a < Character.pcList.Count; a++)
                            {
                                Character chr = Character.pcList[a];
                                if (chr != null)
                                {
                                    chr.WriteToDisplay("SYSTEM: " + args);
                                }
                            }
                            for (a = 0; a < Character.confList.Count; a++)
                            {
                                Character chr = Character.confList[a];
                                if (chr.PCState == Globals.ePlayerState.CONFERENCE)
                                {
                                    chr.WriteLine("SYSTEM: " + args, Protocol.TextType.System);
                                }
                            }
                            Utils.Log("LOCAL SYSTEM ANNOUNCEMENT: " + args, Utils.LogType.Unknown);
                            return;
                        }
                        if (ch.ImpLevel >= Globals.eImpLevel.GM)
                        {
                            Utils.Log(GetStaffTitle(ch) + ch.GetLogString() + " announced '" + args + "'", Utils.LogType.Unknown);
                            ch.SendToAll("SYSTEM: " + args); // send to all in game
                            ch.SendToAllInConferenceRoom("SYSTEM: " + args, Protocol.TextType.System); // send to all in chat room....
                        }
                        else { Conference.SendInvalidCommand(ch); }
                        break;
                    #endregion
                    case "/boot":
                        cmd = new Command(ch);
                        cmd.impboot(args);
                        break;
                    case "/ban":
                        cmd = new Command(ch);
                        cmd.impban(args);
                        break;
                    #region /selfannounce
                    case "/selfannounce":
                        if (ch == null) // coming from local server window
                        {
                            int a;
                            for (a = 0; a < Character.pcList.Count; a++)
                            {
                                Character chr = Character.pcList[a];
                                if (chr != null)
                                {
                                    chr.WriteToDisplay("SYSTEM: " + args);
                                }
                            }
                            for (a = 0; a < Character.confList.Count; a++)
                            {
                                Character chr = Character.confList[a];
                                if (chr.PCState == Globals.ePlayerState.CONFERENCE)
                                {
                                    chr.WriteLine("SYSTEM: " + args, Protocol.TextType.System);
                                }
                            }
                            Utils.Log("LOCAL SYSTEM ANNOUNCEMENT: " + args, Utils.LogType.Unknown);
                            return;
                        }
                        if (ch.ImpLevel >= Globals.eImpLevel.GM)
                        {
                            Utils.Log(GetStaffTitle(ch) + ch.GetLogString() + " announced '" + args + "'", Utils.LogType.Unknown);
                            ch.SendToAll("SYSTEM: " + ch.Name + ": " + args); // send to all in game
                            ch.SendToAllInConferenceRoom("SYSTEM: " + ch.Name + ": " + args, Protocol.TextType.System); // send to all in chat room....
                        }
                        else { Conference.SendInvalidCommand(ch); }
                        break;
                    #endregion
                    #region /immortal
                    case "/immortal":
                        if (ch.ImpLevel >= Globals.eImpLevel.GM)
                        {
                            ch.IsImmortal = !ch.IsImmortal;
                            if (!ch.IsImmortal) { ch.WriteLine("You are no longer immortal.", Protocol.TextType.Status); }
                            else { ch.WriteLine("You are now immortal.", Protocol.TextType.Status); }
                            PC.saveField(ch.PlayerID, "immortal", ch.IsImmortal, null); // save to the Player table
                            if (ch.ImpLevel < Globals.eImpLevel.DEV) // log immortal if a staff member other than dev
                            {
                                Utils.Log("[" + ch.PlayerID + "]" + ch.Name + "(" + ch.account + ") toggled immortal to " + Convert.ToString(ch.IsImmortal) + ".", Utils.LogType.CommandImmortal);
                            }
                        }
                        else { SendInvalidCommand(ch); }
                        break;
                    #endregion
                    #region /invis
                    case "/invis":
                    case "/invisible":
                        if (ch.ImpLevel >= Globals.eImpLevel.GM)
                        {
                            ch.IsInvisible = !ch.IsInvisible;
                            if (!ch.IsInvisible)
                            {
                                ch.WriteLine("You are no longer invisible.", Protocol.TextType.Status);
                                FriendNotify(ch, true);
                                ch.SendToAllInConferenceRoom(GetStaffTitle(ch) + ch.Name + " has entered the room.", Protocol.TextType.Enter);
                            }
                            else
                            {
                                ch.WriteLine("You are now invisible.", Protocol.TextType.Status);
                                ch.SendToAllInConferenceRoom(GetStaffTitle(ch) + ch.Name + " has left the world.", Protocol.TextType.Exit);
                                FriendNotify(ch, false);
                            }
                            Protocol.UpdateUserLists();
                            PC.saveField(ch.PlayerID, "invisible", ch.IsInvisible, null);
                        }
                        else { Conference.SendInvalidCommand(ch); }
                        break;
                    #endregion
                    #region /rename
                    case "/rename":
                        if (ch.ImpLevel >= Globals.eImpLevel.GM)
                        {
                            if (args == null || args.ToLower() == "help" || args == "?")
                            {
                                ch.WriteLine("Format: /rename <current name> <new name>", Protocol.TextType.Help);
                                ch.WriteLine("Format: /rename <new name>  -  Used to rename yourself.", Protocol.TextType.Help);
                                break;
                            }
                            else
                            {

                                string[] sArgs = args.Split(" ".ToCharArray());
                                
                                if (CharGen.CharacterNameDenied(ch, sArgs[1]))
                                {
                                    ch.WriteLine("Illegal name.", Protocol.TextType.Error);
                                    break;
                                }
                                int id = PC.GetPlayerID(sArgs[0]);
                                if (id == -1) // cannot find player ID
                                {
                                    ch.WriteLine("Player '" + sArgs[0] + "' was not found.", Protocol.TextType.Error);
                                    break;
                                }
                                ch.WriteLine(PC.GetName(id) + "'s name has been changed to " + sArgs[1] + ".", Protocol.TextType.System);
                                PC.saveField(id, "name", sArgs[1], null);
                                Character online = PC.getOnline(id);
                                if (online != null)
                                {
                                    online.Name = sArgs[1];
                                    online.WriteLine("Your name has been changed to " + online.Name + ".", Protocol.TextType.System);
                                }
                            }
                        }
                        else
                        {
                            if (args == null || args.ToLower() == "help" || args == "?")
                            {
                                ch.WriteLine("Format: /rename <new name>", Protocol.TextType.Help);
                                break;
                            }
                        }
                        break;
                    #endregion
                    #region /restock
                    case "/restock":
                        if (ch.ImpLevel >= Globals.eImpLevel.GM)
                        {
                            StoreItem.RestockStores();
                            ch.WriteLine("You have restocked all stores with their original stock items.", Protocol.TextType.System);
                        }
                        else
                        {
                            SendInvalidCommand(ch);
                        }
                        break;
                    #endregion
                    #region /clearstores
                    case "/clearstores":
                        if (ch.ImpLevel >= Globals.eImpLevel.GM)
                        {
                            StoreItem.ClearStores();
                            ch.WriteLine("You have cleared all stores of their non original stock items.", Protocol.TextType.System);
                        }
                        else
                        {
                            SendInvalidCommand(ch);
                        }
                        break;
                    #endregion
                    // DEV Commands
                    #region /Shutdown
                    case "/shutdown":
                        if (ch.ImpLevel >= Globals.eImpLevel.DEV)
                        {
                            DragonsSpineMain.ServerStatus = DragonsSpineMain.ServerState.ShuttingDown;
                        }
                        else SendInvalidCommand(ch);
                        break;
                    #endregion
                    #region /ProcessEmptyWorld <on|off>
                    case "/processemptyworld":
                        if (ch.ImpLevel >= Globals.eImpLevel.DEV)
                        {
                            if (args == "on")
                            {
                                DragonsSpineMain.ProcessEmptyWorld = true;
                                ch.WriteLine("Empty World Processing enabled.", Protocol.TextType.System);
                            }
                            else
                            {
                                DragonsSpineMain.ProcessEmptyWorld = false;
                                ch.WriteLine("Empty World Processing disabled.", Protocol.TextType.System);
                            }
                        }
                        else { SendInvalidCommand(ch); }
                        break;
                    #endregion
                    #region /PurgeAccount
                    case "/purgeaccount":
                        if (ch.ImpLevel >= Globals.eImpLevel.DEV)
                        {
                            Account account = DAL.DBAccount.GetAccountByName(args);
                            if (account != null)
                            {
                                ch.WriteLine("Purging account " + account.accountName + "...", Protocol.TextType.System);
                                foreach (string chrName in account.players)
                                {
                                    if (chrName.Length > 0)
                                    {
                                        if (DAL.DBPlayer.deleteCharacter(PC.GetPlayerID(chrName)))
                                        {
                                            ch.WriteLine("Deleted character " + chrName + ".", Protocol.TextType.System);
                                        }
                                        else
                                        {
                                            ch.WriteLine("Failed to delete character " + chrName + ".", Protocol.TextType.System);
                                        }
                                    }
                                }
                                DAL.DBAccount.DeleteAccount(account.accountID);
                                ch.WriteLine("Purge completed of account " + account.accountName + ".", Protocol.TextType.System);
                            }
                            else
                            {
                                ch.WriteLine("Failed to find account with the name " + args + ".", Protocol.TextType.Error);
                            }
                        }
                        else { SendInvalidCommand(ch); }
                        break;
                    #endregion
                    #region /searchnpc
                    case "/searchnpc":
                        if (ch.ImpLevel >= Globals.eImpLevel.DEV)
                        {
                            string[] testArgs = args.Split(" ".ToCharArray());
                            foreach (NPC npc in Character.NPCList)
                            {
                                if (npc.spawnArmor.IndexOf(" " + testArgs[0] + " ") != -1)
                                {
                                    ch.WriteLine(npc.Name + "(" + npc.npcID + ") has item " + testArgs[0] + " in spawnArmor.", Protocol.TextType.System);
                                }
                                if (npc.spawnRightHand.IndexOf(" " + testArgs[0] + " ") != -1)
                                {
                                    ch.WriteLine(npc.Name + "(" + npc.npcID + ") has item " + testArgs[0] + " in spawnRightHand.", Protocol.TextType.System);
                                }
                                if (npc.spawnLeftHand.IndexOf(" " + testArgs[0] + " ") != -1)
                                {
                                    ch.WriteLine(npc.Name + "(" + npc.npcID + ") has item " + testArgs[0] + " in spawnLeftHand.", Protocol.TextType.System);
                                }
                                if (npc.lootVeryCommonArray.IndexOf(" " + testArgs[0] + " ") != -1)
                                {
                                    ch.WriteLine(npc.Name + "(" + npc.npcID + ") has item " + testArgs[0] + " in lootVeryCommonArray.", Protocol.TextType.System);
                                }
                                if (npc.lootCommonArray.IndexOf(" " + testArgs[0] + " ") != -1)
                                {
                                    ch.WriteLine(npc.Name + "(" + npc.npcID + ") has item " + testArgs[0] + " in lootCommonArray.", Protocol.TextType.System);
                                }
                                if (npc.lootRareArray.IndexOf(" " + testArgs[0] + " ") != -1)
                                {
                                    ch.WriteLine(npc.Name + "(" + npc.npcID + ") has item " + testArgs[0] + " in lootRareArray.", Protocol.TextType.System);
                                }
                                if (npc.lootLairArray.IndexOf(" " + testArgs[0] + " ") != -1)
                                {
                                    ch.WriteLine(npc.Name + "(" + npc.npcID + ") has item " + testArgs[0] + " in lootLairArray.", Protocol.TextType.System);
                                }
                                if (npc.lootAlwaysArray.IndexOf(" " + testArgs[0] + " ") != -1)
                                {
                                    ch.WriteLine(npc.Name + "(" + npc.npcID + ") has item " + testArgs[0] + " in lootAlwaysArray.", Protocol.TextType.System);
                                }
                                if (npc.lootBeltArray.IndexOf(" " + testArgs[0] + " ") != -1)
                                {
                                    ch.WriteLine(npc.Name + "(" + npc.npcID + ") has item " + testArgs[0] + " in lootBeltArray.", Protocol.TextType.System);
                                }
                            }
                        }
                        else { SendInvalidCommand(ch); }
                        break;
                    #endregion
                    #region /listf
                    case "/listf":
                        if (ch.ImpLevel >= Globals.eImpLevel.DEV)
                        {
                            try
                            {
                                System.Reflection.FieldInfo[] fieldInfo = typeof(Character).GetFields();
                                
                                ch.WriteLine("", Protocol.TextType.Listing);
                                ch.WriteLine("Character Fields", Protocol.TextType.Listing);
                                ch.WriteLine("----------------", Protocol.TextType.Listing);
                                if (ch.usingClient)
                                {
                                    for (int a = 0; a < fieldInfo.Length; a++)
                                    {
                                        ch.WriteLine(a + ". " + fieldInfo[a].Name, Protocol.TextType.Help);
                                    }
                                }
                                else
                                {
                                    string output = "";
                                    for (int a = 0; a < fieldInfo.Length; a++)
                                    {
                                        output += "" + a + ". " + fieldInfo[a].Name + "\r\n";
                                    }

                                    ch.WriteLine(output, Protocol.TextType.Listing);
                                }
                                ch.WriteLine("", Protocol.TextType.Listing);
                            }
                            catch (Exception e)
                            {
                                Utils.Log("ERROR: ChatRoom.ChatCommands(" + ch.GetLogString() + ", " + command + ", " + args + ")", Utils.LogType.CommandFailure);
                                Utils.LogException(e);
                            }
                        }
                        else { SendInvalidCommand(ch); }
                        break;
                    #endregion
                    #region /getf
                    case "/getf":
                        if (ch.ImpLevel >= Globals.eImpLevel.DEV)
                        {
                            // /getf ebony..rose 1
                            if (args == null || args.ToLower() == "help" || args == "?")
                            {
                                ch.WriteLine("Format: /getf <name> <field#>", Protocol.TextType.Help);
                                ch.WriteLine("<name> is the full name of the player (not case sensitive)", Protocol.TextType.Help);
                                ch.WriteLine("<field#> is the number of the field from the /listf output", Protocol.TextType.Help);
                                break;
                            }
                            try
                            {
                                String[] getfArgs = args.Split(" ".ToCharArray());
                                int id = PC.GetPlayerID(getfArgs[0]);
                                if (id == -1)
                                {
                                    ch.WriteLine("Player '" + getfArgs[0] + "' was not found.", Protocol.TextType.Error);
                                    break;
                                }
                                System.Reflection.FieldInfo[] fieldInfo = typeof(Character).GetFields(); // get field info array
                                int num = Convert.ToInt32(getfArgs[1]);
                                string classField = fieldInfo[Convert.ToInt32(getfArgs[1])].Name;
                                string f = "";
                                Character online = PC.getOnline(id);
                                if (online != null) // get the value from the online Character
                                {
                                    f = Convert.ToString(fieldInfo[num].GetValue(online));
                                    ch.WriteLine(online.Name + "'s '" + classField + "' is set to \"" + f + "\".", Protocol.TextType.System);
                                }
                                else // get the value from the database
                                {
                                    string[] columns = DAL.DBPlayer.getPlayerTableColumnNames(id);
                                    string tableField = "";
                                    for (int a = 0; a < columns.Length; a++)
                                    {
                                        if (columns[a].ToUpper() == classField.ToUpper())
                                        {
                                            tableField = columns[a];
                                            break;
                                        }
                                    }
                                    if (tableField != "")
                                    {
                                        if (online == null) { f = Convert.ToString(DAL.DBPlayer.getPlayerField(id, tableField, fieldInfo[num].FieldType)); }
                                        ch.WriteLine(PC.GetName(id) + "'s \"" + classField + "\" is set to \"" + f + "\".", Protocol.TextType.System);
                                    }
                                    else
                                    {
                                        ch.WriteLine("Failed to find a column that matches the field '" + classField + "'.", Protocol.TextType.Error);
                                    }
                                }

                            }
                            catch (Exception e)
                            {
                                Utils.Log("ChatRoom.ChatCommands(" + ch.GetLogString() + ", " + command + ", " + args + ")", Utils.LogType.CommandFailure);
                                Utils.LogException(e);
                            }
                        }
                        else { SendInvalidCommand(ch); }
                        break;
                    #endregion
                    #region /setf
                    case "/setf":
                        if (ch.ImpLevel >= Globals.eImpLevel.DEV)
                        {
                            // /setf ebony..rose 7 false
                            if (args == null || args.ToLower() == "help" || args == "?")
                            {
                                ch.WriteLine("Format: /setf <name> <field#> <value> <notify>", Protocol.TextType.Help);
                                ch.WriteLine("<name> is the full name of the player (not case sensitive)", Protocol.TextType.Help);
                                ch.WriteLine("<field#> is the number of the field from the /listf output", Protocol.TextType.Help);
                                ch.WriteLine("<value> to set the Character field to", Protocol.TextType.Error);
                                ch.WriteLine("<notify> true will notify the user that the value has been changed", Protocol.TextType.Help);
                                ch.WriteLine("**Please note that booleans must be 'true or 'false', not 1 or 0.", Protocol.TextType.Help);
                                break;
                            }

                            try
                            {
                                String[] setfArgs = args.Split(" ".ToCharArray()); // split the arguments
                                int id = PC.GetPlayerID(setfArgs[0]); // get the player ID using character's full name
                                if (id == -1) // cannot find player ID
                                {
                                    ch.WriteLine("Player '" + setfArgs[0] + "' was not found.", Protocol.TextType.Error);
                                    break;
                                }
                                //System.Reflection.Assembly dspine = System.Reflection.Assembly.GetExecutingAssembly(); // executing assembly
                                System.Reflection.FieldInfo[] fieldInfo = typeof(DragonsSpine.Character).GetFields(); // get field info array
                                int num = Convert.ToInt32(setfArgs[1]); // the number of the field according to /listf listing...
                                string classField = fieldInfo[num].Name; // get the name of the field

                                Object getValue = null;
                                Object setValue = Convert.ChangeType(setfArgs[2], Convert.GetTypeCode(fieldInfo[num].FieldType));
                                Character online = PC.getOnline(id);

                                if (online != null)
                                {
                                    getValue = fieldInfo[Convert.ToInt32(setfArgs[1])].GetValue(online); // get current value
                                    if (!getValue.Equals(setValue))
                                    {
                                        //Utils.setFieldValue(online, fieldInfo[num].Name, setValue);
                                        fieldInfo[num].SetValue(online, Convert.ChangeType(setValue, fieldInfo[num].FieldType)); // set value
                                        // notify dev that the field has been changed
                                        ch.WriteLine(online.Name + "'s \"" + classField + "\" has been changed from \"" + Convert.ToString(getValue) + "\" to \"" + Convert.ToString(fieldInfo[num].GetValue(online)) + "\".", Protocol.TextType.System);
                                        // notify the online player that the field has been changed if desired by the dev
                                        if (setfArgs.Length >= 4 && setfArgs[3].ToLower() == "true")
                                        {
                                            online.WriteLine("Your " + classField + " has been changed from \"" + Convert.ToString(getValue) + "\" to \"" + Convert.ToString(fieldInfo[num].GetValue(online)) + "\".", Protocol.TextType.System);
                                            ch.WriteLine(online.Name + " has been notified of the change.", Protocol.TextType.System);
                                        }
                                    }
                                    else
                                    {
                                        ch.WriteLine(online.Name + "'s " + classField + " is already equal to \"" + Convert.ToString(fieldInfo[num].GetValue(online)) + "\".", Protocol.TextType.Error);
                                        break;
                                    }
                                }

                                string[] columns = DAL.DBPlayer.getPlayerTableColumnNames(id); // get Player Table column names for comparison to class fields
                                string tableField = "";
                                for (int a = 0; a < columns.Length; a++) // compare column name and class field
                                {
                                    if (columns[a].ToUpper() == classField.ToUpper())
                                    {
                                        tableField = columns[a];
                                        break;
                                    }
                                }
                                if (tableField != "")
                                {
                                    if (getValue == null)
                                    {
                                        getValue = DAL.DBPlayer.getPlayerField(id, tableField, fieldInfo[num].FieldType);
                                    }
                                    if (DAL.DBPlayer.savePlayerField(id, tableField, setValue) != -1)
                                    {
                                        ch.WriteLine(PC.GetName(id) + "'s '" + classField + "' has been saved to column \"" + tableField + "\" in the database.", Protocol.TextType.System);
                                    }
                                    else
                                    {
                                        ch.WriteLine("There was an error saving the value to the Player table. The field may only be available while the character is online.", Protocol.TextType.Error);
                                    }
                                }
                                else
                                {
                                    ch.WriteLine("Failed to find a column in the Player table that matches the field '" + classField + "'. The value was not saved to the database.", Protocol.TextType.Error);
                                }
                            }
                            catch (Exception e)
                            {
                                Utils.Log("ChatRoom.ChatCommands(" + ch.GetLogString() + ", " + command + ", " + args + ")", Utils.LogType.CommandFailure);
                                Utils.LogException(e);
                            }
                        }
                        else { SendInvalidCommand(ch); }
                        break;
                    #endregion
                    #region /implevel
                    case "/implevel":
                        if (args == null || args.ToLower() == "help" || args == "?")
                        {
                            ch.WriteLine("Format: /implevel <name> <impLevel#>", Protocol.TextType.Help);
                            ch.WriteLine("<name> is the full name of the player (not case sensitive)", Protocol.TextType.Help);
                            ch.WriteLine("<impLevel#> is the new impLevel, ranging from 0 to " + Enum.GetValues(ch.ImpLevel.GetType()).Length + ".", Protocol.TextType.Help);
                            break;
                        }
                        try
                        {
                            String[] impArgs = args.Split(" ".ToCharArray());
                            int id = PC.GetPlayerID(impArgs[0]);
                            if (id == -1)
                            {
                                ch.WriteLine("Player '" + impArgs[0] + "' was not found.", Protocol.TextType.Error);
                                break;
                            }
                            Character online = PC.getOnline(id);
                            Globals.eImpLevel oldImpLevel = (Globals.eImpLevel)PC.GetField(id, "impLevel", (int)ch.ImpLevel, null);
                            Globals.eImpLevel newImpLevel = (Globals.eImpLevel)Convert.ToInt32(impArgs[1]);
                            if (online != null) // if character is online, set their new implevel and alert them of the change
                            {
                                online.ImpLevel = newImpLevel;
                                Protocol.UpdateUserLists(); // send new user lists to protocol users
                                online.WriteLine("Your impLevel has been changed from " + oldImpLevel.ToString() + " to " + newImpLevel.ToString() + ".", Protocol.TextType.System);
                            }
                            else
                            {
                                ch.WriteLine(PC.GetName(id) + "'s impLevel has been changed from " + oldImpLevel.ToString() + " to " + newImpLevel.ToString() + ".", Protocol.TextType.System);
                            }
                            PC.saveField(id, "impLevel", (int)online.ImpLevel, null); // save new implevel to Player table
                            Utils.Log(online.GetLogString() + " impLevel was changed from " + oldImpLevel + " to " + newImpLevel + " by " + ch.GetLogString() + ".", Utils.LogType.Unknown);
                        }
                        catch (Exception e)
                        {
                            Utils.Log("ChatRoom.ChatCommands(" + ch.GetLogString() + ", " + command + ", " + args + ")", Utils.LogType.CommandFailure);
                            Utils.LogException(e);
                            Conference.ChatCommands(ch, "/implevel", null);
                        }
                        break;
                    #endregion
                    #region /lockserver
                    case "/lockserver":
                        if (ch.ImpLevel >= Globals.eImpLevel.DEV)
                        {
                            DragonsSpineMain.ServerStatus = DragonsSpineMain.ServerState.Locked;
                            ch.WriteLine("Game world has been locked.", Protocol.TextType.System);
                        }
                        else { SendInvalidCommand(ch); }
                        break;
                    #endregion
                    #region /unlockserver
                    case "/unlockserver":
                        if (ch.ImpLevel >= Globals.eImpLevel.DEV)
                        {
                            DragonsSpineMain.ServerStatus = DragonsSpineMain.ServerState.Running;
                            ch.WriteLine("Game world has been unlocked.", Protocol.TextType.System);
                        }
                        else { SendInvalidCommand(ch); }
                        break;
                    #endregion
                    #region /bootplayers
                    case "/bootplayers":
                        if (ch.ImpLevel >= Globals.eImpLevel.DEV)
                        {
                            DragonsSpineMain.ServerStatus = DragonsSpineMain.ServerState.Locked;
                            foreach (Character chr in new List<Character>(Character.pcList))
                            {
                                Command.ParseCommand(chr, "forcequit", "");
                            }
                            ch.WriteLine("Game World locked, players booted.", Protocol.TextType.System);
                        }
                        else { SendInvalidCommand(ch); }
                        break;
                    #endregion
                    default:
                        Conference.SendInvalidCommand(ch);
                        break;
                }
            }
            else
            {
                if (ch.IsInvisible)
                {
                    ch.SendToAllInConferenceRoom("Someone: " + all, Protocol.TextType.PlayerChat);
                }
                else
                {
                    ch.SendToAllInConferenceRoom(GetStaffTitle(ch) + ch.Name + ": " + all, Protocol.TextType.PlayerChat);
                }
            }
            return;
        }

        public static ArrayList getAllInRoom(Character ch)
        {
            ArrayList a = new ArrayList();
            if (ch.ImpLevel < Globals.eImpLevel.GM)
            {
                foreach (Character chr in Character.confList)
                {
                    if (chr.PCState == Globals.ePlayerState.CONFERENCE && chr != ch && !chr.IsInvisible)
                    {
                        if (ch.confRoom == chr.confRoom) a.Add(chr);
                    }
                }
                return a;
            }
            else
            {
                foreach (Character chr in Character.confList)
                {
                    if (chr.PCState == Globals.ePlayerState.CONFERENCE && chr != ch)
                    {
                        if (ch.confRoom == chr.confRoom) a.Add(chr);
                    }
                }
                return a;
            }
        }

        public static string GetUserLocation(Character ch)
        {
            if (ch.Name == "Nobody") { return "Character Generator"; }

            switch (ch.PCState)
            {
                case Globals.ePlayerState.PLAYING:
                    string location = ch.Map.Name;
                    if (ch.IsDead)
                    {
                        location = location + " (dead)";
                    }
                    return location;
                case Globals.ePlayerState.CONFERENCE:
                    return "Room " + rooms[ch.confRoom];
                default:
                    return "Menu";
            }
        }

        public static string GetStaffTitle(Character ch)
        {
            if (ch.showStaffTitle)
            {
                switch ((int)ch.ImpLevel)
                {
                    case 4:
                    case 3:
                        return "[DEV]";
                    case 2:
                        return "[GM]";
                    default:
                        return "";
                }
            }
            else
            {
                return "";
            }
        }

        public static void SendInvalidCommand(Character ch)
        {
            string invalid = "Invalid Command. Type /help for a list of commands.";
            ch.WriteLine(invalid, Protocol.TextType.Error);
        }

        public static string FilterProfanity(string message) // filter profanity and return filtered message
        {
            for (int a = 0; a < ProfanityArray.Length; a++)
            {
                if (message.ToLower().IndexOf(ProfanityArray[a]) != -1)
                {
                    do
                    {
                        int b = message.ToLower().IndexOf(ProfanityArray[a]);
                        message = message.Remove(b, ProfanityArray[a].Length);
                        string replacement = "";
                        for (int c = 0; c < ProfanityArray[a].Length; c++)
                        {
                            replacement = replacement + "*";
                        }
                        message = message.Insert(b, replacement);
                    }
                    while (message.IndexOf(ProfanityArray[a]) != -1);
                }
            }
            return message;
        }

        public static void FriendNotify(Character friend, bool login) // send friend notify messages
        {
            if (!friend.IsInvisible) // only do friend notify if the friend is visible
            {
                // notify users at the menu
                foreach (Character user in Character.menuList)
                {
                    if (Array.IndexOf(user.friendsList, friend.PlayerID) != -1 && user.friendNotify) // if the player is a friend and notifications are on
                    {
                        if (Array.IndexOf(friend.ignoreList, user.PlayerID) == -1) // ** if the friend has this user ignored the message will not be sent
                        {
                            if (login) { user.WriteLine(GetStaffTitle(friend) + friend.Name + " has logged on.", Protocol.TextType.Friend); }
                            else { user.WriteLine(GetStaffTitle(friend) + friend.Name + " has logged off.", Protocol.TextType.Friend); }
                        }
                    }
                }

                // notify users in conference
                foreach (Character user in Character.confList)
                {
                    if (Array.IndexOf(user.friendsList, friend.PlayerID) != -1 && user.friendNotify) // if the player is a friend and notifications are on
                    {
                        if (Array.IndexOf(friend.ignoreList, user.PlayerID) == -1) // ** if the friend has this user ignored the message will not be sent
                        {
                            if (login) { user.WriteLine(GetStaffTitle(friend) + friend.Name + " has logged on.", Protocol.TextType.Friend); }
                            else { user.WriteLine(GetStaffTitle(friend) + friend.Name + " has logged off.", Protocol.TextType.Friend); }
                        }
                    }
                }

                // notify users in the game
                foreach (Character user in Character.pcList)
                {
                    if (Array.IndexOf(user.friendsList, friend.PlayerID) != -1 && user.friendNotify) // if the player is a friend and notifications are on
                    {
                        if (Array.IndexOf(friend.ignoreList, user.PlayerID) == -1) // ** if the friend has this user ignored the message will not be sent
                        {
                            if (login) { user.WriteLine(GetStaffTitle(friend) + friend.Name + " has logged on.", Protocol.TextType.Friend); }
                            else { user.WriteLine(GetStaffTitle(friend) + friend.Name + " has logged off.", Protocol.TextType.Friend); }
                        }
                    }
                }
            }
        }
    }
}
