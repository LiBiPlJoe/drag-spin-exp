namespace DragonsSpine
{
	using System;
	using System.Collections;
	//using System.Collections.Specialized;
    using System.Collections.Generic;
	using System.IO;

    public class CharGen
    {
        public static List<PC> chargenList = new List<PC>();
        public static ArrayList Newbies = new ArrayList();

			// dice rolling
			//TODO: get time for seed
			Random dice = new Random();

			// Used to get STR roll for females and Shallants
			public int[] strLookup = new int[100] {
				5,6,6,7,7,8,8,9,9,10,
				10,11,11,12,12,12,12,12,12,12,
				12,12,12,13,13,13,13,13,13,13,
				13,13,13,14,14,14,14,14,14,14,
				14,14,14,15,15,15,15,15,15,15,
				15,15,15,15,15,15,15,15,16,16,
				16,16,16,16,16,16,16,16,17,17,
				17,17,17,17,17,17,17,17,18,18,
				18,18,18,18,18,18,18,18,19,19,
				20,20,21,21,22,22,23,23,24,24
			};

			// Used to get stat rolls for everyone else
			public int[] statLookup = new int[100] {
				5,6,7,7,8,8,9,9,10,10,
				11,11,11,11,11,12,12,12,12,12,
				13,13,13,13,13,14,14,14,14,14,
				15,15,15,15,15,16,16,16,16,16,
				17,17,17,17,17,17,17,17,17,17,
				18,18,18,18,18,19,19,19,19,19,
				20,20,20,20,20,21,21,21,21,21,
				22,22,22,22,22,23,23,23,23,23,
				24,24,24,24,24,25,25,25,26,26,
				26,27,27,27,28,28,29,29,30,30
			};

        public static bool LoadNewbies()
        {
            try
            {
                foreach (Globals.eHomeland homeland in Enum.GetValues(typeof(Globals.eHomeland)))
                {
                    foreach (Character.ClassType classType in Enum.GetValues(typeof(Character.ClassType)))
                    {
                        if (classType != Character.ClassType.None && classType < Character.ClassType.Knight)
                        {
                            Character newbie = new Character();
                            newbie.race = homeland.ToString();
                            newbie.BaseProfession = classType;
                            DAL.DBWorld.SetupNewCharacter(newbie);
                            Newbies.Add(newbie);
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return false;
            }
        }

		public static string newChar()
		{
			string rtnStr;

			rtnStr = "Welcome to the character generator.";

			return rtnStr;
		}

		// List of available races
		public static string pickRace()
		{
			string rtnStr;
			rtnStr =			"\r\nRaces:\r\n";
			rtnStr = rtnStr +	"  I  -  Illyria\r\n";
			rtnStr = rtnStr +	"  M  -  Mu\r\n";
			rtnStr = rtnStr +	"  L  -  Lemuria\r\n";
			rtnStr = rtnStr +	"  LG -  Leng\r\n";
			rtnStr = rtnStr +	"  D  -  Draznia\r\n";
			rtnStr = rtnStr +	"  H  -  Hovath\r\n";
			rtnStr = rtnStr +	"  MN -  Mnar\r\n";
			rtnStr = rtnStr +	"  B  -  Barbarian\r\n";
			rtnStr = rtnStr +	"\r\nPlease Select a Race: ";

			return rtnStr;
		}
		// List of available genders
		public static string pickGender()
		{
			string rtnStr;
			rtnStr =			"\r\nGender:\r\n";
			rtnStr = rtnStr +	"  1 - Male\r\n";
			rtnStr = rtnStr +	"  2 - Female\r\n";
			rtnStr = rtnStr +   "\r\nPlease Select a Gender: ";
			return rtnStr;
		}


		//roll up stats
        public static string RollStats(Character ch)
        {
            string rtnStr = "";
            string manaStr = "\r\n";

            ch.CurrentCell = Cell.GetCell(0, 0, 0, 41, 34, 0);

            if (ch.Level != 3) { ch.Level = 3; } // set level to 3

            ch.Strength = AdjustMinMaxStat(RollStat() + GetRacialBonus(ch, "strength"));
            ch.Dexterity = AdjustMinMaxStat(RollStat() + GetRacialBonus(ch, "dexterity"));
            ch.Intelligence = AdjustMinMaxStat(RollStat() + GetRacialBonus(ch, "intelligence"));
            ch.Wisdom = AdjustMinMaxStat(RollStat() + GetRacialBonus(ch, "wisdom"));
            ch.Constitution = AdjustMinMaxStat(RollStat() + GetRacialBonus(ch, "constitution"));
            ch.Charisma = AdjustMinMaxStat(RollStat() + GetRacialBonus(ch, "charisma"));

            if (ch.Strength >= 16)
                ch.strengthAdd = 1;
            else ch.strengthAdd = 0;

            if (ch.Dexterity >= 16)
                ch.dexterityAdd = 1;
            else ch.dexterityAdd = 0;

            ch.HitsMax = Rules.GetHitsGain(ch, ch.Level) + (int)(ch.Land.StatCapOperand / 1.5);
            ch.StaminaMax = Rules.GetStaminaGain(ch, 1) + (int)(ch.Land.StatCapOperand / 8);
            ch.ManaMax = 0;
            if (ch.IsSpellUser)
            {
                ch.ManaMax = Rules.GetManaGain(ch, 1) + (int)(ch.Land.StatCapOperand / 8);
                manaStr = "Mana:    " + ch.ManaMax.ToString().PadLeft(2) + "\r\n";
            }

            if (ch.HitsMax > Rules.GetMaximumHits(ch))
                ch.HitsMax = Rules.GetMaximumHits(ch);

            if (ch.protocol == DragonsSpineMain.APP_PROTOCOL)
            {
                return Protocol.CHARGEN_ROLLER_RESULTS + Protocol.FormatCharGenRollerResults(ch) + Protocol.CHARGEN_ROLLER_RESULTS_END;
            }

            rtnStr = rtnStr + "\r\nCharacter Stats:\r\n";
            rtnStr = rtnStr + "  Strength:     " + ch.Strength.ToString().PadLeft(2).PadRight(10) + "Adds:     " + ch.strengthAdd + "\r\n";
            rtnStr = rtnStr + "  Dexterity:    " + ch.Dexterity.ToString().PadLeft(2).PadRight(10) + "Adds:     " + ch.dexterityAdd + "\r\n";
            rtnStr = rtnStr + "  Intelligence: " + ch.Intelligence.ToString().PadLeft(2) + "\r\n";
            rtnStr = rtnStr + "  Wisdom:       " + ch.Wisdom.ToString().PadLeft(2).PadRight(10) + "Hits:    " + ch.HitsMax.ToString().PadLeft(2) + "\r\n";
            rtnStr = rtnStr + "  Constitution: " + ch.Constitution.ToString().PadLeft(2).PadRight(10) + "Stamina: " + ch.StaminaMax.ToString().PadLeft(2) + "\r\n";
            rtnStr = rtnStr + "  Charisma:     " + ch.Charisma.ToString().PadLeft(2).PadRight(10) + manaStr;
            rtnStr = rtnStr + "\r\nRoll Again? (y,n): ";

            return rtnStr;
        }

		public static string pickClass()
		{
			string rtnStr = "";

				rtnStr = rtnStr + "\r\nPlease select a character class:\r\n";
				rtnStr = rtnStr + "  FI  -  Fighter\r\n";
				rtnStr = rtnStr + "  TH  -  Thaumaturge\r\n";
				rtnStr = rtnStr + "  WI  -  Wizard\r\n";
				rtnStr = rtnStr + "  MA  -  Martial Artist\r\n";
				rtnStr = rtnStr + "  TF  -  Thief\r\n";
				rtnStr = rtnStr + "\r\nSelect a Character Class: ";

			return rtnStr;
		}

		public static bool raceVerify(string race)
		{
			switch(race.ToLower())
			{
				case "i":
					return true;
				case "m":
					return true;
				case "l":
					return true;
				case "lg":
					return true;
				case "d":
					return true;
				case "h":
					return true;
				case "mn":
					return true;
				case "b":
					return true;
				default:
					return false;
			}
		}

		public static bool genderVerify(string gender)
		{
				if(gender == "1" || gender == "2")
				{
					return true;
				}
				else
				{
					return false;
				}
		}

		public static bool statVerify(string stat)
		{
				if(stat == "y" || stat == "n")
				{
					return true;
				}
				else
				{
					return false;
				}
		}

		public static Character.ClassType classVerify(string choice)
		{
			switch(choice.ToLower())
			{
                case "fighter":
				case "fi":
					return Character.ClassType.Fighter;
                case "thaumaturge":
				case "th":
					return Character.ClassType.Thaumaturge;
                case "wizard":
				case "wi":
					return Character.ClassType.Wizard;
                case "martial artist":
				case "ma":
					return Character.ClassType.Martial_Artist;
                case "thief":
				case "tf":
					return Character.ClassType.Thief;
                default:
                    return Character.ClassType.None;
			}
		}

		public static bool CharacterNameDenied(Character chr, string name) 
		{
			bool deny = false;

			string[] specialnames = new string[]{"orc","gnoll","goblin","skeleton","kobold","dragon","drake","griffin","manticore",
												 "banshee","demon","bear","boar","vampire","ydnac","lars","sven","oskar","olaf",
												 "marlis","neela","phong","ironbar","vulcan","sheriff","rolf","troll","wyvern",
												 "ydmos","tanner","crazy.derf","trambuskar","ianta","alia","priest","statue",
												 "shidosha","pazuzu","asmodeus","damballa","glamdrang","samael","perdurabo",
												 "thamuz","knight","martialartist","thief","thaum","thaumaturge","wizard","fighter",
                                                 "thisson"};

            string[] silly = new string[]{"pvp","lol","haha","hehe","btw","atm","jeje","rofl","roflmao","lmao","lmfao","lmho","dragonsspine",
											"dragonspine","nobody","somebody","anybody","account","character"};

			string acceptable = "abcdefghijklmnopqrstuvwxyz.";

			foreach(Character ch in Character.allCharList)
			{
				if(name.ToLower() == ch.Name.ToLower())
				{
					deny = true;
				}
			}

			foreach(Character npc in Character.NPCList)
			{
				if(name.ToLower() == npc.Name.ToLower())
				{
					deny = true;
				}
			}
			foreach(string special in specialnames)
			{
				if(name.ToLower() == special)
				{
					deny = true;
				}
			}
			foreach(string nasty in Conference.ProfanityArray)
			{
				if(name.ToLower().IndexOf(nasty) > -1)
				{
					deny = true;
				}
			}
			foreach(string word in silly)
			{
				if(name.ToLower().IndexOf(word) > -1)
				{
					deny = true;
				}
			}
			foreach(char charcheck in name.ToLower())
			{
				if(acceptable.IndexOf(charcheck) == -1)
                {
                    deny = true;
                }
			}
			if(name.Length > Character.NAME_MAX_LENGTH || name.Length < Character.NAME_MIN_LENGTH)
			{
                string adjective = "";
                if(name.Length > Character.NAME_MAX_LENGTH)
                {
                    adjective = "long";
                }
                else if (name.Length < Character.NAME_MIN_LENGTH)
                {
                    adjective = "short";
                }
                if (chr.usingClient)
                {
                    Protocol.sendMessageBox(chr, "The name you have chosen is too " + adjective + ". Character names must be between " + Character.NAME_MIN_LENGTH + " and " + Character.NAME_MAX_LENGTH + " letters in length.", 
                        "Invalid Name", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.None);
                }
                else
                {
                    chr.WriteLine("The name you have chosen is too " + adjective + ". Character names must be between " + Character.NAME_MIN_LENGTH + " and " + Character.NAME_MAX_LENGTH + " letters in length.", Protocol.TextType.Error);
                }
				return true;
			}
            else if (deny)
            {
                if (chr.usingClient)
                {
                    Protocol.sendMessageBox(chr, "The name you have chosen is not acceptable. Please choose another.",
                        "Invalid Name", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.None);
                }
                else
                {
                    chr.WriteLine("The name you have chosen is not acceptable.", Protocol.TextType.Error);
                }
                return deny;
            }
            else
            {
                if (DAL.DBPlayer.playerExists(name))
                {
                    if (chr.usingClient)
                    {
                        Protocol.sendMessageBox(chr, "A character with the name \""+name+"\" already exists. Please choose another.",
                        "Name Exists", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.None);
                    }
                    else
                    {
                        chr.WriteLine("A character with the name you have chosen already exists.", Protocol.TextType.Error);
                    }
                    return true;
                }
            }

			return deny;
		}

		public static bool PasswordDenied(string password) 
		{
			if(password.Length > Account.PASSWORD_MAX_LENGTH || password.Length < Account.PASSWORD_MIN_LENGTH)
				return true;

            return false;
		}

		public static bool PasswordVerified(Character ch, string vPass) 
		{
			if(ch.Password.Equals(vPass))
				return true;

            return false;
		}

		public static void SetupNewCharacter(Character ch)
		{
			DAL.DBWorld.SetupNewCharacter(ch);

            #region Unused
            //Barbarian race name was used to setup new character
            //if(ch.race == "Barbarian"){ch.race = "the plains";}

            //alignment
            //ch.alignment = ch.alignment;

            //starting stats
            //ch.hitsmax = ch.hitsmax;
            //ch.stamina = ch.stamina;
            //ch.manaMax = ch.manaMax;

            //ch.bow = ch.bow; // starting skills
            //ch.dagger = ch.dagger;
            //ch.flail = ch.flail;
            //ch.halberd = ch.halberd;
            //ch.mace = ch.mace;
            //ch.rapier = ch.rapier;
            //ch.shuriken = ch.shuriken;
            //ch.staff = ch.staff;
            //ch.sword = ch.sword;
            //ch.threestaff = ch.threestaff;
            //ch.twoHanded = ch.twoHanded;
            //ch.thievery = ch.thievery;
            //ch.magic = ch.magic;
            //ch.unarmed = ch.unarmed; 

            //ch.RightHand = ch.RightHand; // starting equipment
            //ch.LeftHand = ch.LeftHand;
            //ch.wearing = ch.wearing;
            //ch.beltList = ch.beltList;
            #endregion

            #region Adjust Stats Based on Class and Ability Scores
            //switch (ch.constitution)
            //{
            //    case 16:
            //        ch.hitsmax += 1;
            //        break;
            //    case 17:
            //        ch.hitsmax += 2;
            //        break;
            //    case 18:
            //        ch.hitsmax += 3;
            //        break;
            //    default:
            //        break;
            //}

            //switch (ch.dexterity)
            //{
            //    case 16:
            //        ch.stamina += 1;
            //        break;
            //    case 17:
            //        ch.stamina += 2;
            //        break;
            //    case 18:
            //        ch.stamina += 3;
            //        break;
            //    default:
            //        break;
            //}

            //switch (ch.classType) // add to manaMax if ability scores meet requirements
            //{
            //    case Character.ClassType.Thaumaturge:
            //        switch (ch.wisdom)
            //        {
            //            case 16:
            //                ch.manaMax += 1;
            //                break;
            //            case 17:
            //                ch.manaMax += 2;
            //                break;
            //            case 18:
            //                ch.manaMax += 3;
            //                break;
            //            default:
            //                break;
            //        }
            //        break;
            //    case Character.ClassType.Wizard:
            //    case Character.ClassType.Thief:
            //        switch (ch.intelligence)
            //        {
            //            case 16:
            //                ch.manaMax += 1;
            //                break;
            //            case 17:
            //                ch.manaMax += 2;
            //                break;
            //            case 18:
            //                ch.manaMax += 3;
            //                break;
            //            default:
            //                break;
            //        }
            //        break;
            //    default:
            //        break;
            //}
            #endregion

            #region Set High Skills
            ch.highMace = ch.mace; // high skills
            ch.highBow = ch.bow;
            ch.highFlail = ch.flail;
            ch.highDagger = ch.dagger;
            ch.highRapier = ch.rapier;
            ch.highTwoHanded = ch.twoHanded;
            ch.highStaff = ch.staff;
            ch.highShuriken = ch.shuriken;
            ch.highSword = ch.sword;
            ch.highThreestaff = ch.threestaff;
            ch.highHalberd = ch.halberd;
            ch.highUnarmed = ch.unarmed;
            ch.highThievery = ch.thievery;
            ch.highMagic = ch.magic; 
            #endregion            

            #region 1 Gold Coin
            Item coin = Item.CopyItemFromDictionary(Item.ID_COINS); // all new characters get a single shiny coin
            coin.coinValue = 1;
            ch.SackItem(coin); 
            #endregion

            #region Set Starting Location
            switch (ch.BaseProfession) // starting location in the game
            {
                case Character.ClassType.Thief:
                    ch.LandID = 0;
                    ch.MapID = 0;
                    ch.X = 51;
                    ch.Y = 26;
                    ch.Z = 0;
                    ch.CurrentCell = Cell.GetCell(0, 0, 0, 51, 26, 0);
                    break;
                default:
                    ch.LandID = 0;
                    ch.MapID = 0;
                    ch.X = 41;
                    ch.Y = 33;
                    ch.Z = 0;
                    ch.CurrentCell = Cell.GetCell(0, 0, 0, 41, 33, 0);
                    break;
            } 
            #endregion

            ch.Experience = 1600; // set character experience and level
			ch.Level = 3;

            ch.Hits = ch.HitsMax; // set stats to max
            ch.Stamina = ch.StaminaMax;
            ch.Mana = ch.ManaMax;

            ch.dirPointer = "^";

            PC pc = (PC)ch;
            pc.Save(); // save us

            //ch = (Character)newpc; // cast the newpc back into a Character
            Menu.PrintMainMenu(ch); // back to the menu
			ch.PCState = Globals.ePlayerState.MENU;
		}

        private static int DND_rollStat(int dice)
        {
            int statRoll1 = 0;
            int statRoll2 = 0;

            switch (dice)
            {
                case 3:
                    statRoll1 = Rules.RollD(3, 6);
                    statRoll2 = Rules.RollD(3, 6);
                    break;
                case 4:
                    for (int a = 0; a <= 1; a++)
                    {
                        int[] rolls = new int[4];
                        for (int b = 0; b < rolls.Length; b++)
                        {
                            rolls[b] = Rules.RollD(1, 6);
                        }
                        Array.Sort(rolls);
                        if (a == 0)
                        {
                            statRoll1 = rolls[0] + rolls[1] + rolls[2];
                        }
                        else
                        {
                            statRoll2 = rolls[0] + rolls[1] + rolls[2];
                        }
                    }
                    break;
            }

            if (statRoll1 > statRoll2)
            {
                return statRoll1;
            }
            else
            {
                return statRoll2;
            }	
        }

        private static int RollStat()
        {
            int strRoll1;
            int strRoll2;

            strRoll1 = Rules.dice.Next(3, 21);
            strRoll2 = Rules.dice.Next(3, 21);

            return Math.Max(strRoll1, strRoll2);
        }

        private static int AdjustMinMaxStat(int stat)
        {
            if (stat < 3) stat = 3;
            else if (stat > 18) stat = 18;
            return stat;
        }

        private static int GetRacialBonus(Character ch, string stat)
        {
            switch (ch.race)
            {
                case "Illyria":
                    if (stat == "stamina" || stat == "wisdom" || stat == "constitution")
                        return 1;
                    else if (stat == "intelligence")
                        return -1;
                    return 0;
                case "Mu":
                    if (stat == "strength")
                        return 2;
                    else if (stat == "constitution")
                        return 1;
                    else if (stat == "intelligence" || stat == "wisdom")
                        return -1;
                    return 0;
                case "Lemuria":
                    if (stat == "dexterity" || stat == "charisma")
                        return 1;
                    else if (stat == "wisdom" || stat == "constitution")
                        return -1;
                    return 0;
                case "Leng":
                    if (stat == "dexterity")
                        return 2;
                    else if (stat == "constitution" || stat == "stamina")
                        return 1;
                    else if (stat == "intelligence")
                        return -1;
                    else if (stat == "charisma")
                        return -2;
                    return 0;
                case "Draznia":
                    if (stat == "dexterity")
                        return 1;
                    else if (stat == "intelligence")
                        return 2;
                    else if (stat == "constitution")
                        return -2;
                    return 0;
                case "Hovath":
                    if (stat == "wisdom")
                        return 2;
                    else if (stat == "intelligence")
                        return 1;
                    else if (stat == "charisma")
                        return -1;
                    return 0;
                case "Mnar":
                    if (stat == "strength" || stat == "stamina")
                        return 2;
                    else if (stat == "intelligence")
                        return -1;
                    else if (stat == "charisma")
                        return 1;
                    return 0;
                case "Barbarian":
                    if (stat == "strength" || stat == "constitution")
                        return 2;
                    else if (stat == "wisdom" || stat == "intelligence")
                        return -2;
                    else if (stat == "dexterity")
                        return -1;
                    return 0;
                default:
                    return 0;
            }
        }
    }
}
