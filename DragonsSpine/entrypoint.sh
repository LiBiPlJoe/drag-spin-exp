#!/bin/bash

if [ -z "$SQLSERVR_SA_PASSWORD" ]; then
  echo "The SQLSERVR_SA_PASSWORD environment variable must be set to the password for the 'sa'"
  echo " user on the SQL server."
  exit 1
fi

if [ -z "$SQLSERVR_DB_NAME" ]; then
  echo "The SQLSERVR_DB_NAME environment variable must be set to name of the database on the SQL"
  echo " server to use for this instance of the game server."
  exit 1
fi

set -o pipefail
if [ "$DS_DEBUG" = "true" ] ; then
  echo "Starting DEBUG game server..."
  sed -i s/TempBadPassw0rd/$SQLSERVR_SA_PASSWORD/ bin/Debug/DragSpinExp.exe.config
  sed -i s/TempDatabaseName/$SQLSERVR_DB_NAME/ bin/Debug/DragSpinExp.exe.config
  mono --debug bin/Debug/DragSpinExp.exe | tee bin/Debug/DragSpinExp.out
else
  echo "Starting RELEASE game server..."
  sed -i s/TempBadPassw0rd/$SQLSERVR_SA_PASSWORD/ bin/Release/DragSpinExp.exe.config
  sed -i s/TempDatabaseName/$SQLSERVR_DB_NAME/ bin/Release/DragSpinExp.exe.config
  mono bin/Release/DragSpinExp.exe | tee bin/Release/DragSpinExp.out
fi

# Unable to use coverage profile due to crashes:
#  https://github.com/mono/mono/issues/21248 "Coverage does not work, SIGABRT"
# apt install mono-profiler -y
# mono --debug --profile=coverage bin/Debug/DragSpinExp.exe | tee bin/Debug/DragSpinExp.out

tail -f /dev/null
