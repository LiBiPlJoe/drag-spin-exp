using System;
using System.Collections;
using System.Collections.Generic;

namespace DragonsSpine
{
    public class SpawnZone
    {
        static Dictionary<int, SpawnZone> spawnsDictionary = new Dictionary<int, SpawnZone>(); // key = zoneID, value = SpawnZone

        #region Private Data
        private bool m_enabled = false;
        protected readonly short m_landID = 0;
        protected readonly short m_mapID = 0;
        protected readonly int m_maxAllowed = 0;
        protected readonly int m_maxZone = 0;
        protected readonly int m_minZone = 0;
        protected readonly int m_npcID = 0;
        protected readonly string m_npcList = "";
        private short m_numInZone = 0;
        protected readonly int m_radius = 0;
        protected readonly string m_spawnMessage = "";
        private int m_spawnTimer = 0;
        protected readonly int m_zoneID = 0;
        private int m_timer = 0;
        protected readonly int m_spawnX = 0;
        protected readonly int m_spawnY = 0;
        private int m_spawnZ = 0;
        protected readonly List<string> m_spawnCellsList = new List<string>(); // list of keys used to get cells from map.cells
        protected readonly List<int> m_spawnZRangeList = new List<int>(); // list of random z coordinates
        #endregion

        #region Public Properties
        public bool IsEnabled
        {
            get { return this.m_enabled; }
            set { this.m_enabled = value; }
        }
        public int ZoneID
        {
            get { return this.m_zoneID; }
        }
        public int NPCID
        {
            get { return this.m_npcID; }
        }
        public int SpawnTimer
        {
            get { return this.m_spawnTimer; }
        }
        public int Timer
        {
            get { return this.m_timer; }
            set { this.m_timer = value; }
        }
        public int MaxAllowedInZone
        {
            get { return this.m_maxAllowed; }
        }
        public short NumberInZone
        {
            get { return this.m_numInZone; }
            set { this.m_numInZone = value; }
        }
        public string SpawnMessage
        {
            get { return this.m_spawnMessage; }
        }
        public string NPCList
        {
            get { return this.m_npcList; }
        }
        public int MinimumInZone
        {
            get { return this.m_minZone; }
        }
        public int MaximumInZone
        {
            get { return this.m_maxZone; }
        }
        public short LandID
        {
            get { return this.m_landID; }
        }
        public short MapID
        {
            get { return this.m_mapID; }
        }
        public int X
        {
            get { return this.m_spawnX; }
        }
        public int Y
        {
            get { return this.m_spawnY; }
        }
        public int Z
        {
            get { return m_spawnZ; }
            set { m_spawnZ = value; }
        }
        public int Radius
        {
            get { return this.m_radius; }
        }
        public List<string> SpawnCells
        {
            get { return this.m_spawnCellsList; }
        }
        public List<int> SpawnZRange
        {
            get { return this.m_spawnZRangeList; }
        }
        #endregion

        #region Constructors (2)
        public SpawnZone()
        {

        }

        public SpawnZone(System.Data.DataRow dr)
        {
            try
            {
                this.m_enabled = Convert.ToBoolean(dr["enabled"]);
                this.m_zoneID = Convert.ToInt32(dr["zoneID"]);
                this.m_npcID = Convert.ToInt16(dr["npcID"]);
                this.m_spawnTimer = Convert.ToInt32(dr["spawnTimer"]);
                this.m_spawnMessage = dr["spawnMessage"].ToString();
                this.m_npcList = dr["npcList"].ToString();
                this.m_minZone = Convert.ToInt32(dr["minZone"]);
                this.m_maxZone = Convert.ToInt32(dr["maxZone"]);
                if (this.m_minZone > 0 && this.m_maxZone > 0)
                {
                    this.m_maxAllowed = Rules.dice.Next(m_minZone, m_maxZone + 1);
                }
                else
                {
                    this.m_maxAllowed = Convert.ToInt32(dr["maxAllowedInZone"]);
                }
                this.m_landID = Convert.ToInt16(dr["spawnLand"]);
                this.m_mapID = Convert.ToInt16(dr["spawnMap"]);
                this.m_spawnX = Convert.ToInt16(dr["spawnX"]);
                this.m_spawnY = Convert.ToInt16(dr["spawnY"]);
                this.m_spawnZ = Convert.ToInt32(dr["spawnZ"]);
                this.m_radius = Convert.ToInt16(dr["spawnRadius"]);
                if (this.m_radius > 0)
                {
                    this.EstablishSpawnRadius();
                }
                if (dr["spawnZRange"].ToString() != "")
                {
                    string[] zRange = dr["spawnZRange"].ToString().Trim().Split(" ".ToCharArray());
                    for(int a = 0; a < zRange.Length; a++)
                    {
                        this.m_spawnZRangeList.Add(Convert.ToInt32(zRange[a]));
                    }
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }
        #endregion

        #region Private Methods
        void EstablishSpawnRadius()
        {
            this.m_spawnCellsList.Clear();

            string key = "";
            int finalX = 0;
            int finalY = 0;

            for (int x = -this.Radius; x <= this.Radius; x++)
            {
                for (int y = -this.Radius; y <= this.Radius; y++)
                {
                    finalX = this.X + x;
                    finalY = this.Y + y;
                    key = finalX.ToString() + "," + finalY.ToString() + "," + this.Z;
                    if (World.GetFacetByIndex(0).GetLandByID(this.LandID).GetMapByID(this.MapID).cells.ContainsKey(key))
                    {
                        if (!World.GetFacetByIndex(0).GetLandByID(this.LandID).GetMapByID(this.MapID).cells[key].IsOutOfBounds)
                        {
                            this.m_spawnCellsList.Add(key);
                        }
                    }
                }
            }
        } 
        #endregion

        #region Public Methods
        public void EstablishSpawnRadius(int zCord)
        {
            this.m_spawnCellsList.Clear();

            Map map = World.GetFacetByIndex(0).GetLandByID(this.LandID).GetMapByID(this.MapID);

            string key = "";

            for (int x = map.XCordMin[zCord]; x <= map.XCordMax[zCord]; x++)
            {
                for (int y = map.YCordMin[zCord]; y <= map.YCordMax[zCord]; y++)
                {
                    key = x.ToString() + "," + y.ToString() + "," + zCord;
                    if (map.cells.ContainsKey(key))
                    {
                        this.m_spawnCellsList.Add(key);
                    }
                }
            }
        } 
        #endregion

        #region Static Methods and Properties
        public static Dictionary<int, SpawnZone> Spawns
        {
            get
            {
                return SpawnZone.spawnsDictionary;
            }
        }

        public static void Add(SpawnZone szl)
        {
            SpawnZone.spawnsDictionary.Add(szl.ZoneID, szl);
        }

        public static void AddSpawnZonesToList(ArrayList zones)
        {
            foreach (SpawnZone zone in zones)
            {
                SpawnZone.spawnsDictionary.Add(zone.ZoneID, zone);
            }
        }
        #endregion

        #region Static Functions
        public static bool LoadSpawnZones()
        {
            return DAL.DBWorld.LoadSpawnZones();
        }
        #endregion
    }
}
