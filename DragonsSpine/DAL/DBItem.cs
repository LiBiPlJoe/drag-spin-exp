using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


namespace DragonsSpine.DAL
{
	/// <summary>
	/// Summary description for DBItem.
	/// </summary>
	public class DBItem
	{
		public DBItem()
		{
			//
			// TODO: Add constructor logic here
			//
		}


		internal static int InsertItem(
			int itemID,
			int itemType,
			int baseType,
			string name,
			string shortDesc,
			string longDesc,
			int wearLocation,
			int weight,
			int cValue,
			int size,
			int blockRank,
			int effectType,
			int effectAmount,
			int effectDuration,
			string special,
			int minDamage,
			int maxDamage,
			string skillType,
			int currPage,
			int vRandLow,
			int vRandHigh,
			string KeyName,
			bool IsRecall,
			int attackRank,
			int attackRankCode,
			int ArmorClass,
            Book.BookType bookType,
            int maxPages,
			string pages
			)

		{


			try
			{
				SqlStoredProcedure sp = new SqlStoredProcedure("prApp_CatalogItem_Insert", DataAccess.GetSQLConnection());
				sp.AddParameter("@itemID",				SqlDbType.Int,		4,	ParameterDirection.Input, itemID);
				sp.AddParameter("@itemType",		SqlDbType.Int,		4,	ParameterDirection.Input, itemType);
				sp.AddParameter("@baseType",		SqlDbType.Int,		4,	ParameterDirection.Input, baseType);
				sp.AddParameter("@name",			SqlDbType.VarChar,	255,ParameterDirection.Input, name);
				sp.AddParameter("@shortDesc",			SqlDbType.VarChar,	255,ParameterDirection.Input, shortDesc);
				sp.AddParameter("@longDesc",			SqlDbType.VarChar,	255,ParameterDirection.Input, longDesc);
				sp.AddParameter("@wearLocation",	SqlDbType.Int,		4,	ParameterDirection.Input, wearLocation);
				sp.AddParameter("@weight",				SqlDbType.Int,		4,	ParameterDirection.Input, weight);
				sp.AddParameter("@coinValue",			SqlDbType.Int,		4,	ParameterDirection.Input, cValue);
				sp.AddParameter("@size",			SqlDbType.Int,		4,	ParameterDirection.Input, size);
				sp.AddParameter("@blockRank",		SqlDbType.Int,		4,	ParameterDirection.Input, blockRank);
				sp.AddParameter("@effectType",		SqlDbType.NVarChar,255,	ParameterDirection.Input, effectType);
				sp.AddParameter("@effectAmount",		SqlDbType.NVarChar,		255,	ParameterDirection.Input, effectAmount);
				sp.AddParameter("@effectDuration",		SqlDbType.Int,		4,	ParameterDirection.Input, effectDuration);
				sp.AddParameter("@special",				SqlDbType.VarChar,	255,ParameterDirection.Input, special);
				sp.AddParameter("@minDamage",			SqlDbType.Int,		4,	ParameterDirection.Input, minDamage);
				sp.AddParameter("@maxDamage",			SqlDbType.Int,		4,	ParameterDirection.Input, maxDamage);
				sp.AddParameter("@skillType",			SqlDbType.VarChar,	255,ParameterDirection.Input, skillType);
				
				sp.AddParameter("@vRandLow",			SqlDbType.Int,		4,	ParameterDirection.Input, vRandLow);
				sp.AddParameter("@vRandHigh",			SqlDbType.Int,		4,	ParameterDirection.Input, vRandHigh);
				sp.AddParameter("@key",				SqlDbType.VarChar,	50,	ParameterDirection.Input, KeyName);
				sp.AddParameter("@recall",			SqlDbType.Int,		4,	ParameterDirection.Input, IsRecall);
				sp.AddParameter("@attackRankCode",		SqlDbType.Int,		4,	ParameterDirection.Input, attackRankCode);
				sp.AddParameter("@armorClass",			SqlDbType.Int,		4,	ParameterDirection.Input, ArmorClass);
                sp.AddParameter("@bookType", SqlDbType.Int, 4, ParameterDirection.Input, (int)bookType);
                sp.AddParameter("@maxPages", SqlDbType.Int, 4, ParameterDirection.Input, maxPages);
				sp.AddParameter("@pages",				SqlDbType.Text,		4000,ParameterDirection.Input, pages);
				return sp.ExecuteNonQuery();
			}
			catch(Exception e)
			{
                Utils.LogException(e);
				return -1;
			}
		}

		internal static bool LoadItems()
		{
			try
			{
				SqlStoredProcedure sp = new SqlStoredProcedure("prApp_CatalogItem_Select_All", DataAccess.GetSQLConnection());
				DataTable dtCatalogItem = sp.ExecuteDataTable();
				foreach(DataRow dr in dtCatalogItem.Rows)
				{
                    if(!Item.ItemDictionary.ContainsKey(Convert.ToInt32(dr["itemID"])))
                    {
                    Item.ItemDictionary.Add(Convert.ToInt32(dr["itemID"]), new Item(dr));
                    }
                    else
                    {
                        Utils.Log("Failed to add Item ID " + dr["itemID"].ToString() + " to Item Dictionary, key exists.", Utils.LogType.SystemFailure);
                    }
				}
				return true;
			}
			catch(Exception e)
			{
                Utils.LogException(e);
				return false;
			}
		}
	}
}
