using System;
using System.Data;
using System.Data.SqlClient;

namespace DragonsSpine.DAL
{
	/// <summary>
	/// Summary description for DataAccess.
	/// </summary>
	public class DataAccess
	{
		public DataAccess()
		{
			//
			// TODO: Add constructor logic here
			//
		}



		public static SqlConnection GetSQLConnection()
		{

			try
			{
				SqlConnection sCon = new SqlConnection(DragonsSpineMain.SQL_CONNECTION);
                return sCon;
			}
			catch(Exception e)
			{
				string error =  e.Message;
				return new SqlConnection();
			}
		}

		public static SqlConnection GetSQLConnection(string sConnStr)
		{
			SqlConnection sCon = new SqlConnection(sConnStr);
			return sCon;
		}


	}
}
