using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;

namespace DragonsSpine.DAL
{
    /// <summary>
    /// Summary description for DBNPC.
    /// </summary>
    public class DBNPC
    {
        public DBNPC()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        internal static bool LoadNPCDictionary()
        {
            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_NPC_Select_All", DataAccess.GetSQLConnection());
                DataTable dtNPCs = sp.ExecuteDataTable();
                foreach (DataRow dr in dtNPCs.Rows)
                {
                    int npcid = Convert.ToInt32(dr["npcID"]);
                    NPC.Add(npcid, dr);
                }
                Utils.Log("Loaded NPCs (" + NPC.NPCDictionary.Count.ToString() + ")...", Utils.LogType.SystemGo);
                return true;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return false;
            }
        }

        internal static NPC GetNPCByID(int NpcID)
        {
            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_NPC_Select", DataAccess.GetSQLConnection());
                sp.AddParameter("@npcID", SqlDbType.Int, 4, ParameterDirection.Input, NpcID);
                DataTable dtNPCs = sp.ExecuteDataTable();
                return new NPC(dtNPCs.Rows[0]);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return null;
            }
        }

        internal static ArrayList GetRandomNPCs(int diff)
        {
            ArrayList mylist = new ArrayList();
            int num = 6;
            SqlStoredProcedure sp = new SqlStoredProcedure("prApp_NPC_Select_Random", DataAccess.GetSQLConnection());
            ArrayList npclist = new ArrayList();
            sp.AddParameter("@difficulty", SqlDbType.Int, 8, ParameterDirection.Input, diff);
            DataTable dtNPCs = sp.ExecuteDataTable();
            foreach (DataRow dr in dtNPCs.Rows)
            {
                npclist.Add(new NPC(dr));
            }
            for (int x = 0; x < num; x++)
            {
                mylist.Add(npclist[Rules.dice.Next(npclist.Count)]);
            }
            return mylist;
        }

        //internal static Quests convertDataToQuest(DataRow dritem)
        //{
        //    Quests nq = new Quests();
        //    nq.failQuestString = Convert.ToString(dritem["failQuestString"]);
        //    nq.finishQuestString = Convert.ToString(dritem["finishQuestString"]);
        //    nq.giveQuestString = Convert.ToString(dritem["giveQuestString"]);
        //    nq.QuestID = Convert.ToInt32(dritem["QuestID"]);
        //    nq.QuestItems = Convert.ToString(dritem["QuestItems"]);
        //    nq.QuestItemValue = Convert.ToInt32(dritem["QuestItemValue"]);
        //    nq.QuestName = Convert.ToString(dritem["QuestName"]);
        //    nq.QuestRewardItem = Convert.ToString(dritem["QuestRewardItem"]);
        //    nq.QuestRewardType = Convert.ToInt32(dritem["QuestRewardType"]);
        //    nq.QuestRewardValue = Convert.ToInt32(dritem["QuestRewardValue"]);
        //    nq.startQuestString = Convert.ToString(dritem["startQuestString"]);
        //    return nq;
        //}

        internal static List<NPC> npcsToUpdate = new List<NPC>();
        internal static int roundToUpdate = 0;
        internal static object lockObjectLiveNpcUpdate = new object();

        internal static bool ClearLiveNpcData() {
            int result = 0;
            try {
                lock (lockObjectLiveNpcUpdate) {
                    string sptouse = "prApp_LiveNPC_Clear";
                    using (SqlConnection tempConnection = DataAccess.GetSQLConnection())
                    using (SqlStoredProcedure sp = new SqlStoredProcedure(sptouse, tempConnection)) {
                        result = sp.ExecuteNonQuery();
                    }
                    DAL.DBNPC.npcsToUpdate.Clear();
                }
            } catch (Exception e) {
                Utils.LogException(e);
                return false;
            }
            return (result >= 0);
        }

        internal static int SaveLiveNpc(NPC npc) {
            int result = 0;
            lock (lockObjectLiveNpcUpdate) {
                if (DragonsSpineMain.GameRound != DAL.DBNPC.roundToUpdate) {
                    try {
                        String sptouse = "prApp_LiveNPC_Update";
                        using (SqlConnection tempConnection = DataAccess.GetSQLConnection()) {
                            Utils.Log("Round " + DragonsSpineMain.GameRound + ": Inserting " + DAL.DBNPC.npcsToUpdate.Count + " live NPCs.", Utils.LogType.Unknown);
                            foreach (NPC currNpc in DAL.DBNPC.npcsToUpdate) {
                                using (SqlStoredProcedure sp = new SqlStoredProcedure(sptouse, tempConnection)) {
                                    int NPCIndex = Character.NPCList.IndexOf(currNpc);
                                    sp.AddParameter("@uniqueId", SqlDbType.Int, 4, ParameterDirection.Input, currNpc.worldNpcID);
                                    sp.AddParameter("@name", SqlDbType.NVarChar, 255, ParameterDirection.Input, currNpc.Name);
                                    sp.AddParameter("@facet", SqlDbType.Int, 4, ParameterDirection.Input, currNpc.FacetID);
                                    sp.AddParameter("@map", SqlDbType.Int, 4, ParameterDirection.Input, currNpc.MapID);
                                    sp.AddParameter("@xCord", SqlDbType.Int, 4, ParameterDirection.Input, currNpc.X);
                                    sp.AddParameter("@yCord", SqlDbType.Int, 4, ParameterDirection.Input, currNpc.Y);
                                    sp.AddParameter("@zCord", SqlDbType.Int, 4, ParameterDirection.Input, currNpc.Z);
                                    sp.AddParameter("@level", SqlDbType.Int, 4, ParameterDirection.Input, currNpc.Level);
                                    sp.AddParameter("@hits", SqlDbType.Int, 4, ParameterDirection.Input, currNpc.Hits);
                                    sp.AddParameter("@fullHits", SqlDbType.Int, 4, ParameterDirection.Input, currNpc.HitsFull);
                                    sp.AddParameter("@mana", SqlDbType.Int, 4, ParameterDirection.Input, currNpc.Mana);
                                    sp.AddParameter("@fullMana", SqlDbType.Int, 4, ParameterDirection.Input, currNpc.ManaFull);
                                    if (currNpc.LastCommand != null)
                                        sp.AddParameter("@lastCommand", SqlDbType.NVarChar, 255, ParameterDirection.Input, currNpc.LastCommand);
                                    else
                                        sp.AddParameter("@lastCommand", SqlDbType.NVarChar, 255, ParameterDirection.Input, "!nothing yet!");
                                    sp.AddParameter("@lastActiveRound", SqlDbType.Int, 4, ParameterDirection.Input, DAL.DBNPC.roundToUpdate);
                                    sp.AddParameter("@isDead", SqlDbType.Bit, 1, ParameterDirection.Input, currNpc.IsDead);
                                    if (currNpc.mostHated != null) {
                                        sp.AddParameter("@mostHatedId", SqlDbType.Int, 4, ParameterDirection.Input, currNpc.mostHated.worldNpcID);
                                        sp.AddParameter("@hateCenterX", SqlDbType.Int, 4, ParameterDirection.Input, currNpc.hateCenterX);
                                        sp.AddParameter("@hateCenterY", SqlDbType.Int, 4, ParameterDirection.Input, currNpc.hateCenterY);
                                    }
                                    /*
                                                                    @loveCenterX smallint=NULL,
                                                                    @loveCenterY smallint=NULL,
                                                                    @fearLove int=NULL,
                                                                    @NpcTypeCode 		int=NULL,
                                                                    @BaseTypeCode 		int=NULL,
                                                                    @CharacterClassCode 	int=NULL,
                                                                    @AlignCode 		int=NULL,
                                                                    @numAttackers int=NULL,
                                                                    @lairLocationX smallint=NULL,
                                                                    @lairLocationY smallint=NULL,
                                                                    @lairLocationZ int=NULL,
                                                                    @priorityCode int=NULL,
                                                                    @effects nvarchar(255)=NULL
                                */
                                    result = sp.ExecuteNonQuery();
                                }
                            }
                            DAL.DBNPC.npcsToUpdate.Clear();
                            DAL.DBNPC.roundToUpdate = DragonsSpineMain.GameRound;
                        }
                    } catch (Exception e) {
                        Utils.LogException(e);
                        result = -1;
                    }
                }
                npcsToUpdate.Add(npc);
                return result;
            }
        }
    }
}
