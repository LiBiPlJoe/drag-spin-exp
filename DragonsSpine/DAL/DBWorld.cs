using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;

namespace DragonsSpine.DAL
{
	/// <summary>
	/// Summary description for DBWorld.
	/// </summary>
    /// 
    public class DBWorld
    {
        internal static bool LoadFacets()
        {
            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Facet_Select_All", DataAccess.GetSQLConnection());
                DataTable dtFacets = sp.ExecuteDataTable();
                foreach (DataRow dr in dtFacets.Rows)
                {
                    World.Add(new Facet(dr));
                    Utils.Log("Added Facet: " + dr["Name"].ToString(),Utils.LogType.SystemGo);
                }
                
                return true;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return false;
            }
        }

        internal static bool LoadLands(Facet facet) // return true if lands were loaded correctly
        {
            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Land_Select", DataAccess.GetSQLConnection());
                DataTable dtLands = sp.ExecuteDataTable();
                foreach (DataRow dr in dtLands.Rows)
                {
                    facet.Add(new Land(facet.FacetID, dr));
                    Utils.Log("Added Land: " + dr["Name"].ToString()+ " to Facet: "+facet.Name, Utils.LogType.SystemGo);
                }
                return true;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return false;
            }
        }

        internal static bool LoadMaps(Land land)
        {
            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Map_Select", DataAccess.GetSQLConnection());
                sp.AddParameter("@landID", SqlDbType.SmallInt, 2, ParameterDirection.Input, land.LandID);
                DataTable dtMaps = sp.ExecuteDataTable();
                foreach (DataRow dr in dtMaps.Rows)
                {
                    land.Add(new Map(land.FacetID, dr));
                    Utils.Log("Loaded Map: " + dr["name"].ToString(), Utils.LogType.SystemGo);
                }
                return true;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return false;
            }
        }        

        internal static bool LoadQuests()
        {
            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Quest_Select", DataAccess.GetSQLConnection());
                DataTable dtQuest = sp.ExecuteDataTable();
                foreach (DataRow dr in dtQuest.Rows)
                {
                    Quest.Add(new Quest(dr));
                    Utils.Log("Loaded Quest: " + dr["name"].ToString(), Utils.LogType.SystemGo);
                }
                return true;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return false;
            }
        }
        
        #region Cell Info
        internal static List<Cell> GetCellList(string name)
        {
            
            try
            {
                List<Cell> cellsList = new List<Cell>();

                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Cell_Select_By_Map", DataAccess.GetSQLConnection());
                sp.AddParameter("@mapName", SqlDbType.NVarChar, 50, ParameterDirection.Input, name);
                DataTable dtCellsInfo = sp.ExecuteDataTable();
                if (dtCellsInfo != null)
                {
                    foreach (DataRow dr in dtCellsInfo.Rows)
                    {
                        cellsList.Add(new Cell(dr));
                    }
                    Utils.Log("Loaded Cell Info for "+name+"...", Utils.LogType.SystemGo);
                    return cellsList;
                }
                else
                {
                    Utils.Log("DBWorld.GetCellsList returned null for map " + name, Utils.LogType.SystemFailure);
                    return null;
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return null;
            }
        }

        internal static Dictionary<Cell,bool> cellsToUpdate = new Dictionary<Cell,bool>();
        internal static int roundToUpdate = 0;
        internal static object lockObjectLiveCellUpdate = new object();

        internal static bool ClearLiveCellData() {
            int result = 0;
            try {
                lock (lockObjectLiveCellUpdate) {
                    string sptouse = "prApp_LiveCell_Clear";
                    using (SqlConnection tempConnection = DataAccess.GetSQLConnection())
                    using (SqlStoredProcedure sp = new SqlStoredProcedure(sptouse, tempConnection)) {
                        result = sp.ExecuteNonQuery();
                    }
                    DAL.DBNPC.npcsToUpdate.Clear();
                }
            } catch (Exception e) {
                Utils.LogException(e);
                return false;
            }
            return (result >= 0);
        }

        internal static int SaveLiveCell(Cell cell) {
            int result = 0;
            lock (lockObjectLiveCellUpdate) {
                if (DragonsSpineMain.GameRound != DAL.DBWorld.roundToUpdate) {
                    try {
                        String sptouse = "prApp_LiveCell_Update";
                        using (SqlConnection tempConnection = DataAccess.GetSQLConnection()) {
                            Utils.Log("Round " + DragonsSpineMain.GameRound + ": Inserting " + DAL.DBWorld.cellsToUpdate.Count + " live cells.", Utils.LogType.Unknown);
                            tempConnection.Open();
                            SqlTransaction trans = tempConnection.BeginTransaction();
                            foreach (Cell currCell in DAL.DBWorld.cellsToUpdate.Keys) {
                                using (SqlStoredProcedure sp = new SqlStoredProcedure(sptouse, tempConnection, trans)) {
                                    sp.AddParameter("@lastRoundChanged", SqlDbType.Int, 4, ParameterDirection.Input, DAL.DBWorld.roundToUpdate);
                                    sp.AddParameter("@facet", SqlDbType.Int, 4, ParameterDirection.Input, currCell.FacetID);
                                    sp.AddParameter("@map", SqlDbType.Int, 4, ParameterDirection.Input, currCell.MapID);
                                    sp.AddParameter("@xCord", SqlDbType.Int, 4, ParameterDirection.Input, currCell.X);
                                    sp.AddParameter("@yCord", SqlDbType.Int, 4, ParameterDirection.Input, currCell.Y);
                                    sp.AddParameter("@zCord", SqlDbType.Int, 4, ParameterDirection.Input, currCell.Z);
                                    sp.AddParameter("@cellGraphic", SqlDbType.Char, 2, ParameterDirection.Input, currCell.CellGraphic);
                                    sp.AddParameter("@displayGraphic", SqlDbType.Char, 2, ParameterDirection.Input, currCell.DisplayGraphic);
                                    sp.ExecuteNonQuery();
                                }
                            }
                            trans.Commit();
                            DAL.DBWorld.cellsToUpdate.Clear();
                            DAL.DBWorld.roundToUpdate = DragonsSpineMain.GameRound;
                        }
                    } catch (Exception e) {
                        Utils.LogException(e);
                        result = -1;
                    }
                }
                if (cell != null) {
                    cellsToUpdate[cell] = true;
                }
                return result;
            }
        }


        #endregion

        #region Spells
        internal static ArrayList LoadSpells()
        {
            ArrayList SpellsList = new ArrayList();

            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Spells_Select", DataAccess.GetSQLConnection());
                DataTable dtSpellList = sp.ExecuteDataTable();
                foreach (DataRow dr in dtSpellList.Rows)
                {
                    SpellsList.Add(new Spell(dr));
                }
                Utils.Log("Loaded Spells (" + SpellsList.Count.ToString() + ") ... ", Utils.LogType.SystemGo);
                return SpellsList;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return null;
            }
        }
        #endregion

        #region CharGen
        internal static void SetupNewCharacter(Character ch)
        {
            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_CharGen_Select", DataAccess.GetSQLConnection());
                sp.AddParameter("@race", SqlDbType.VarChar, 20, ParameterDirection.Input, ch.race);
                sp.AddParameter("@classType", SqlDbType.Int, 4, ParameterDirection.Input, (int)ch.BaseProfession);
                DataTable dtSetupNewChar = sp.ExecuteDataTable();
                foreach (DataRow dr in dtSetupNewChar.Rows)
                    ConvertRowToNewCharacter(ch, dr);
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        }

        internal static void ConvertRowToNewCharacter(Character newpc, DataRow drItem)
        {
            try
            {
                Character.SetCharacterVisualKey(newpc);

                // alignment
                newpc.Alignment = (Globals.eAlignment)Convert.ToInt32(drItem["alignment"]);

                //starting skills
                newpc.bow = Convert.ToInt32(drItem["bow"]);
                newpc.dagger = Convert.ToInt32(drItem["dagger"]);
                newpc.flail = Convert.ToInt32(drItem["flail"]);
                newpc.halberd = Convert.ToInt32(drItem["halberd"]);
                newpc.mace = Convert.ToInt32(drItem["mace"]);
                newpc.rapier = Convert.ToInt32(drItem["rapier"]);
                newpc.shuriken = Convert.ToInt32(drItem["shuriken"]);
                newpc.staff = Convert.ToInt32(drItem["staff"]);
                newpc.sword = Convert.ToInt32(drItem["sword"]);
                newpc.threestaff = Convert.ToInt32(drItem["threestaff"]);
                newpc.thievery = Convert.ToInt32(drItem["thievery"]);
                newpc.magic = Convert.ToInt32(drItem["magic"]);
                newpc.unarmed = Convert.ToInt32(drItem["unarmed"]);
                newpc.bash = Convert.ToInt32(drItem["bash"]);

                //starting equipment
                if (Convert.ToInt32(drItem["rightHand"]) != 0)
                {
                    newpc.RightHand = Item.CopyItemFromDictionary(Convert.ToInt32(drItem["rightHand"]));
                }
                if (Convert.ToInt32(drItem["leftHand"]) != 0)
                {
                    newpc.LeftHand = Item.CopyItemFromDictionary(Convert.ToInt32(drItem["leftHand"]));
                }
                String[] startingGear = drItem["wearing"].ToString().Split(" ".ToCharArray());
                foreach (string wItem in startingGear)
                {
                    newpc.wearing.Add(Item.CopyItemFromDictionary(Convert.ToInt32(wItem)));
                }
                String[] startingBelt = drItem["belt"].ToString().Split(" ".ToCharArray());
                foreach (string bItem in startingBelt)
                {
                    newpc.BeltItem(Item.CopyItemFromDictionary(Convert.ToInt32(bItem)));
                }

                //starting spells
                if (drItem["spells"].ToString() != "0")
                {
                    String[] spellArray = drItem["spells"].ToString().Split(" ".ToCharArray());

                    string spellChant = "";

                    foreach (string spellID in spellArray)
                    {
                        spellChant = Spell.GenerateMagicWords();
                        while (newpc.spellList.Exists(spellChant))
                        {
                            spellChant = Spell.GenerateMagicWords();
                        }
                        newpc.spellList.Add(Convert.ToInt32(spellID), spellChant);
                    }
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
            }
        } 
        #endregion

        #region Stores
        internal static int clearStoreItems()
        {
            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Stores_Clear", DataAccess.GetSQLConnection());
                return sp.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return -1;
            }
        }

        internal static int restockStoreItems()
        {
            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Stores_Restock", DataAccess.GetSQLConnection());
                return sp.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return -1;
            }
        }

        internal static int updateStoreItem(int stockID, int stocked) // set a new stocked amount for this stockID
        {
            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Stores_Update", DataAccess.GetSQLConnection());
                sp.AddParameter("@stockID", SqlDbType.Int, 4, ParameterDirection.Input, stockID);
                sp.AddParameter("@stocked", SqlDbType.Int, 4, ParameterDirection.Input, stocked);
                DataTable dtStockItem = sp.ExecuteDataTable();
                if (dtStockItem == null)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return -1;
            }
        }

        internal static int deleteStoreItem(int stockID) // delete this stockID from a store
        {
            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Stores_Delete", DataAccess.GetSQLConnection());
                sp.AddParameter("@stockID", SqlDbType.Int, 4, ParameterDirection.Input, stockID);
                return sp.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return -1;
            }
        }

        internal static int insertStoreItem(int NPCIdent, StoreItem storeItem)
        {
            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Stores_Insert", DataAccess.GetSQLConnection());
                sp.AddParameter("@NPCIdent", SqlDbType.Int, 4, ParameterDirection.Input, NPCIdent);
                sp.AddParameter("@original", SqlDbType.Bit, 1, ParameterDirection.Input, storeItem.original);
                sp.AddParameter("@itemID", SqlDbType.Int, 4, ParameterDirection.Input, storeItem.itemID);
                sp.AddParameter("@sellPrice", SqlDbType.Int, 4, ParameterDirection.Input, storeItem.sellPrice);
                sp.AddParameter("@stocked", SqlDbType.SmallInt, 2, ParameterDirection.Input, storeItem.stocked);

                sp.AddParameter("@charges", SqlDbType.SmallInt, 2, ParameterDirection.Input, storeItem.charges);
                sp.AddParameter("@figExp", SqlDbType.BigInt, 8, ParameterDirection.Input, storeItem.figExp);

                sp.AddParameter("@seller", SqlDbType.VarChar, 20, ParameterDirection.Input, storeItem.seller);
                return sp.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return -1;
            }
        }

        internal static List<StoreItem> LoadStoreItems(int NPCIdent)
        {
            List<StoreItem> store = new List<StoreItem>();

            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Stores_Select", DataAccess.GetSQLConnection());
                sp.AddParameter("@NPCIdent", SqlDbType.Int, 4, ParameterDirection.Input, NPCIdent);
                DataTable dtScoresItem = sp.ExecuteDataTable();
                foreach (DataRow dr in dtScoresItem.Rows)
                {
                    store.Add(new StoreItem(dr));
                }

                return store;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return null;
            }
        }
        #endregion

        #region Scores

        internal static ArrayList p_getScores()
        {
            ArrayList scoresList = new ArrayList();
            PC score = new PC();
            SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Scores_Select", DataAccess.GetSQLConnection());
            sp.AddParameter("@devRequest", SqlDbType.Bit, 1, ParameterDirection.Input, true);
            sp.AddParameter("@classType", SqlDbType.Int, 4, ParameterDirection.Input, 0);
            DataTable dtScores = sp.ExecuteDataTable();

            int max = Protocol.MAX_SCORES;
            foreach (DataRow dr in dtScores.Rows)
            {
                if (max <= 0) { return scoresList; }
                score = new PC();
                score = ConvertRowToScore(score, dr, true);
                score.PlayerID = Convert.ToInt32(dr["playerID"]);
                score.IsAnonymous = (bool)PC.GetField(score.PlayerID, "anonymous", score.IsAnonymous, null);
                scoresList.Add(score);
                max--;
            }
            return scoresList;
        }

        internal static ArrayList GetScores(Character.ClassType classType, int amount, bool devRequest, string playerName, bool pvp)
        {
            ArrayList scoresList = new ArrayList();
            PC score = new PC();
            int rank = 1;
            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Scores_Select", DataAccess.GetSQLConnection());
                sp.AddParameter("@devRequest", SqlDbType.Bit, 1, ParameterDirection.Input, devRequest);
                sp.AddParameter("@classType", SqlDbType.VarChar, 50, ParameterDirection.Input, classType.ToString());
                DataTable dtScores = sp.ExecuteDataTable();

                foreach (DataRow dr in dtScores.Rows)
                {
                    if (playerName != "")
                    {
                        if (dr["name"].ToString().ToLower() == playerName.ToString().ToLower())
                        {
                            score = new PC();
                            score.PlayerID = Convert.ToInt32(dr["playerID"]);
                            score.IsAnonymous = (bool)PC.GetField(score.PlayerID, "anonymous", score.IsAnonymous, null);
                            if (score.IsAnonymous)
                            {
                                if (devRequest)
                                {
                                    score.PlayerID = rank;
                                    score = ConvertRowToScore(score, dr, devRequest);
                                    scoresList.Add(score);
                                }
                                return scoresList;
                            }
                            else
                            {
                                score.PlayerID = rank;
                                score = ConvertRowToScore(score, dr, devRequest);
                                scoresList.Add(score);
                                return scoresList;
                            }
                        }
                        if (Convert.ToInt32(dr["impLevel"]) == 0 || score.Name.ToLower() == playerName.ToLower())
                        {
                            rank++;
                        }
                    }
                    else
                    {
                        if (amount == 0) { break; }
                        score = new PC();
                        score.PlayerID = Convert.ToInt32(dr["playerID"]);
                        score.IsAnonymous = (bool)DBPlayer.getPlayerField(score.PlayerID, "anonymous", score.IsAnonymous.GetType());   
                                         // was: (bool)PC.GetField(score.PlayerID, "anonymous", score.IsAnonymous, null);
                        if (devRequest) // devs get to see a list of everyone
                        {
                            score.PlayerID = rank; // player ID is used to store the player's rank
                            score = ConvertRowToScore(score, dr, devRequest);
                            scoresList.Add(score);
                            if (!score.IsAnonymous && score.ImpLevel == Globals.eImpLevel.USER)
                            {
                                amount--;
                            }
                        }
                        else // only add the score to the list if they are not anonymous
                        {
                            score.PlayerID = rank;
                            score = ConvertRowToScore(score, dr, devRequest);
                            scoresList.Add(score);
                            amount--;
                        }
                        rank++;
                    }
                }
                return scoresList;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return null;
            }
        }

        internal static PC ConvertRowToScore(PC score, DataRow dr, bool devRequest)
        {
            try
            {
                if (dr != null)
                {
                    if (!score.IsAnonymous || devRequest)
                    {
                        score.Name = dr["name"].ToString();
                        score.classFullName = dr["classFullName"].ToString();
                        score.BaseProfession = (Character.ClassType)Enum.Parse(typeof(Character.ClassType), dr["classType"].ToString(), true);
                        score.Level = Convert.ToInt16(dr["level"]);
                        score.Experience = Convert.ToInt64(dr["exp"]);
                        score.Kills = Convert.ToInt32(dr["numKills"]);
                        score.RoundsPlayed = Convert.ToInt64(dr["roundsPlayed"]);
                        score.lastOnline = Convert.ToDateTime(dr["lastOnline"]);
                        score.pvpNumDeaths = Convert.ToInt64(dr["pvpDeaths"]);
                        score.pvpNumKills = Convert.ToInt64(dr["pvpKills"]);
                        score.ImpLevel = (Globals.eImpLevel)Convert.ToInt32(dr["impLevel"]);
                        score.LandID = Convert.ToInt16(dr["land"]);
                    }
                    else
                    {
                        score.Name = "--";
                        score.classFullName = dr["classFullName"].ToString();
                        score.BaseProfession = (Character.ClassType)Enum.Parse(typeof(Character.ClassType), dr["classType"].ToString(), true);
                        score.Level = Convert.ToInt16(dr["level"]);
                        score.Experience = Convert.ToInt64(dr["exp"]);
                        score.Kills = Convert.ToInt32(dr["numKills"]);
                        score.RoundsPlayed = Convert.ToInt64(dr["roundsPlayed"]);
                        score.lastOnline = Convert.ToDateTime(dr["lastOnline"]);
                        score.pvpNumDeaths = Convert.ToInt64(dr["pvpDeaths"]);
                        score.pvpNumKills = Convert.ToInt64(dr["pvpKills"]);
                        score.ImpLevel = Globals.eImpLevel.USER;
                        score.LandID = Convert.ToInt16(dr["land"]);
                    }
                }
                return score;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return null;
            }
        } 
        #endregion

        internal static bool LoadSpawnZones()
        {
            try
            {
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_SpawnZone_Select_All", DataAccess.GetSQLConnection());
                DataTable dtCatalogItem = sp.ExecuteDataTable();
                foreach (DataRow dr in dtCatalogItem.Rows)
                {
                    SpawnZone.Add(new SpawnZone(dr));
                }
                Utils.Log("Loaded SpawnZones (" + SpawnZone.Spawns.Count + ")...", Utils.LogType.SystemGo);
                return true;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return false;
            }
        }

        internal static ArrayList GetSpawnLinksByMap(int map)
        {
            ArrayList zones = new ArrayList();
            SqlStoredProcedure sp = new SqlStoredProcedure("prApp_SpawnZone_Select_By_Map", DataAccess.GetSQLConnection());
            sp.AddParameter("@map", SqlDbType.Int, 4, ParameterDirection.Input, map);
            DataTable szTable = sp.ExecuteDataTable();
            foreach (DataRow dr in szTable.Rows)
            {
                zones.Add(new SpawnZone(dr));
            }
            return zones;
        }
        
        //internal static bool getAllAreas()
        //{
        //    ArrayList AllAreas = new ArrayList();
        //    try
        //    {
        //        SqlStoredProcedure sp = new SqlStoredProcedure("prApp_Area_Select_All", DataAccess.GetSQLConnection());
        //        DataTable dtAreas = sp.ExecuteDataTable();
        //        foreach (DataRow dr in dtAreas.Rows)
        //        {
        //            AllAreas.Add(ConvertRowToArea(dr));
        //        }
        //        for (int x = 0; x < AllAreas.Count; x++)
        //        {
        //            DragonsSpineMain.Areas[x] = (Area)AllAreas[x];
        //        }
        //        return true;
        //    }
        //    catch(Exception e)
        //    {
        //        Utils.LogException(e);
        //        return false;
        //    }
        //}
        //internal static Area ConvertRowToArea(DataRow dr)
        //{
        //    try
        //    {
        //        Area area = new Area();
        //        if (dr != null)
        //        {
        //            area.ID = Convert.ToInt32(dr["AreaID"]);
        //            area.AbsMapX = Convert.ToInt32(dr["AbsMapX"]);
        //            area.AbsMapY = Convert.ToInt32(dr["AbsMapY"]);
        //            area.AreaAboveID = Convert.ToInt32(dr["AreaAbove"]);
        //            area.AreaBelowID = Convert.ToInt32(dr["AreaBelow"]);
        //            area.AreaHeight = Convert.ToInt32(dr["AreaHeight"]);
        //            area.AreaMap = Convert.ToInt32(dr["AreaMap"]);
        //            area.AreaSizeX = Convert.ToInt32(dr["SizeX"]);
        //            area.AreaSizeY = Convert.ToInt32(dr["SizeY"]);
        //            area.AnchorX = Convert.ToInt32(dr["AnchorX"]);
        //            area.AnchorY = Convert.ToInt32(dr["AnchorY"]);
        //            area.OneWay = Convert.ToBoolean(dr["OneWay"]);
        //            return area;
        //        }
        //        return null;
        //    }
        //    catch(Exception e)
        //    {
        //        Utils.LogException(e);
        //        return null;
        //    }
        //}
        internal static ArrayList loadBannedIPList()
        {
            try
            {
                ArrayList bannedIPList = new ArrayList();
                SqlStoredProcedure sp = new SqlStoredProcedure("prApp_BannedIP_Select", DataAccess.GetSQLConnection());
                DataTable dtBannedIPList = sp.ExecuteDataTable();
                foreach (DataRow dr in dtBannedIPList.Rows)
                {
                    bannedIPList.Add(dr["bannedIP"].ToString());
                }
                return bannedIPList;
            }
            catch (Exception e)
            {
                Utils.LogException(e);
                return null;
            }
        }
    }
}
