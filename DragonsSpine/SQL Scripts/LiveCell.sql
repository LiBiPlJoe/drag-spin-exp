﻿
/****** Object:  Table [dbo].[LiveCell] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LiveCell]') AND type in (N'U'))
DROP TABLE [dbo].[LiveCell]
GO

/****** Object:  Table [dbo].[LiveCell] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LiveCell](
	[lastRoundChanged] [int] NOT NULL,
	[facet] [smallint] NOT NULL,
	[map] [smallint] NOT NULL,
	[xCord] [smallint] NOT NULL,
	[yCord] [smallint] NOT NULL,
	[zCord] [int] NOT NULL,
	[cellGraphic] [char](2) NOT NULL,
	[displayGraphic] [char](2) NOT NULL
 CONSTRAINT [PK_LiveCell] PRIMARY KEY CLUSTERED 
(
	[facet] ASC,
	[map] ASC,
	[xCord] ASC,
	[yCord] ASC,
	[zCord] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

