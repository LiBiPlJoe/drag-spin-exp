if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK__Player__AccountI__6AFACD50]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Player] DROP CONSTRAINT FK__Player__AccountI__6AFACD50
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK__PlayerBel__Playe__7854C86E]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[PlayerBelt] DROP CONSTRAINT FK__PlayerBel__Playe__7854C86E
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK__PlayerEff__Playe__7E0DA1C4]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[PlayerEffects] DROP CONSTRAINT FK__PlayerEff__Playe__7E0DA1C4
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK__PlayerLoc__Playe__03C67B1A]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[PlayerLocker] DROP CONSTRAINT FK__PlayerLoc__Playe__03C67B1A
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK__PlayerRin__Playe__0A7378A9]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[PlayerRings] DROP CONSTRAINT FK__PlayerRin__Playe__0A7378A9
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK__PlayerSac__Playe__11207638]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[PlayerSack] DROP CONSTRAINT FK__PlayerSac__Playe__11207638
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK__PlayerSpe__Playe__16D94F8E]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[PlayerSpells] DROP CONSTRAINT FK__PlayerSpe__Playe__16D94F8E
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK__PlayerWea__Playe__1C9228E4]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[PlayerWearing] DROP CONSTRAINT FK__PlayerWea__Playe__1C9228E4
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Account]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Account]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Player]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Player]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlayerBelt]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[PlayerBelt]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlayerEffects]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[PlayerEffects]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlayerLocker]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[PlayerLocker]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlayerRings]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[PlayerRings]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlayerSack]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[PlayerSack]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlayerSpells]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[PlayerSpells]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PlayerWearing]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[PlayerWearing]
GO

CREATE TABLE [dbo].[Account] (
	[AccountID] [int] IDENTITY (1, 1) NOT NULL ,
	[AccountName] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Password] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[IPAddress] [nvarchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Player] (
	[PlayerID] [int] IDENTITY (1, 1) NOT NULL ,
	[AccountID] [int] NOT NULL ,
	[Account] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[protocol] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[gender] [int] NULL ,
	[Race] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CharClass] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CharClassFull] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ClassID] [int] NULL ,
	[Align] [int] NULL ,
	[ConfRoom] [int] NULL ,
	[ImpLevel] [int] NULL ,
	[ShowTitle] [bit] NOT NULL ,
	[Echo] [bit] NOT NULL ,
	[Anonymous] [bit] NOT NULL ,
	[TimeOut] [int] NULL ,
	[Land] [int] NULL ,
	[Map] [int] NULL ,
	[XCord] [int] NULL ,
	[YCord] [int] NULL ,
	[DirPointer] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CurPos] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Stunned] [int] NULL ,
	[Floating] [int] NULL ,
	[IsDead] [bit] NOT NULL ,
	[IsHidden] [bit] NOT NULL ,
	[IsInvisible] [bit] NOT NULL ,
	[NightVision] [bit] NOT NULL ,
	[FeatherFall] [bit] NOT NULL ,
	[BreatheWater] [bit] NOT NULL ,
	[Blind] [bit] NOT NULL ,
	[Poisoned] [int] NULL ,
	[Shield] [int] NULL ,
	[FireProtection] [int] NULL ,
	[IceProtection] [int] NULL ,
	[DeathProtection] [int] NULL ,
	[FearProtection] [int] NULL ,
	[BlindProtection] [int] NULL ,
	[StunProtection] [int] NULL ,
	[LightningProtection] [int] NULL ,
	[PoisonProtection] [int] NULL ,
	[FighterSpecial] [varchar] (255) NULL ,
	[KnightRing] [bit] NOT NULL ,
	[Level] [int] NULL ,
	[Exp] [bigint] NULL ,
	[Hits] [int] NULL ,
	[HitsMax] [int] NULL ,
	[Stamina] [int] NULL ,
	[StamLeft] [int] NULL ,
	[Mana] [int] NULL ,
	[ManaMax] [int] NULL ,
	[Age] [int] NULL ,
	[RoundsPlayed] [bigint] NULL ,
	[NumKills] [int] NULL ,
	[NumDeaths] [int] NULL ,
	[BankGold] [float] NULL ,
	[Strength] [int] NULL ,
	[Dexterity] [int] NULL ,
	[Intelligence] [int] NULL ,
	[Wisdom] [int] NULL ,
	[Constitution] [int] NULL ,
	[Charisma] [int] NULL ,
	[TempStrength] [int] NULL ,
	[TempDexterity] [int] NULL ,
	[TempIntelligence] [int] NULL ,
	[TempWisdom] [int] NULL ,
	[TempConstitution] [int] NULL ,
	[TempCharisma] [int] NULL ,
	[StrAdd] [int] NULL ,
	[DexAdd] [int] NULL ,
	[Mace] [bigint] NULL ,
	[Bow] [bigint] NULL ,
	[Flail] [bigint] NULL ,
	[Dagger] [bigint] NULL ,
	[Rapier] [bigint] NULL ,
	[Twohanded] [bigint] NULL ,
	[Staff] [bigint] NULL ,
	[Shuriken] [bigint] NULL ,
	[Sword] [bigint] NULL ,
	[Threestaff] [bigint] NULL ,
	[Halberd] [bigint] NULL ,
	[Unarmed] [bigint] NULL ,
	[Thievery] [bigint] NULL ,
	[Magic] [bigint] NULL ,
	[trainedMace] [bigint] NULL ,
	[trainedBow] [bigint] NULL ,
	[trainedFlail] [bigint] NULL ,
	[trainedDagger] [bigint] NULL ,
	[trainedRapier] [bigint] NULL ,
	[trainedTwoHanded] [bigint] NULL ,
	[trainedStaff] [bigint] NULL ,
	[trainedShuriken] [bigint] NULL ,
	[trainedSword] [bigint] NULL ,
	[trainedThreestaff] [bigint] NULL ,
	[trainedHalberd] [bigint] NULL ,
	[trainedUnarmed] [bigint] NULL ,
	[trainedThievery] [bigint] NULL ,
	[trainedMagic] [bigint] NULL ,
	[SavedHitsMax] [int] NULL ,
	[SavedManaMax] [int] NULL ,
	[Birthday] [datetime] NULL ,
	[LastSave] [datetime] NULL ,
	[MPRegen] [int] NULL ,
	[Encumb] [int] NULL ,
	[LHItemID] [int] NULL ,
	[LHItemAttuned] [int] NULL ,
	[RHItemID] [int] NULL ,
	[RHItemAttuned] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[PlayerBelt] (
	[BeltID] [int] IDENTITY (1, 1) NOT NULL ,
	[PlayerID] [int] NOT NULL ,
	[BeltSlot] [int] NULL ,
	[BeltItem] [int] NULL ,
	[Attuned] [int] NULL ,
	[Special] [varchar] NULL ,
	[CoinValue] [double] NULL ,
	[Charges] [int] NULL ,
	[Venom] [int] NULL ,
	[WillAttune] [bit] NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[PlayerEffects] (
	[EffectListID] [int] IDENTITY (1, 1) NOT NULL ,
	[PlayerID] [int] NOT NULL ,
	[EffectSlot] [int] NULL ,
	[EffectID] [int] NULL ,
	[Amount] [int] NULL ,
	[Duration] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[PlayerLocker] (
	[LockerID] [int] IDENTITY (1, 1) NOT NULL ,
	[PlayerID] [int] NOT NULL ,
	[LockerSlot] [int] NULL ,
	[LockerItem] [int] NULL ,
	[Attuned] [int] NULL ,
	[Special] [varchar] NULL ,
	[CoinValue] [double] NULL ,
	[Charges] [int] NULL ,
	[WillAttune] [bit] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[PlayerRings] (
	[RingID] [int] IDENTITY (1, 1) NOT NULL ,
	[PlayerID] [int] NOT NULL ,
	[RingFinger] [int] NULL ,
	[RingItem] [int] NULL ,
	[Attuned] [int] NULL ,
	[isRecall] [bit] NOT NULL ,
	[wasRecall] [bit] NOT NULL ,
	[recallLand] [int] NULL ,
	[recallMap] [int] NULL ,
	[recallX] [int] NULL ,
	[recallY] [int] NULL ,
	[Special] [varchar] NULL ,
	[CoinValue] [double] NULL ,
	[Charges] [int] NULL ,
	[WillAttune] [bit] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[PlayerSack] (
	[SackID] [int] IDENTITY (1, 1) NOT NULL ,
	[PlayerID] [int] NOT NULL ,
	[SackSlot] [int] NULL ,
	[SackItem] [int] NULL ,
	[Attuned] [int] NULL ,
	[SackGold] [float] NULL ,
	[Special] [varchar] NULL ,
	[CoinValue] [double] NULL ,
	[Charges] [int] NULL ,
	[Venom] [int] NULL ,
	[WillAttune] [bit] NOT NULL ,
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[PlayerSpells] (
	[SpellListID] [int] IDENTITY (1, 1) NOT NULL ,
	[PlayerID] [int] NOT NULL ,
	[SpellSlot] [int] NULL ,
	[SpellID] [int] NULL ,
	[ChantString] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[PlayerWearing] (
	[WearingID] [int] IDENTITY (1, 1) NOT NULL ,
	[PlayerID] [int] NOT NULL ,
	[WearingSlot] [int] NULL ,
	[WearingItem] [int] NULL ,
	[Attuned] [int] NULL ,
	[WearLocationCode] [float] NULL ,
	[Special] [varchar] NULL ,
	[CoinValue] [double] NULL ,
	[Charges] [int] NULL ,
	[Venom] [int] NULL ,
	[WillAttune] [bit] NOT NULL
) ON [PRIMARY]
GO

