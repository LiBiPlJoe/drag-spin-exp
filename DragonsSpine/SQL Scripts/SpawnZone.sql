DROP TABLE [SpawnZone]

CREATE TABLE [SpawnZone] 
(
	[SpawnZoneID] 		[int] NULL ,
	[NPCID] 		[int] NULL ,
	[SpawnTimer] 		[int] NULL ,
	[MaxAllowedInZone] 	[int] NULL ,
	[NPCID] 		[int] NULL ,
	[SpawnTimer] 		[int] NULL ,
	[MaxAllowedInZone] 	[int] NULL ,
	[SpawnMessage] 		[nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL

) 
ON [PRIMARY]
GO


<SpawnZone>
	<spawnZoneID>1</spawnZoneID>
	<SpawnLand>0</SpawnLand>
	<SpawnMap>0</SpawnMap>
	<spawnMinY>42</spawnMinY>
	<spawnMinX>124</spawnMinX>
	<spawnMaxY>42</spawnMaxY>
	<spawnMaxX>124</spawnMaxX>
</SpawnZone>