DROP TABLE Segue

CREATE TABLE [Segue] 
(
	[SegueID] 		[int] NULL ,
	[LandID] 		[int] NULL ,
	[MapID] 		[int] NULL ,
	[Xcord1] 		[int] NULL ,
	[Ycord1] 		[int] NULL ,
	[Xcord2] 		[int] NULL ,
	[Ycord2] 		[int] NULL ,
	[Height] 		[int] NULL ,
) 
ON [PRIMARY]
GO



<Segue>
	<yCord1>5</yCord1>
	<xCord1>22</xCord1>
	<yCord2>44</yCord2>
	<xCord2>23</xCord2>
	<height>0</height>
</Segue>