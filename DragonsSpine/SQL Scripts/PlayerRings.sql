DROP TABLE [PlayerRings]

CREATE TABLE [PlayerRings] 
(
	[RingID]	[int]	IDENTITY NOT NULL PRIMARY KEY NONCLUSTERED ,
	[PlayerID]	[int]   NOT NULL REFERENCES Player(PlayerID) ,
	[RingFinger]	[int]	NULL ,
	[RingItem]	[int]	NULL ,
	[Attuned]	[int]	NULL ,
	[isRecall]	[bit]	NOT NULL DEFAULT (0) ,
	[wasRecall] [bit]	NOT NULL DEFAULT (0) ,
	[recallLand] [int]  NULL ,
	[recallMap]	[int]	NULL ,
	[recallX]	[int]	NULL ,
	[recallY]	[int]	NULL ,
	[Special]	[nvarchar] (255)	NULL ,
	[CoinValue]	[float]	NULL ,
	[Charges]	[int]	NULL ,
	[WillAttune] [bit]	NOT NULL DEFAULT (0)
) 
ON [PRIMARY]
GO





