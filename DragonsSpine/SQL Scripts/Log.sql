USE [DragSpinDB]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Log_LogTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Log] DROP CONSTRAINT [DF_Log_LogTime]
END

GO

/****** Object:  Table [dbo].[Log]    Script Date: 01/02/2016 14:59:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Log]') AND type in (N'U'))
DROP TABLE [dbo].[Log]
GO

/****** Object:  Table [dbo].[Log]    Script Date: 01/02/2016 14:59:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[logtime] [datetime] NOT NULL,
	[logtype] [nvarchar](50) NULL,
	[message] [nvarchar](max) NULL,
 CONSTRAINT [PK__Log__03A67F89] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[Log] ADD  CONSTRAINT [DF_Log_LogTime]  DEFAULT (getdate()) FOR [logtime]
GO

