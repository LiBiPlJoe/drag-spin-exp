USE [DragSpinDB]

/****** Object:  StoredProcedure [dbo].[prApp_Log_By_MinID] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prApp_Log_By_MinID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[prApp_Log_By_MinID]
GO

/****** Object:  StoredProcedure [dbo].[prApp_Log_By_MinID] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------
-- Select all log entries starting at a certain ID
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Log_By_MinID]
	@minId int

AS

SELECT	*
FROM	Log
WHERE id >= @minId
ORDER BY id

GO

