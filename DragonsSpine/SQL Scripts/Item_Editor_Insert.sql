----------------------------------------------------------------------------
-- Insert a single record into CatalogItem
----------------------------------------------------------------------------
CREATE PROC prApp_CatalogItem_Insert
	@Notes nvarchar(255) = NULL,
	@ItemID int,
	@ItemTypeCode int,
	@ItemBaseCode char(10) = NULL,
	@ItemName nvarchar(255) = NULL,
	@ShortDesc nvarchar(255) = NULL,
	@LongDesc nvarchar(255) = NULL,
	@WearLocationCode int,
	@Weight int = NULL,
	@CoinValue int = NULL,
	@SizeCode int = NULL,
	@BlockRankCode int,
	@EffectTypeCode int,
	@EffectAmount nvarchar(255) = NULL,
	@EffectDuration int = NULL,
	@Special varchar(255) = NULL,
	@MinDamage int = NULL,
	@MaxDamage int = NULL,
	@SkillType varchar(255) = NULL,
	@NumThrowAttacks int = NULL,
	@HeldRange int = NULL,
	@ThrowRange int = NULL,
	@BookTypeCode int = NULL,
	@CurrentPage int = NULL,
	@MaxPages int = NULL,
	@vRandLow int = NULL,
	@vRandHigh int = NULL,
	@KeyName varchar(50),

@IsRecall		bit,
@WasRecall		bit,
@Alignment		int= NULL,
@MagicRegen		int= NULL,
@ItemCharges		int= NULL,
@HasSpells		bit,
@AttackRankCode	int= NULL,
@Resistance		int= NULL,
@AttackTypeCode	varchar	(50)= NULL,
@Enchantment		varchar	(50)= NULL,
@ProcChance		int= NULL,
@IsReturning		bit,
@willAttune	bit,
@FigurineExperience	int= NULL,
@HasVenom		int,
@armorType		int,
@page1			varchar(255),
@page2			varchar(255),
@page3			varchar(255),
@page4			varchar(255),
@page5			varchar(255),
@page6			varchar(255),
@page7			varchar(255),
@page8			varchar(255),
@page9			varchar(255)






AS

INSERT CatalogItem(Notes,ItemID,ItemTypeCode,ItemBaseCode,ItemName,ShortDesc,LongDesc,WearLocationCode,Weight,CoinValue,SizeCode,BlockRankCode,EffectTypeCode,EffectAmount,EffectDuration,Special,MinDamage,MaxDamage,SkilLType,NumThrowAttacks,HeldRange,ThrowRange,BookTypeCode,CurrentPage,MaxPages,vRandLow,vRandHigh,KeyName,IsRecall,WasRecall,HasVenom,Alignment,MagicRegen,ItemCharges,HasSpells,AttackRankCode,Resistance,AttackTypeCode,Enchantment,ProcChance,IsReturning,willAttune,FigurineExperience,armorType,page1,page2,page3,page4,page5,page6,page7,page8,page9)
VALUES (@Notes,@ItemID,@ItemTypeCode,@ItemBaseCode,@ItemName,@ShortDesc,@LongDesc,@WearLocationCode,@Weight,@CoinValue,@SizeCode,@BlockRankCode,@EffectTypeCode,@EffectAmount,@EffectDuration,@Special,@MinDamage,@MaxDamage,@SkilLType,@NumThrowAttacks,@HeldRange,@ThrowRange,@BookTypeCode,@CurrentPage,@MaxPages,@vRandLow,@vRandHigh,@KeyName,@IsRecall,@WasRecall,@HasVenom,@Alignment,@MagicRegen,@ItemCharges,@HasSpells,@AttackRankCode,@Resistance,@AttackTypeCode,@Enchantment,@ProcChance,@IsReturning,@willAttune,@FigurineExperience,@armorType,@page1,@page2,@page3,@page4,@page5,@page6,@page7,@page8,@page9)
GO
