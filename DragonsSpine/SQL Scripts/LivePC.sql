﻿
/****** Object:  Table [dbo].[LivePC] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LivePC]') AND type in (N'U'))
DROP TABLE [dbo].[LivePC]
GO

/****** Object:  Table [dbo].[LivePC] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LivePC](
	[uniqueId] [int] NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[facet] [smallint] NOT NULL,
	[map] [smallint] NOT NULL,
	[xCord] [smallint] NOT NULL,
	[yCord] [smallint] NOT NULL,
	[zCord] [int] NOT NULL,
	[level] [int] NOT NULL,
	[hits] [int] NOT NULL,
	[fullHits] [int] NOT NULL,
	[mana] [int] NOT NULL,
	[fullMana] [int] NOT NULL,
	[lastCommand] [nvarchar](255) NOT NULL,
	[lastActiveRound] [int] NOT NULL,
	[firstActiveRound] [int] NOT NULL,
	[isDead] [bit] NOT NULL  DEFAULT (0),
	[effects] [nvarchar](255) NULL
 CONSTRAINT [PK_LivePC] PRIMARY KEY CLUSTERED 
(
	[uniqueId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

