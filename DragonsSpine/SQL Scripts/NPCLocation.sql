
/****** Object:  Table [dbo].[NPCLocation]    Script Date: 08/18/2015 07:43:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NPCLocation]') AND type in (N'U'))
DROP TABLE [dbo].[NPCLocation]
GO

/****** Object:  Table [dbo].[NPCLocation]    Script Date: 08/18/2015 07:43:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NPCLocation](
	[NPCIndex] [int] NOT NULL,
	[active] [int] NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[facet] [smallint] NOT NULL,
	[land] [smallint] NOT NULL,
	[map] [smallint] NOT NULL,
	[xCord] [smallint] NOT NULL,
	[yCord] [smallint] NOT NULL,
	[zCord] [int] NOT NULL,
 CONSTRAINT [PK_NPCLocation] PRIMARY KEY CLUSTERED 
(
	[NPCIndex] ASC,
	[facet] ASC,
	[land] ASC,
	[map] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

