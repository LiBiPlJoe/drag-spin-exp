
/****** Object:  StoredProcedure [dbo].[prApp_LiveCell_Update] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prApp_LiveCell_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[prApp_LiveCell_Update]
GO

/****** Object:  StoredProcedure [dbo].[prApp_LiveCell_Update] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Joe
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[prApp_LiveCell_Update] 
	@lastRoundChanged int,
	@facet smallint,
	@map smallint,
	@xCord smallint,
	@yCord smallint,
	@zCord int,
	@cellGraphic char(2),
	@displayGraphic char(2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE LiveCell
	SET lastRoundChanged=@lastRoundChanged,
	facet=@facet,
	map=@map,
	xCord=@xCord,
	yCord=@yCord,
	zCord=@zCord,
	cellGraphic=@cellGraphic,
	displayGraphic=@displayGraphic
	WHERE facet=@facet AND map=@map AND xCord=@xCord AND yCord=@yCord AND zCord=@zCord; 
	IF @@ROWCOUNT=0
		INSERT INTO LiveCell (
			lastRoundChanged,
			facet,
			map,
			xCord,
			yCord,
			zCord,
			cellGraphic,
			displayGraphic
		)
		VALUES (
			@lastRoundChanged,
			@facet,
			@map,
			@xCord,
			@yCord,
			@zCord,
			@cellGraphic,
			@displayGraphic
		)
END

GO

