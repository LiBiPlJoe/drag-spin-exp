
/****** Object:  StoredProcedure [dbo].[prApp_LiveNPC_By_MapID] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prApp_LiveNPC_By_MapID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[prApp_LiveNPC_By_MapID]
GO

/****** Object:  StoredProcedure [dbo].[prApp_LiveNPC_By_MapID] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Joe
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[prApp_LiveNPC_By_MapID] 
	-- Add the parameters for the stored procedure here
	@facet smallint,
	@map smallint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM LiveNPC
	WHERE facet=@facet AND map=@map 
END

GO

