DROP TABLE [PlayerEffects]

CREATE TABLE [PlayerEffects] 
(
	[EffectListID]	[int]	IDENTITY NOT NULL PRIMARY KEY NONCLUSTERED,
	[PlayerID]	[int]   NOT NULL REFERENCES Player(PlayerID),
	[EffectSlot]	[int]	NULL,
	[EffectID]	[int]	NULL,
	[Amount]	[int]	NULL,
	[Duration]	[int]	NULL
)
	
ON [PRIMARY]
GO





