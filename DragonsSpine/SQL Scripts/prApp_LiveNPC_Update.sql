
/****** Object:  StoredProcedure [dbo].[prApp_LiveNPC_Update] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prApp_LiveNPC_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[prApp_LiveNPC_Update]
GO

/****** Object:  StoredProcedure [dbo].[prApp_LiveNPC_Update] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Joe
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[prApp_LiveNPC_Update] 
	@uniqueId int,
	@name nvarchar(255),
	@facet smallint,
	@map smallint,
	@xCord smallint,
	@yCord smallint,
	@zCord int,
	@level int,
	@hits int,
	@fullHits int,
	@mana int,
	@fullMana int,
	@lastCommand nvarchar(255),
	@lastActiveRound int,
	@isDead bit,
	@mostHatedId int=NULL,
	@hateCenterX smallint=NULL,
	@hateCenterY smallint=NULL,
	@loveCenterX smallint=NULL,
	@loveCenterY smallint=NULL,
	@fearLove int=NULL,
	@NpcTypeCode 		int=NULL,
	@BaseTypeCode 		int=NULL,
	@CharacterClassCode 	int=NULL,
	@AlignCode 		int=NULL,
	@numAttackers int=NULL,
	@lairLocationX smallint=NULL,
	@lairLocationY smallint=NULL,
	@lairLocationZ int=NULL,
	@priorityCode int=NULL,
	@effects nvarchar(255)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE LiveNPC
	SET uniqueId=@uniqueId,
	name=@name,
	facet=@facet,
	map=@map,
	xCord=@xCord,
	yCord=@yCord,
	zCord=@zCord,
	level=@level,
	hits=@hits,
	fullHits=@fullHits,
	mana=@mana,
	fullMana=@fullMana,
	lastCommand=@lastCommand,
	lastActiveRound=@lastActiveRound,
	isDead=@isDead,
	mostHatedId=@mostHatedId,
	hateCenterX=@hateCenterX,
	hateCenterY=@hateCenterY,
	loveCenterX=@loveCenterX,
	loveCenterY=@loveCenterY,
	fearLove=@fearLove,
	NpcTypeCode=@NpcTypeCode,
	BaseTypeCode=@BaseTypeCode,
	CharacterClassCode=@CharacterClassCode,
	AlignCode=@AlignCode,
	numAttackers=@numAttackers,
	lairLocationX=@lairLocationX,
	lairLocationY=@lairLocationY,
	lairLocationZ=@lairLocationZ,
	priorityCode=@priorityCode,
	effects=@effects
	WHERE uniqueId=@uniqueId; 
	IF @@ROWCOUNT=0
		INSERT INTO LiveNPC (
			uniqueId,
			name,
			facet,
			map,
			xCord,
			yCord,
			zCord,
			level,
			hits,
			fullHits,
			mana,
			fullMana,
			lastCommand,
			lastActiveRound,
			firstActiveRound,
			isDead,
			mostHatedId,
			hateCenterX,
			hateCenterY,
			loveCenterX,
			loveCenterY,
			fearLove,
			NpcTypeCode,
			BaseTypeCode,
			CharacterClassCode,
			AlignCode,
			numAttackers,
			lairLocationX,
			lairLocationY,
			lairLocationZ,
			priorityCode,
			effects
		)
		VALUES (
			@uniqueId,
			@name,
			@facet,
			@map,
			@xCord,
			@yCord,
			@zCord,
			@level,
			@hits,
			@fullHits,
			@mana,
			@fullMana,
			@lastCommand,
			@lastActiveRound,
			@lastActiveRound,
			@isDead,
			@mostHatedId,
			@hateCenterX,
			@hateCenterY,
			@loveCenterX,
			@loveCenterY,
			@fearLove,
			@NpcTypeCode,
			@BaseTypeCode,
			@CharacterClassCode,
			@AlignCode,
			@numAttackers,
			@lairLocationX,
			@lairLocationY,
			@lairLocationZ,
			@priorityCode,
			@effects
		)
END

GO

