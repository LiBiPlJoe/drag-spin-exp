FROM mono:6

RUN apt-get update
RUN apt-get install -y apt-utils

# Looks like buster-backports is gone
# https://backports.debian.org/news/Removal_of_buster-backports_from_the_debian_archive/
#
# RUN echo "deb http://deb.debian.org/debian buster-backports main" >> /etc/apt/sources.list
# RUN echo "Package: *" >> /etc/apt/preferences.d/preferences
# RUN echo "Pin: release n=buster-backports" >> /etc/apt/preferences.d/preferences
# RUN echo "Pin-Priority: 1001" >> /etc/apt/preferences.d/preferences
# RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get upgrade -y e2fsprogs


ADD . /source
WORKDIR /source/DragonsSpine

RUN xbuild /p:Configuration=Debug DragonsSpine.sln
RUN xbuild /p:Configuration=Release DragonsSpine.sln

# Grant permissions for our scripts to be executable
RUN chmod +x entrypoint.sh

RUN apt-get remove -y curl perl
RUN apt-get autoremove -y

# The entrypoint script relies on both SQLSERVR_SA_PASSWORD and SQLSERVR_DB_NAME environment
#  variables being set.
ENTRYPOINT ["./entrypoint.sh"]
